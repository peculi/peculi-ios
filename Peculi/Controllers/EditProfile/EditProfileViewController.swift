//
//  EditProfileViewController.swift
//  Peculi
//
//  Created by apple on 02/11/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Foundation
import SVProgressHUD
import DropDown
import IQKeyboardManagerSwift

class EditProfileViewController: UIViewController,UITextFieldDelegate, UITextViewDelegate {
    
    //MARK: - Outltes
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserEmail: UILabel!
        
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var dateOfBirthTextField: UITextField!
    @IBOutlet weak var selectYourGenderTextField: UITextField!
    @IBOutlet weak var addressLine1TextField: UITextField!
    @IBOutlet weak var addressLine2TextField: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var countrytextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var navigationBackButton: UIButton!
    @IBOutlet weak var navaigationTitleLbl: UILabel!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var postalCodeTextField: UITextField!
    @IBOutlet weak var addressLine1TextView: UITextView!
    @IBOutlet weak var addressLine1Height: NSLayoutConstraint!
    @IBOutlet weak var addressLine2TextView: UITextView!
    
    @IBOutlet weak var addressLine2Height: NSLayoutConstraint!
    @IBOutlet weak var firstnameView: UIView!
    @IBOutlet weak var lastNameView: UIView!
    @IBOutlet weak var dobView: UIView!
    @IBOutlet weak var phoneNumberView: UIView!
    @IBOutlet weak var countryView: UIView!
    @IBOutlet weak var addressLine1View: UIView!
    @IBOutlet weak var addressLine2View: UIView!
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var postalCodeView: UIView!
    @IBOutlet weak var genderView: UIView!
    @IBOutlet var superView: UIView!
    @IBOutlet weak var superChildView: UIView!
    @IBOutlet weak var childPreviousView: IQPreviousNextView!
    
    //MARK: - Declare variables
    
    var dateTo = Date()
    var dropGender = DropDown()
    var dropCountries = DropDown()
    var dropState = DropDown()
    var dropCity = DropDown()
    var allCountries = [String]()
    var allStates = [String]()
    var allCity = [String]()
    var allCurrency = [String]()
    var shortName = [String]()
    var countryShortName = "GB"
    var currencyName = "GBP"
    var placeHolderImage = #imageLiteral(resourceName: "ic_user_icon")
    var emailVal = String()
    let minTextViewHeight: CGFloat = 32
    let maxTextViewHeight: CGFloat = 64
    let addressLine1Placeholder = "Enter House Name/Number,Street Name"
    let addressLine2Placeholder = "Enter Apartment, suite, unit, building, floor etc.."
    
    
    //MARK: - ViewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UserDefaults.standard.string(forKey: UserDefaultConstants.personalDetailsStatus.rawValue) == "1" {
            getAllStates(countryShortName: countryShortName)
        }
        
        fullNameTextField.delegate = self
        lastNameTextField.delegate = self
        dateOfBirthTextField.delegate = self
        addressLine1TextView.delegate = self
        addressLine2TextView.delegate = self
        addressLine1TextField.delegate = self
        addressLine2TextField.delegate = self
        postalCodeTextField.delegate = self
        let dobTapAction = UITapGestureRecognizer(target: self, action: #selector(self.dobAction(_:)))
        dateOfBirthTextField?.isUserInteractionEnabled = true
        dateOfBirthTextField?.addGestureRecognizer(dobTapAction)
        let genderTapAction = UITapGestureRecognizer(target: self, action: #selector(self.genderDropdownAction(_:)))
        selectYourGenderTextField?.isUserInteractionEnabled = true
        selectYourGenderTextField?.addGestureRecognizer(genderTapAction)
        selectYourGenderTextField.delegate = self
        countrytextField.delegate = self
        cityTextField.delegate = self
        
        
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.black
        DropDown.appearance().textColor = UIColor.black
        DropDown.appearance().selectedTextColor = UIColor.white
        DropDown.appearance().cornerRadius = 4
                if UserDefaults.exists(key: "UserImage") {
                    
                    var originalString = String()
                    originalString = UserDefaults.standard.value(forKey: "UserImage") as! String
                    let ImageUrl = String(format: "%@%@", AppConfig.userProfileImageUrl,originalString)
                    
                    profileImg.sd_setImage(with: URL(string:ImageUrl), placeholderImage:UIImage(named: "placeholder_Img"), completed: { (image, error, cacheType, url) -> Void in
                        if ((error) != nil) {
                            // set the placeholder image here
                            //cell.imgVw.image = #imageLiteral(resourceName: "doctorAvtar_Img")
                        } else {
                            // success ... use the image
                        }
                    })
                    if let profileImg = profileImg.image {
                        //selectedImg = profileImg
                    }
            
                }
        if UserDefaults.standard.string(forKey: UserDefaultConstants.personalDetailsStatus.rawValue) == "1"{
            if UserDefaults.exists(key: UserDefaultConstants.FirstName.rawValue){
                fullNameTextField.text = UserDefaults.standard.value(forKey: UserDefaultConstants.FirstName.rawValue) as? String
            }
            
            if UserDefaults.exists(key: UserDefaultConstants.LastName.rawValue){
                lastNameTextField.text = UserDefaults.standard.value(forKey: UserDefaultConstants.LastName.rawValue) as? String
            }
            
            if UserDefaults.exists(key: UserDefaultConstants.userGender.rawValue) && UserDefaults.standard.string(forKey: UserDefaultConstants.userGender.rawValue) != ""{
                selectYourGenderTextField.text = UserDefaults.standard.value(forKey: UserDefaultConstants.userGender.rawValue) as? String
            }
            
            if UserDefaults.exists(key: UserDefaultConstants.UserDob.rawValue){
                dateOfBirthTextField.text = UserDefaults.standard.value(forKey: UserDefaultConstants.UserDob.rawValue) as? String
            }
            
            if UserDefaults.exists(key: UserDefaultConstants.UserEmail.rawValue){
                emailVal = UserDefaults.standard.value(forKey: UserDefaultConstants.UserEmail.rawValue) as? String ?? ""
            }
            
            if UserDefaults.exists(key: UserDefaultConstants.Address1.rawValue){
                addressLine1TextView.text = UserDefaults.standard.value(forKey: UserDefaultConstants.Address1.rawValue) as? String
                if UserDefaults.standard.string(forKey: UserDefaultConstants.Address1.rawValue) == ""{
                    addressLine1TextView.text = addressLine1Placeholder
                    
                }
                
            }
            
            if UserDefaults.exists(key: UserDefaultConstants.Address2.rawValue) {
                
                addressLine2TextView.text = UserDefaults.standard.value(forKey: UserDefaultConstants.Address2.rawValue) as? String
                if UserDefaults.standard.string(forKey: UserDefaultConstants.Address2.rawValue) == ""{
                    addressLine2TextView.text = addressLine2Placeholder
                    
                }
                
            }
            
            if UserDefaults.exists(key: UserDefaultConstants.postcode.rawValue){
                
                postalCodeTextField.text = UserDefaults.standard.value(forKey: UserDefaultConstants.postcode.rawValue) as? String
                
            }
            
            if UserDefaults.exists(key: UserDefaultConstants.PhoneNumber.rawValue){
                txtPhoneNumber.text = UserDefaults.standard.value(forKey: UserDefaultConstants.PhoneNumber.rawValue) as? String
            }
            
            if UserDefaults.exists(key: UserDefaultConstants.countryName.rawValue){
                countrytextField.text = UserDefaults.standard.value(forKey: UserDefaultConstants.countryName.rawValue) as? String
            }
            
            //        if UserDefaults.exists(key: "state"){
            //            stateTextField.text = UserDefaults.standard.value(forKey: "state") as? String
            //        }
            
            if UserDefaults.exists(key: UserDefaultConstants.city.rawValue){
                
                cityTextField.text = UserDefaults.standard.value(forKey: UserDefaultConstants.city.rawValue) as? String
            }
            
//            dobView.isHidden = false
//            genderView.isHidden = false
//            countryView.isHidden = false
//            addressLine1View.isHidden = false
//            addressLine2View.isHidden = false
//            cityView.isHidden = false
//            postalCodeView.isHidden = false
            
        }
        else {
            if UserDefaults.exists(key: UserDefaultConstants.FirstName.rawValue){
                fullNameTextField.text = UserDefaults.standard.value(forKey: UserDefaultConstants.FirstName.rawValue) as? String
            }
            
            if UserDefaults.exists(key: UserDefaultConstants.LastName.rawValue){
                lastNameTextField.text = UserDefaults.standard.value(forKey: UserDefaultConstants.LastName.rawValue) as? String
            }
            if UserDefaults.exists(key: UserDefaultConstants.UserEmail.rawValue){
                emailVal = UserDefaults.standard.value(forKey: UserDefaultConstants.UserEmail.rawValue) as? String ?? ""
            }
            if UserDefaults.exists(key: UserDefaultConstants.PhoneNumber.rawValue){
                txtPhoneNumber.text = UserDefaults.standard.value(forKey: UserDefaultConstants.PhoneNumber.rawValue) as? String
            }
            
            if UserDefaults.exists(key: UserDefaultConstants.userGender.rawValue) && UserDefaults.standard.string(forKey: UserDefaultConstants.userGender.rawValue) != ""{
                selectYourGenderTextField.text = UserDefaults.standard.value(forKey: UserDefaultConstants.userGender.rawValue) as? String
            }
            
            
            if UserDefaults.exists(key: UserDefaultConstants.UserEmail.rawValue){
                emailVal = UserDefaults.standard.value(forKey: UserDefaultConstants.UserEmail.rawValue) as? String ?? ""
            }
            
            if UserDefaults.exists(key: UserDefaultConstants.Address1.rawValue){
                addressLine1TextView.text = UserDefaults.standard.value(forKey: UserDefaultConstants.Address1.rawValue) as? String
                if UserDefaults.standard.string(forKey: UserDefaultConstants.Address1.rawValue) == ""{
                    addressLine1TextView.text = addressLine1Placeholder
                    
                }
                
            }
            
            
            if UserDefaults.exists(key: UserDefaultConstants.postcode.rawValue){
                
                postalCodeTextField.text = UserDefaults.standard.value(forKey: UserDefaultConstants.postcode.rawValue) as? String
                
            }
         
            
            if UserDefaults.exists(key: UserDefaultConstants.countryName.rawValue){
                countrytextField.text = UserDefaults.standard.value(forKey: UserDefaultConstants.countryName.rawValue) as? String
            }
            
                    if UserDefaults.exists(key: "state"){
                        stateTextField.text = UserDefaults.standard.value(forKey: "state") as? String
                    }
            
            if UserDefaults.exists(key: UserDefaultConstants.city.rawValue){
                
                cityTextField.text = UserDefaults.standard.value(forKey: UserDefaultConstants.city.rawValue) as? String
            }
            
            if UserDefaults.exists(key: UserDefaultConstants.UserDob.rawValue){
                dateOfBirthTextField.text = UserDefaults.standard.value(forKey: UserDefaultConstants.UserDob.rawValue) as? String
            }
            
            if UserDefaults.exists(key: UserDefaultConstants.Address2.rawValue) {
                addressLine2TextView.text = UserDefaults.standard.value(forKey: UserDefaultConstants.Address2.rawValue) as? String
                if UserDefaults.standard.string(forKey: UserDefaultConstants.Address2.rawValue) == "" {
                    addressLine2TextView.text = addressLine2Placeholder
                }
            }
            
            
            self.lblUserName.text = "\(fullNameTextField.text ?? "")" + " \(lastNameTextField.text ?? "")"
            
            self.lblUserEmail.text = emailVal
            
            
            
//            dobView.isHidden = true
//            genderView.isHidden = true
//            countryView.isHidden = true
//            addressLine1View.isHidden = true
//            addressLine2View.isHidden = true
//            cityView.isHidden = true
//            postalCodeView.isHidden = true
            
            
            
        }
        
        
        
    }
    
    
    
    //MARK: - textview delegates
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == addressLine1TextView{
            
            if textView.text == addressLine1Placeholder{
                textView.text = ""
            }
            
        }
        else if textView == addressLine2TextView{
            if textView.text == addressLine2Placeholder{
                textView.text = ""
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == addressLine1TextView{
            
            if textView.text == ""
            {
                addressLine1Height.constant = 39
                textView.text = addressLine1Placeholder
                
            }
        }
        else if textView == addressLine2TextView{
            
            if textView.text == ""
            {
                addressLine2Height.constant = 39
                textView.text = addressLine2Placeholder
                
            }
        }
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView == addressLine1TextView{
            var height = ceil(textView.contentSize.height)
            
            if (height < minTextViewHeight + 5) {
                height = minTextViewHeight
            }
            if (height > maxTextViewHeight) {
                height = maxTextViewHeight
            }
            
            if height != addressLine1Height.constant {
                addressLine1Height.constant = height
                textView.setContentOffset(CGPoint.zero, animated: false)
            }
            print(height)
        }
        if textView == addressLine2TextView{
            var height = ceil(textView.contentSize.height)
            
            if (height < minTextViewHeight + 5) {
                height = minTextViewHeight
            }
            if (height > maxTextViewHeight) {
                height = maxTextViewHeight
            }
            
            if height != addressLine2Height.constant {
                addressLine2Height.constant = height
                textView.setContentOffset(CGPoint.zero, animated: false)
            }
            print(height)
        }
        
        let currentText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        return currentText.count <= 50
    }
    
    
    
    
    //MARK:- textfield delegates
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //        if textField == self.fullNameTextField || textField == self.lastNameTextField{
        //
        ////            do {
        ////
        ////                let regex = try NSRegularExpression(pattern: ".*[^A-Za-z ].*", options: [])
        ////                if regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.count)) != nil {
        ////                    return false
        ////                }
        ////            }
        ////            catch {
        ////                print("ERROR")
        ////            }
        //
        //
        //        }
        
        if textField == self.addressLine1TextField || textField == self.addressLine2TextField{
            
            //            do {
            //                let regex = try NSRegularExpression(pattern: ".*[^A-Za-z0-9()-/,.\\ ].*", options: [])
            //                if regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.count)) != nil {
            //                    return false
            //                }
            //            }
            //            catch {
            //                print("ERROR")
            //            }
            
            
        }
        
        else if textField == self.postalCodeTextField{
            if string == " "{
                return false
            }
            textField.text = (textField.text! as NSString).replacingCharacters(in: range, with: string.uppercased())
            return false
        }
        
        return true
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.countrytextField || textField == self.cityTextField || textField == self.selectYourGenderTextField || textField == self.dateOfBirthTextField{
            return false
        }
        return true
    }
    
    
    //MARK: - actions
    @objc func dobAction(_ sender: UITapGestureRecognizer) {
        addToDatePicker()
    }
    
    @objc func genderDropdownAction(_ sender: UITapGestureRecognizer) {
        setupGenderDropDown()
        dropGender.show()
    }
    
    @IBAction func countryDropDwnAction(_ sender: Any) {
        
        setupCountriesDropDown()
        dropCountries.show()
        
        
    }
    @IBAction func cityDropdwnBtn(_ sender: Any) {
        setupCityDropDown()
        dropCity.show()
    }
    
    //MARK:- ViewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        navigartionSetup()
    }
    
    //MARK: - Navigation Setup
    func navigartionSetup(){
        navigationController?.navigationBar.isHidden = true
        title = "Edit Profile"
        
        navigationView.layer.masksToBounds = false
        navigationView.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        navigationView.layer.shadowOpacity = 0.6
        navigationView.layer.shadowOffset = CGSize(width: 0, height: 3)
        navigationView.layer.shadowRadius = 3.0
        
        //MARK: - Navigation LeftBar Button:-
        
        let leftBarButton = UIBarButtonItem(image: UIImage(named: "icBackArrowWhite"), style: .done, target: self, action: #selector(icBackButtonAction))
        self.navigationItem.leftBarButtonItem  = leftBarButton
    }
    
        func chooseImageMethod(){
            
            let alert = UIAlertController(title: "Choose image from", message: "", preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
                self.SelectImageMethod(type:"camera")
            }))
            alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (action) in
                self.SelectImageMethod(type:"gallery")
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
    
            }))
            self.present(alert, animated: true)
        }
    //
        func SelectImageMethod(type:String) {
            print("image get from \(type)")
            if type == "camera" {
                if UIImagePickerController.isSourceTypeAvailable(.camera){
                    let myPickerController = UIImagePickerController()
                    myPickerController.delegate = self
                    myPickerController.sourceType = .camera
                    myPickerController.allowsEditing = true
                    self.present(myPickerController, animated: true, completion: nil)
                }
            } else {
                if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                    let myPickerController = UIImagePickerController()
                    myPickerController.delegate = self
                    myPickerController.sourceType = .photoLibrary
                    myPickerController.allowsEditing = true
                    self.present(myPickerController, animated: true, completion: nil)
                }
            }
        }
    
    func setupGenderDropDown() {
        dropGender.anchorView = selectYourGenderTextField
        dropGender.bottomOffset = CGPoint(x: 0, y: selectYourGenderTextField.bounds.height)
        dropGender.dataSource = ["Male","Female", "Prefer not to say"]
        dropGender.selectionAction = { [weak self] (index, item) in
            self!.selectYourGenderTextField.text = item
            //let data = self!.searLang[index]
        }
    }
    
    func setupCityDropDown() {
        dropCity.anchorView = cityTextField
        dropCity.bottomOffset = CGPoint(x: 0, y: cityTextField.bounds.height)
        
        dropCity.dataSource = allCity
        
        dropCity.selectionAction = { [weak self] (index, item) in
            
            self?.cityTextField.text = item
            // let data = self!.searLang[index]
        }
    }
    
    //    func setupStateDropDown() {
    //        dropState.anchorView = stateTextField
    //        dropState.bottomOffset = CGPoint(x: 0, y: stateTextField.bounds.height)
    //        dropState.dataSource = allStates
    //        dropState.selectionAction = { [weak self] (index, item) in
    //            self?.stateTextField.text = item
    //            self?.getAllCities(shortName:self?.countryShortName ?? "",stateName:item)
    //            // let data = self!.searLang[index]
    //        }
    //    }
    
    
    func setupCountriesDropDown() {
        dropCountries.anchorView = countrytextField
        dropCountries.bottomOffset = CGPoint(x: 0, y: countrytextField.bounds.height)
        dropCountries.dataSource = allStates
        dropCountries.selectionAction = { [weak self] (index, item) in
            self?.countrytextField.text = item
            self?.getAllCities(shortName:self?.countryShortName ?? "",stateName:item)
            //            self?.countrytextField.text = ""
            
            //let data = self!.searLang[index]
        }
    }
    
    func addNewEvents(_ img: UIImage)
    {
        
        
        let dict = ["firstname": fullNameTextField.text ?? "","lastname": lastNameTextField.text ?? "", "email":emailVal, "gender":selectYourGenderTextField.text ?? "", "country": countryShortName , "state": stateTextField.text ?? "", "city": cityTextField.text ?? "", "dob":dateOfBirthTextField.text ?? "","address1":addressLine1TextView.text ?? "", "address2": addressLine2TextView.text ?? "", "postcode": postalCodeTextField.text ?? "","phone":txtPhoneNumber.text ?? "", "country_name": countrytextField.text ?? "","user_id":UserDefaults.standard.value(forKey: UserDefaultConstants.UserID.rawValue) as! String] as [String : Any]
        
        let pickedImage = img
        let Url = AppConfig.BaseUrl + constantApis.updateUserInfoApi
        let jpegData = pickedImage.jpegData(compressionQuality: 0.5)!
        self.postComplexPictures(url:NSURL.init(string: Url)! as URL , params: dict, imageToUpload: jpegData) { (arg0) in
            let (_, list, isSuccess) = arg0
            if(isSuccess) {
                
                print(list)
                UserDefaults.standard.set(list["user_id"], forKey: UserDefaultConstants.UserID.rawValue)
                UserDefaults.standard.set(list["firstname"], forKey: UserDefaultConstants.FirstName.rawValue)
                UserDefaults.standard.set(list["lastname"], forKey: UserDefaultConstants.LastName.rawValue)
                UserDefaults.standard.set(list["gender"], forKey: UserDefaultConstants.userGender.rawValue)
//                UserDefaults.standard.set(list["image"], forKey: "UserImage")
                UserDefaults.standard.set(list["dob"], forKey: UserDefaultConstants.UserDob.rawValue)
                UserDefaults.standard.set(list["email"], forKey: UserDefaultConstants.UserEmail.rawValue)
                UserDefaults.standard.set(list["phone"], forKey: UserDefaultConstants.PhoneNumber.rawValue)
                UserDefaults.standard.set(list["address1"], forKey: UserDefaultConstants.Address1.rawValue)
                UserDefaults.standard.set(list["address2"], forKey: UserDefaultConstants.Address1.rawValue)
                UserDefaults.standard.set(list["postcode"], forKey: UserDefaultConstants.postcode.rawValue)
                UserDefaults.standard.set(list["country_name"], forKey: UserDefaultConstants.countryName.rawValue)
//                UserDefaults.standard.set(list["state"], forKey: "state")
                UserDefaults.standard.set(list["city"], forKey: UserDefaultConstants.city.rawValue)
                UserDefaults.standard.set(self.countryShortName, forKey: UserDefaultConstants.countryShortName.rawValue)
                let currencySymbol = self.currencyName.getSymbol(forCurrencyCode: self.currencyName)
                GlobalVariables.currency = currencySymbol ?? ""
                UserDefaults.standard.set(currencySymbol, forKey: "currency")
                
                UserDefaults.standard.synchronize()
                
                self.showAlert("","Profile Updated Successfully")
            }
            else{
                print(list)
                AlertView.simpleViewAlert(title: "",arg0.message , self)
            }
        }
    }
    
    func postComplexPictures(url:URL, params:[String:Any], imageToUpload : Data, finish: @escaping ((message:String, list:[String: Any],isSuccess:Bool)) -> Void) {
        var result:(message:String, list:[String: Any],isSuccess:Bool) = (message: "Fail", list:[:],isSuccess : false)
        SVProgressHUD.show()
        // let headers: HTTPHeaders
        // headers = ["Content-Type":"application/json"]
        
        AF.upload(multipartFormData: { (multipartFormData) in
            let jpegData = imageToUpload
            //let jsonData = try! JSONSerialization.data(withJSONObject: params, options: [])
            // multipartFormData.append(jsonData, withName: "" )
            multipartFormData.append(jpegData, withName: "image",fileName: "file.jpg", mimeType: "image/jpg")
            for (key, value) in params {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: url, usingThreshold: UInt64.init(), method: .post, headers: nil).response{ response in
            
            if((response.error == nil))
            {
                do
                {
                    if let jsonData = response.data
                    {
                        SVProgressHUD.dismiss()
                        let parsedData = try JSONSerialization.jsonObject(with: jsonData) as! Dictionary<String, AnyObject>
                        
                        let status = parsedData["status"] as? String ?? ""
                        let msg = parsedData["message"] as? String ?? ""
                        
                        print("parsedData=", parsedData)
                        
                        if(status=="success") {
                            if let jsonArray = parsedData["result"] as? [String: Any] {
                                result.isSuccess = true
                                result.list = jsonArray
                            }
                        } else {
                            result.isSuccess = false
                            result.message=msg
                        }
                    }
                    SVProgressHUD.dismiss()
                    finish(result)
                } catch {
                    SVProgressHUD.dismiss()
                    finish(result)
                }
            } else {
                SVProgressHUD.dismiss()
                print("Resonse.error",response.error?.localizedDescription as Any)
                finish(result)
            }
        }
    }
    
    func addToDatePicker(){
        let alert = UIAlertController(style: .alert, title: "")
        let date = Calendar.current.date(byAdding: .year, value: 0, to: Date())
        dateTo = date ?? Date()
        alert.addDatePicker(mode: .date, date: Date(), minimumDate: nil, maximumDate: nil) { date in
            self.dateTo = date
        }
        alert.addAction( title: "OK", style: .default, isEnabled: true) { (action) in
            let formatter1 = DateFormatter()
            formatter1.dateFormat = "dd-MM-yyyy"
            
            formatter1.locale = NSLocale(localeIdentifier: "en_US") as Locale?
            self.dateOfBirthTextField.text = formatter1.string(from: self.dateTo)
            
        }
        alert.addAction(title: "Cancel", style: .cancel){ (action) in
            //self.txtDateOfBirth.resignFirstResponder()
            //self.txtPassword.becomeFirstResponder()
        }
        alert.show()
    }
    
    //MARK:-   Alert Function
    
    func showAlert( _ title:String, _ messageVal:String  ){
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "CustomDefaultAlertViewController") as? CustomDefaultAlertViewController else{return}
        vc.titleVal = title
        vc.messageVal = messageVal
        vc.setTransition()
        vc.completionHandler = { [weak self] in
            
            self?.dismiss(animated: true) {
                self?.pressed()
            }
            
        }
        
        self.present(vc, animated: true, completion: nil)
    }
    
    func pressed(){
        popVc()
    }
    
    //MARK:- Object Function Button Action
    @objc func icBackButtonAction(){
        popVc()
        
    }
    
    @IBAction func btnSaveAction(_ sender: Any) {
        
        if UserDefaults.exists(key: UserDefaultConstants.UserID.rawValue){
            
            if UserDefaults.standard.string(forKey: UserDefaultConstants.personalDetailsStatus.rawValue) == "1"{
                ///check if required fields are empty or not
                if fullNameTextField.text?.count == 0 {
                    AlertView.simpleViewAlert(title: "", "Please enter first name", self)
                    return
                }
                else if lastNameTextField.text?.count == 0{
                    
                    AlertView.simpleViewAlert(title: "", "Please enter last name", self)
                }
                
                
                else if dateOfBirthTextField.text?.count == 0 {
                    AlertView.simpleViewAlert(title: "", "Please enter date of birth", self)
                    return
                }
                
                else if txtPhoneNumber.text?.count == 0 {
                    AlertView.simpleViewAlert(title: "", "Please enter Phone Number", self)
                    return
                }
                else if countrytextField.text?.count == 0{
                    AlertView.simpleViewAlert(title: "", "Please select country", self)
                    return
                    
                }
                else if addressLine1TextView.text == "" || addressLine1TextView.text == addressLine1Placeholder{
                    AlertView.simpleViewAlert(title: "", "Please enter address", self)
                    return
                }
                
                else if addressLine2TextView.text == "" || addressLine2TextView.text == addressLine2Placeholder{
                    AlertView.simpleViewAlert(title: "", "Please enter address", self)
                    return
                }
                
                else if cityTextField.text?.count == 0{
                    AlertView.simpleViewAlert(title: "", "Please select city", self)
                    return
                    
                }
                
                else if postalCodeTextField.text?.count == 0{
                    
                    AlertView.simpleViewAlert(title: "", "Enter postal code", self)
                }
                
                else if selectYourGenderTextField.text?.count == 0 {
                    AlertView.simpleViewAlert(title: "", "Please select gender", self)
                    return
                }
                else if txtPhoneNumber.text?.count ?? 0 < 6{
                    
                    AlertView.simpleViewAlert(title: "", "Please enter valid phone number", self)
                    return
                    
                }
                
                else{
                    verifyPostalCode(postalCodeTextField.text ?? "")
                }
            }
            else{
                if fullNameTextField.text?.count == 0 {
                    AlertView.simpleViewAlert(title: "", "Please enter first name", self)
                    return
                }
                else if lastNameTextField.text?.count == 0{
                    
                    AlertView.simpleViewAlert(title: "", "Please enter last name", self)
                }
                else if txtPhoneNumber.text?.count ?? 0 < 6{
                    
                    AlertView.simpleViewAlert(title: "", "Please enter valid phone number", self)
                    return
                    
                }
                else{
                    UpdateUserInfo1(img: self.placeHolderImage)
                }
                
                
            }
            
        }
        
    }
    
        @IBAction func btnProfileImg(_ sender: Any) {
            chooseImageMethod()
        }
    
    @IBAction func navigationBackButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension EditProfileViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
        internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    
            // To handle image
    
            if let img = info[.originalImage] as? UIImage {
                // self.imagePickedBlock?(img)
                profileImg.image = img
                //selectedImg = img
                // imgFlag = true
                if let imgUrl = info[.originalImage] as? URL{
                    let strImage = imgUrl.absoluteString
                    // self.uploadImageMethod(image: img, name: strImage )
                }
            } else{
                print("Something went wrong in  image")
            }
            // To handle video
            if let videoUrl = info[.mediaURL] as? NSURL{
                print("videourl: ", videoUrl)
                //trying compression of video
                let data = NSData(contentsOf: videoUrl as URL)!
                print("File size before compression: \(Double(data.length / 1048576)) mb")
                // self.videoPickedBlock?(videoUrlFromPhone, size)
            }
            else{
                print("Something went wrong in  video")
            }
            self.dismiss(animated: true, completion: nil)
        }
}


//MARK:- Api calls
extension EditProfileViewController{
    
    //    func getAllCountries() {
    //        SVProgressHUD.show()
    //        DataService.sharedInstance.getCountries() { (resultDict, errorMsg) in
    //            SVProgressHUD.dismiss()
    //            print(resultDict as Any)
    //            if errorMsg == nil {
    //                self.allCountries.removeAll()
    //                if let result = resultDict?["result"] as? [NSDictionary] {
    //                    for allDate in result {
    //                        let name = allDate["name"] as? String
    //                        let shortName = allDate["shortName"] as? String
    //                        let currency = allDate["currency"] as? String
    //                        self.allCountries.append(name ?? "")
    //                        self.shortName.append(shortName ?? "")
    //                        self.allCurrency.append(currency ?? "")
    //                    }
    //                    if self.countrytextField.text == "" {
    //                        if self.allCountries[0] != "" {
    //                            self.countrytextField.text = self.allCountries[0]
    //                            self.countryShortName = self.shortName[0]
    //                            self.currencyName = self.allCurrency[0]
    //                            if self.shortName[0] != ""{
    //                                self.getAllStates(countryShortName:self.shortName[0])
    //                            }
    //                        }
    //                    }
    //                    else{
    //
    //                        let shortNameTxt = UserDefaults.standard.string(forKey: "country")
    //                        self.getAllStates(countryShortName:shortNameTxt ?? "")
    //                    }
    //                } else {
    //                    if let message = resultDict?["message"] as? String {
    //                        //self.presentAlert(withTitle: "", message: message)
    //                    }
    //                }
    //            } else {
    //                // Toast.show(message: "Something went wrong", controller: self)
    //            }
    //        }
    //    }
    
    func getAllStates(countryShortName:String) {
        SVProgressHUD.show()
        DataService.sharedInstance.getAllStates(shortName: countryShortName) { (resultDict, errorMsg) in
            SVProgressHUD.dismiss()
            print(resultDict as Any)
            if errorMsg == nil {
                self.allStates.removeAll()
                if let result = resultDict?["result"] as? NSArray {
                    let swiftArray: [String] = result.compactMap({ $0 as? String })
                    self.allStates.append(contentsOf: swiftArray)
                    if self.countrytextField.text == "" {
                        if self.allStates[0] != "" {
                            self.countrytextField.text = self.allStates[0]
                            self.getAllCities(shortName:self.countryShortName,stateName:self.allStates[0])
                        }
                    }
                    else{
                        
                        //                        let shortCountryName = UserDefaults.standard.string(forKey: "country")
                        //                        self.getAllCities(shortName:shortCountryName ?? "",stateName: self.countrytextField.text ?? "")
                        
                    }
                } else {
                    if let message = resultDict?["message"] as? String {
                        //self.presentAlert(withTitle: "", message: message)
                    }
                }
                
            } else {
                // Toast.show(message: "Something went wrong", controller: self)
            }
        }
    }
    
    func getAllCities(shortName:String,stateName:String) {
        SVProgressHUD.show()
        DataService.sharedInstance.getAllCities(shortName:shortName, stateName:stateName) { (resultDict, errorMsg) in
            SVProgressHUD.dismiss()
            print(resultDict as Any)
            if errorMsg == nil {
                self.allCity.removeAll()
                
                
                if resultDict?["code"] as? String == "400"{
                    
                    self.cityTextField.text = stateName
                    
                    self.allCity = [stateName]
                    
                } else if resultDict?["code"] as? String == "200" {
                    if let result = resultDict?["result"] as? NSArray {
                        
                        let cityArray: [String] = result.compactMap({ $0 as? String })
                        self.allCity.append(contentsOf: cityArray)
                        
                        if self.allCity[0] != "" {
                            self.cityTextField.text = self.allCity[0]
                        }
                        
                        
                        //                        self.allStates.append(contentsOf: swiftArray)
                        
                        
                    }
                    
                }else {
                    if let message = resultDict?["message"] as? String {
                        //self.presentAlert(withTitle: "", message: message)
                    }
                }
            } else {
                // Toast.show(message: "Something went wrong", controller: self)
            }
        }
    }
    
    func verifyPostalCode( _ postCode:String){
        SVProgressHUD.show()
        DataService.sharedInstance.checkPostCodeApiCall(postCode: postCode) { (reponse, error) in
            
            if reponse != nil{
                SVProgressHUD.dismiss()
                if let code = reponse?["code"] as? Int{
                    let msg = reponse?["message"] as? String
                    if code == 200{
                        
                        if let result = reponse?["result"] as? String{
                            if result == "1"{
                                
                                self.addNewEvents(self.placeHolderImage)
                                
                            }
                            else{
                                
                                self.customAlertDismiss(title: "Alert", message: "Postal Code is not valid")
                            }
                            
                        }
                        
                    }
                    else{
                        
                        self.customAlertDismiss(title: "Alert", message: msg ?? "")
                        
                    }
                    
                }
                print(reponse)
            }
            else{
                SVProgressHUD.dismiss()
                self.customAlertDismiss(title: "Error", message: "Something Went Wrong!!")
                
            }
            
        }
        
    }
    
    
    func UpdateUserInfo1(img: UIImage){
        
        
        
        let dict = ["firstname": fullNameTextField.text ?? "","lastname": lastNameTextField.text ?? "", "email":emailVal,"phone":txtPhoneNumber.text ?? "","user_id":UserDefaults.standard.value(forKey: "UserID") as! String] as [String : Any]
        
        let pickedImage = img
        let Url = AppConfig.BaseUrl + constantApis.updateUserInfo1
        let jpegData = pickedImage.jpegData(compressionQuality: 0.5)!
        self.postComplexPictures(url:NSURL.init(string: Url)! as URL , params: dict, imageToUpload: jpegData) { (arg0) in
            let (_, list, isSuccess) = arg0
            if(isSuccess) {
                print(list)
                UserDefaults.standard.set(list["user_id"], forKey: UserDefaultConstants.UserID.rawValue)
                UserDefaults.standard.set(list["firstname"], forKey: UserDefaultConstants.FirstName.rawValue)
                UserDefaults.standard.set(list["lastname"], forKey: UserDefaultConstants.LastName.rawValue)
//                                UserDefaults.standard.set(list["gender"], forKey: "UserGender")
//                                UserDefaults.standard.set(list["image"], forKey: "UserImage")
//                                UserDefaults.standard.set(list["dob"], forKey: "UserDob")
                UserDefaults.standard.set(list["email"], forKey: UserDefaultConstants.UserEmail.rawValue)
                UserDefaults.standard.set(list["phone"], forKey: UserDefaultConstants.PhoneNumber.rawValue)
                //                UserDefaults.standard.set(list["address1"], forKey: "Address1")
                //                UserDefaults.standard.set(list["address2"], forKey: "Address2")
                //                UserDefaults.standard.set(list["postcode"], forKey: "postcode")
                //                UserDefaults.standard.set(list["country_name"], forKey: "countryName")
                //                UserDefaults.standard.set(list["state"], forKey: "state")
                //                UserDefaults.standard.set(list["city"], forKey: "city")
                UserDefaults.standard.set(self.countryShortName, forKey: UserDefaultConstants.countryShortName.rawValue)
                let currencySymbol = self.currencyName.getSymbol(forCurrencyCode: self.currencyName)
                GlobalVariables.currency = currencySymbol ?? ""
                UserDefaults.standard.set(currencySymbol, forKey: "currency")
                
                UserDefaults.standard.synchronize()
                
                self.showAlert("","Profile Updated Successfully")
            }
            else{
                print(list)
                self.customAlertDismiss(title: "", message: arg0.message)
                //                AlertView.simpleViewAlert(title: "",arg0.message , self)
            }
        }
    }
    
}

