//
//  EnterPinViewController.swift
//  Peculi
//
//  Created by apple on 06/11/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit
import SVProgressHUD
class EnterPinViewController: UIViewController {
    
    
    //MARK: - Outltes
    
    @IBOutlet weak var enterPinLbl: UILabel!
    @IBOutlet weak var wrongPinLbl: UILabel!
    @IBOutlet weak var textOneView: UIView!
    @IBOutlet weak var textTwoView: UIView!
    @IBOutlet weak var textThreeView: UIView!
    @IBOutlet weak var textFourView: UIView!
    @IBOutlet weak var textFiveView: UIView!
    @IBOutlet weak var textSixView: UIView!
    @IBOutlet weak var textSevenView: UIView!
    @IBOutlet weak var textEightView: UIView!
    @IBOutlet weak var textNineView: UIView!
    @IBOutlet weak var textZeroView: UIView!
    @IBOutlet weak var removeTxtView: UIView!
    
    @IBOutlet weak var circleOneView: UIView!
    @IBOutlet weak var circleTwoView: UIView!
    @IBOutlet weak var circleThreeView: UIView!
    @IBOutlet weak var circleFourView: UIView!
    
    @IBOutlet weak var numberOneButton: UIButton!
    @IBOutlet weak var numberTwoButton: UIButton!
    @IBOutlet weak var numberThreeButton: UIButton!
    @IBOutlet weak var numberFourButton: UIButton!
    @IBOutlet weak var numberFiveButton: UIButton!
    @IBOutlet weak var numberSixButton: UIButton!
    @IBOutlet weak var numberSeavenButton: UIButton!
    @IBOutlet weak var numberEightButton: UIButton!
    @IBOutlet weak var numberNineButton: UIButton!
    @IBOutlet weak var numberZeroButton: UIButton!
    
    
    @IBOutlet weak var textFieldOne: UITextField!
    @IBOutlet weak var textFieldTwo: UITextField!
    @IBOutlet weak var textFieldThree: UITextField!
    @IBOutlet weak var textFieldFour: UITextField!
    
    //MARK: - Variables
    
    var commingFromScreen = ""
    var savePin = ""
    var newPin = ""
    var changePin = ""
    var oldPin = ""
    var pinChngeOrNot = true
    var allowToChangePin = false
    var oldPinChecked = false
    var fromVC: AllControllers?
    
    //MARK: - ViewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        removeTxtView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        removeTxtView.layer.borderWidth = 1
        wrongPinLbl.isHidden = true
        
    }
    
    //MARK: - View will appear
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
        lblTextColorChange()
        uiSetup()
        newUpdateToken_Api()
    }
    
    /**** Setup UI */
    func uiSetup(){
        textOneView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        textOneView.layer.borderWidth = 1
        textOneView.backgroundColor = .clear
        textTwoView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        textTwoView.layer.borderWidth = 1
        textTwoView.backgroundColor = .clear
        textThreeView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        textThreeView.layer.borderWidth = 1
        textThreeView.backgroundColor = .clear
        textFourView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        textFourView.layer.borderWidth = 1
        textFourView.backgroundColor = .clear
        textFiveView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        textFiveView.layer.borderWidth = 1
        textFiveView.backgroundColor = .clear
        textSixView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        textSixView.layer.borderWidth = 1
        textSixView.backgroundColor = .clear
        textSevenView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        textSevenView.layer.borderWidth = 1
        textSevenView.backgroundColor = .clear
        textEightView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        textEightView.layer.borderWidth = 1
        textEightView.backgroundColor = .clear
        textNineView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        textNineView.layer.borderWidth = 1
        textNineView.backgroundColor = .clear
        textZeroView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        textZeroView.layer.borderWidth = 1
        textZeroView.backgroundColor = .clear
        
        circleOneView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        circleOneView.layer.borderWidth = 1
        circleOneView.layer.cornerRadius = 5
        circleOneView.layer.masksToBounds = true
        circleOneView.clipsToBounds = true
        
        circleTwoView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        circleTwoView.layer.borderWidth = 1
        circleTwoView.layer.cornerRadius = 5
        circleTwoView.layer.masksToBounds = true
        
        circleThreeView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        circleThreeView.layer.borderWidth = 1
        circleFourView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        circleFourView.layer.borderWidth = 1
        
        textFieldOne.text = ""
        textFieldTwo.text = ""
        textFieldThree.text = ""
        textFieldFour.text = ""
        
        
    }
    
    /****set text according to flow*/
    func lblTextColorChange() {
        if UserDefaults.exists(key: UserDefaultConstants.UserPin.rawValue){
            if UserDefaults.standard.string(forKey: UserDefaultConstants.UserPin.rawValue) != "" {
                
                if commingFromScreen == "settings" {
                    
                    let string = NSMutableAttributedString(string: "Enter your old pin to confirm your identity")
                    string.setColorForText("old", with: #colorLiteral(red: 1, green: 0.6588235294, blue: 0.231372549, alpha: 1))
                    string.setColorForText("Enter your", with: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
                    string.setColorForText("pin to confirm your identity", with: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
                    enterPinLbl.attributedText = string
                    
                }
                else {
                    let string = NSMutableAttributedString(string: "Enter your PIN number")
                    string.setColorForText("PIN", with: #colorLiteral(red: 1, green: 0.6588235294, blue: 0.231372549, alpha: 1))
                    string.setColorForText("Enter your", with: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
                    string.setColorForText("number", with: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
                    enterPinLbl.attributedText = string
                }
                
            }
            else{
                
                let string = NSMutableAttributedString(string: "Enter your PIN number")
                string.setColorForText("PIN", with: #colorLiteral(red: 1, green: 0.6588235294, blue: 0.231372549, alpha: 1))
                string.setColorForText("Enter your", with: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
                string.setColorForText("number", with: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
                enterPinLbl.attributedText = string
                
                
            }
        }
        else {
            let string = NSMutableAttributedString(string: "Enter your PIN number")
            string.setColorForText("PIN", with: #colorLiteral(red: 1, green: 0.6588235294, blue: 0.231372549, alpha: 1))
            string.setColorForText("Enter your", with: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
            string.setColorForText("number", with: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
            enterPinLbl.attributedText = string
            
        }
        
        
    }
    
    
    //MARK:- UIButton Action
    
    @IBAction func numberPadAllJointButtonAcrtion(_ sender: UIButton) {
        
        switch sender.tag {
        case 0:
            textOneView.backgroundColor = #colorLiteral(red: 1, green: 0.6588235294, blue: 0.231372549, alpha: 1)
            UIView.animate(withDuration: 2, delay: 0.0, options:[], animations: {
                self.textOneView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                self.textOneView.layer.borderWidth = 1
                self.textOneView.backgroundColor = .clear
            }, completion: nil)
            textTwoView.backgroundColor = .clear
            textThreeView.backgroundColor = .clear
            textFourView.backgroundColor = .clear
            textFiveView.backgroundColor = .clear
            textSixView.backgroundColor = .clear
            textSevenView.backgroundColor = .clear
            textEightView.backgroundColor = .clear
            textNineView.backgroundColor = .clear
            textZeroView.backgroundColor = .clear
            textTwoView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textTwoView.layer.borderWidth = 1
            textThreeView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textThreeView.layer.borderWidth = 1
            textFourView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textFourView.layer.borderWidth = 1
            textFiveView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textFiveView.layer.borderWidth = 1
            textSixView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textSixView.layer.borderWidth = 1
            textSevenView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textSevenView.layer.borderWidth = 1
            textEightView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textEightView.layer.borderWidth = 1
            textNineView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textNineView.layer.borderWidth = 1
            textZeroView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textZeroView.layer.borderWidth = 1
            savePin = "1"
            textfieldCheck()
        case 1:
            textTwoView.backgroundColor = #colorLiteral(red: 1, green: 0.6588235294, blue: 0.231372549, alpha: 1)
            UIView.animate(withDuration: 2, delay: 0.0, options:[], animations: {
                self.textTwoView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                self.textTwoView.layer.borderWidth = 1
                self.textTwoView.backgroundColor = .clear
            }, completion: nil)
            textOneView.backgroundColor = .clear
            textThreeView.backgroundColor = .clear
            textFourView.backgroundColor = .clear
            textFiveView.backgroundColor = .clear
            textSixView.backgroundColor = .clear
            textSevenView.backgroundColor = .clear
            textEightView.backgroundColor = .clear
            textNineView.backgroundColor = .clear
            textZeroView.backgroundColor = .clear
            textOneView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textOneView.layer.borderWidth = 1
            textThreeView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textThreeView.layer.borderWidth = 1
            textFourView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textFourView.layer.borderWidth = 1
            textFiveView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textFiveView.layer.borderWidth = 1
            textSixView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textSixView.layer.borderWidth = 1
            textSevenView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textSevenView.layer.borderWidth = 1
            textEightView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textEightView.layer.borderWidth = 1
            textNineView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textNineView.layer.borderWidth = 1
            textZeroView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textZeroView.layer.borderWidth = 1
            savePin = "2"
            textfieldCheck()
        case 2:
            textThreeView.backgroundColor = #colorLiteral(red: 1, green: 0.6588235294, blue: 0.231372549, alpha: 1)
            UIView.animate(withDuration: 2, delay: 0.0, options:[], animations: {
                self.textThreeView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                self.textThreeView.layer.borderWidth = 1
                self.textThreeView.backgroundColor = .clear
            }, completion: nil)
            textOneView.backgroundColor = .clear
            textTwoView.backgroundColor = .clear
            textFourView.backgroundColor = .clear
            textFiveView.backgroundColor = .clear
            textSixView.backgroundColor = .clear
            textSevenView.backgroundColor = .clear
            textEightView.backgroundColor = .clear
            textNineView.backgroundColor = .clear
            textZeroView.backgroundColor = .clear
            textOneView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textOneView.layer.borderWidth = 1
            textTwoView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textTwoView.layer.borderWidth = 1
            textFourView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textFourView.layer.borderWidth = 1
            textFiveView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textFiveView.layer.borderWidth = 1
            textSixView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textSixView.layer.borderWidth = 1
            textSevenView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textSevenView.layer.borderWidth = 1
            textEightView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textEightView.layer.borderWidth = 1
            textNineView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textNineView.layer.borderWidth = 1
            textZeroView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textZeroView.layer.borderWidth = 1
            savePin = "3"
            textfieldCheck()
        case 3:
            textFourView.backgroundColor = #colorLiteral(red: 1, green: 0.6588235294, blue: 0.231372549, alpha: 1)
            UIView.animate(withDuration: 2, delay: 0.0, options:[], animations: {
                self.textFourView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                self.textFourView.layer.borderWidth = 1
                self.textFourView.backgroundColor = .clear
            }, completion: nil)
            textOneView.backgroundColor = .clear
            textTwoView.backgroundColor = .clear
            textThreeView.backgroundColor = .clear
            textFiveView.backgroundColor = .clear
            textSixView.backgroundColor = .clear
            textSevenView.backgroundColor = .clear
            textEightView.backgroundColor = .clear
            textNineView.backgroundColor = .clear
            textZeroView.backgroundColor = .clear
            textOneView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textOneView.layer.borderWidth = 1
            textTwoView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textTwoView.layer.borderWidth = 1
            textThreeView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textThreeView.layer.borderWidth = 1
            textFiveView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textFiveView.layer.borderWidth = 1
            textSixView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textSixView.layer.borderWidth = 1
            textSevenView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textSevenView.layer.borderWidth = 1
            textEightView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textEightView.layer.borderWidth = 1
            textNineView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textNineView.layer.borderWidth = 1
            textZeroView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textZeroView.layer.borderWidth = 1
            savePin = "4"
            textfieldCheck()
        case 4:
            textFiveView.backgroundColor = #colorLiteral(red: 1, green: 0.6588235294, blue: 0.231372549, alpha: 1)
            UIView.animate(withDuration: 2, delay: 0.0, options:[], animations: {
                self.textFiveView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                self.textFiveView.layer.borderWidth = 1
                self.textFiveView.backgroundColor = .clear
            }, completion: nil)
            textOneView.backgroundColor = .clear
            textTwoView.backgroundColor = .clear
            textThreeView.backgroundColor = .clear
            textFourView.backgroundColor = .clear
            textSixView.backgroundColor = .clear
            textSevenView.backgroundColor = .clear
            textEightView.backgroundColor = .clear
            textNineView.backgroundColor = .clear
            textZeroView.backgroundColor = .clear
            textOneView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textOneView.layer.borderWidth = 1
            textTwoView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textTwoView.layer.borderWidth = 1
            textThreeView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textThreeView.layer.borderWidth = 1
            textFourView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textFourView.layer.borderWidth = 1
            textSixView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textSixView.layer.borderWidth = 1
            textSevenView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textSevenView.layer.borderWidth = 1
            textEightView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textEightView.layer.borderWidth = 1
            textNineView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textNineView.layer.borderWidth = 1
            textZeroView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textZeroView.layer.borderWidth = 1
            savePin = "5"
            textfieldCheck()
        case 5:
            textSixView.backgroundColor = #colorLiteral(red: 1, green: 0.6588235294, blue: 0.231372549, alpha: 1)
            UIView.animate(withDuration: 2, delay: 0.0, options:[], animations: {
                self.textSixView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                self.textSixView.layer.borderWidth = 1
                self.textSixView.backgroundColor = .clear
            }, completion: nil)
            textOneView.backgroundColor = .clear
            textTwoView.backgroundColor = .clear
            textThreeView.backgroundColor = .clear
            textFourView.backgroundColor = .clear
            textFiveView.backgroundColor = .clear
            textSevenView.backgroundColor = .clear
            textEightView.backgroundColor = .clear
            textNineView.backgroundColor = .clear
            textZeroView.backgroundColor = .clear
            textOneView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textOneView.layer.borderWidth = 1
            textTwoView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textTwoView.layer.borderWidth = 1
            textThreeView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textThreeView.layer.borderWidth = 1
            textFourView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textFourView.layer.borderWidth = 1
            textFiveView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textFiveView.layer.borderWidth = 1
            textSevenView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textSevenView.layer.borderWidth = 1
            textEightView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textEightView.layer.borderWidth = 1
            textNineView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textNineView.layer.borderWidth = 1
            textZeroView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textZeroView.layer.borderWidth = 1
            savePin = "6"
            textfieldCheck()
        case 6:
            textSevenView.backgroundColor = #colorLiteral(red: 1, green: 0.6588235294, blue: 0.231372549, alpha: 1)
            UIView.animate(withDuration: 2, delay: 0.0, options:[], animations: {
                self.textSevenView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                self.textSevenView.layer.borderWidth = 1
                self.textSevenView.backgroundColor = .clear
            }, completion: nil)
            textOneView.backgroundColor = .clear
            textTwoView.backgroundColor = .clear
            textThreeView.backgroundColor = .clear
            textFourView.backgroundColor = .clear
            textFiveView.backgroundColor = .clear
            textSixView.backgroundColor = .clear
            textEightView.backgroundColor = .clear
            textNineView.backgroundColor = .clear
            textZeroView.backgroundColor = .clear
            textOneView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textOneView.layer.borderWidth = 1
            textTwoView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textTwoView.layer.borderWidth = 1
            textThreeView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textThreeView.layer.borderWidth = 1
            textFourView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textFourView.layer.borderWidth = 1
            textFiveView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textFiveView.layer.borderWidth = 1
            textSixView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textSixView.layer.borderWidth = 1
            textEightView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textEightView.layer.borderWidth = 1
            textNineView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textNineView.layer.borderWidth = 1
            textZeroView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textZeroView.layer.borderWidth = 1
            savePin = "7"
            textfieldCheck()
        case 7:
            textEightView.backgroundColor = #colorLiteral(red: 1, green: 0.6588235294, blue: 0.231372549, alpha: 1)
            UIView.animate(withDuration: 2, delay: 0.0, options:[], animations: {
                self.textEightView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                self.textEightView.layer.borderWidth = 1
                self.textEightView.backgroundColor = .clear
            }, completion: nil)
            textOneView.backgroundColor = .clear
            textTwoView.backgroundColor = .clear
            textThreeView.backgroundColor = .clear
            textFourView.backgroundColor = .clear
            textFiveView.backgroundColor = .clear
            textSixView.backgroundColor = .clear
            textSevenView.backgroundColor = .clear
            textNineView.backgroundColor = .clear
            textZeroView.backgroundColor = .clear
            textOneView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textOneView.layer.borderWidth = 1
            textTwoView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textTwoView.layer.borderWidth = 1
            textThreeView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textThreeView.layer.borderWidth = 1
            textFourView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textFourView.layer.borderWidth = 1
            textFiveView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textFiveView.layer.borderWidth = 1
            textSixView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textSixView.layer.borderWidth = 1
            textSevenView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textSevenView.layer.borderWidth = 1
            textNineView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textNineView.layer.borderWidth = 1
            textZeroView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textZeroView.layer.borderWidth = 1
            savePin = "8"
            textfieldCheck()
        case 8:
            textNineView.backgroundColor = #colorLiteral(red: 1, green: 0.6588235294, blue: 0.231372549, alpha: 1)
            UIView.animate(withDuration: 2, delay: 0.0, options:[], animations: {
                self.textNineView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                self.textNineView.layer.borderWidth = 1
                self.textNineView.backgroundColor = .clear
            }, completion: nil)
            textOneView.backgroundColor = .clear
            textTwoView.backgroundColor = .clear
            textThreeView.backgroundColor = .clear
            textFourView.backgroundColor = .clear
            textFiveView.backgroundColor = .clear
            textSixView.backgroundColor = .clear
            textSevenView.backgroundColor = .clear
            textEightView.backgroundColor = .clear
            textZeroView.backgroundColor = .clear
            textOneView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textOneView.layer.borderWidth = 1
            textTwoView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textTwoView.layer.borderWidth = 1
            textThreeView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textThreeView.layer.borderWidth = 1
            textFourView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textFourView.layer.borderWidth = 1
            textFiveView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textFiveView.layer.borderWidth = 1
            textSixView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textSixView.layer.borderWidth = 1
            textSevenView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textSevenView.layer.borderWidth = 1
            textEightView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textEightView.layer.borderWidth = 1
            textZeroView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textZeroView.layer.borderWidth = 1
            savePin = "9"
            textfieldCheck()
        case 9:
            self.textZeroView.backgroundColor = #colorLiteral(red: 1, green: 0.6588235294, blue: 0.231372549, alpha: 1)
            //  UIView.AnimationOptions.repeat, UIView.AnimationOptions.autoreverse
            UIView.animate(withDuration: 2, delay: 0.0, options:[], animations: {
                self.textZeroView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                self.textZeroView.layer.borderWidth = 1
                self.textZeroView.backgroundColor = .clear
            }, completion: nil)
            textOneView.backgroundColor = .clear
            textTwoView.backgroundColor = .clear
            textThreeView.backgroundColor = .clear
            textFourView.backgroundColor = .clear
            textFiveView.backgroundColor = .clear
            textSixView.backgroundColor = .clear
            textSevenView.backgroundColor = .clear
            textEightView.backgroundColor = .clear
            textNineView.backgroundColor = .clear
            textOneView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textOneView.layer.borderWidth = 1
            textTwoView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textTwoView.layer.borderWidth = 1
            textThreeView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textThreeView.layer.borderWidth = 1
            textFourView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textFourView.layer.borderWidth = 1
            textFiveView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textFiveView.layer.borderWidth = 1
            textSixView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textSixView.layer.borderWidth = 1
            textSevenView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textSevenView.layer.borderWidth = 1
            textEightView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textEightView.layer.borderWidth = 1
            textNineView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            textNineView.layer.borderWidth = 1
            savePin = "0"
            textfieldCheck()
        default:
            allBtnView()
        }
        print(savePin)
        
    }
    
    
    func allBtnView() {
        textOneView.backgroundColor = .clear
        textTwoView.backgroundColor = .clear
        textThreeView.backgroundColor = .clear
        textFourView.backgroundColor = .clear
        textFiveView.backgroundColor = .clear
        textSixView.backgroundColor = .clear
        textSevenView.backgroundColor = .clear
        textEightView.backgroundColor = .clear
        textNineView.backgroundColor = .clear
        textZeroView.backgroundColor = .clear
        textOneView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        textOneView.layer.borderWidth = 1
        textTwoView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        textTwoView.layer.borderWidth = 1
        textThreeView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        textThreeView.layer.borderWidth = 1
        textFourView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        textFourView.layer.borderWidth = 1
        textFiveView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        textFiveView.layer.borderWidth = 1
        textSixView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        textSixView.layer.borderWidth = 1
        textSevenView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        textSevenView.layer.borderWidth = 1
        textEightView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        textEightView.layer.borderWidth = 1
        textNineView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        textNineView.layer.borderWidth = 1
        textZeroView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        textZeroView.layer.borderWidth = 1
        
    }
    
    func selectedBtnView() {
        circleOneView.backgroundColor = .clear
        circleTwoView.backgroundColor = .clear
        circleThreeView.backgroundColor = .clear
        circleFourView.backgroundColor = .clear
        
        circleOneView.layer.borderColor =  #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        circleOneView.layer.borderWidth = 1
        circleOneView.layer.cornerRadius = 5
        circleOneView.layer.masksToBounds = true
        circleOneView.clipsToBounds = true
        
        circleTwoView.layer.borderColor =  #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        circleTwoView.layer.borderWidth = 1
        circleTwoView.layer.cornerRadius = 5
        circleTwoView.layer.masksToBounds = true
        
        circleThreeView.layer.borderColor =  #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        circleThreeView.layer.borderWidth = 1
        circleFourView.layer.borderColor =  #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        circleFourView.layer.borderWidth = 1
        
    }
    
    func selectedTxtFieldRemoveAll() {
        textFieldOne.text?.removeAll()
        textFieldTwo.text?.removeAll()
        textFieldThree.text?.removeAll()
        textFieldFour.text?.removeAll()
        
    }
    
    ///check if pin entered
    func textfieldCheck(){
        if textFieldOne.text == "" {
            textFieldOne.text = savePin
            circleOneView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        else if textFieldTwo.text == "" {
            textFieldTwo.text = savePin
            circleTwoView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        else if textFieldThree.text == "" {
            textFieldThree.text = savePin
            circleThreeView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        } else if textFieldFour.text == "" {
            
            if commingFromScreen == "SignIn" || commingFromScreen == "Signup"  {
                
                
                if UserDefaults.exists(key: UserDefaultConstants.UserPin.rawValue){
                    
                    if UserDefaults.standard.string(forKey: UserDefaultConstants.UserPin.rawValue) ?? "" != "" {
                        
                        newPin = "\(textFieldOne.text ?? "")\(textFieldTwo.text ?? "")\(textFieldThree.text ?? "")\(savePin)"
                        //pin check
                        if UserDefaults.standard.string(forKey: UserDefaultConstants.UserPin.rawValue) ?? "" == newPin {
                            
                            UserDefaults.standard.set("pin", forKey: UserDefaultConstants.Auth.rawValue)
                            UserDefaults.standard.set("3", forKey: UserDefaultConstants.LoginStatus.rawValue)
                            //                    UserDefaults.standard.set("3", forKey: "LoginStatus")
                            self.setRootHomeVC()
                            wrongPinLbl.isHidden = true
                        } else if UserDefaults.standard.string(forKey: UserDefaultConstants.UserPin.rawValue) ?? "" != newPin {
                            wrongPinLbl.isHidden = false
                            selectedBtnView()
                            selectedTxtFieldRemoveAll()
                            allBtnView()
                        }
                        
                    }
                    else{
                        
                        
                        changepin()
                    }
                    
                }
                else{
                    
                    changepin()
                }
                
                
                
            }else if commingFromScreen == "settings"{
                
                if allowToChangePin == false{
                    checkOldPin()
                }
                else{
                    if oldPinChecked == false{
                        checkIfSamePin()
                    }
                    else{
                        changepin()
                    }
                    
                }
                
            }
            else {
                changepin()
                
                
                
            }
        }
        else{
            print("Someting went wrong")
        }
    }
    
    /** check if user has entered the same pin as previous one **/
    func checkIfSamePin(){
        
        newPin = "\(textFieldOne.text ?? "")\(textFieldTwo.text ?? "")\(textFieldThree.text ?? "")\(savePin)"
        if oldPin == newPin{
            oldPinChecked = false
            newPin = ""
            let string = NSMutableAttributedString(string: "Same PIN number as old one. Please enter the another one.")
            string.setColorForText("PIN", with: #colorLiteral(red: 1, green: 0.6588235294, blue: 0.231372549, alpha: 1))
            string.setColorForText("Same", with: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
            string.setColorForText("number as old one. Please enter the another one.", with: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
            enterPinLbl.attributedText = string
            selectedTxtFieldRemoveAll()
            allBtnView()
            selectedBtnView()
            
        }
        else{
            oldPinChecked = true
            changepin()
            
        }
        
        
    }
    
    
    //old pin check
    func checkOldPin(){
        
        oldPin = "\(textFieldOne.text ?? "")\(textFieldTwo.text ?? "")\(textFieldThree.text ?? "")\(savePin)"
        
        if UserDefaults.standard.string(forKey: UserDefaultConstants.UserPin.rawValue) == oldPin{
            
            let string = NSMutableAttributedString(string: "Enter your new PIN number")
            string.setColorForText("PIN", with: #colorLiteral(red: 1, green: 0.6588235294, blue: 0.231372549, alpha: 1))
            string.setColorForText("Enter your new", with: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
            string.setColorForText("number", with: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
            enterPinLbl.attributedText = string
            allowToChangePin = true
            
        }
        else{
            newPin.removeAll()
            changePin.removeAll()
            enterPinLbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            enterPinLbl.text = "Incorrect old PIN entered"
            let generator = UINotificationFeedbackGenerator()
            generator.notificationOccurred(.error)
            
        }
        selectedTxtFieldRemoveAll()
        allBtnView()
        selectedBtnView()
        
    }
    
    
    /****Allow user to change pin */
    func changepin(){
        
        if pinChngeOrNot == true {
            newPin = "\(textFieldOne.text ?? "")\(textFieldTwo.text ?? "")\(textFieldThree.text ?? "")\(savePin)"
            enterPinLbl.text = "Enter pin code Again"
            enterPinLbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            pinChngeOrNot = false
        } else if pinChngeOrNot == false {
            changePin = "\(textFieldOne.text ?? "")\(textFieldTwo.text ?? "")\(textFieldThree.text ?? "")\(savePin)"
            pinChngeOrNot = true
        }
        //  updateUserPin()
        
        selectedTxtFieldRemoveAll()
        allBtnView()
        selectedBtnView()
        
        if changePin != "" {
            if newPin == changePin {
                updateUserPin()
            } else if newPin != changePin {
                newPin.removeAll()
                changePin.removeAll()
                enterPinLbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                enterPinLbl.text = "Pin codes does not match"
                let generator = UINotificationFeedbackGenerator()
                generator.notificationOccurred(.error)
                
            }
        } else {
            
        }
        
    }
    
    //MARK: - button actions
    @IBAction func removeTxtBtnAction(_ sender: Any) {
        if textFieldFour.text != "" {
            textFieldFour.text?.removeAll()
            circleFourView.backgroundColor = .clear
        } else if textFieldThree.text != "" {
            textFieldThree.text?.removeAll()
            circleThreeView.backgroundColor = .clear
        } else if textFieldTwo.text != "" {
            textFieldTwo.text?.removeAll()
            circleTwoView.backgroundColor = .clear
        } else if textFieldOne.text != "" {
            textFieldOne.text?.removeAll()
            circleOneView.backgroundColor = .clear
        }
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}



extension EnterPinViewController {
    
    /****Update user pin to userDefaults */
    
    func updateUserPin(){
        
        if self.commingFromScreen == "Signup" || self.commingFromScreen == "SignIn"{
            
            UserDefaults.standard.set("pin", forKey: UserDefaultConstants.Auth.rawValue)
            UserDefaults.standard.set(newPin, forKey: UserDefaultConstants.UserPin.rawValue)
            UserDefaults.standard.set("3", forKey: UserDefaultConstants.LoginStatus.rawValue)
            self.setRootHomeVC()
            
        }
        else {
            
            //            UserDefaults.standard.set("pin", forKey: "Auth")
            UserDefaults.standard.set(newPin, forKey: UserDefaultConstants.UserPin.rawValue)
            //            UserDefaults.standard.set("3", forKey: "LoginStatus")
            guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "CustomDefaultAlertViewController") as? CustomDefaultAlertViewController else {
                return
            }
            vc.titleVal = "Message"
            vc.messageVal = "Pin number updated successfully!"
            vc.completionHandler = { [weak self] in
                self?.dismiss(animated: false) {
                    self?.setRootHomeVC()
                }
            }
            vc.setTransition()
            self.present(vc, animated: true, completion: nil)
            
            
            
            
            
        }
        
        
        
        
        //        SVProgressHUD.show()
        //        DataService.sharedInstance.updatePin(pin: newPin) { (resultDict, errorMsg) in
        //            SVProgressHUD.dismiss()
        //            print(resultDict as Any)
        //
        //            if errorMsg == nil{
        //                if let result = resultDict?["result"] as? [NSDictionary] {
        //                    let pinVal = result[0]["pin"] as? String
        //                    if let status = resultDict?["status"] as? String {
        //                        if status == "success"{
        //                            if self.commingFromScreen == "SignIn"{
        //                                print(pinVal)
        //                                UserDefaults.standard.set("pin", forKey: "Auth")
        //                                UserDefaults.standard.set(pinVal, forKey: "UserPin")
        //                                UserDefaults.standard.set("3", forKey: "LoginStatus")
        //                                self.setRootHomeVC()
        //                            }
        //                            else if self.commingFromScreen == "Signup" {
        //
        //                                UserDefaults.standard.set("pin", forKey: "Auth")
        //                                UserDefaults.standard.set(pinVal, forKey: "UserPin")
        //                                UserDefaults.standard.set("3", forKey: "LoginStatus")
        //                                self.setRootHomeVC()
        ////                                guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "TutorialVc") as? TutorialVc else{return}
        ////
        ////                                self.navigationController?.pushViewController(vc, animated: true)
        //
        //
        //                            }
        //
        //
        //                                else {
        //                                UserDefaults.standard.set("pin", forKey: "Auth")
        //                                UserDefaults.standard.set("3", forKey: "LoginStatus")
        //                                UserDefaults.standard.set(pinVal, forKey: "UserPin")
        //                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        //                                self.navigationController?.pushViewController(vc, animated: true)
        //                            }
        //                        } else {
        //
        //
        //
        //                        }
        //                    }
        //                }
        //                else{
        //                    if let message = resultDict?["message"] as? String {
        //                        //self.presentAlert(withTitle: "", message: message)
        //                    }
        //                }
        //
        //            } else {
        //                // Toast.show(message: "Something went wrong", controller: self)
        //            }
        //        }
    }
    
    
    
    
    func newUpdateToken_Api(){
        
//self.addPeculiPopup(view: self.view)
        
        let params:[String:Any] = ["userId": UserDefaults.standard.value(forKey: UserDefaultConstants.UserID.rawValue) ?? ""
                                   
        ]

        print(params)
        
        DataService.sharedInstance.postApiCall(endpoint: constantApis.newUpdateToken_Api, params: params) { (response, error) in
            
           // self.hidePeculiPopup(view: self.view)
       
            print(response)
            
            if response != nil {
                
                print(response ?? [:])
                
                let codeInt = response?["code"] as? Int ?? 0
                let codeString = response?["code"] as? String ?? ""
                let message = response?["message"] as? String ?? ""
                
                if codeInt == 200 || codeString == "200" {
                    
                    print("edit api call 1===========>>>>>>>>>>>")
                    
                    let token = response?["token"] as? String ?? ""
                    let monthlyLimit = response?["monthlyLimit"] as? String ?? ""
                    let dayLimit = response?["dayLimit"] as? String ?? ""
                    let yearlyLimit = response?["yearlyLimit"] as? String ?? ""
                    let maxLoad = response?["maxLoad"] as? String ?? ""
                    let transactionCount = response?["transactionCount"] as? String ?? ""
                    let transactionLimit = response?["transactionLimit"] as? String ?? ""
                    let transactionSum = response?["transactionSum"] as? String ?? ""
                     
                    UserDefaults.standard.set(token, forKey: UserDefaultConstants.jwToken.rawValue)
                    
                    UserDefaults.standard.set(monthlyLimit, forKey: UserDefaultConstants.monthlyLimit.rawValue)
                    UserDefaults.standard.set(dayLimit, forKey: UserDefaultConstants.dayLimit.rawValue)
                    UserDefaults.standard.set(yearlyLimit, forKey: UserDefaultConstants.yearlyLimit.rawValue)
                    UserDefaults.standard.set(maxLoad, forKey: UserDefaultConstants.maxLoad.rawValue)
                    UserDefaults.standard.set(transactionCount, forKey: UserDefaultConstants.transactionCount.rawValue)
                    UserDefaults.standard.set(transactionLimit, forKey: UserDefaultConstants.transactionLimit.rawValue)
                    UserDefaults.standard.set(transactionSum, forKey: UserDefaultConstants.transactionSum.rawValue)
                    
                    
                } else {
                   
                 //   self.customAlertDismiss(title: "Alert", message: message)
                    
                }
                
            }
            else {
                
//                self.customAlertDismiss(title: "Alert", message: "Something went wrong!")
//
//                self.dismiss(animated: true, completion: nil)
            }
        }
        
    }
    
}


