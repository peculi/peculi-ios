//
//  SuccessLottieFile_VC.swift
//  Peculi
//
//  Created by PPI on 5/13/22.
//  Copyright © 2022 Stealth Tech. All rights reserved.
//

import UIKit
import Lottie

class SuccessLottieFile_VC: UIViewController {

    
    @IBOutlet weak var animationView: AnimationView!
    @IBOutlet weak var bgview: UIView!
    
    
    private var lottieAnimation: AnimationView?
    private var backLottieAnimation: AnimationView?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
       self.backLottieAnimation = AnimationView(name: "success_transfer")
       self.backLottieAnimation?.frame = self.animationView.bounds
       self.backLottieAnimation?.contentMode = .scaleToFill
       self.backLottieAnimation?.center = self.animationView.center
        self.backLottieAnimation?.loopMode = .playOnce
       self.animationView.addSubview(self.backLottieAnimation!)
        
        
        
     //   self.backLottieAnimation?.play()
      
        playAnimation()
        

//        let jsonName = "Watermelon"
//        let animation = Animation.named(jsonName)
//
//        // Load animation to AnimationView
//        let animationView = AnimationView(animation: animation)
//        animationView.frame = CGRect(x: 0, y: 0, width: 200, height: 200)
//
//        // Add animationView as subview
//        animationView.addSubview(animationView)
//
//        // Play the animation
//        animationView.play()
        

    }
    
    
    func playAnimation() {
        self.backLottieAnimation?.play(completion: { played in
            if played == true {
                print("Navigate here")
               
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: PeculiOfferViewController.self) {
                        self.navigationController!.popToViewController(controller, animated: true)
                        break
                    }
                }
                
            } else {
                self.playAnimation()
            }
        })
    }
    
    
    
    
    


}



