//
//  SelectedPot_VC.swift
//  Peculi
//  Created by PPI on 5/5/22.
//  Copyright © 2022 Stealth Tech. All rights reserved.


import UIKit

class SelectedPot_VC: UIViewController {
    
    
    @IBOutlet weak var myTableview: UITableView!
    @IBOutlet weak var txtEnterAmount: UITextField!
    
    var selectedRows:[IndexPath] = []
    var dataUser:[UserCategoriesInfo] = []
    var to_Id = ""
    var toCategioryId = ""
    var from_Id = ""
    var fromCategiory_Id = ""
    var amountSet : Double?
    
    var getPotsTransferData:[GetPotsTransferModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        from_Id = ""
        fromCategiory_Id = ""
        
        myTableview.delegate = self
        myTableview.dataSource = self
        
        
        self.addGetPots_APICall(catId: to_Id)
        
        navigationController?.navigationBar.isHidden = true
        navigationController?.navigationBar.barStyle = .black
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.txtEnterAmount.text = String(amountSet ?? 0.00)
        
    }
    
    //MARK: - FUNCTION
    
    func navigartionSetup(){
        
        //icBackArrowWhite
        
        navigationController?.navigationBar.isHidden = false
        title = ""
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2203302979, green: 0.1600231826, blue: 0.3218392134, alpha: 1)
        self.navigationController?.navigationBar.isTranslucent = true
        
        //MARK: - Navigation LeftBar Button:-

        //        let leftBarButton = UIBarButtonItem(image: UIImage(named: "icBackArrowWhite"), style: .done, target: self, action: #selector(icBackButtonAction))
        //        self.navigationItem.leftBarButtonItem  = leftBarButton
        
        
        
    }
    
    
    @IBAction func btnCross_Action(_ sender: Any) {
        popVc()
    }
    
    
    @IBAction func btnProceed_Action(_ sender: Any) {
        
        let amount = Double(txtEnterAmount.text ?? "") ?? 0.00
        
        let price = amount.format()
        
        if  amount > amountSet ?? 0.00 {
            
            Toast.show(message: "Please enter amount less then or equal to \(amountSet ?? 0.00 )", controller: self)
            
        } else if txtEnterAmount.text == ""  {
            
            Toast.show(message: "Please enter amount less then or equal to \(amountSet ?? 0.00)", controller: self)
            
        } else if from_Id == "" &&  fromCategiory_Id == "" {

            Toast.show(message: "Please Select Pot ", controller: self)
            
        }
                       
        else {
            
            print("mohit")
            
            potTransfer_APICall(amount:price)
            
        }
  
    }

    
}


extension SelectedPot_VC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getPotsTransferData.count //dataUser.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectedPotTV_Cell") as! SelectedPotTV_Cell
        
        cell.productNameLbl.text = "\(self.getPotsTransferData[indexPath.row].category_details.name.capitalizingFirstLetter()) Pot"
        
        let priceConvert = Double(self.getPotsTransferData[indexPath.row].categoryBalance)
        let price = priceConvert?.format()
        
        cell.productPrizeLbl.text = "\(GlobalVariables.currency)\(price ?? "")"
        var originalString = String()
        
        originalString = self.getPotsTransferData[indexPath.row].category_details.image
        let ImageUrl = String(format: "%@%@", AppConfig.ImageUrl,originalString)
        
        cell.productImageView.sd_setImage(with: URL(string:ImageUrl), placeholderImage:UIImage(named: "placeholder_Img"), completed: { (image, error, cacheType, url) -> Void in
            if ((error) != nil) {
                // set the placeholder image here
                //cell.imgVw.image = #imageLiteral(resourceName: "doctorAvtar_Img")
                
            } else {
                
                // success ... use the image
                
            }
        })
        
        
        if selectedRows.contains(indexPath) {
            
            cell.circleImg.image = UIImage.init(named: "icSelectedRadioButtonSignup")
            
        } else {
            
            
            cell.circleImg.image = UIImage.init(named: "icUnselectedRadioButtonSignup")
        }
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return  UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedIndexPath = IndexPath(row: indexPath.row, section: 0)
        
        self.selectedRows.removeAll()
        
        if self.selectedRows.contains(selectedIndexPath) {
            
            self.selectedRows.remove(at: self.selectedRows.index(of: selectedIndexPath)!)
            
        } else {
            
            from_Id = self.getPotsTransferData[indexPath.row].fromid
            fromCategiory_Id = self.getPotsTransferData[indexPath.row].category_details.fromCatId
            self.selectedRows.append(selectedIndexPath)
            
        }
        
        myTableview.reloadData()
        
    }
    
}



extension SelectedPot_VC {
    
    
    //MARK: - FUNCTION API CALL
    
    func addGetPots_APICall(catId:String){
        
        self.addPeculiPopup(view: self.view)
        let params:[String: Any] = ["catId": catId,
        ]
        
        print(params)
        DataService.sharedInstance.postApiCall(endpoint: constantApis.getPots_Api, params: params) { (response, error) in
            self.hidePeculiPopup(view: self.view)
            
            print(response)
            
            if response != nil {
                
                
                if let result = response?["result"] as? [NSDictionary] {
                    
                    print(result)
                    
                    self.getPotsTransferData.removeAll()
                    for allData in result {
                        
                        let fromId = allData["_id"] as? String ?? ""
                        let categoryBalance = allData["categoryBalance"] as? String ?? ""
                        
                        if let allCatDetail = allData["category_details"] as? NSDictionary {
                            
                            print(allCatDetail)
                            
                            let fromCatId = allCatDetail["_id"] as? String ?? ""
                            let name = allCatDetail["name"] as? String ?? ""
                            let image = allCatDetail["image"] as? String ?? ""
                            
                            self.getPotsTransferData.append(GetPotsTransferModel.init(fromid: fromId, categoryBalance: categoryBalance, category_details: category_details.init(fromCatId: fromCatId, name: name, image: image)))
                        }
                    }
                    
                    print("self.getPotsTransferData======>>>>>>>>   \(self.getPotsTransferData)")
                    self.myTableview.reloadData()
                    
                }
                
            }
            else {
                
                self.customAlertDismiss(title: "Message", message: "Something went wrong!")
            }
        }
        
    }
    
    
    
    func potTransfer_APICall(amount:String){
        
        self.addPeculiPopup(view: self.view)
        let params:[String: Any] = ["to": from_Id,//to_Id,
                                    "toCatId": toCategioryId,
                                    "from": to_Id,//from_Id,
                                    "fromCatId": fromCategiory_Id,
                                    "amount" : amount //txtEnterAmount.text ?? ""
        ]
        
        print(params)
        
        DataService.sharedInstance.postApiCall(endpoint: constantApis.potTransfer_Api, params: params) { (response, error) in
            self.hidePeculiPopup(view: self.view)
            
            print(response)
            
            if response != nil {
                
                
                Toast.show(message: "Sucessfuly transfer", controller: self)
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SuccessLottieFile_VC") as! SuccessLottieFile_VC
                
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
            else {
                
                self.customAlertDismiss(title: "Message", message: "Something went wrong!")
            }
        }
        
    }

    
}
