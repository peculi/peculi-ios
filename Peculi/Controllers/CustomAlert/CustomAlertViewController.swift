//
//  CustomAlertViewController.swift
//  Peculi
//
//  Created by iApp on 23/03/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import UIKit
import SVProgressHUD

class CustomAlertViewController: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var textFieldSuperView: UIView!
    @IBOutlet weak var popUpChildView: UIView!
    @IBOutlet var popUpSuperView: UIView!
    @IBOutlet weak var nameTextField: UITextField!
    
    //MARK: - declare Variables
    
    var textFieldData = String()
    var categoryId = String()
    var okCompletionHandler: (() -> Void)?
    
    //MARK: - ViewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    //MARK: - view will appear
    
    override func viewWillAppear(_ animated: Bool) {
        setupView()
        animateView()
        nameTextField.text = textFieldData
    }

    
    /**** UI setup */
    func setupView(){
    
        textFieldSuperView.layer.masksToBounds = true
        textFieldSuperView.layer.borderColor = #colorLiteral(red: 1, green: 0.6588235294, blue: 0.231372549, alpha: 1)
        textFieldSuperView.layer.borderWidth = 1.0
              self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
              popUpSuperView.backgroundColor = UIColor.black.withAlphaComponent(0.4)

    }
    
    ///animation setup
    func animateView() {
        popUpChildView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
            self.popUpChildView.transform = .identity
        }, completion: {(finished: Bool) -> Void in
            
        })
        
    }
    
    //MARK: - button actions
    
    @IBAction func cancelBtnTapped(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func okBtnTapped(_ sender: Any) {
        if textFieldData != nameTextField.text {
            updateCatoryApiCall()
        }
        else {
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    ///API call
    func updateCatoryApiCall(){
        
        SVProgressHUD.show()
        let userId = UserDefaults.standard.value(forKey: UserDefaultConstants.UserID.rawValue) as! String
        
        DataService.sharedInstance.updateCategoryApiCall(userID: userId, categoryName: nameTextField.text ?? "", categoryId: self.categoryId) { (response, error) in
            
            print(response)
            
            if response != nil {
                
                SVProgressHUD.dismiss()
                if let code = response?["code"] as? Int{
                    if code == 200 {
                       self.dismiss(animated: true, completion: nil)

                        self.okCompletionHandler?()
                        
                    }
                    else{
                        AlertView.simpleViewAlert(title: "Error", response?["message"] as! String, self)
                    }
                }
                
            }
            else{
                SVProgressHUD.dismiss()
                AlertView.simpleViewAlert(title: "Error","Somthing went wrong", self)
            }
            
            
        }
        
    }
    
    
    
}
