//
//  TutorialVc.swift
//  Peculi
//
//  Created by Stealth on 29/10/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit

class TutorialVc: UIViewController ,UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    //MARK: - outlets
    
    @IBOutlet weak var collectionVw: UICollectionView!
    
    @IBOutlet weak var scrollVw: UIScrollView!
    
    @IBOutlet weak var pager: UIPageControl!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblDetail: UILabel!
    
    @IBOutlet weak var bgView: UIView!
    
    //MARK: - variables
    var tutorial  = tutorialModelClass()
    var countNextBtnTaps = 0
    
    //MARK: - did load method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUi()
        scrollTop()
        showValuesAt(0)
        
    }
    
    /****setup UI */
    func updateUi(){

        collectionVw.delegate = self
        collectionVw.dataSource = self
        self.navigationController?.navigationBar.isHidden = true

        
        bgView.clipsToBounds = true
        bgView.layer.cornerRadius = 25
        bgView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
        
    }
    
    
    func scrollTop(){
        
        if #available(iOS 11.0, *) {
            scrollVw.contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
        scrollVw.contentInset = UIEdgeInsets.zero;
        scrollVw.scrollIndicatorInsets = UIEdgeInsets.zero;
        scrollVw.contentOffset = CGPoint(x: 0.0, y: 0.0)
        
    }
    
    ///collectionView delegates
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        let count = tutorial.ImgArray.count
        pager.numberOfPages = count
        pager.isHidden = !(count > 1)
        
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionVw.dequeueReusableCell(withReuseIdentifier: "TutorialCvc",
                                                    for: indexPath as IndexPath) as! TutorialCvc
        
        
        cell.imgVw.image = tutorial.ImgArray[indexPath.row]
        
        
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.width, height: 350)
        
    }
    

   // UIScreen.main.bounds.width
    
    ///scrollView Delegates
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        pager?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        
        pager?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
   
        
        let visibleIndex = Int(targetContentOffset.pointee.x / collectionVw.frame.width)
        print(visibleIndex)
        countNextBtnTaps = visibleIndex
        showValuesAt(visibleIndex)
        
    }
    
    
    ///move to specific index

    func showValuesAt(_ Index:Int) {
        
        lblTitle.text = tutorial.Title[Index].uppercased()
        lblDetail.text = tutorial.Description[Index]
    }
    
    
    //MARK: - button actions
    
    /*** learn more action */
    @IBAction func btnLearnMore(_ sender: Any) {
        
        print(countNextBtnTaps)
        
        if countNextBtnTaps < 3 {//4
            
            countNextBtnTaps += 1
            pager.currentPage = countNextBtnTaps
            collectionVw.scrollToNextItem()
            showValuesAt(countNextBtnTaps)
        }
        else {

            guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeScreenVc") as? WelcomeScreenVc else {
                return
            }
            
            pushViewController(viewController: vc)
        }
        
    }
    
    /*** skip button action */
    @IBAction func btnSkip(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeScreenVc") as! WelcomeScreenVc
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
 
}

extension UICollectionView {
    func scrollToNextItem() {
        let contentOffset = CGFloat(floor(self.contentOffset.x + self.bounds.size.width))
        self.moveToFrame(contentOffset: contentOffset)
    }
    func moveToFrame(contentOffset : CGFloat) {
        self.setContentOffset(CGPoint(x: contentOffset, y: self.contentOffset.y), animated: true)
    }
}
