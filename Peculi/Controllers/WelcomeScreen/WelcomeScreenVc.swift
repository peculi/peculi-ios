//
//  WelcomeScreenVc.swift
//  Peculi
//
//  Created by Stealth on 28/10/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.




import UIKit
import SVProgressHUD

class WelcomeScreenVc: UIViewController {
    
    //MARK: - Outlets
    
    @IBOutlet weak var loginVw: UIView!
    @IBOutlet weak var signupVw: UIView!
    @IBOutlet weak var skipView: UIView!
    
    //MARK: - variables
    
    var fromScreen: AllControllers?
    var currencyName = "GBP"
    
    //MARK: - view did load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //if userID available
        if UserDefaults.exists(key: UserDefaultConstants.UserID.rawValue){
            
            let userID = UserDefaults.standard.string(forKey: UserDefaultConstants.UserID.rawValue)
            
            if userID != "" {
                
//                let userID = "0"
//                UserDefaults.standard.setValue(userID, forKey: UserDefaultConstants.UserID.rawValue)
//
                skipView.isHidden = true

            }
            else {
                
                skipView.isHidden = false
                
            }
        }
        else {
            
            skipView.isHidden = false
        }
        
    }
    
    //MARK: - view will appear
    
    override func viewWillAppear(_ animated: Bool) {
        updateUi()
    }
    
    /*** setup design */
    
    func updateUi(){
        
        self.navigationController?.navigationBar.isHidden = true
        navigationController?.navigationBar.barStyle = .black
        loginVw.layer.borderWidth = 1
        loginVw.layer.borderColor = UIColor.white.cgColor
        signupVw.layer.borderWidth = 1
        signupVw.layer.borderColor = UIColor.white.cgColor
        
    }
    
    //MARK: - button actions
    
    @IBAction func btnLoginAction(_ sender: Any) {
        
        let vc = SignInVc.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.fromVC = fromScreen
        self.pushViewController(viewController: vc)
        
    }
    

    @IBAction func btnSignUpAction(_ sender: Any) {
        
            let userID = UserDefaults.standard.string(forKey: UserDefaultConstants.UserID.rawValue)
            
            if userID == nil {
                
                let vc = SignUpVc.instantiateFromAppStoryboard(appStoryboard: .main)
                
                let userID = "0"
                UserDefaults.standard.setValue(userID, forKey: UserDefaultConstants.UserID.rawValue)

                vc.fromVC = fromScreen
                self.pushViewController(viewController: vc)
                
            }
            else {
                
                let vc = SignUpVc.instantiateFromAppStoryboard(appStoryboard: .main)
                vc.fromVC = fromScreen
                self.pushViewController(viewController: vc)
               
            }
     
        
//        let vc = SignUpVc.instantiateFromAppStoryboard(appStoryboard: .main)
//        vc.fromVC = fromScreen
//        self.pushViewController(viewController: vc)
        
    }
    
    @IBAction func skipBtnAction(_ sender: Any) {
        
        guestLoginApiCall()
       
    }
    
    
    func guestLoginApiCall() {
 
           SVProgressHUD.show()
   
           DataService.sharedInstance.guestLogin { (response, error) in
   
               SVProgressHUD.dismiss()
               if response != nil {
   
                   let code = response?["code"] as? Int
                  
                   let message = response?["message"] as? String //response?["message"] as? String
                   if code == 200 {
                      // let result = response?["result"] ?? "" //as? [String: Any]
                     //  let status = result?["status"] as? Int
                       let status = response?["status"] as? Int
                       let deviceToken = response?["token"] as? String
                       let deviceId = response?["device_id"] as? String
                       let userID = response?["user_id"] as? String
                       
                       UserDefaults.standard.set(userID, forKey:  UserDefaultConstants.UserID.rawValue)
                       
                       UserDefaults.standard.set(deviceToken, forKey:  UserDefaultConstants.GlobalDeviceToken.rawValue)
                       UserDefaults.standard.set(deviceId, forKey:  UserDefaultConstants.GlobalDeviceId.rawValue)
                       
                       UserDefaults.standard.set(true, forKey:  UserDefaultConstants.isguestLogin.rawValue)
                       UserDefaults.standard.set(status, forKey:  UserDefaultConstants.LoginStatus.rawValue)
                       
                       UserDefaults.standard.set("0", forKey: UserDefaultConstants.showPopupHomeScreen.rawValue)
                       UserDefaults.standard.set("0", forKey: UserDefaultConstants.showPopup_PeculiOfferScreen.rawValue)
                       UserDefaults.standard.set("0", forKey: UserDefaultConstants.showPopup_redeemScreen.rawValue)
                       
                       self.setRootHomeVC()
                       
                       
                   }  else {
                       
                       Toast.show(message: message ?? "", controller: self)
                       
                   }
   

                   }
                  
   
   
         
               
   
           }
   
       }
       
    
    
    
    
    
    
    
    
    
    
    
    //guest login API call
//    func guestLoginApiCall(){
//
//        SVProgressHUD.show()
//
//        DataService.sharedInstance.guestLogin { (response, error) in
//
//            SVProgressHUD.dismiss()
//            if response != nil {
//
//                let code = response?["code"] as? Int
//                let userID = response?["user_id"] as? String
//                let message = response?["status"] as? String //response?["message"] as? String
//                if code == 200 {
//                    let result = response?["result"] ?? "" //as? [String: Any]
//                  //  let status = result?["status"] as? Int
//                    let statusString = response?["status"] as? String
//                    let deviceToken = response?["device_token"] as? String
//                    let deviceId = response?["device_id"] as? String
//
//                    UserDefaults.standard.set(deviceToken, forKey:  UserDefaultConstants.GlobalDeviceToken.rawValue)
//                    UserDefaults.standard.set(deviceId, forKey:  UserDefaultConstants.GlobalDeviceId.rawValue)
//                    UserDefaults.standard.set(statusString, forKey:  UserDefaultConstants.LoginStatus.rawValue)
//
//                    //                    if statusString != "" {
////                        UserDefaults.standard.set(statusString, forKey:  UserDefaultConstants.LoginStatus.rawValue)
////                    }
////                    else {
////                        UserDefaults.standard.set(status, forKey:  UserDefaultConstants.LoginStatus.rawValue)
////                    }
//
//                    let currency = self.currencyName.getSymbol(forCurrencyCode: self.currencyName)
//                    GlobalVariables.currency = currency ?? ""
//                    UserDefaults.standard.set(currency, forKey: "currency")
//                    UserDefaults.standard.set(userID, forKey: UserDefaultConstants.UserID.rawValue)
//                    UserDefaults.standard.set(true, forKey: UserDefaultConstants.isguestLogin.rawValue)
//                    UserDefaults.standard.synchronize()
//                    self.setRootHomeVC()
//
//                }
//                else{
//                    self.customAlertDismiss(title: "Alert", message: message ?? "")
//                }
//
//
//            }
//            else{
//
//                self.customAlertDismiss(title: "Alert", message: "Something went wrong!")
//            }
//
//        }
//
//    }
    
    
    
}
