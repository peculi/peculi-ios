//
//  PotToBankTransfer_VC.swift
//  Peculi
//
//  Created by PPI on 5/19/22.
//  Copyright © 2022 Stealth Tech. All rights reserved.
//

import UIKit

class PotToBankTransfer_VC: UIViewController {
    
    
    @IBOutlet weak var imgPot: UIImageView!
    @IBOutlet weak var lblPotName: UILabel!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEnterAmount: UITextField!
    @IBOutlet weak var txtEnterAccount: UITextField!
    @IBOutlet weak var txtSortCode: UITextField!
    

    var to_Id = ""
    var toCategioryId = ""
    var amountSet : Double?
    var potnametext = ""
    var potimage = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigartionSetup()
        updateImgAndLblName()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.txtEnterAmount.text = String(amountSet ?? 0.00)
    }
    

    func navigartionSetup(){
        //icBackArrowWhite
        
        navigationController?.navigationBar.isHidden = false
        title = "Withdrawal"
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2203302979, green: 0.1600231826, blue: 0.3218392134, alpha: 1)
        self.navigationController?.navigationBar.isTranslucent = true
        
        //MARK: - Navigation LeftBar Button:-

                let leftBarButton = UIBarButtonItem(image: UIImage(named: "icBackArrowWhite"), style: .done, target: self, action: #selector(icBackButtonAction))
                self.navigationItem.leftBarButtonItem  = leftBarButton

    }
    
    
    @objc func icBackButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func updateImgAndLblName() {
        
        lblPotName.text = potnametext.capitalizingFirstLetter()
        
        var originalString = String()
        
        originalString = potimage
        
        let ImageUrl = String(format: "%@%@", AppConfig.ImageUrl,originalString)
        
        imgPot.sd_setImage(with: URL(string:ImageUrl), placeholderImage:UIImage(named: "placeholder_Img"), completed: { (image, error, cacheType, url) -> Void in
            if ((error) != nil) {
                
                // set the placeholder image here
                //cell.imgVw.image = #imageLiteral(resourceName: "doctorAvtar_Img")
            } else {
                // success ... use the image
            }
        })
       
    }
    

    @IBAction func btnContinue_Action(_ sender: Any) {
        
        let amount = Double(txtEnterAmount.text ?? "") ?? 0.00
        let price = amount.format()
        
        if  amount > amountSet ?? 0.00 {
            Toast.show(message: "Please enter amount less then or equal to \(amountSet ?? 0.00 )", controller: self)
        } else if txtFirstName.text == "" {
            
            Toast.show(message: "Please enter amount First Name", controller: self)
            
        } else if txtLastName.text == "" {
            Toast.show(message: "Please enter amount Last Name", controller: self)
            
        } else if txtEnterAmount.text == "" {
            
        } else if txtEnterAccount.text == "" {
            
        } else if txtSortCode.text == "" {
            
        } else if to_Id == "" &&  toCategioryId == "" {
            
            Toast.show(message: "Please Enter catId & fromId", controller: self)
            
        } else {
            
            self.bankTransfers_ApiCall(amount: price)
        }
 
    }
    

}

extension PotToBankTransfer_VC {

    func bankTransfers_ApiCall(amount:String){
        
        self.addPeculiPopup(view: self.view)
        
        let params:[String: Any] = [ "firstName": txtFirstName.text ?? "",
                                    "lastName": txtLastName.text ?? "",
                                    "paymentAmount": amount, //txtEnterAmount.text ?? "",
                                    "accountIdentifier": txtEnterAccount.text ?? "",
                                    "bankIdentifier" : txtSortCode.text ?? "",
                                    "catId" : toCategioryId,
                                    "fromId" : to_Id
        ]
        
        print(params)
        
        DataService.sharedInstance.postApiCall(endpoint: constantApis.bankTransfers_Api, params: params) { (response, error) in
            self.hidePeculiPopup(view: self.view)
            
            print(response)
            
            if response != nil {
                
                
                Toast.show(message: "Sucessfuly transfer", controller: self)
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SuccessLottieFile_VC") as! SuccessLottieFile_VC
                
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
            else {
                
                self.customAlertDismiss(title: "Message", message: "Something went wrong!")
            }
        }
        
    }
}


