//
//  PotAddPopUp_VC.swift
//  Peculi
//
//  Created by PPI on 4/7/22.
//  Copyright © 2022 Stealth Tech. All rights reserved.
//

import UIKit

class PotAddPopUp_VC: UIViewController {
    
    
    //MARK: - outlets
      
       @IBOutlet weak var titleLbl: UILabel!
       @IBOutlet weak var messgeLbl: UILabel!
       @IBOutlet var superView: UIView!
       
       //MARK: - declare variables
       
       var completionHandler : (()->Void)?
       var titleVal = ""
       var messageVal = ""
       
       //MARK: - view did load
       
       override func viewDidLoad() {
           super.viewDidLoad()
           
           self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
           superView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
           
           titleLbl.text = titleVal
           messgeLbl.text = messageVal

       }
       
       //MARK: - button action
       
       @IBAction func okBtnAction(_ sender: Any) {
           
           completionHandler?()
       }
       

   }
