//
//  NewEditProfile_VC.swift
//  Peculi
//
//  Created by PPI on 4/28/22.
//  Copyright © 2022 Stealth Tech. All rights reserved.
//

import UIKit
import DropDown

class NewEditProfile_VC: UIViewController {
    
    //MARK: - OUTLETS
    
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var txtMobileNumber: UITextField!
    @IBOutlet weak var txtDob: UITextField!
    @IBOutlet weak var txtGender: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtZipCode: UITextField!
    @IBOutlet weak var txtProfession: UITextField!
    @IBOutlet weak var txtOccuption: UITextField!
    @IBOutlet weak var txtMonthlyIncome: UITextField!
    @IBOutlet weak var txtCountryCode: UITextField!
    @IBOutlet weak var lblAccountNumbr: UILabel!
    
    
    //MARK: - VARIABLES
    
    var dropdwn = DropDown()
    var clickDropDown = "0"
    
    var allOccupationProfsionArr = [String]()
    var selectOccuptionArr = [String]()
    var selectMonthlyIncomeArr = [String]()
    var selectgenderArr = [String]()
    var dateTo = Date()
    
    var base64Image: String?
    var fileName: String?
    
    var profileUserData = [GetUserProfile_Model]()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.getUserProfile_APICall()
        
        self.setUpDropDown()
        self.navigartionSetup()
        
    }
    

    //MARK: - BUTTON ACTION
    
    
    @IBAction func btnDob_Action(_ sender: Any) {
        dobPicker()
    }
    

    @IBAction func btnProfileImg(_ sender: Any) {
        self.chooseImageMethod()
    }
    
    
    @IBAction func btnSelectGender_Action(_ sender: Any) {
        clickDropDown = "0"
        setupDropDownOpen()
        dropdwn.show()
    }
    
    @IBAction func btnProfession_Action(_ sender: Any) {
        clickDropDown = "1"
        setupDropDownOpen()
        dropdwn.show()
    }
    
    @IBAction func btnOccuption_Action(_ sender: Any) {
        clickDropDown = "2"
        setupDropDownOpen()
        dropdwn.show()
    }

    @IBAction func btnMonthlyIncome_Action(_ sender: Any) {
        clickDropDown = "3"
        setupDropDownOpen()
        dropdwn.show()
    }
    
    @IBAction func btnSave_Action(_ sender: Any) {

        
        if txtDob.text == "" {
            
            
            AlertView.simpleViewAlert(title: "", "Please enter date of birth!", self)
            return

        } else {
            
            let dob : String = txtDob.text ?? "" //"05-31-1996"
            
                    // Format Date
            
                    var myFormatte = DateFormatter()
            
                    myFormatte.dateFormat = "yyyy-MM-dd" //"MM-dd-yyyy"
            
                    // Convert DOB to new Date
            
                    var finalDate : Date = myFormatte.date(from: dob)!
            
                    // Todays Date
            
                    let now = Date()
            
                    // Calender
            
                    let calendar = Calendar.current
            
                    // Get age Components
            
            
                    let ageComponents = calendar.dateComponents([.year], from: finalDate, to: now)
            
                    print("Age is \(ageComponents.year!)") // Output 21
            
            
          if ageComponents.year! < 18 {
                
                AlertView.simpleViewAlert(title: "", "Peculi accounts are only available to over 18’s.", self)
                return

          } else {
              
              self.updateUserProfile_APICall()
              
          }
    }
    
    }
    //MARK: - FUNCTIONS
    
    func navigartionSetup(){

        //icBackArrowWhite
        
        navigationController?.navigationBar.isHidden = false
        title = "Edit Profile"
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2078431373, green: 0.16875422, blue: 0.3097219765, alpha: 1) //#colorLiteral(red: 0.2203302979, green: 0.1600231826, blue: 0.3218392134, alpha: 1)
        self.navigationController?.navigationBar.isTranslucent = true

        //MARK: - Navigation LeftBar Button:-

        let leftBarButton = UIBarButtonItem(image: UIImage(named: "icBackArrowWhite"), style: .done, target: self, action: #selector(icBackButtonAction))
        self.navigationItem.leftBarButtonItem  = leftBarButton
        
        
    }
    
    @objc func icBackButtonAction(){
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    func chooseImageMethod(){
        
        let alert = UIAlertController(title: "Choose image from", message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            self.SelectImageMethod(type:"camera")
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (action) in
            self.SelectImageMethod(type:"gallery")
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
        }))
        
        self.present(alert, animated: true)

    }
    
    
    func SelectImageMethod(type:String) {
        print("image get from \(type)")
        
        if type == "camera" {
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                let myPickerController = UIImagePickerController()
                myPickerController.delegate = self
                myPickerController.sourceType = .camera
                myPickerController.allowsEditing = true
                self.present(myPickerController, animated: true, completion: nil)
            }
        } else {
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                let myPickerController = UIImagePickerController()
                myPickerController.delegate = self
                myPickerController.sourceType = .photoLibrary
                myPickerController.allowsEditing = true
                self.present(myPickerController, animated: true, completion: nil)
            }
        }
    }

    
    func setUpDropDown() {
        
        self.clickDropDown = "0"
        self.selectgenderArr = ["Male","Female","Other"]

        self.allOccupationProfsionArr = ["Director / Owner","Executive","Manager","Employee / Worker","Self employed","Student","Retiree","Unemployed"]
        
        self.selectOccuptionArr = [ "Public servant / Police / Military","Agriculture","Craftwork / Trade","Arts / Culture / Sport","Banking / Insurance / Finance / Auditing","Construction / Publicworks","Education","Manufacturing / maintenance","Medical / Paramedical","Food industry / Work from home / Hospitali","Services / IT","Social Security / NGO","Politician / Elected Member of Parliament" ]
        
        self.selectMonthlyIncomeArr = ["LESS THAN 500 GBP","500 to 1000 GBP","1001 to 1500 GBP","1501 to 2000 GBP","2001 to 3000 GBP","MORE THAN 3000 GBP"]
       
        //self.ShowDropDown()
        
        DropDown.appearance().backgroundColor = #colorLiteral(red: 0.1846595407, green: 0.1497857869, blue: 0.2699982524, alpha: 1)//UIColor.gray
        DropDown.appearance().selectionBackgroundColor = #colorLiteral(red: 0.1846595407, green: 0.1497857869, blue: 0.2699982524, alpha: 1) //UIColor.black
        DropDown.appearance().textColor = UIColor.white
        DropDown.appearance().selectedTextColor = UIColor.white
        DropDown.appearance().cornerRadius = 4
        dropdwn.direction = .bottom
      
        dropdwn.shadowColor = UIColor.clear
        
    }
    
    func setupDropDownOpen() {
        
        if clickDropDown == "0" {
            dropdwn.anchorView = txtGender
            dropdwn.bottomOffset = CGPoint(x: 0, y: txtGender.bounds.height + 10 )//
            dropdwn.dataSource = selectgenderArr
            dropdwn.selectionAction = { [weak self] (index, item) in
                self?.txtGender.text = item
            }
            
        } else if clickDropDown == "1" {
            dropdwn.anchorView = txtProfession
            dropdwn.bottomOffset = CGPoint(x: 0, y: txtProfession.bounds.height + 10)
            dropdwn.dataSource = allOccupationProfsionArr
            dropdwn.selectionAction = { [weak self] (index, item) in
                self?.txtProfession.text = item
                
            }
            
        } else if clickDropDown == "2" {
            dropdwn.anchorView = txtOccuption
            dropdwn.bottomOffset = CGPoint(x: 0, y: txtOccuption.bounds.height + 10)
            dropdwn.dataSource = selectOccuptionArr
            dropdwn.selectionAction = { [weak self] (index, item) in
                
                self?.txtOccuption.text = item
            }
        }
        else  {

            dropdwn.anchorView = txtMonthlyIncome
            dropdwn.bottomOffset = CGPoint(x: 0, y: txtMonthlyIncome.bounds.height + 10)
            dropdwn.dataSource = selectMonthlyIncomeArr
            dropdwn.selectionAction = { [weak self] (index, item) in
                self?.txtMonthlyIncome.text = item
            }
        }
    }
    
    
    func dobPicker() {
        
        let alert = UIAlertController(style: .alert, title: "")
        let date = Calendar.current.date(byAdding: .year, value: 0, to: Date())
        dateTo = date ?? Date()
        alert.addDatePicker(mode: .date, date: Date(), minimumDate: nil, maximumDate: Date()) { date in
            self.dateTo = date
        }
        alert.addAction( title: "OK", style: .default, isEnabled: true) { (action) in
            let formatter1 = DateFormatter()
            formatter1.dateFormat = "yyyy-MM-dd" //"dd-MM-yyyy"
            formatter1.locale = NSLocale(localeIdentifier: "en_US") as Locale?
            self.txtDob.text = formatter1.string(from: self.dateTo)
        }
        alert.addAction(title: "Cancel", style: .cancel){ (action) in
            
        }
        alert.show()
    }
    
}


extension NewEditProfile_VC: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
        internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    
            // To handle image
 
            if let img = info[.originalImage] as? UIImage {
             
                profileImg.image = img
                
               // base64Image = convertImageToBase64(image: img)
                
                base64Image = convertImageToBase64String(img:profileImg.image!)
                
                print("base64Image ==>>>\(base64Image)")
                
               // DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
//                DispatchQueue.main.async {
//                    self.updateImgProfile_APICall(base64Image:self.base64Image!)
//                }
//
              //  }

                if let imgUrl = info[.originalImage] as? URL {
                    
                    let strImage = imgUrl.absoluteString
                  
                    
                }

            } else {
                
                print("Something went wrong in  image")
            }
            // To handle video
            if let videoUrl = info[.mediaURL] as? NSURL{
                print("videourl: ", videoUrl)
                //trying compression of video
                let data = NSData(contentsOf: videoUrl as URL)!
                print("File size before compression: \(Double(data.length / 1048576)) mb")
                // self.videoPickedBlock?(videoUrlFromPhone, size)
            }
            else{
                print("Something went wrong in  video")
            }
            self.dismiss(animated: true, completion: nil)
        }
    
    
//    func convertImageToBase64String(img: UIImage) -> String {
//            let imageData = img.pngData()!
//            return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
//        }
    
    func convertImageToBase64String (img: UIImage) -> String {
        let imageData:NSData = img.jpegData(compressionQuality: 0.50)! as NSData //UIImagePNGRepresentation(img)
        let imgString = imageData.base64EncodedString(options: .init(rawValue: 0))
        return imgString
    }

}

extension NewEditProfile_VC {
    
    
    func getUserProfile_APICall(){
        
        self.addPeculiPopup(view: self.view)
        
        let params:[String: Any] = ["premiumCheck" : "true" ]
        
        print(params)
        DataService.sharedInstance.postApiCall(endpoint: constantApis.getUserProfile_Api, params: params) { (response, error) in
            self.hidePeculiPopup(view: self.view)

            print(response)
            
           
            
            if response != nil {
                
                self.profileUserData.removeAll()
                
                if let result = response?["result"] as? [String: Any] {
                    
                    let username = result["username"] as? String ?? ""
                    let email =  result["email"] as? String ?? ""
                    let gender = result["gender"] as? String ?? ""
                    let dob =  result["dob"] as? String ?? ""
                    let phone =  result["phone"] as? String ?? ""
                    let image = result["image"] as? String ?? ""
                    let city =  result["city"] as? String ?? ""
                    let countrycode = result["countrycode"] as? String ?? ""
                    let zipcode =  result["zipcode"] as? String ?? ""
                    let address = result["address"] as? String ?? ""
                    let income =  result["income"] as? String ?? "Select Monthly Income"
                    let occupation = result["occupation"] as? String ?? "Select Occupation"
                    let profession = result["profession"] as? String ?? "Select Occupation Profession"
                    let accountNumber = result["cardHolderId"] as? String ?? ""
                  
                    let substring1 = accountNumber.dropFirst(4)
   
                    print("accountNumber==>>\(substring1)")
                    
                    self.lblAccountNumbr.text = ("Account Number : ") + "\(substring1)"
                    
                    //print(accountNumber.dropFirst(4))

                    self.profileUserData.append(GetUserProfile_Model.init(username: username, email: email, gender: gender, dob: dob, phone: phone, image: image, city: city, countrycode: countrycode, zipcode: zipcode, address: address, income: income, occupation: occupation, profession: profession))
                    
                }

                self.txtMobileNumber.text = self.profileUserData[0].phone
                self.txtDob.text = self.profileUserData[0].dob
                self.txtGender.text = self.profileUserData[0].gender
                self.txtAddress.text = self.profileUserData[0].address
                self.txtCity.text = self.profileUserData[0].city
                self.txtZipCode.text = self.profileUserData[0].zipcode
                self.txtProfession.text = self.profileUserData[0].profession
                self.txtOccuption.text = self.profileUserData[0].occupation
                self.txtMonthlyIncome.text = self.profileUserData[0].income
                self.lblEmail.text = self.profileUserData[0].email
                self.lblUserName.text = self.profileUserData[0].username
                self.txtCountryCode.text = self.profileUserData[0].countrycode


                var originalString = String()
                originalString = self.profileUserData[0].image
                let ImageUrl = String(format: "%@%@", AppConfig.userProfileImageUrl,originalString)

                self.profileImg.sd_setImage(with: URL(string:ImageUrl), placeholderImage:UIImage(named: "placeholder_Img"), completed: { (image, error, cacheType, url) -> Void in
                    if ((error) != nil) {
                        
                        self.profileImg.image = UIImage(named: "placeholder_Img") //imageLiteral(resourceName: "placeholder_Img")
                        // set the placeholder image here
                       // cell.imgVw.image = imageLiteral(resourceName: "doctorAvtar_Img")
                    } else {
                        // success ... use the image
                    }
                })
                
            }
            else {
                
                self.customAlertDismiss(title: "Message", message: "Something went wrong!")
            }
        }
        
    }
    
    func updateUserProfile_APICall(){
        
        self.addPeculiPopup(view: self.view)
        
        let params:[String: Any] = ["address": txtAddress.text ?? "",
                                    "city" : txtCity.text ?? "",
                                    "dob": txtDob.text ?? "",
                                    "zipcode" : txtZipCode.text ?? "",
                                    "email": lblEmail.text ?? "",
                                    "gender" : txtGender.text ?? "",
                                    "phone": txtMobileNumber.text ?? "",
                                    "profession" : txtProfession.text ?? "",
                                    "occupation": txtOccuption.text ?? "",
                                    "income" : txtMonthlyIncome.text ?? "",]

        print(params)
        
        DataService.sharedInstance.postApiCall(endpoint: constantApis.updateProfileDetails_Api, params: params) { (response, error) in
            self.hidePeculiPopup(view: self.view)

            print(response)
            
            if response != nil {
                
               // let message = response?[""]
                //self.getUserProfile_APICall()
                DispatchQueue.main.async {
                    self.updateImgProfile_APICall(base64Image: self.base64Image ?? "")
                }
                
            } else {
                                
                self.customAlertDismiss(title: "Message", message: "Something went wrong!")
            }
        }
        
    }

    
    
    func updateImgProfile_APICall(base64Image:String){

        self.addPeculiPopup(view: self.view)

        let params:[String: Any] = ["profileImage": base64Image
                                    ]

        print(params)

        DataService.sharedInstance.postApiCall(endpoint: constantApis.uploadProfileImage_Api, params: params) { (response, error) in
            self.hidePeculiPopup(view: self.view)

            print(response)

            if response != nil {

                self.customAlertDismiss(title: "Message", message: "Successfully Upload Img!")
                
                DispatchQueue.main.async {
                    self.getUserProfile_APICall()
                }
               

                // let message = response?[""]
               // self.getUserProfile_APICall()

            } else {
                
                self.customAlertDismiss(title: "Message", message: "Image Do Not Upload Successfully!")
                
                DispatchQueue.main.async {
                    self.getUserProfile_APICall()
                }

                
            }
        }

    }
    
//    func post_request_image(api:String){
//
//        if (profileImg.image == nil)
//        {
//            return
//        }
//        let image_data = profileImg.image!.jpegData(compressionQuality: 1.0)
//
//        if(image_data == nil)
//        {
//            return
//        }
//
//        self.addPeculiPopup(view: self.view) // loader.showLoadingAlert(view: self.view, title: "")
//        var web_apis_3 = constantApis.uploadProfileImage_Api //api
//        // print(web_apis_3)
//        var request = URLRequest(url: URL(string: web_apis_3)!)
//         request.httpMethod = "POST"
//         do {
//
//             let imgBase64 = image_data?.base64EncodedString()
//            request.httpBody = imgBase64
//
//
//        } catch let error {
//            print(error.localizedDescription)
//        }
//        // let content = String(data: json!, encoding:
//            // (String.Encoding.utf8)
//
//        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//
//        let task = URLSession.shared.dataTask(with: request) { data, response, error in
//            guard let data = data, error == nil else {
//                print("error=\(error)")
//                return
//            }
//
//            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
//                print("statusCode should be 200, but is \(httpStatus.statusCode)")
//                print("response = \(response)")
//
//            }
//
//
//}
//    }

}


