//
//  NewProfile_VC.swift
//  Peculi
//
//  Created by PPI on 6/7/22.
//  Copyright © 2022 Stealth Tech. All rights reserved.
//

import UIKit
import DropDown

class NewProfile_VC: UIViewController {

    
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var lblAccountNumbr: UILabel!
    @IBOutlet weak var btnDropDown: UIButton!
    
    @IBOutlet weak var dltView: UIView!
    
    var profileUserData = [GetUserProfile_Model]()
    var dropdwnTarget = DropDown()
    var setDropDownArr = [String]()
    
    var globalBalace = 0.00
    var valueHideButton = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigartionSetup()
        setUpDropDown()
        
        dltView.layer.cornerRadius = 8
        dltView.layer.borderWidth = 0.8
        dltView.layer.borderColor = UIColor.white.cgColor
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        getUserProfile_APICall()
        
        
        
        let globalBalace1 = UserDefaults.standard.value(forKey: UserDefaultConstants.GlobalBalance.rawValue) as? String
        globalBalace = Double(globalBalace1 ?? "") ?? 0.00
        print("globalBalace check---->>>>>>>   \(globalBalace)")
        
        
    }
    
    
    //MARK: - Button action
    
    
    @IBAction func btnDltMyAccount(_ sender: Any) {
        
        if globalBalace == 0.00 {
            
            valueHideButton = "0"
            
            
            popupSubscriptionAlert()
            
        } else {
            
            valueHideButton = "1"
            
            popupSubscriptionAlert()

        }
        
        
    }
    
    
    
    
    func popupSubscriptionAlert() {
            
            
            let sb = UIStoryboard(name: "Main", bundle:nil)
            let addCardAlert = sb.instantiateViewController(withIdentifier: "DltMyAccountPopUp_VC") as! DltMyAccountPopUp_VC
            //     customAlert.textFieldData = navName.capitalizingFirstLetter()
            
            //addCardAlert.categoryId = CategoryInfo[0].userData.categoryDetails._id
         addCardAlert.hideForButton = valueHideButton
            
            addCardAlert.okCompletionHandler = {
                print("call Add Card PopUp")
                
                self.deleteAccount_Api()
       
            }
            
            addCardAlert.providesPresentationContextTransitionStyle = true
            addCardAlert.definesPresentationContext = true
            addCardAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            addCardAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            
            self.present(addCardAlert, animated: true, completion: nil)
        }

    
    
    
    
    
    
    //MARK: - FUNCTIONS
    
    func navigartionSetup(){

        //icBackArrowWhite
        
        navigationController?.navigationBar.isHidden = false
        title = "Profile Details"
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2078431373, green: 0.16875422, blue: 0.3097219765, alpha: 1) //#colorLiteral(red: 0.2203302979, green: 0.1600231826, blue: 0.3218392134, alpha: 1)
        self.navigationController?.navigationBar.isTranslucent = true

        //MARK: - Navigation LeftBar Button:-

        let leftBarButton = UIBarButtonItem(image: UIImage(named: "icBackArrowWhite"), style: .done, target: self, action: #selector(icBackButtonAction))
        self.navigationItem.leftBarButtonItem  = leftBarButton
        
        
        let rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "icDotMenuWhite"), style: .done, target: self, action: #selector(icRightButtonAction))
        self.navigationItem.rightBarButtonItem  = rightBarButtonItem
        
        
    }
    
    @objc func icBackButtonAction(){
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @objc func icRightButtonAction(){
        
        setupTargetDropDown()
        dropdwnTarget.show()
        
    }

    
    func setUpDropDown() {

        setDropDownArr = ["Edit Profile"]

        
       // DropDown.appearance().backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)  //UIColor.gray
       // DropDown.appearance().selectionBackgroundColor = UIColor.black
       // DropDown.appearance().textColor = UIColor.white
       // DropDown.appearance().selectedTextColor = UIColor.white
        DropDown.appearance().cornerRadius = 4
        dropdwnTarget.direction = .bottom
        self.dropdwnTarget.width = 150
       // dropdwnTarget.shadowColor = UIColor.clear
        dropdwnTarget.backgroundColor = UIColor.white
        dropdwnTarget.textColor = UIColor.black
        
        
    }
    
    
    func setupTargetDropDown() {
        
 
        
     
            dropdwnTarget.anchorView = btnDropDown
        dropdwnTarget.bottomOffset = CGPoint(x: -120 , y: btnDropDown.bounds.height)//
            dropdwnTarget.dataSource = setDropDownArr
            dropdwnTarget.selectionAction = { [weak self] (index, item) in
                
               
                
                if item == "Edit Profile" {
          
                    let vc = NewEditProfile_VC.instantiateFromAppStoryboard(appStoryboard: .main)
                    self?.pushViewController(viewController: vc)
               
                }
            }

        }
    
    
    
    func getUserProfile_APICall(){
        
        self.addPeculiPopup(view: self.view)
        
        let params:[String: Any] = ["premiumCheck" : "true" ]
        
        print(params)
        DataService.sharedInstance.postApiCall(endpoint: constantApis.getUserProfile_Api, params: params) { (response, error) in
            self.hidePeculiPopup(view: self.view)

            print(response)
            
            if response != nil {
                
                self.profileUserData.removeAll()
                
                if let result = response?["result"] as? [String: Any] {
                    
                    let username = result["username"] as? String ?? ""
                    let email =  result["email"] as? String ?? ""
                    let gender = result["gender"] as? String ?? ""
                    let dob =  result["dob"] as? String ?? ""
                    let phone =  result["phone"] as? String ?? ""
                    let image = result["image"] as? String ?? ""
                    let city =  result["city"] as? String ?? ""
                    let countrycode = result["countrycode"] as? String ?? ""
                    let zipcode =  result["zipcode"] as? String ?? ""
                    let address = result["address"] as? String ?? ""
                    let income =  result["income"] as? String ?? "Select Monthly Income"
                    let occupation = result["occupation"] as? String ?? "Select Occupation"
                    let profession = result["profession"] as? String ?? "Select Occupation Profession"
                    let accountNumber = result["cardHolderId"] as? String ?? ""
                  
                    let substring1 = accountNumber.dropFirst(4)
   
                    print("accountNumber==>>\(substring1)")
                    
                    self.lblAccountNumbr.text = ("Account Number : ") + "\(substring1)"
                    
                    //print(accountNumber.dropFirst(4))

                    self.profileUserData.append(GetUserProfile_Model.init(username: username, email: email, gender: gender, dob: dob, phone: phone, image: image, city: city, countrycode: countrycode, zipcode: zipcode, address: address, income: income, occupation: occupation, profession: profession))
                }
               
                self.lblEmail.text = self.profileUserData[0].email
                self.lblUserName.text = self.profileUserData[0].username

                var originalString = String()
                originalString = self.profileUserData[0].image
                let ImageUrl = String(format: "%@%@", AppConfig.userProfileImageUrl,originalString)

                self.profileImg.sd_setImage(with: URL(string:ImageUrl), placeholderImage:UIImage(named: "placeholder_Img"), completed: { (image, error, cacheType, url) -> Void in
                    if ((error) != nil) {
                        
                        self.profileImg.image = UIImage(named: "placeholder_Img") //imageLiteral(resourceName: "placeholder_Img")
                        // set the placeholder image here
                       // cell.imgVw.image = imageLiteral(resourceName: "doctorAvtar_Img")
                    } else {
                        // success ... use the image
                    }
                })
            }
            else {
                
                self.customAlertDismiss(title: "Message", message: "Something went wrong!")
            }
        }
        
    }
    
    
    
    
    
    func deleteAccount_Api() {
        
        self.addPeculiPopup(view: self.view)
        
        let params:[String: Any] = [ :  ]
        
        print(params)
        DataService.sharedInstance.postApiCall(endpoint: constantApis.deleteAccount_Api, params: params) { (response, error) in
            self.hidePeculiPopup(view: self.view)

            print(response)
            
            if response != nil {
                
                self.clearUserDefault()
                
                SignUpVc.setRootVC()
            }
            else {
                
                self.customAlertDismiss(title: "Message", message: "Something went wrong!")
            }
        }
        
    }
    
    
    

   
}
