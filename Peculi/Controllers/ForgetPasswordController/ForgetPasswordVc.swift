//
//  ForgetPasswordVc.swift
//  Peculi
//
//  Created by Stealth on 29/10/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit
import SVProgressHUD

class ForgetPasswordVc: UIViewController, UITextFieldDelegate {
    
    //MARK: - outlets
    @IBOutlet weak var txtEmail: UITextField!
    
    //MARK: - did load method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    //MARK: - view will appear
    override func viewWillAppear(_ animated: Bool) {
        updateUi()
    }
    
    /**** Updating values */
    
    func updateUi() {
        
        self.navigationController?.navigationBar.isHidden = true
        txtEmail.delegate = self
        txtEmail.attributedPlaceholder = NSAttributedString(string: "Enter Email",
                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
    }
    
    //MARK: - button actions
    
    /*** back button action */
    @IBAction func btnBackAction(_ sender: Any) {
        popVc()
    }
    
    
    /*** request OTP action */
    @IBAction func btnRequestOtp(_ sender: Any) {
        
        if txtEmail.text?.count == 0 {
            AlertView.simpleViewAlert(title: "", "Please enter email", self)
            return
        }
        else if !(txtEmail.text?.EmailValidation())!{
            
            AlertView.simpleViewAlert(title: "", "Please enter correct email", self)
            
            return
            
        }
        else {
            
            callForgetApi(txtEmail.text ?? "")
        }
        
    }
    
    
    
    
    //Forget password API Call
    
//    func callForgetApi(_ email : String){
//
//        ForgetPasswordModel().fetchInfo(email){ (userinfo) in
//
//            print(userinfo)
//            if userinfo.count > 0 {
//                if userinfo[0].statuscheck == "Success"{
//
//                    self.moveToOTPVc()
//
//                }else{
//                    self.customAlertDismiss(title: "Message", message: userinfo[0].message)
//
//                }
//
//            }
//            else{
//
//                self.customAlertDismiss(title: "", message: "Something went wrong!")
//            }
//
//        }
//    }
    
    
    
    func callForgetApi(_ email : String){
        
        SVProgressHUD.show()
        let params:[String: Any] = ["email": email
                                    ]
        DataService.sharedInstance.postApiCall(endpoint: constantApis.ForgetApi, params: params) { (response, error) in
            
            SVProgressHUD.dismiss()
            
            
            print(response)
            
            if response != nil {
                
                let user_Id = response?["user_id"] as? String ?? ""
                
                UserDefaults.standard.setValue(user_Id, forKey: UserDefaultConstants.UserID.rawValue)
                
               // UserDefaults.standard.string(forKey: UserDefaultConstants.UserID.rawValue) ?? ""

                self.moveToOTPVc()
                
            }
            else {
                self.customAlertDismiss(title: "Message", message: "Something went wrong!")
            }
            
        }
        
        
    }
    
    
    
    
    
    
    
    
    func moveToOTPVc()
    {
        let vc = OtpScreenVc.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.email = txtEmail.text ?? ""
        
        self.pushViewController(viewController: vc)
    }
    
    
    
    
}
