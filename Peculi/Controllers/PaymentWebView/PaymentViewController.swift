//
//  PaymentViewController.swift
//  Peculi
//
//  Created by iApp on 21/07/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.


import UIKit
import WebKit
import SVProgressHUD

class PaymentViewController: UIViewController, WKNavigationDelegate {

    //MARK: - outlets
    
    @IBOutlet weak var paymentWebView: WKWebView!
    @IBOutlet var superView: UIView!
    
    //MARK: - variables
    var urlData = ""
    var paymentType = ""
    var PaymentCardId = ""
    var transsctionsAmount = ""
    var catIdForUpdateTransctonStatus = ""
    
    
    //MARK: - view did load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigartionSetup()
        paymentWebView.navigationDelegate = self
        paymentWebView.scrollView.bounces = false
        paymentWebView.load(NSURLRequest(url: URL(string: urlData)!) as URLRequest)
        print(urlData)
     
    }
    
    //MARK: - Navigation Setup
    
    func navigartionSetup(){
        
        navigationController?.navigationBar.isHidden = false
        title = "Add Funds"
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.07450980392, green: 0.1019607843, blue: 0.1529411765, alpha: 1)
        self.navigationController?.navigationBar.isTranslucent = false
        
        //MARK: - Navigation LeftBar Button:-
        
        let leftBarButton = UIBarButtonItem(image: UIImage(named: "icBackArrowWhite"), style: .done, target: self, action: #selector(icBackButtonAction))
        
        self.navigationItem.leftBarButtonItem = leftBarButton
        
        
    }
    
    //MARK: - Object Function Button Action
    
    @objc func icBackButtonAction(){
        popVc()
        
    }
    
    
    func webViewDidFinishLoad(_ webView: WKWebView) {
        print(webView.url ?? "" )
        
        
//        if let request = webView.canGoBack {
//            let response = URLCache.shared.cachedResponse(for: request)
//            // ...
//        }
        
    }

    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print(webView.url ?? "")
        
        if webView.url?.absoluteString == "http://www.test.com/Successful" {
            
            SVProgressHUD.dismiss()
            updateTransactionStatus()

        } else if webView.url?.absoluteString == "http://www.test.com/NotSuccessful" {
            
            SVProgressHUD.dismiss()
            Toast.show(message: "url not succesfully", controller: self)
            AddMoneyViewController.setRootVC()
            //AddMoneyViewController.setRootVC()
            
        }
        
        SVProgressHUD.show()
        
    }
    
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print(webView.url ?? "")
        SVProgressHUD.dismiss()
        
    }
    
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print(webView.url ?? "")
        SVProgressHUD.dismiss()
    }
    


        func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
            print(error.localizedDescription)
        }
  
    
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        
        if let url = webView.url?.absoluteString {
            print("url = \(url)")
        }
        
    }
    
    //transaction_id,
//    type,
//    merchentToken,
//    amount
//    cardId
//    user_id
    
    
    
    func updateTransactionStatus() {
        
        SVProgressHUD.show()
        
        let params:[String: Any] = [ "user_id": UserDefaults.standard.string(forKey: UserDefaultConstants.UserID.rawValue) ?? "",
                                    "transaction_id": UserDefaults.standard.string(forKey: UserDefaultConstants.Transaction_Id.rawValue) ?? "", //UserDefaults.standard.string(forKey: "SAVETRANSACTIONID") ?? "",
                                    "type" : paymentType,
                                    "merchentToken" : UserDefaults.standard.string(forKey: UserDefaultConstants.MerchentToken.rawValue) ?? "",
                                    "amount" : transsctionsAmount,
                                    "cardId" : PaymentCardId,
                                    "catId" : catIdForUpdateTransctonStatus
                                    ]
        
        print(params)
        
        DataService.sharedInstance.postApiCall(endpoint: constantApis.updateTransactionStatus, params: params) { (response, error) in
            
            SVProgressHUD.dismiss()
            
            print(response)
            
            if response != nil {
                
                if GlobalVariables.moveAfterTransactions == "0" {

                   // RedeemNowVC.setRootVC()
                    
                    for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: RedeemNowVC.self) {
                            self.navigationController!.popToViewController(controller, animated: true)
                            break
                        }
                    }
                    
                    
                } else if GlobalVariables.moveAfterTransactions == "1" {
                    
                   // PeculiOfferViewController.setRootVC()
                    
                    for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: PeculiOfferViewController.self) {
                            self.navigationController!.popToViewController(controller, animated: true)
                            break
                        }
                    }
                    
                    
                    
                    
                    
                } else {

                    //HomeViewController.setRootVC()
                    
                    
                    for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: HomeViewController.self) {
                            self.navigationController!.popToViewController(controller, animated: true)
                            break
                        }
                    }
                    
                    
                }
               // HomeViewController.setRootVC()
            }
            else {
                
                self.customAlertDismiss(title: "Message", message: "Something went wrong!")
            }
            
        }
        
        
    }


}
