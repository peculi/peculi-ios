//
//  EmailVerificationViewController.swift
//  Peculi
//
//  Created by iApp on 15/07/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import UIKit
import SVProgressHUD
import Lottie
class EmailVerificationViewController: UIViewController, UITextFieldDelegate {
    
    //MARK: - outlets

    @IBOutlet weak var firstTextField: UITextField!
    @IBOutlet weak var firstLbl: UILabel!
    @IBOutlet weak var secondTextField: UITextField!
    @IBOutlet weak var secondLbl: UILabel!
    @IBOutlet weak var thirdTextField: UITextField!
    @IBOutlet weak var thirdLbl: UILabel!
    @IBOutlet weak var fourthTextField: UITextField!
    @IBOutlet weak var fourthLbl: UILabel!
    @IBOutlet weak var fifthTextField: UITextField!
    @IBOutlet weak var fifthLbl: UILabel!
    @IBOutlet weak var sixthTextField: UITextField!
    @IBOutlet weak var sixthLbl: UILabel!
    
    //MARK: - variables
    
    var otp = String() // Int()
    var idOtp = String()
    var email = String()
    var response = Dictionary<String,AnyObject>()
    
    //MARK: - view did load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
    
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
        updateUi()
//        fillOTP()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
                firstTextField.becomeFirstResponder()
    }
    
    
//    func fillOTP(){
//        print(otp)
//        if otp.count == 6{
//            firstTextField.text = String(otp[otp.index(otp.startIndex, offsetBy: 0)])
//            secondTextField.text = String(otp[otp.index(otp.startIndex, offsetBy: 1)])
//            thirdTextField.text = String(otp[otp.index(otp.startIndex, offsetBy: 2)])
//            fourthTextField.text = String(otp[otp.index(otp.startIndex, offsetBy: 3)])
//            fifthTextField.text = String(otp[otp.index(otp.startIndex, offsetBy: 4)])
//            sixthTextField.text = String(otp[otp.index(otp.startIndex, offsetBy: 5)])
//            firstLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
//            secondLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
//            thirdLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
//            fourthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
//            fifthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
//            sixthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//            sixthTextField.becomeFirstResponder()
//        }
//
//    }
    
    @IBAction func textFieldEditChanged(_ sender: UITextField) {
        let count = sender.text?.count

        if count == 1 {
            switch sender {
            case firstTextField:
                firstLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                secondLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                thirdLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fourthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fifthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                sixthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                secondTextField.becomeFirstResponder()
               
            case secondTextField:
                firstLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                secondLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                thirdLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                fourthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fifthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                sixthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                thirdTextField.becomeFirstResponder()
                
            case thirdTextField:
                firstLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                secondLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                thirdLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fourthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                fifthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                sixthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fourthTextField.becomeFirstResponder()
                
            case fourthTextField:
                firstLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                secondLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                thirdLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fourthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fifthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                sixthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fifthTextField.becomeFirstResponder()
            case fifthTextField:
                firstLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                secondLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                thirdLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fourthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fifthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                sixthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                sixthTextField.becomeFirstResponder()
            case sixthTextField:
                firstLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                secondLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                thirdLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fourthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fifthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                sixthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                sixthTextField.becomeFirstResponder()
                
            default:
                print("default")
            }
        }
        
    }
    
    
    
    @IBAction func textFieldDidBeginEditing(_ sender: UITextField) {
        sender.text = " "
    }
    
    
    
    /*** setup UI */
    func updateUi(){
        
        firstTextField.delegate = self
        secondTextField.delegate = self
        thirdTextField.delegate = self
        fourthTextField.delegate = self
        fifthTextField.delegate = self
        sixthTextField.delegate = self
        firstLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        secondLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
        thirdLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
        fourthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
        fifthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
        sixthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
        
    }
    
    func clearTextFields(){
        
        firstTextField.text = ""
        secondTextField.text = ""
        thirdTextField.text = ""
        fourthTextField.text = ""
        fifthTextField.text = ""
        sixthTextField.text = ""
        
        
        firstLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        secondLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
        thirdLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
        fourthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
        fifthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
        sixthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
        firstTextField.becomeFirstResponder()
        
    }
    
    //textfield delegate methods

    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange, replacementString string: String) -> Bool {
        textField.text = ""
      
        if string == "" {
            
            if textField == sixthTextField {
                firstLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                secondLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                thirdLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fourthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fifthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                sixthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fifthTextField?.becomeFirstResponder()
            }
            else if textField == fifthTextField {
                firstLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                secondLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                thirdLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fourthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                fifthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                sixthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fourthTextField?.becomeFirstResponder()
            }
            else if textField == fourthTextField {
                firstLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                secondLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                thirdLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                fourthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fifthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                sixthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                thirdTextField?.becomeFirstResponder()
            }
            else if textField == thirdTextField {
                firstLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                secondLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                thirdLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fourthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fifthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                sixthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                secondTextField?.becomeFirstResponder()
            }
            
            else if textField == secondTextField {
                firstLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                secondLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                thirdLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fourthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fifthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                sixthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                firstTextField?.becomeFirstResponder()
            }
            else if textField == firstTextField {
                firstLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                secondLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                thirdLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fourthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fifthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                sixthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                firstTextField?.becomeFirstResponder()
            }
            textField.text = ""
            return false
        }

        return true
    }
    
    //MARK: - UIButton Action
    
    /***Back action */
    
    @IBAction func backBtnAction(_ sender: Any) {
        
        popVc()
        
    }
    
    
    @IBAction func resendCodeAction(_ sender: Any) {
        
        self.clearTextFields()
        
        resendOTPApiCall()

    }
    
    @IBAction func changeEmailAddressAction(_ sender: Any) {
        
        SignUpVc.setRootVC()

       // popVc()
        
    }
    
    
    @IBAction func submitBtnAction(_ sender: Any) {
        
        
        otp = String(format: "%@%@%@%@%@%@", firstTextField.text ?? "", secondTextField.text ?? "", thirdTextField.text ?? "", fourthTextField.text ?? "", fifthTextField.text ?? "", sixthTextField.text ?? "")
        
        otp = otp.trimmingCharacters(in: .whitespaces)
        
        if otp.count == 6 {
            
            self.clearTextFields()
           
            verifyOTPApi(otp: otp)
            
        } else {
            
            AlertView.simpleViewAlert(title: "", "Please enter the OTP", self)
            
        }
        
                
//        if firstTextField.text?.count == 0 || secondTextField.text?.count == 0 || thirdTextField.text?.count == 0 || fourthTextField.text?.count == 0 || fifthTextField.text?.count == 0 || sixthTextField.text?.count == 0{
//
//            AlertView.simpleViewAlert(title: "Error", "Please enter OTP!", self)
//
//        }
//        else {
//
//
//          //  otp = String(format: "%@%@%@%@%@%@", firstTextField.text ?? "", secondTextField.text ?? "", thirdTextField.text ?? "", fourthTextField.text ?? "", fifthTextField.text ?? "", sixthTextField.text ?? "")
//            self.clearTextFields()
//            verifyOTPApi()
//
//        }
        
        
    }
    
    //MARK: - API call
    
    /****Resend OTP*/
    
//    func resendOTPApiCall(){
//
//
//        SVProgressHUD.show()
//
//        let params:[String:Any] = ["id" : idOtp] //"email": self.email]
//        DataService.sharedInstance.postApiCall(endpoint: constantApis.resendOTP, params: params) { (response, error) in
//
//            SVProgressHUD.dismiss()
//            if response != nil {
//
//                let code = response?["code"] as? Int
//                let msg = response?["message"] as? String
//                if code == 200 {
////                    self.otp = response?["otp"] as? String ?? ""
////                    self.fillOTP()
//                    self.clearTextFields()
//                }
//                else {
//                    self.customAlertDismiss(title: "", message: msg ?? "")
//                }
//
//            }
//            else {
//
//                self.customAlertDismiss(title: "", message: "Something went wrong!")
//            }
//        }
//    }
    
    func resendOTPApiCall() {
        
         SVProgressHUD.show()
        
      //  self.showLottieIndicator(show: true)
        
        let params:[String: Any] = ["email": self.email,
                                    "id" : idOtp ] //UserDefaults.standard.string(forKey: UserDefaultConstants.UserID.rawValue) ?? "" ]
        
    print(params)
        DataService.sharedInstance.postApiCall(endpoint: constantApis.resendOTP, params: params) { (response, error) in
            
            SVProgressHUD.dismiss()
           // self.showLottieIndicator(show: false)
            
            print(response)
            
            if response != nil {
                
                Toast.show(message: "One time passcode sent on your email", controller: self)
                
                self.clearTextFields()

            }
            else {
                
                self.customAlertDismiss(title: "Message", message: "Something went wrong!")
            }
            
        }
    
    }
    
    /**** Verify OTP */
    
    func verifyOTPApi(otp:String) {
        
       SVProgressHUD.show()
     //   self.showLottieIndicator(show: true)
        
        let params:[String:Any] = ["otp": otp,
                                   "user_id": idOtp
                                   //"id":idOtp //"email":email
        ]
        
        print(params)
        
        DataService.sharedInstance.postApiCall(endpoint: constantApis.otpVerify, params: params) { (response, error) in
            
         SVProgressHUD.dismiss()
       
            if response != nil {
                
                print(response ?? [:])
                let codeInt = response?["code"] as? Int ?? 0
                let codeString = response?["code"] as? String ?? ""
                let message = response?["message"] as? String ?? ""
                if codeInt == 200 || codeString == "200" {
                    
                    self.saveDataToUserDefault()

                    UserDefaults.standard.set(true, forKey: UserDefaultConstants.otpStatus.rawValue)
                    UserDefaults.standard.set("2", forKey: UserDefaultConstants.LoginStatus.rawValue)
                    UserDefaults.standard.set("0", forKey: UserDefaultConstants.personalDetailsStatus.rawValue)
                    UserDefaults.standard.set(false, forKey: UserDefaultConstants.isguestLogin.rawValue)
                    UserDefaults.standard.synchronize()
                    
                    let vc = FingerprintVc.instantiateFromAppStoryboard(appStoryboard: .main)
                    vc.commingFromScreen = "SignIn"
                    
                    self.pushViewController(viewController: vc)
                    
//                    guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "PersonalDetailsViewController") as? PersonalDetailsViewController else {
//                        return
//                    }
//                    self.pushViewController(viewController: vc)
                    
                } else {
                    
                    self.customAlertDismiss(title: "Alert", message: message)
                }
                
            }
            else {
                self.customAlertDismiss(title: "Alert", message: "Something went wrong!")
            }
        }
        
    }
    

    
    func saveDataToUserDefault(){
        
        if let result = response["result"] as? NSDictionary {
            let user_id = result["_id"] as? String ?? ""
            let firstName = result["first_name"] as? String ?? ""
            let lastName = result["last_name"] as? String ?? ""
            let userEmail = result["email"] as? String ?? ""
            let userPhoneNumber = result["phone"] as? String ?? ""
            UserDefaults.standard.set(firstName, forKey: UserDefaultConstants.FirstName.rawValue)
            UserDefaults.standard.set(lastName, forKey: UserDefaultConstants.LastName.rawValue)
            UserDefaults.standard.set(userEmail, forKey: UserDefaultConstants.UserEmail.rawValue)
            UserDefaults.standard.set(userPhoneNumber, forKey: UserDefaultConstants.PhoneNumber.rawValue)
            UserDefaults.standard.set("GB", forKey: "countryShortName")
            let currencyName = "GBP"
            let currencySymbol = currencyName.getSymbol(forCurrencyCode: currencyName)
            print(currencySymbol ?? "")
            GlobalVariables.currency = currencySymbol ?? ""
            UserDefaults.standard.set(currencySymbol, forKey: "currency")
            UserDefaults.standard.set(user_id, forKey: UserDefaultConstants.UserID.rawValue)
            UserDefaults.standard.synchronize()
            
        }
        
//        UserDefaults.standard.synchronize()
        
    }
    
    
}
