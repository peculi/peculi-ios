//
//  SignInVc.swift
//  Peculi
//
//  Created by Stealth on 28/10/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit

class SignInVc: UIViewController , UITextFieldDelegate{
    
    //MARK: - outlets
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var forgetPasswordlbl: UILabel!
    @IBOutlet weak var hidepasswordBtn: UIButton!
    
    //MARK: - variables
    var fromVC: AllControllers?
    
    //MARK: - did load method
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    
    //MARK: - view will appear
    override func viewWillAppear(_ animated: Bool) {
        updateUi()
    }
    
    /*** setup design */
    func updateUi(){
        
        self.navigationController?.navigationBar.isHidden = true
        navigationController?.navigationBar.barStyle = .black
        
        usernameTextField.delegate = self
        usernameTextField.attributedPlaceholder = NSAttributedString(string: "Enter Email",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        passwordTextField.delegate = self
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "Enter Password",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        let forgetGestureAction = UITapGestureRecognizer(target: self, action: #selector(self.forgetPasswordAction(_:)))
        forgetPasswordlbl?.isUserInteractionEnabled = true
        forgetPasswordlbl?.addGestureRecognizer(forgetGestureAction)
        
    }
    
    
    
    /**** Forget password action */
    @objc func forgetPasswordAction(_ sender: UITapGestureRecognizer) {
        
        
        let vc = ForgetPasswordVc.instantiateFromAppStoryboard(appStoryboard: .main)
        self.pushViewController(viewController: vc)
        
    }
    
    
    @IBAction func btnBackAction(_ sender: Any) {
        
        ///check if guest user or not
        if checkIfGuest() == true{
            popVc()
        }
        else{
            WelcomeScreenVc.setRootVC()
        }
        
    }
    
    /**** unhidePassword action */
    @IBAction func unhidePasswordBtnAction(_ sender: Any) {
        
        if passwordTextField.tag == 0{
            passwordTextField.tag = 1
            hidepasswordBtn.setImage(UIImage(named: "ic_eye"), for: .normal)
            passwordTextField.isSecureTextEntry = false
        }
        else {
            passwordTextField.tag = 0
            hidepasswordBtn.setImage(UIImage(named: "ic_eyeclose1"), for: .normal)
            passwordTextField.isSecureTextEntry = true
        }
        
    }
    
    
    /****Sign up API call */
    func callSignInApi(_ email : String,_ password : String){
        
        SignInModel().fetchInfo(email,password){(userinfo) in
            
            print(userinfo)
            
            if userinfo.count > 0 {
                
                
               if UserDefaults.standard.value(forKey: "SAVECODE") as? Int  == 200 {
               // if userinfo[0].statuscheck == 200 { //"success" {
              //storing values to userDefault
                    
                    UserDefaults.standard.set(false, forKey: UserDefaultConstants.isguestLogin.rawValue)
                    UserDefaults.standard.set(userinfo[0]._id, forKey: UserDefaultConstants.UserID.rawValue)
                    UserDefaults.standard.set(userinfo[0].firstName, forKey: UserDefaultConstants.FirstName.rawValue)
                    UserDefaults.standard.set(userinfo[0].lastName, forKey: UserDefaultConstants.LastName.rawValue)
                    UserDefaults.standard.set(userinfo[0].gender, forKey: UserDefaultConstants.userGender.rawValue)
                   
//                    UserDefaults.standard.set(userinfo[0].image, forKey: "UserImage")
                   
                    UserDefaults.standard.set(userinfo[0].dob, forKey: UserDefaultConstants.UserDob.rawValue)
                    UserDefaults.standard.set(userinfo[0].email, forKey: UserDefaultConstants.UserEmail.rawValue)
                    UserDefaults.standard.set(userinfo[0].status, forKey: UserDefaultConstants.LoginStatus.rawValue)
                    UserDefaults.standard.set(userinfo[0].phone, forKey: UserDefaultConstants.PhoneNumber.rawValue)
//                    UserDefaults.standard.set(userinfo[0].address, forKey: "Address")
                    UserDefaults.standard.set(userinfo[0].country, forKey: "country")
                    UserDefaults.standard.set(userinfo[0].state, forKey: "state")
                    UserDefaults.standard.set(userinfo[0].city, forKey: UserDefaultConstants.city.rawValue)
                    UserDefaults.standard.set(userinfo[0].countryName, forKey: UserDefaultConstants.countryName.rawValue)
                    UserDefaults.standard.set(userinfo[0].address1, forKey: UserDefaultConstants.Address1.rawValue)
                    UserDefaults.standard.set(userinfo[0].address2, forKey: UserDefaultConstants.Address2.rawValue)
                    UserDefaults.standard.set(userinfo[0].zipCode, forKey: UserDefaultConstants.postcode.rawValue)
                   
                    UserDefaults.standard.set(userinfo[0].personalDetailsStatus, forKey: UserDefaultConstants.personalDetailsStatus.rawValue)
                    UserDefaults.standard.set(userinfo[0].otp, forKey: UserDefaultConstants.otp.rawValue)
                    UserDefaults.standard.set(userinfo[0].jwToken, forKey: UserDefaultConstants.jwToken.rawValue)
                    UserDefaults.standard.set(userinfo[0].kycStatus, forKey: UserDefaultConstants.kycStatus.rawValue)
                   
                   
                   
                   UserDefaults.standard.set("0", forKey: UserDefaultConstants.showPopupHomeScreen.rawValue)
                   UserDefaults.standard.set("0", forKey: UserDefaultConstants.showPopup_PeculiOfferScreen.rawValue)
                   UserDefaults.standard.set("0", forKey: UserDefaultConstants.showPopup_redeemScreen.rawValue)
                   
                   
                   
                    
                    //get currency symbol
                    
                    let currencyName = userinfo[0].currency.getSymbol(forCurrencyCode: userinfo[0].currency)
                    GlobalVariables.currency = currencyName ?? "£"
                    UserDefaults.standard.set(currencyName, forKey: "currency")
                    UserDefaults.standard.synchronize()

                   self.moveToFingerprintVc()
                    
                    
                } else {
                    
                    self.customAlertDismiss(title: "Alert", message: userinfo[0].message)
                }
            } else {
                
                self.customAlertDismiss(title: "Alert", message: userinfo[0].message)
            }
        }
    }
    
    // function to move to fingerprint ViewController
    
    func moveToFingerprintVc() {
        
        let vc = FingerprintVc.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.commingFromScreen = "SignIn"
        vc.fromVC = fromVC
        self.pushViewController(viewController: vc)
        
    }
    
    /****signIn button action */
    
    @IBAction func btnSignIn(_ sender: Any) {
        
        if usernameTextField.text?.count == 0 {
            AlertView.simpleViewAlert(title: "", "Please enter email", self)
            return
        }
        else if passwordTextField.text?.count == 0 {
            
            AlertView.simpleViewAlert(title: "", "Please enter password", self)
            return
        }
        else if !(usernameTextField.text?.EmailValidation())!{
            AlertView.simpleViewAlert(title: "", "Please enter correct email", self)
            return
        }
        
        else if !(self.passwordTextField.text?.isValidPassword())! {
            
            AlertView.simpleViewAlert(title: "", "Minimum Password length is 8 character includes a lower case letter , an upper case letter, a special character and no whitespace allowed. e.g (Peculi@1234!!)", self)
            
        }
        else {

            let deviceid = UIDevice.current.identifierForVendor?.uuidString ?? ""
            callSignInApi(usernameTextField.text ?? "",passwordTextField.text ?? "")

        }
        
    }
    
    /****Signup button action*/
    
    @IBAction func btnSignUp(_ sender: Any) {
        
        let vc = SignUpVc.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.fromVC = fromVC
        self.pushViewController(viewController: vc)
        
    }
    
    
    
    
}
