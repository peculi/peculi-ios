//
//  AddCard_PopUp_Vc.swift
//  Peculi
//
//  Created by PPI on 4/13/22.
//  Copyright © 2022 Stealth Tech. All rights reserved.
//

import UIKit

class AddCard_PopUp_Vc: UIViewController {
    
    // MARK: - OUTLETS
    
    @IBOutlet weak var cardNameTxtField: UITextField!
    @IBOutlet weak var popUpChildView: UIView!
    @IBOutlet var popUpSuperView: UIView!
    
    //MARK: - declare Variables
        
        var textFieldData = String()
        var categoryId = String()
        var okCompletionHandler: (() -> Void)?
    
    
    // MARK: - VIEW LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    override func viewWillAppear(_ animated: Bool) {
        setupView()
        animateView()
    }
    
    
    // MARK: - FUNCTION
    
    /**** UI setup */
    func setupView(){
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        popUpSuperView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        
    }
     
     ///animation setup
     func animateView() {
         popUpChildView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
         UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
             self.popUpChildView.transform = .identity
         }, completion: {(finished: Bool) -> Void in
             
         })
         
     }
    
    
    // MARK: - BUTTON ACTION
    
    @IBAction func btnCancel_Action(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        print("call cancel ")
    }

    
    @IBAction func btnContinue_Action(_ sender: Any) {
        
//        if textFieldData != nameTextField.text {
//              print("call function")
//           }
//           else {
//               self.dismiss(animated: true, completion: nil)
//           }
        
        self.addNewCard_Api()
        
        print("call function")
      
        
        
    }
    
    
    
    
    func addNewCard_Api(){
        
        self.addPeculiPopup(view: self.view)
        
        let params:[String:Any] = ["cardName": cardNameTxtField.text ?? ""
        ]
        
        print(params)
        
        DataService.sharedInstance.postApiCall(endpoint: constantApis.addNewCards_Api, params: params) { (response, error) in
            
            self.hidePeculiPopup(view: self.view)
       
            
            if response != nil {
                print(response ?? [:])
                let codeInt = response?["code"] as? Int ?? 0
                let codeString = response?["code"] as? String ?? ""
                let message = response?["message"] as? String ?? ""
                if codeInt == 200 || codeString == "200" {
                    
                   let message = response?["message"] as? String ?? "0"
                    Toast.show(message: message, controller: self)
    
                    self.dismiss(animated: true, completion: nil)

                    self.okCompletionHandler?()
                  
                } else {
                   
                    self.customAlertDismiss(title: "Alert", message: message)
                    
                    self.dismiss(animated: true, completion: nil)
                }
                
            }
            else {
                self.customAlertDismiss(title: "Alert", message: "Something went wrong!")
                
                self.dismiss(animated: true, completion: nil)
            }
        }
        
    }

   

}
