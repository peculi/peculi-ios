//
//  PotDescriptionVC.swift
//  Peculi
//
//  Created by Stealth on 1/25/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import UIKit
import WebKit

class PotDescriptionVC: UIViewController, WKNavigationDelegate {
  
    //MARK:- outlets
    @IBOutlet weak var descriptionWebView: WKWebView!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    //MARK:- variables
    var navName = ""
    var descriptionData = ""
    
    //MARK:- view did load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.descriptionWebView.navigationDelegate = self
        self.descriptionWebView.isOpaque = false
              
        self.descriptionWebView.backgroundColor = #colorLiteral(red: 0.07450980392, green: 0.1019607843, blue: 0.1529411765, alpha: 1)
        
        let HTMLString = "<html><body style='font-family:Montserrat-Regular;font-size:40px; color:#ffffff'><style>a:link { color: #0f83d9; }</style>\(descriptionData)</body></html>"
       self.descriptionWebView.loadHTMLString(HTMLString, baseURL: nil)
        
    }
    
    //MARK:- view will appear
    override func viewWillAppear(_ animated: Bool) {
        navigartionSetup()
        setNeedsStatusBarAppearanceUpdate()
    
    }
    

    //MARK:- Navigation Setup
    func navigartionSetup(){
        navigationController?.navigationBar.isHidden = false
        title = navName
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.07450980392, green: 0.1019607843, blue: 0.1529411765, alpha: 1)
        self.navigationController?.navigationBar.isTranslucent = false
       
        
        //MARK:- Navigation LeftBar Button:-
        let leftBarButton = UIBarButtonItem(image: UIImage(named: "icBackArrowWhite"), style: .done, target: self, action: #selector(icBackButtonAction))
        self.navigationItem.leftBarButtonItem  = leftBarButton
    }
    
    @objc func icBackButtonAction(){
        popVc()
    }


}
