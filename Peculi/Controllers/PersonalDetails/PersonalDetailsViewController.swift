//
//  PersonalDetailsViewController.swift
//  Peculi
//
//  Created by iApp on 15/07/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import UIKit
import Foundation
import SVProgressHUD
import DropDown

class PersonalDetailsViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate {
    
    //MARK: - outlets
    @IBOutlet weak var genderTextField: UITextField!
    @IBOutlet weak var genderBtn: UIButton!
    @IBOutlet weak var dateOfBirthTextField: UITextField!
    @IBOutlet weak var dobBtn: UIButton!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var countryBtn: UIButton!
    @IBOutlet weak var addressLine1TextView: UITextView!
    @IBOutlet weak var AddressLine1Height: NSLayoutConstraint!
    @IBOutlet weak var addressLine2TextView: UITextView!
    @IBOutlet weak var addressLine2Height: NSLayoutConstraint!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var cityBtn: UIButton!
    @IBOutlet weak var postalCodeTextField: UITextField!
    @IBOutlet weak var genderView: UIView!
    @IBOutlet weak var dobView: UIView!
    @IBOutlet weak var countryView: UIView!
    @IBOutlet weak var addressLine1View: UIView!
    @IBOutlet weak var addressLine2View: UIView!
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var postCodeView: UIView!
    
    //MARK: - Variables
    let addressLine1Placeholder = "Enter House Name/Number,Street Name"
    let addressLine2Placeholder = "Enter Apartment, suite, unit, building, floor etc.."
    let minTextViewHeight: CGFloat = 32
    let maxTextViewHeight: CGFloat = 64
    var dateTo = Date()
    var dropdwn = DropDown()
    var allStates = [String]()
    var allCity = [String]()
    
    
    
    //MARK: - view did load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateUI()
        getAllStates(countryShortName: "GB")
        
    }
    
    /**** Setup UI */
    func updateUI(){
        
        
        self.navigationController?.navigationBar.isHidden = true
        
        genderTextField.delegate = self
        genderTextField.attributedPlaceholder = NSAttributedString(string: "Select Gender",
                                                                   attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        dateOfBirthTextField.delegate = self
        dateOfBirthTextField.attributedPlaceholder = NSAttributedString(string: "Select Date Of Birth",
                                                                        attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        countryTextField.delegate = self
        countryTextField.attributedPlaceholder = NSAttributedString(string: "Select Country",
                                                                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        addressLine1TextView.delegate = self
        addressLine2TextView.delegate = self
        
        addressLine1TextView.text = addressLine1Placeholder
        
        addressLine2TextView.text = addressLine2Placeholder
        
        cityTextField.delegate = self
        cityTextField.attributedPlaceholder = NSAttributedString(string: "Select City",
                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        postalCodeTextField.delegate = self
        postalCodeTextField.attributedPlaceholder = NSAttributedString(string: "Enter Post Code",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        postalCodeTextField.autocapitalizationType = UITextAutocapitalizationType.allCharacters
        
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.black
        DropDown.appearance().textColor = UIColor.black
        DropDown.appearance().selectedTextColor = UIColor.white
        DropDown.appearance().cornerRadius = 4
        
        
    }
    
    //MARK: - textview delegate methods
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == addressLine1TextView{
            
            if textView.text == addressLine1Placeholder {
                textView.text = ""
            }
            
        }
        else if textView == addressLine2TextView {
            if textView.text == addressLine2Placeholder{
                textView.text = ""
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == addressLine1TextView{
            
            if textView.text == ""
            {
                AddressLine1Height.constant = 39
                textView.text = addressLine1Placeholder
                
            }
        }
        else if textView == addressLine2TextView{
            
            if textView.text == ""
            {
                addressLine2Height.constant = 39
                textView.text = addressLine2Placeholder
                
            }
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        
        if textView == addressLine1TextView{
            var height = ceil(textView.contentSize.height)
            
            if (height < minTextViewHeight + 5) {
                height = minTextViewHeight
            }
            if (height > maxTextViewHeight) {
                height = maxTextViewHeight
            }
            
            if height != AddressLine1Height.constant {
                AddressLine1Height.constant = height
                textView.setContentOffset(CGPoint.zero, animated: false)
            }
            print(height)
        }
        
        else if textView == addressLine2TextView{
            var height = ceil(textView.contentSize.height)
            
            if (height < minTextViewHeight + 5) {
                height = minTextViewHeight
            }
            if (height > maxTextViewHeight) {
                height = maxTextViewHeight
            }
            
            if height != addressLine2Height.constant {
                addressLine2Height.constant = height
                textView.setContentOffset(CGPoint.zero, animated: false)
            }
            print(height)
            
            
        }
        let currentText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        return currentText.count <= 50
        
    }
    
    //MARK: - textField delegate methods
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == self.genderTextField || textField == self.countryTextField || textField == self.cityTextField || textField == self.dateOfBirthTextField{
            return false
        }
        return true
        
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == self.postalCodeTextField{
            if string == " " {
                return false
            }
            textField.text = (textField.text! as NSString).replacingCharacters(in: range, with: string.uppercased())
            return false
        }
        
        return true
    }
    
    
    
    
    //MARK: - UIButton actions
    
    @IBAction func backBtnAction(_ sender: Any) {
        
        popVc()
        
    }
    
    /****  Select Gender */
    @IBAction func selectGenderAction(_ sender: Any) {
        
        setupDropDwn(SetForView: genderBtn, list: ["Male","Female","Prefer not to say"]) {[weak self] (selectedValue) in
            self?.genderTextField.text = selectedValue
        }
        
        dropdwn.show()
        
        
    }
    
    /**** Select DOB */
    @IBAction func selectDOBAction(_ sender: Any) {
        
        dobPicker()
        
    }
    
    /**** Select Country */
    @IBAction func selectCountryAction(_ sender: Any) {
        
        setupDropDwn(SetForView: countryBtn, list: allStates) {[weak self] (selectedValue) in
            
            self?.countryTextField.text = selectedValue
            self?.getAllCities(shortName: "GB", stateName: selectedValue)
        }
        
        dropdwn.show()
        
    }
    
    /**** Select City */
    @IBAction func selectCityAction(_ sender: Any) {
        
        setupDropDwn(SetForView: cityBtn, list: allCity) {[weak self] (selectedValue) in
            
            self?.cityTextField.text = selectedValue
            
        }
        
        dropdwn.show()
        
    }
    
    @IBAction func submitBtnAction(_ sender: Any) {
        
        if genderTextField.text?.count == 0 {
            AlertView.simpleViewAlert(title: "Error", "Please select Gender!", self)
            return
        }
        else if dateOfBirthTextField.text?.count == 0 {
            AlertView.simpleViewAlert(title: "Error", "Please select Date of birth!", self)
            return
        }
        else if countryTextField.text?.count == 0{
            AlertView.simpleViewAlert(title: "Error", "Please select Country!", self)
            return
        }
        else if addressLine1TextView.text?.count == 0 || addressLine1TextView.text == addressLine1Placeholder{
            AlertView.simpleViewAlert(title: "Error", "Please Fill your address details!", self)
            return
        }
        else if addressLine2TextView.text?.count == 0 || addressLine2TextView.text == addressLine2Placeholder{
            AlertView.simpleViewAlert(title: "Error", "Please Fill your address details!", self)
            return
        }
        else if cityTextField.text?.count == 0{
            AlertView.simpleViewAlert(title: "Error", "Please select City!", self)
            return
        }
        else if postalCodeTextField.text?.count == 0{
            AlertView.simpleViewAlert(title: "Error", "Please enter Postal code!", self)
            return
        }
        else {
            verifyPostalCode(postalCodeTextField.text ?? "")
        }
        
    }
    
    // function for dropdowns
    
    func dobPicker() {
        
        let alert = UIAlertController(style: .alert, title: "")
        let date = Calendar.current.date(byAdding: .year, value: 0, to: Date())
        dateTo = date ?? Date()
        alert.addDatePicker(mode: .date, date: Date(), minimumDate: nil, maximumDate: nil) { date in
            self.dateTo = date
        }
        alert.addAction( title: "OK", style: .default, isEnabled: true) { (action) in
            let formatter1 = DateFormatter()
            formatter1.dateFormat = "dd-MM-yyyy"
            
            formatter1.locale = NSLocale(localeIdentifier: "en_US") as Locale?
            self.dateOfBirthTextField.text = formatter1.string(from: self.dateTo)
        }
        alert.addAction(title: "Cancel", style: .cancel){ (action) in
            
        }
        alert.show()
    }
    
    
    func setupDropDwn(SetForView: UIView, list: [String], callback: @escaping ((String)-> Void)) {
        
        dropdwn.anchorView = SetForView
        dropdwn.bottomOffset = CGPoint(x: 0, y: SetForView.bounds.height)
        dropdwn.dataSource = list
        dropdwn.selectionAction = { (index, item) in
            
            callback(item)
        }
        
    }
    
    
    //MARK: - API calls
    
    /**** fetch all states */
    func getAllStates(countryShortName:String) {
        SVProgressHUD.show()
        DataService.sharedInstance.getAllStates(shortName: countryShortName) { (resultDict, errorMsg) in
            SVProgressHUD.dismiss()
            print(resultDict as Any)
            if errorMsg == nil {
                self.allStates.removeAll()
                if let result = resultDict?["result"] as? NSArray {
                    let swiftArray: [String] = result.compactMap({ $0 as? String })
                    self.allStates.append(contentsOf: swiftArray)
                    //                    if self.countryTextField.text == "" {
                    //                        if self.allStates[0] != "" {
                    //                            self.countryTextField.text = self.allStates[0]
                    //
                    //                        }
                    //                    }
                }
                
            }
        }
    }
    
    /**** Fetch all cities API */
    func getAllCities(shortName:String,stateName:String) {
        //        SVProgressHUD.show()
        DataService.sharedInstance.getAllCities(shortName:shortName, stateName:stateName) { (resultDict, errorMsg) in
            //            SVProgressHUD.dismiss()
            print(resultDict as Any)
            if errorMsg == nil {
                self.allCity.removeAll()
                if resultDict?["code"] as? String == "400"{
                    self.cityTextField.text = stateName
                    self.allCity = [stateName]
                } else if resultDict?["code"] as? String == "200" {
                    if let result = resultDict?["result"] as? NSArray {
                        
                        let cityArray: [String] = result.compactMap({ $0 as? String })
                        self.allCity.append(contentsOf: cityArray)
                        
                        if self.allCity[0] != "" {
                            self.cityTextField.text = self.allCity[0]
                        }
                        
                        
                        
                    }
                }
            }
        }
    }
    
    
    /**** Verify postal code API */
    func verifyPostalCode( _ postCode:String){
        SVProgressHUD.show()
        DataService.sharedInstance.checkPostCodeApiCall(postCode: postCode) { (reponse, error) in
            
            if reponse != nil{
                SVProgressHUD.dismiss()
                if let code = reponse?["code"] as? Int{
                    let msg = reponse?["message"] as? String
                    if code == 200{
                        
                        if let result = reponse?["result"] as? String{
                            if result == "1"{
                                
                                self.personalDetailsAPICall()
                                
                            }
                            else{
                                
                                self.customAlertDismiss(title: "Alert", message: "Postal Code is not valid")
                            }
                            
                        }
                        
                    }
                    else{
                        
                        self.customAlertDismiss(title: "Alert", message: msg ?? "")
                        
                    }
                    
                }
                print(reponse)
            }
            else{
                SVProgressHUD.dismiss()
                self.customAlertDismiss(title: "Error", message: "Something Went Wrong!!")
                
            }
            
        }
        
    }
    
    
    /**** Personal Details */
    func personalDetailsAPICall(){
        
        SVProgressHUD.show()
        let firstname = UserDefaults.standard.string(forKey: UserDefaultConstants.FirstName.rawValue) ?? ""
        let lastName = UserDefaults.standard.string(forKey: UserDefaultConstants.LastName.rawValue) ?? ""
        let email = UserDefaults.standard.string(forKey: UserDefaultConstants.UserEmail.rawValue) ?? ""
        let phoneNo = UserDefaults.standard.string(forKey: UserDefaultConstants.PhoneNumber.rawValue) ?? ""
        let userID = UserDefaults.standard.string(forKey: UserDefaultConstants.UserID.rawValue) ?? ""
        
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd"
        dateFormat.locale = NSLocale(localeIdentifier: "en_US") as Locale?
        let dateString = dateFormat.string(from: self.dateTo)
        
      
        
        let params:[String:Any] = ["firstname": firstname,
                                   "lastname": lastName,
                                   "email": email,
                                   "gender": genderTextField.text ?? "",
                                   "dob": dateString,
                                   "country": "GB",
                                   "country_name": countryTextField.text ?? "",
                                   "state": "",
                                   "city": cityTextField.text ?? "",
                                   "postcode": postalCodeTextField.text ?? "",
                                   "currency": "GB",
                                   "address1": addressLine1TextView.text ?? "",
                                   "address2": addressLine2TextView.text ?? "",
                                   "phone": phoneNo,
                                   "user_id":userID
                                   
        ]
        
        DataService.sharedInstance.formDataAPICall(endpoint: constantApis.personalDetailsRegister, params: params) { (arg0) in
            let (_, list, isSuccess) = arg0
            SVProgressHUD.dismiss()
            print(list)
            
            if arg0.isSuccess{
                UserDefaults.standard.set(list["gender"] as? String, forKey: UserDefaultConstants.userGender.rawValue)
                UserDefaults.standard.set(list["dob"], forKey: UserDefaultConstants.UserDob.rawValue)
                UserDefaults.standard.set(list["country_name"], forKey: UserDefaultConstants.countryName.rawValue)
                UserDefaults.standard.set(list["address1"], forKey: UserDefaultConstants.Address1.rawValue)
                UserDefaults.standard.set(list["address2"], forKey: UserDefaultConstants.Address2.rawValue)
                UserDefaults.standard.set(list["city"], forKey: UserDefaultConstants.city.rawValue)
                
                UserDefaults.standard.set(list["postcode"], forKey: UserDefaultConstants.postcode.rawValue)
//                UserDefaults.standard.set("3", forKey: "LoginStatus")
                UserDefaults.standard.set("1", forKey: UserDefaultConstants.personalDetailsStatus.rawValue)
                UserDefaults.standard.synchronize()
//                let vc = FingerprintVc.instantiateFromAppStoryboard(appStoryboard: .main)
//                vc.commingFromScreen = "Signup"
//                self.pushViewController(viewController: vc)
                self.setRootHomeVC()
            }
            else{
                self.customAlertDismiss(title: "Alert", message: arg0.message)
            }
        }
   
        
    }
    
}
