//
//  AddAmountSwaprificeVC.swift
//  Peculi
//
//  Created by Stealth on 1/11/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import UIKit
import SVProgressHUD

class AddAmountSwaprificeVC: UIViewController,UITextFieldDelegate {
    
    //MARK: - outlets
    
    @IBOutlet weak var addAmountTxtField: UITextField!
    @IBOutlet weak var tierName: UILabel!
    @IBOutlet weak var tierImage: UIImageView!
    @IBOutlet weak var myTableView: UITableView!
    
    //MARK: - variables
    
    var potImage = ""
    var potName = ""
    var categoryId = ""
    
    
    //MMMM
    
    var cardId = ""
    var cardName = ""
    var cardSelectId = ""
    var selectedRows:[IndexPath] = []
    var getCardModel = [GetCArd_ModelStructure]()
    var TapCell = false
    var catIdForUpdateTransctonStatus = ""
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myTableView.delegate = self
        myTableView.dataSource = self
        
        self.GetCard_Api()
        TapCell = false
        tierName.text = potName.capitalizingFirstLetter()
        
        let ImageUrl = String(format: "%@%@", AppConfig.ImageUrl,potImage)
        
        tierImage.sd_setImage(with: URL(string:ImageUrl), placeholderImage:UIImage(named: "placeholder_Img"), completed: { (image, error, cacheType, url) -> Void in
            if ((error) != nil) {
                // set the placeholder image here
                //cell.imgVw.image = #imageLiteral(resourceName: "doctorAvtar_Img")
            } else {
                // success ... use the image
            }
        })
//        addAmountTxtField.delegate = self
//        addAmountTxtField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: addAmountTxtField.frame.height))
//        addAmountTxtField.leftViewMode = .always
//        addAmountTxtField.attributedPlaceholder = NSAttributedString(string: "Add Amount Here",
//                                                                     attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
//
//        addAmountTxtField.layer.borderWidth = 2.0
//        addAmountTxtField.layer.borderColor =  #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
//        addAmountTxtField.layer.cornerRadius = 10.0
        
    }
    
    //MARK: - ViewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        //navigartionSetup()
        
        navigartionSetup()
        setNeedsStatusBarAppearanceUpdate()
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    //MARK: - Navigation Setup
    
//    func navigartionSetup(){
//
//        navigationController?.navigationBar.isHidden = false
//        title = "Swaprifice"
//        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2117647059, green: 0.1725490196, blue: 0.3058823529, alpha: 1)
//        self.navigationController?.navigationBar.isTranslucent = false
//
//
//        //MARK: - Navigation LeftBar Button:-
//        let leftBarButton = UIBarButtonItem(image: UIImage(named: "icBackArrowWhite"), style: .done, target: self, action: #selector(icBackButtonAction))
//        self.navigationItem.leftBarButtonItem  = leftBarButton
//    }
//
//    //MARK: - Object Function Button Action
//    @objc func icBackButtonAction(){
//        popVc()
//    }
    
    
    func navigartionSetup(){

        //icBackArrowWhite
        
        navigationController?.navigationBar.isHidden = false
        title = "Swaprifice"
       // self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 0.2203302979, green: 0.1600231826, blue: 0.3218392134, alpha: 1)
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2203302979, green: 0.1600231826, blue: 0.3218392134, alpha: 1)
        self.navigationController?.navigationBar.isTranslucent = true
        

        //MARK: - Navigation LeftBar Button:-

        let leftBarButton = UIBarButtonItem(image: UIImage(named: "icBackArrowWhite"), style: .done, target: self, action: #selector(icBackButtonAction))
        self.navigationItem.leftBarButtonItem  = leftBarButton
    }
    
    
    @objc func icBackButtonAction(){
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
//    func navigartionSetup(){
//
//        //icBackArrowWhite
//
//        navigationController?.navigationBar.isHidden = false
//        title = "Manage Details"
//        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2203302979, green: 0.1600231826, blue: 0.3218392134, alpha: 1)
//        self.navigationController?.navigationBar.isTranslucent = true
//
//        //MARK: - Navigation LeftBar Button:-
//
//        let leftBarButton = UIBarButtonItem(image: UIImage(named: "icBackArrowWhite"), style: .done, target: self, action: #selector(icBackButtonAction))
//        self.navigationItem.leftBarButtonItem  = leftBarButton
//    }
    
    
    
    
    
    
    @IBAction func btnAddNewCard_Action(_ sender: Any) {
        
        print("Button Add new Card Pressed")
        
        popupAddCardAlert()
        
    }

    
    @IBAction func saveBtn(_ sender: Any) {
        let amountDouble = (addAmountTxtField.text! as NSString).doubleValue
        var globalAmount = Double()
        if let globalBal = UserDefaults.standard.string(forKey: UserDefaultConstants.GlobalBalance.rawValue){
            globalAmount = (globalBal as NSString).doubleValue
        }
        else {
            globalAmount = 0.00
        }
        
        let totalAmount = amountDouble + globalAmount
        
        if addAmountTxtField.text == ""{
            AlertView.simpleViewAlert(title: "Error", "Please enter amount!", self)
        }
        else if amountDouble == 0.000{
            
            AlertView.simpleViewAlert(title: "Error", "Please enter valid amount!", self)
        }
        
        //        else if totalAmount > 100{
        //            if UserDefaults.standard.string(forKey: UserDefaultConstants.personalDetailsStatus.rawValue) == "1"{
        //
        //            }
        //            else{
        //                eKYCProcess()
        //            }
        //        }
        
        else {

            if self.checkIfGuest() == false{
                
                
                if totalAmount > 100{
                    
                    if UserDefaults.standard.string(forKey: UserDefaultConstants.kycStatus.rawValue) == "1"{
                        confirmPopup()
                    }
                    else {
                        eKYCProcess()
                    }
                }
                else{
                    confirmPopup()
                }
                
            }
            else {
                self.popupToRegister()
            }

        }
    }
}

extension AddAmountSwaprificeVC {
    ///API call
    func addAmount() {
        SVProgressHUD.show()
        DataService.sharedInstance.addAmountSwaprifice(categoryId: categoryId, amount: addAmountTxtField.text ?? "") { (resultDict, errorMsg) in
            SVProgressHUD.dismiss()
            print(resultDict as Any)
            if errorMsg == nil {
                if let result = resultDict?["status"] as? String {
                    if result == "success" {
                        self.alertView(title: "Success", "Amount added successfully", self)
                    }
                }
                else {

                    if let message = resultDict?["message"] as? String {
                        self.customAlertDismiss(title: "Alert", message: message)
                    }
                }
                
            } else {
                Toast.show(message: "Something went wrong", controller: self)
            }
        }
    }
    
    
    //MARK: -  Alert action setup-
    
    
    
    
    //MARK: - alert setup
   
   func popupAddCardAlert() {
      
           
           let sb = UIStoryboard(name: "Main", bundle:nil)
           let addCardAlert = sb.instantiateViewController(withIdentifier: "AddCard_PopUp_Vc") as! AddCard_PopUp_Vc
      //     customAlert.textFieldData = navName.capitalizingFirstLetter()
           
       //addCardAlert.categoryId = CategoryInfo[0].userData.categoryDetails._id
           
       addCardAlert.okCompletionHandler = {
               print("call Add Card PopUp")
           self.GetCard_Api()
           }
       addCardAlert.providesPresentationContextTransitionStyle = true
       addCardAlert.definesPresentationContext = true
       addCardAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
       addCardAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
           
           self.present(addCardAlert, animated: true, completion: nil)
   }
    
    
    
    func popupEditCardNameAlert(){
        
        
        let sb = UIStoryboard(name: "Main", bundle:nil)
        let editCardAlert = sb.instantiateViewController(withIdentifier: "EditCard_PopUp_Vc") as! EditCard_PopUp_Vc
  
        editCardAlert.cardId =  cardId
        editCardAlert.textFieldData = cardName
        
        editCardAlert.okCompletionHandler = {
            
           // DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
               
                self.GetCard_Api()
             
                print("call editCardAlert PopUp")
                
          //  }

        }
        
        editCardAlert.providesPresentationContextTransitionStyle = true
        editCardAlert.definesPresentationContext = true
        editCardAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        editCardAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        
        self.present(editCardAlert, animated: true, completion: nil)
}
   
    
    func alertView(title:String, _ message:String, _ view:UIViewController){
        
        
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "CustomDefaultAlertViewController") as? CustomDefaultAlertViewController else{return}
        
        vc.titleVal = title
        vc.messageVal = message
        vc.setTransition()
        vc.completionHandler = { [weak self] in
            
            view.dismiss(animated: true) {
                
                self?.setRootHomeVC()
            }
            
        }
        
        
        view.present(vc, animated: true, completion: nil)
        
    }
    
    
    func confirmPopup(){
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutAlertViewController") as? LogoutAlertViewController else{return}
        vc.setTransition()
        vc.titleLblData = "Message"
        vc.messageLblData = "Please note this amount will be taken from your stored payment method immediately."
        vc.okBtnTitle = "Confirm"
        vc.okCompletionHandler = { [weak self] in
            self?.dismiss(animated: false) {
                
                self?.addmoneyAPICall()
                
            }
            
        }
        
        vc.cancelCompletionHandler = { [weak self] in
            self?.dismiss(animated: true, completion: nil)
        }
        
        self.present(vc, animated: true, completion: nil)
    }
    
    func eKYCProcess(){
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutAlertViewController") as? LogoutAlertViewController else{return}
        vc.setTransition()
        vc.titleLblData = "Message"
        vc.messageLblData = "By adding this amount your Global Balance will exceed £100 and you will be required to complete the ‘Know your customer’ process, it’s free and only takes a couple of minutes. Otherwise please amend your deposit to maintain a balance below £100."
        vc.okBtnTitle = "Proceed"
        vc.okCompletionHandler = { [weak self] in
            self?.dismiss(animated: false) {
                
                guard let eKYCVc = self?.storyboard?.instantiateViewController(withIdentifier: "KYCProcessViewController") as? KYCProcessViewController else{return}
                eKYCVc.showleftArrow = true
                self?.pushViewController(viewController: eKYCVc)

            }
            
            
        }
        vc.cancelCompletionHandler = { [weak self] in
            self?.dismiss(animated: true, completion: nil)
        }
        
        self.present(vc, animated: true, completion: nil)
    }
    
    
    func addmoneyAPICall(){
        
        SVProgressHUD.show()
        let params:[String: Any] = ["user_id": UserDefaults.standard.string(forKey: UserDefaultConstants.UserID.rawValue) ?? "",
                                    "category_id": categoryId,
                                    "amount": addAmountTxtField.text ?? "",
                                    "cardId": cardSelectId]
        DataService.sharedInstance.postApiCall(endpoint: constantApis.addmoney, params: params) { (response, error) in
            
            SVProgressHUD.dismiss()
            
            print(response)
            if response != nil{
                
                let transactionId = response?["transactionId"] as? String ?? ""
                let merchentToken =  response?["merchentToken"] as? String ?? ""
                
                print(transactionId)
                
                UserDefaults.standard.setValue(transactionId, forKey: UserDefaultConstants.Transaction_Id.rawValue) //merchentToken
                UserDefaults.standard.setValue(merchentToken, forKey: UserDefaultConstants.MerchentToken.rawValue)
                
                
                
                if let result = response?["result"] as? [String: Any]{
                    
                    if let body = result["body"] as? [String: Any]{
                        
                        let paymentURL = body["nextURL"] as? String ?? ""
                        
                        let string = paymentURL
                        
                        if string.contains("http://") {
                            
                            
                            let vc = PaymentViewController.instantiateFromAppStoryboard(appStoryboard: .main)
                            
                            vc.urlData = string
                            vc.paymentType = "onetime"
                            vc.PaymentCardId = self.cardSelectId
                            vc.transsctionsAmount = self.addAmountTxtField.text ?? ""
                            vc.catIdForUpdateTransctonStatus = self.catIdForUpdateTransctonStatus
                            
                            self.pushViewController(viewController: vc)
                            
                        } else {
                            
                            self.updateTransactionStatus()
                            
                        }
                      
                        
                        
                    }
                    
                }
                
                
            }
            else {
                self.customAlertDismiss(title: "Message", message: "Something went wrong!")
            }
            
        }

    }
    
    
    func updateTransactionStatus(){
        
        SVProgressHUD.show()
        let params:[String: Any] = ["user_id": UserDefaults.standard.string(forKey: UserDefaultConstants.UserID.rawValue) ?? "",
                                    "transaction_id": UserDefaults.standard.string(forKey: UserDefaultConstants.Transaction_Id.rawValue) ?? "", //UserDefaults.standard.string(forKey: "SAVETRANSACTIONID") ?? "",
                                    "type" : "onetime",
                                    "merchentToken" : UserDefaults.standard.string(forKey: UserDefaultConstants.MerchentToken.rawValue) ?? "",
                                    "amount" : self.addAmountTxtField.text ?? "",
                                    "cardId" : cardSelectId,
                                    "catId" : catIdForUpdateTransctonStatus
                                    ]
        
        print(params)
        
        DataService.sharedInstance.postApiCall(endpoint: constantApis.updateTransactionStatus, params: params) { (response, error) in
            
            SVProgressHUD.dismiss()
            
            print(response)
            
            if response != nil{
                
               // HomeViewController.setRootVC()
                
                
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: SwaprificeViewController.self) {
                        self.navigationController!.popToViewController(controller, animated: true)
                        break
                    }
                }
                
            }
            else {
                self.customAlertDismiss(title: "Message", message: "Something went wrong!")
            }
            
        }
        
    }

    
}



//MARK: - EXTENSION FOR TABLE VIEW DELEGATES & DATASOURCE

extension AddAmountSwaprificeVC : UITableViewDelegate, UITableViewDataSource  {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getCardModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddSwaprificeTv_Cell") as! AddSwaprificeTv_Cell
       
        cell.cardLbl.text = getCardModel[indexPath.row].cardName
        
        cell.selectionStyle = .none
        
        
        if TapCell == false {
            
            if getCardModel[indexPath.row].primaryCard == true {
                
                cell.circleImg.image = UIImage.init(named: "Ic_SelectCircle")
                cardSelectId = getCardModel[indexPath.row]._id
                 print("cardSelectId===>>>> \(cardSelectId)")
               
                
            } else {
                cell.circleImg.image = UIImage.init(named: "Ic_UnselectCircle-1")
            }
            
        } else {
            
            
            if selectedRows.contains(indexPath) {
      
                cell.circleImg.image = UIImage.init(named: "Ic_SelectCircle")
               
           

            } else {
               
                cell.circleImg.image = UIImage.init(named: "Ic_UnselectCircle-1")
     
            }
            
        }
        
    


        cell.backgroundColor = UIColor.clear
        
        // Button Pressed inside cell
        
        cell.btnEditCardPressed = {
            
            print("Botton pressed")
            
            self.cardId = self.getCardModel[indexPath.row]._id
            self.cardName = self.getCardModel[indexPath.row].cardName
     
           self.popupEditCardNameAlert()
            
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    
        
        let selectedIndexPath = IndexPath(row: indexPath.row, section: 0)
        
        self.selectedRows.removeAll()
        if self.selectedRows.contains(selectedIndexPath) {
            
            self.selectedRows.remove(at: self.selectedRows.index(of: selectedIndexPath)!)
            
        } else {
            
            self.TapCell = true
            
            self.selectedRows.append(selectedIndexPath)
            cardSelectId = getCardModel[indexPath.row]._id
            print("cardSelectId===>>>> \(cardSelectId)")

        }
        
        myTableView.reloadData()
    }
 
    
    
    
    
    func GetCard_Api(){
        

        self.addPeculiPopup(view: self.view)
        
        DataService.sharedInstance.getCard_Api{ (resultDict, errorMsg) in
            
            self.hidePeculiPopup(view: self.view)
            
            print(resultDict as Any)
            
            if errorMsg == nil {
                
                if let result = resultDict?["result"] as? [NSDictionary] {
                   
                    self.getCardModel.removeAll()
                    
                    for allData in result {
                        let _id = allData["_id"] as? String ?? ""
                        
                        let isEnable = allData["isEnable"] as? Bool ?? true
                        
                        let cardName =  allData["cardName"] as? String ?? ""
                        let primaryCard = allData["primaryCard"] as? Bool ?? true
                        let merchentToken = allData["merchentToken"] as? String ?? ""
                        let userId = allData["userId"] as? String ?? ""
                        let createdDate = allData["createdDate"] as? String ?? ""
                        let updatedDate = allData["updatedDate"] as? String ?? ""
                        let __v = allData["__v"] as? Int ?? 0
                       
                        self.getCardModel.append(GetCArd_ModelStructure.init(isEnable: isEnable, _id: _id, cardName: cardName, primaryCard: primaryCard, merchentToken: merchentToken, userId: userId, createdDate: createdDate, updatedDate: updatedDate, __v: __v))
                    }

                    print("getCardModel========>>>>>>>>>>>>>> \(self.getCardModel)")
                        self.myTableView.reloadData()
                    
                       } else {
                           
                           let message = resultDict?["message"] as? String
                             Toast.show(message: message ?? "Something went wrong", controller: self)
               }
                    
            }
                
            else {
                
                Toast.show(message: "Something went wrong", controller: self)
                 //let message = result["message"] as? String
                 // Toast.show(message: message ?? "Something went wrong", controller: self)
               
            }
        }
//            self.hidePeculiPopup(view: self.view)
//            Toast.show(message: "Something went wrong", controller: self)
    }

    
  
    
    
}
