//
//  PeculiOfferViewController.swift
//  Peculi
//
//  Created by apple on 22/02/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import UIKit
import SVProgressHUD
import DropDown


protocol CheckIfBack: class {
    func backAction(_ flag: Int)
}


class PeculiOfferViewController: UIViewController {
    
    //MARK: - Outlets
    
    @IBOutlet var superView: UIView!
    @IBOutlet weak var superChildView: UIView!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var backNavigationButton: UIButton!
    @IBOutlet weak var navigationTitleLbl: UILabel!
    @IBOutlet weak var peculiOfferTableView: UITableView!
    @IBOutlet weak var threeDotMenuButton: UIButton!
    

    //MARK: - Variable
    
    var senderTag = Int()
    var navigationName = String()
    var setPrice = String()
    var allTiers = [AllTiersModel]()
    var potImage = String()
    var targetValue = String()
    var slugName = String()
    var catgoryId = String()
    var categoryNameId = String()
    var categoryName = String()
    var viewOffer = true
    var price = Double()
    var progressValue = Double()
    var tier1 = String()
    var tier2 = String()
    var tier3 = String()
    var dropDwn = DropDown()
    let dropdwnArray = ["Edit", "Delete"]
    var voucherTransactionData = [VoucherTransactionModel]()
    weak var delegate: CheckIfBack?
    var lastSectionHeight = CGFloat()
    var transactionTableHeight:CGFloat = 70
    var catIdForUpdateTransctonStatus = ""
    var premiumStatus = ""
    
    //MARK: - ViewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(setPrice)
        registerTableViewCell()
        getTierRange()
        
        
        
        
        if UserDefaults.standard.value(forKey: UserDefaultConstants.showPopup_PeculiOfferScreen.rawValue) as? String ?? "" == "0" {
            
            UserDefaults.standard.set("1", forKey: UserDefaultConstants.showPopup_PeculiOfferScreen.rawValue)
            self.peculiOffer_ScreenPopUp()
        }
    }
    
    // MARK: - RegisterTablViewCell
    
    func registerTableViewCell(){
        
        peculiOfferTableView.register(UINib(nibName: "ProgressViewTableViewCell", bundle: nil), forCellReuseIdentifier: "ProgressViewTableViewCell")
        
        peculiOfferTableView.register(UINib(nibName: "PeculiViewOfferTableViewCell", bundle: nil), forCellReuseIdentifier: "PeculiViewOfferTableViewCell")
        
        peculiOfferTableView.register(UINib(nibName: "PeculiTransactionsTableViewCell", bundle: nil), forCellReuseIdentifier: "PeculiTransactionsTableViewCell")

        peculiOfferTableView.register(UINib(nibName: "VoucherTransactionsTableViewCell", bundle: nil), forCellReuseIdentifier: "VoucherTransactionsTableViewCell")
        
    }
    
    
    //PopUp Function
    
    func peculiOffer_ScreenPopUp() {
            
            let sb = UIStoryboard(name: "Main", bundle:nil)
            let addCardAlert = sb.instantiateViewController(withIdentifier: "peculioffer_ScreenPopUp") as! peculioffer_ScreenPopUp
            //     customAlert.textFieldData = navName.capitalizingFirstLetter()
            
            //addCardAlert.categoryId = CategoryInfo[0].userData.categoryDetails._id
        
       
            addCardAlert.okCompletionHandler = {
                print("call Add Card PopUp")
                
                self.dismiss(animated: true, completion: nil)
       
            }
            
            addCardAlert.providesPresentationContextTransitionStyle = true
            addCardAlert.definesPresentationContext = true
            addCardAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            addCardAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            
            self.present(addCardAlert, animated: true, completion: nil)
        }
    
    
    //MARK: - ViewWillAppear
    
    override func viewWillAppear(_ animated: Bool) {
        
        navigationController?.navigationBar.isHidden = true
        navigationTitleLbl.text = String(format: "%@ Pot", navigationName.capitalizingFirstLetter()).maxLength(length: 35)
        
        //voucherTransactionsByCatIdApiCall()
        navigationView.layer.masksToBounds = false
        navigationView.layer.shadowRadius = 3.0
        navigationView.layer.shadowOpacity = 0.6
        navigationView.layer.shadowColor = UIColor.clear.cgColor //#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        navigationView.layer.shadowOffset = CGSize(width: 0 , height: 3)
        peculiOfferTableView.backgroundColor = #colorLiteral(red: 0.2117647059, green: 0.168627451, blue: 0.3098039216, alpha: 1)
        
        ///setup round progress value
        let totalAmount  = (setPrice as NSString).doubleValue
        price = Double(setPrice) ?? 0.0
        if price == 0.0 || totalAmount < (tier1 as NSString).doubleValue{
            self.progressValue = 0
        }
        
        else if totalAmount >= (tier1 as NSString).doubleValue && totalAmount < (tier2 as NSString).doubleValue {
            let denominator = (tier2 as NSString).doubleValue - (tier1 as NSString).doubleValue
            let numerator = self.price - (tier1 as NSString).doubleValue
            
            self.progressValue =  ((numerator * 100) / denominator) / 100
        }
        
        else if totalAmount >= (tier2 as NSString).doubleValue && totalAmount < (tier3 as NSString).doubleValue {
            
            let denominator = (tier3 as NSString).doubleValue - (tier2 as NSString).doubleValue
            let numerator = self.price - (tier2 as NSString).doubleValue
            
            self.progressValue =  ((numerator * 100) / denominator) / 100
        }
        else {
            self.progressValue =  1
        }
    }
    
    
    func popUpTransferPotAlert() {
            
            let sb = UIStoryboard(name: "Main", bundle:nil)
            let addTranasactionsAlert = sb.instantiateViewController(withIdentifier: "TransferModePopUp_VC") as! TransferModePopUp_VC
        
       //     customAlert.textFieldData = navName.capitalizingFirstLetter()
        //addCardAlert.categoryId = CategoryInfo[0].userData.categoryDetails._id
        
        if self.price == 0.00 {
            Toast.show(message: "Insufficient funds in \(navigationName)", controller: self)
            //self.dismiss(animated: true, completion: nil)
            
        } else {
                        addTranasactionsAlert.okCompletionHandler = {
                            print("call okCompletionHandler ")
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectedPot_VC") as! SelectedPot_VC
                            vc.to_Id = self.catIdForUpdateTransctonStatus
                            vc.toCategioryId = self.categoryNameId
                            vc.amountSet = self.price
                            
                           
                            self.navigationController?.pushViewController(vc, animated: true)
                            
                        }
            
            
            addTranasactionsAlert.okCompletionHandler1 = {
                
                print("call okCompletionHandler ")
              
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PotToBankTransfer_VC") as! PotToBankTransfer_VC
                vc.to_Id = self.catIdForUpdateTransctonStatus
                vc.toCategioryId = self.categoryNameId
                vc.amountSet = self.price
                vc.potnametext = String(format: "%@ Pot", self.navigationName)
                vc.potimage = self.potImage
               
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
 
        }
       
   
        addTranasactionsAlert.providesPresentationContextTransitionStyle = true
        addTranasactionsAlert.definesPresentationContext = true
        addTranasactionsAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        addTranasactionsAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            
            self.present(addTranasactionsAlert, animated: true, completion: nil)
    }
    
    func popUpTransactionHistoryAlert() {
            
            let sb = UIStoryboard(name: "Main", bundle:nil)
            let addTranasactionsAlert = sb.instantiateViewController(withIdentifier: "potTransactionHistoryPopUP_VC") as! potTransactionHistoryPopUP_VC
        
       //   customAlert.textFieldData =    navName.capitalizingFirstLetter()
        //addTranasactionsAlert.clickTransctionsHistory = "1"
        
        addTranasactionsAlert.voucherData = voucherTransactionData
        addTranasactionsAlert.providesPresentationContextTransitionStyle = true
        addTranasactionsAlert.definesPresentationContext = true
        addTranasactionsAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        addTranasactionsAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            
            self.present(addTranasactionsAlert, animated: true, completion: nil)

    }
    
    
    
    
    //MARK: - UIButton Action
    
    @IBAction func backNavigationButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func settingBtnAction(_ sender: Any) {
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyPotDetailsViewController") as? MyPotDetailsViewController else{return}
        vc.navName = navigationName
        vc.potCatId = categoryNameId
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    
    ///transaction history api call
   
    func voucherTransactionsByCatIdApiCall(){
        
        SVProgressHUD.show()
        let userId = UserDefaults.standard.value(forKey: UserDefaultConstants.UserID.rawValue) as! String
        
        DataService.sharedInstance.voucherTransactionsByCatIdApiCall(userId: userId, categoryId: self.categoryNameId) { (response, error) in
            
            print(response)
            
            if response != nil {
                
                SVProgressHUD.dismiss()
                if let code = response?["code"] as? String {
                    
                    if code == "200" {
                        
                        if let result = response?["result"] as? NSArray {
                            
                            let listedTransactions = result as! NSArray
                            var voucherDict = Dictionary<String,Any>()
                            self.voucherTransactionData.removeAll()
                            
                           for index in listedTransactions {
                               var sessionData = VoucherTransactionModel()
                                voucherDict = index as! Dictionary<String,Any>
                                sessionData.deserilize(values: voucherDict)
                                self.voucherTransactionData.append(sessionData)
                            }
                            
                            self.peculiOfferTableView.reloadData()
                        }
                    }
                    else {
                        //                        AlertView.simpleViewAlert(title: "Error", response?["message"] as! String, self)
                    }
                }
                
            }
            else {
                
                SVProgressHUD.dismiss()
                // AlertView.simpleViewAlert(title: "Error","Somthing went wrong", self)
            }

        }
        
    }
    
    
    func pushToSetTargetVC(){
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SetTargetForPotViewController") as! SetTargetForPotViewController
                
        vc.potName = String(format: "%@ Pot", navigationName)//String(format: "%@ Pot",CategoryInfo[0].userData.categoryDetails.newName)
        
        vc.categoryId = categoryNameId
        vc.catIdForUpdateTransctonStatus = catIdForUpdateTransctonStatus
        GlobalVariables.moveAfterTransactions = "1"

        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    func pushToAddAmountVC(){
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddMoneyViewController") as! AddMoneyViewController
                
        vc.potName = String(format: "%@ Pot", navigationName)//String(format: "%@ Pot",CategoryInfo[0].userData.categoryDetails.newName)
        vc.potImage = potImage// String(format: "%@%@", potImage)
        vc.categoryId = categoryNameId
        vc.catIdForUpdateTransctonStatus = catIdForUpdateTransctonStatus
        GlobalVariables.moveAfterTransactions = "1"
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
//
//    .catId = categoryNameId
//    vc.potnametext = String(format: "%@ Pot", navigationName)
//                   vc.potimage = potImage

    
}


//MARK: - UITableView Delegate & DataSource Method

extension PeculiOfferViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 1:
            if allTiers.count == 0 {
                return 0
            } else {
                return allTiers.count + 1
            }
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if allTiers.count > 0 {
            switch indexPath.section {
            case 0:
                let progressCell = Bundle.main.loadNibNamed("ProgressViewTableViewCell", owner: self, options: nil)?.first as! ProgressViewTableViewCell
                
                let titleString = String(format: "%@ Pot %@%@ target", navigationName.capitalizingFirstLetter(),GlobalVariables.currency, targetValue)
                
                let addString = NSMutableAttributedString(string: titleString)
                
                addString.setColorForText("\(GlobalVariables.currency)\(targetValue)", with: #colorLiteral(red: 1, green: 0.6588235294, blue: 0.231372549, alpha: 1))
                
                let value  = (setPrice as NSString).doubleValue
                
                if value == 0.00 || value < (tier1 as NSString).doubleValue {

                    progressCell.tierNameLbl.text = "Tier 1"
                    progressCell.toValue = progressValue
                }
                
                else if value >= (tier1 as NSString).doubleValue && value < (tier2 as NSString).doubleValue {
                    //progressCell.progressView.progressAnimation(duration: 0)
                    progressCell.tierNameLbl.text = "Tier 1"
                    progressCell.toValue = progressValue
                    
                } else if value >= (tier2 as NSString).doubleValue && value < (tier3 as NSString).doubleValue {
                    
                    progressCell.tierNameLbl.text = "Tier 2"
                    progressCell.toValue = progressValue
                    
                } else {
                    
                    progressCell.tierNameLbl.text = "Tier 3"
                    progressCell.toValue = progressValue
                }

                progressCell.targetValueLbl.text = "\(GlobalVariables.currency)" + "\(setPrice)"
                progressCell.potNameLbl.attributedText = addString
                progressCell.handleProgressView()
                
               
                progressCell.btnAddMoney.isHidden = false
                progressCell.btnAddNewTarget.isHidden = false
                
                progressCell.iconBtnAddMony.isHidden = false
                progressCell.iconBtnAddTarget.isHidden = false
                
                progressCell.lblBtnAddMony.isHidden = false
                progressCell.lblBtnAddTarget.isHidden = false
                
                //cell btn Action
                
                progressCell.btnAddNewTargetPressed = {
                    print("Mohit")

          self.pushToSetTargetVC()
          
                }
                
                progressCell.btnAddMoneyPressed = {
                    print("Rana")
                    self.pushToAddAmountVC()
                }

                // progressCell.toValue = progressValue
                
                return progressCell
                
            case 1:
                let offerCell = Bundle.main.loadNibNamed("PeculiViewOfferTableViewCell", owner: self, options: nil)?.first as! PeculiViewOfferTableViewCell
                
                let value  = (setPrice as NSString).doubleValue
                
                if premiumStatus == "true" {
                    
                    print(premiumStatus)
                    
                    switch indexPath.row {
                        
                    case 0:
                        
                        offerCell.tierNameLbl.text = allTiers[indexPath.row].name
                        let addString = NSMutableAttributedString(string: "Starts at \(GlobalVariables.currency)\(tier1)")
                        addString.setColorForText("Starts at", with: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
                        offerCell.completeStatusLbl.attributedText = addString
                        
                    case 1:
                        offerCell.tierNameLbl.text = allTiers[indexPath.row].name
                        
                        let addString = NSMutableAttributedString(string: "Starts at \(GlobalVariables.currency)\(tier2)")
                        addString.setColorForText("Starts at", with: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
                        offerCell.completeStatusLbl.attributedText = addString
                      
                    case 2:
                        offerCell.tierNameLbl.text = allTiers[indexPath.row].name
                        
                        let addString = NSMutableAttributedString(string: "Starts at \(GlobalVariables.currency)\(tier3)")
                        addString.setColorForText("Starts at", with: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
                        offerCell.completeStatusLbl.attributedText = addString
                        
                    case 3:
                        
                        offerCell.tierNameLbl.text = "Premium"
                        
                        let addString = NSMutableAttributedString(string: "The Best Rewards – any amount")
                        addString.setColorForText("The Best Rewards – any amount", with: #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1))
                        
                        offerCell.completeStatusLbl.attributedText = addString
//                        offerCell.viewOfferButton.setTitle("", for: .normal)
//                        offerCell.viewOfferButton.backgroundColor = #colorLiteral(red: 1, green: 0.6000702977, blue: 0.2412445545, alpha: 1)
                        
                    default:
                        break
                    }
                    
                } else {
                    
                    switch indexPath.row {
                        
                        
                    case 0:
                        if value == 0.00 {
                            
                            offerCell.tierNameLbl.text = allTiers[indexPath.row].name
                            let addString = NSMutableAttributedString(string: "Starts at \(GlobalVariables.currency)\(tier1)")
                            addString.setColorForText("Starts at", with: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
                            offerCell.completeStatusLbl.attributedText = addString
                            offerCell.viewOfferButton.setTitle("Preview Offer", for: .normal)
                            offerCell.viewOfferButton.backgroundColor = #colorLiteral(red: 0.8980392157, green: 0.4392156863, blue: 0.4941176471, alpha: 1)
                            offerCell.viewOfferButton.isHidden = false
                        }
                        
                        else if value >= (tier1 as NSString).doubleValue {
                            
                            offerCell.tierNameLbl.text = allTiers[indexPath.row].name
                            let addString = NSMutableAttributedString(string: "Starts at \(GlobalVariables.currency)\(tier1)")
                            addString.setColorForText("Starts at", with: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
                            offerCell.completeStatusLbl.attributedText = addString
                            
                        } else {

                            offerCell.tierNameLbl.text = allTiers[indexPath.row].name
                            let addString = NSMutableAttributedString(string: "Starts at \(GlobalVariables.currency)\(tier1)")
                            addString.setColorForText("Starts at", with: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
                            offerCell.completeStatusLbl.attributedText = addString
                            offerCell.viewOfferButton.setTitle("Preview Offer", for: .normal)
                            offerCell.viewOfferButton.backgroundColor = #colorLiteral(red: 0.8980392157, green: 0.4392156863, blue: 0.4941176471, alpha: 1)
                            offerCell.viewOfferButton.isHidden = false
                        }
                    case 1:
                        if value == 0.00 {

                            offerCell.tierNameLbl.text = allTiers[indexPath.row].name

                            let addString = NSMutableAttributedString(string: "Starts at \(GlobalVariables.currency)\(tier2)")
                            addString.setColorForText("Starts at", with: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
                            offerCell.completeStatusLbl.attributedText = addString
                            offerCell.viewOfferButton.setTitle("Preview Offer", for: .normal)
                            offerCell.viewOfferButton.backgroundColor = #colorLiteral(red: 0.8980392157, green: 0.4392156863, blue: 0.4941176471, alpha: 1)
                            offerCell.viewOfferButton.isHidden = false
                        }
                        
                        else if value >= (tier2 as NSString).doubleValue {
                            offerCell.tierNameLbl.text = allTiers[indexPath.row].name
                            
                            let addString = NSMutableAttributedString(string: "Starts at \(GlobalVariables.currency)\(tier2)")
                            addString.setColorForText("Starts at", with: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
                            offerCell.completeStatusLbl.attributedText = addString
                            
                        } else {

                            offerCell.tierNameLbl.text = allTiers[indexPath.row].name
                            
                            let addString = NSMutableAttributedString(string: "Starts at \(GlobalVariables.currency)\(tier2)")
                            
                            addString.setColorForText("Starts at", with: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
                            offerCell.completeStatusLbl.attributedText = addString
                            offerCell.viewOfferButton.setTitle("Preview Offer", for: .normal)
                            offerCell.viewOfferButton.backgroundColor = #colorLiteral(red: 0.8980392157, green: 0.4392156863, blue: 0.4941176471, alpha: 1)
                            offerCell.viewOfferButton.isHidden = false
                        }
                    case 2:
                        if value == 0.00 {
                            
                            offerCell.tierNameLbl.text = allTiers[indexPath.row].name
                            
                            let addString = NSMutableAttributedString(string: "Starts at \(GlobalVariables.currency)\(tier3)")
                            addString.setColorForText("Starts at", with: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
                            offerCell.completeStatusLbl.attributedText = addString
                            
                            offerCell.viewOfferButton.setTitle("Preview Offer", for: .normal)
                            offerCell.viewOfferButton.backgroundColor = #colorLiteral(red: 0.8980392157, green: 0.4392156863, blue: 0.4941176471, alpha: 1)
                            offerCell.viewOfferButton.isHidden = false
                        }
                        else if value >= (tier3 as NSString).doubleValue {
                            offerCell.tierNameLbl.text = allTiers[indexPath.row].name
                            
                            let addString = NSMutableAttributedString(string: "Starts at \(GlobalVariables.currency)\(tier3)")
                            addString.setColorForText("Starts at", with: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
                            offerCell.completeStatusLbl.attributedText = addString
                            
                        } else {
                            
                            offerCell.tierNameLbl.text = allTiers[indexPath.row].name
                            let addString = NSMutableAttributedString(string: "Starts at \(GlobalVariables.currency)\(tier3)")
                            addString.setColorForText("Starts at", with: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
                            offerCell.completeStatusLbl.attributedText = addString
                            
                            offerCell.viewOfferButton.setTitle("Preview Offer", for: .normal)
                            offerCell.viewOfferButton.backgroundColor = #colorLiteral(red: 0.8980392157, green: 0.4392156863, blue: 0.4941176471, alpha: 1)
                            offerCell.viewOfferButton.isHidden = false
                        }
                    case 3:
                        offerCell.tierNameLbl.text = "Premium"
                        
                        let addString = NSMutableAttributedString(string: "The Best Rewards – any amount")
                        addString.setColorForText("The Best Rewards – any amount", with: #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1))
                        
                        offerCell.completeStatusLbl.attributedText = addString
                        offerCell.viewOfferButton.setTitle("Get Premium", for: .normal)
                        offerCell.viewOfferButton.backgroundColor = #colorLiteral(red: 1, green: 0.6000702977, blue: 0.2412445545, alpha: 1)
                        offerCell.viewOfferButton.isHidden = false
                    default:
                        break
                    }
                }
    
                offerCell.viewOfferButton.tag = indexPath.row
                offerCell.viewOfferButton.addTarget(self, action: #selector(self.viewOfferButtonTapped(sender:)), for: .touchUpInside)
                return offerCell
                
            case 2:
                let transactionCell = Bundle.main.loadNibNamed("PeculiTransactionsTableViewCell", owner: self, options: nil)?.first as! PeculiTransactionsTableViewCell
                
                transactionCell.btnTransferPressed = {
                    
                    print("btn preesed Transfer")

                    self.popUpTransferPotAlert()
                    
                }
                
                transactionCell.btnTransactionHistryPressed = {
                    
                    print ("btn preesed TransactionHistry")

                    self.popUpTransactionHistoryAlert()
                    
                }

                transactionCell.heightDelegate = self
                transactionCell.voucherData = voucherTransactionData
                transactionCell.viewController = self

                return transactionCell
            default:
                let cell = UITableViewCell()
                cell.backgroundColor = UIColor.clear //#colorLiteral(red: 0.2117647059, green: 0.1725490196, blue: 0.3058823529, alpha: 1) //#colorLiteral(red: 0.07450980392, green: 0.1019607843, blue: 0.1529411765, alpha: 1)
                return cell
            }
        } else {
            
            let cell = UITableViewCell()
            cell.backgroundColor = #colorLiteral(red: 0.2117647059, green: 0.1725490196, blue: 0.3058823529, alpha: 1)
            
            return cell
        }
        
    }
    
    
    /****view offer button action */
    @objc func viewOfferButtonTapped(sender: UIButton){
        
        let value = (setPrice as NSString).integerValue
        
        let buttonPosition = sender.convert(CGPoint.zero, to: self.peculiOfferTableView)
        let indexPath = self.peculiOfferTableView.indexPathForRow(at:buttonPosition)
        let cell = self.peculiOfferTableView.cellForRow(at: indexPath!) as! PeculiViewOfferTableViewCell
        switch sender.tag {
        case 0:
            if value >= 0 && value <= 100 {
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PotsDetailViewController") as! PotsDetailViewController
                if cell.viewOfferButton.titleLabel?.text == "Preview Offer" {
                    vc.offerType = "Preview Offer"
                } else {
                    vc.offerType = "View Offer"
                }
                
                vc.voucherDiscount = tier1
                vc.potnametext = String(format: "%@ Pot", navigationName)
                vc.potimage = potImage
                vc.potPrice = setPrice
                vc.slugName = slugName
                vc.catId = categoryNameId
                vc.tierName = "tier1"
                vc.offer = true
                vc.premiumStatus = premiumStatus
                vc.catIdForUpdateTransctonStatus = catIdForUpdateTransctonStatus
                self.navigationController?.pushViewController(vc, animated: true)
                
            } else {
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PotsDetailViewController") as! PotsDetailViewController
                
                if cell.viewOfferButton.titleLabel?.text == "Preview Offer" {
                    vc.offerType = "Preview Offer"
                } else {
                    vc.offerType = "View Offer"
                }
                vc.potnametext = String(format: "%@ Pot", navigationName)
                vc.potimage = potImage
                vc.potPrice = setPrice
                vc.slugName = slugName
                vc.voucherDiscount = tier1
                vc.catId = categoryNameId
                vc.tierName = "tier1"
                vc.offer = false
                vc.premiumStatus = premiumStatus
                vc.catIdForUpdateTransctonStatus = catIdForUpdateTransctonStatus
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
        case 1:
            if value >= 101 && value <= 200 {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PotsDetailViewController") as! PotsDetailViewController
                if cell.viewOfferButton.titleLabel?.text == "Preview Offer"{
                    vc.offerType = "Preview Offer"
                }else{
                    vc.offerType = "View Offer"
                }
                vc.potnametext = String(format: "%@ Pot", navigationName)
                vc.potimage = potImage
                vc.potPrice = setPrice
                vc.slugName = slugName
                vc.voucherDiscount = tier2
                vc.catId = categoryNameId
                vc.tierName = "tier2"
                vc.offer = true
                vc.premiumStatus = premiumStatus
                vc.catIdForUpdateTransctonStatus = catIdForUpdateTransctonStatus
                self.navigationController?.pushViewController(vc, animated: true)
                
            } else {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PotsDetailViewController") as! PotsDetailViewController
                if cell.viewOfferButton.titleLabel?.text == "Preview Offer"{
                    vc.offerType = "Preview Offer"
                    
                } else {
                    
                    vc.offerType = "View Offer"
                }
                
                vc.potnametext = String(format: "%@ Pot", navigationName)
                vc.potimage = potImage
                vc.potPrice = setPrice
                vc.slugName = slugName
                vc.catId = categoryNameId
                vc.tierName = "tier2"
                vc.voucherDiscount = tier2
                vc.offer = false
                vc.premiumStatus = premiumStatus
                vc.catIdForUpdateTransctonStatus = catIdForUpdateTransctonStatus
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
        case 2:
            if value > 200 {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PotsDetailViewController") as! PotsDetailViewController
                if cell.viewOfferButton.titleLabel?.text == "Preview Offer"{
                    vc.offerType = "Preview Offer"
                } else {
                    vc.offerType = "View Offer"
                }
                vc.potnametext = String(format: "%@ Pot", navigationName)
                vc.potimage = potImage
                vc.potPrice = setPrice
                vc.slugName = slugName
                vc.voucherDiscount = tier3
                vc.catId = categoryNameId
                vc.tierName = "tier3"
                vc.offer = true
                vc.premiumStatus = premiumStatus
                vc.catIdForUpdateTransctonStatus = catIdForUpdateTransctonStatus
                self.navigationController?.pushViewController(vc, animated: true)
                
            } else {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PotsDetailViewController") as! PotsDetailViewController
                if cell.viewOfferButton.titleLabel?.text == "Preview Offer"{
                    vc.offerType = "Preview Offer"
                }else{
                    vc.offerType = "View Offer"
                }
                vc.potnametext = String(format: "%@ Pot", navigationName)
                vc.potimage = potImage
                vc.potPrice = setPrice
                vc.slugName = slugName
                vc.voucherDiscount = tier3
                vc.catId = categoryNameId
                vc.tierName = "tier3"
                vc.offer = false
                vc.premiumStatus = premiumStatus
                vc.catIdForUpdateTransctonStatus = catIdForUpdateTransctonStatus
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
        case 3:
            
            if premiumStatus == "true" {
               // popVc()
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PotsDetailViewController") as! PotsDetailViewController
                
            vc.offerType = "View Offer"
            vc.potnametext = String(format: "%@ Pot", navigationName)
            vc.potimage = potImage
            vc.potPrice = setPrice
            vc.slugName = slugName
            vc.voucherDiscount = tier3
            vc.catId = categoryNameId
            vc.tierName = "premium"
            vc.offer = false
            vc.premiumStatus = premiumStatus
            vc.catIdForUpdateTransctonStatus = catIdForUpdateTransctonStatus
            self.navigationController?.pushViewController(vc, animated: true)
  
                
            } else {
                
//                if "" == "" {
//
//                }
                
                if  checkIfGuest() == true {
                    
                    showAlert()
                    
                } else {
                    
                    showSubscribePopUp()
                }
                
                
               
                
                
            }
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PremiumPlanViewController") as! PremiumPlanViewController
//            self.navigationController?.pushViewController(vc, animated: true)
           
            
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
      //  return UITableView.automaticDimension
        if indexPath.section == 2 {

            print(transactionTableHeight)

            return  transactionTableHeight + 90 //158
    
        } else {
            
            return UITableView.automaticDimension
        }
        
        
}
    
    
    func showSubscribePopUp(){
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "swipeDlt_Popup_VC") as? swipeDlt_Popup_VC else {
            return
        }
        
        vc.setTransition()
        vc.titleLblData = ""
        vc.comingVc = "0"
        vc.messageLblData = "You need to Subscribe to unlock this feature."
        vc.okCompletionHandler = { [weak self] in
            
            self?.dismiss(animated: true) {
                
                let vc = self?.storyboard?.instantiateViewController(withIdentifier: "ChoosePremiumPlan_VC") as! ChoosePremiumPlan_VC
                self?.navigationController?.pushViewController(vc, animated: true)
                
            }
            
        }
        vc.cancelCompletionHandler = { [weak self] in
            
            self?.dismiss(animated: true, completion: nil)
        }
        
        self.present(vc, animated: true, completion: nil)
        
    }
    

    func showAlert(){
        
        var dialogMessage = UIAlertController(title: "Confirm", message: "Please signup first!", preferredStyle: .alert)
        // Create OK button with action handler
        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
            print("Ok button tapped")
            
           SignUpVc.setRootVC()
            
//            for controller in self.navigationController!.viewControllers as Array {
//                if controller.isKind(of: SignUpVc.self) {
//                    self.navigationController!.popToViewController(controller, animated: true)
//                    break
//                }
//            }

        })
        
        // Create Cancel button with action handlder
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
            print("Cancel button tapped")
        }
        //Add OK and Cancel button to an Alert object
        dialogMessage.addAction(ok)
        dialogMessage.addAction(cancel)
        // Present alert message to user
        self.present(dialogMessage, animated: true, completion: nil)
        
    }
    
}
extension PeculiOfferViewController {

    // get tier values API call
  
    func getTierRange() {
        SVProgressHUD.show()
        DataService.sharedInstance.getTiersRange() { (resultDict, errorMsg) in
            SVProgressHUD.dismiss()
            print(resultDict as Any)
            
            if errorMsg == nil {
                
                if let result = resultDict?["result"] as? [NSDictionary] {
                    for allData in result {
                        
                        let max = allData["max"] as? String
                        let min = allData["min"] as? String
                        let name = allData["name"] as? String
                        
                        self.allTiers.append(AllTiersModel.init(max: max ?? "", min: min ?? "", name: name ?? "", percentDiscount: "", denominationType: "", logoImage: "", denominationData: [""], description: "", currencyCode:"",code:"",id :"", termsAndConditon: "", voucher_Id: "", voucherDiscountValue: 0.00))
                    }
                    
                    self.peculiOfferTableView.reloadData()
                    
                }
                else {

                    if let message = resultDict?["message"] as? String {
                        //self.presentAlert(withTitle: "", message: message)
                    }
                }
                
            } else {
                // Toast.show(message: "Something went wrong", controller: self)
            }
        }
        //        getCategoryPrice()
    }
    
    //    func getCategoryPrice() {
    //        SVProgressHUD.show()
    //        DataService.sharedInstance.getCategoryPrice(categoryId: catgoryId) { (resultDict, errorMsg) in
    //            SVProgressHUD.dismiss()
    //            print(resultDict as Any)
    //
    //            if errorMsg == nil{
    //                if let categoryBalance = resultDict?["category_balance"] as? String {
    //                    print(categoryBalance)
    //                }
    //                else {
    //                }
    //            } else {
    //                // Toast.show(message: "Something went wrong", controller: self)
    //            }
    //        }
    //    }
    

    
}


/****protocol  function  defination*/
extension PeculiOfferViewController:  CustomTableView {
    
    func fetchTableViewHeight(height: CGFloat) {
        transactionTableHeight = height
        
    }
    
    
    
    
    
}
