//
//  peculioffer_ScreenPopUp.swift
//  Peculi
//
//  Created by Mohit Rana on 25/08/22.
//  Copyright © 2022 Stealth Tech. All rights reserved.
//

import UIKit

class peculioffer_ScreenPopUp: UIViewController {
    
    // MARK: - OUTLETS
   
    @IBOutlet weak var popUpChildView: UIView!
    @IBOutlet var popUpSuperView: UIView!
    @IBOutlet weak var myTableView: UITableView!
    
    
    
    //MARK: - declare Variables
        
        var okCompletionHandler: (() -> Void)?
        var balanceText = ""
    
    // MARK: - VIEW LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()

        myTableView.register(UINib(nibName: "ProgressViewTableViewCell", bundle: nil), forCellReuseIdentifier: "ProgressViewTableViewCell")
    
        myTableView.delegate = self
        myTableView.dataSource = self
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupView()
        animateView()
   
       
    
    }
    
    
    // MARK: - FUNCTION
    
    /**** UI setup */
    func setupView(){
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.85)
        popUpSuperView.backgroundColor = UIColor.black.withAlphaComponent(0.85)
        
    }
     
     ///animation setup
     func animateView() {
         popUpChildView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
         UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
             self.popUpChildView.transform = .identity
         }, completion: {(finished: Bool) -> Void in
             
         })
         
     }
    
    
    // MARK: - BUTTON ACTION
    
    @IBAction func btnCancel_Action(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        print("call cancel ")
    }

    
    @IBAction func btnContinue_Action(_ sender: Any) {
        

        
       self.okCompletionHandler?()
        
        self.dismiss(animated: true, completion: nil)
        
        print("call function")
      
        
    }

}


extension peculioffer_ScreenPopUp : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let progressCell = Bundle.main.loadNibNamed("ProgressViewTableViewCell", owner: self, options: nil)?.first as! ProgressViewTableViewCell
        progressCell.superView.backgroundColor = UIColor.clear
        progressCell.progressView.backgroundColor = UIColor.clear
        progressCell.targetValueLbl.isHidden = true
   progressCell.potNameLbl.isHidden = true
        
//        let stringSet = "When you have created a pot, select Add Money' and then you will be asked to add your debit card details to 'Add Card'. \n If you wish, you can also send funds directly to your Peculi account using the unique bank details provided under 'Edit Profile from the side menu bar."
 
//        let attributedString = NSMutableAttributedString(string: "\(stringSet)")
//        // *** Create instance of `NSMutableParagraphStyle`
//        let paragraphStyle = NSMutableParagraphStyle()
//        // *** set LineSpacing property in points ***
//        paragraphStyle.lineSpacing = 15 // Whatever line spacing you want in points
//        // *** Apply attribute to string ***
//        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
//        // *** Set Attributed String to your label ***
//        progressCell.lblPopUpText.attributedText = attributedString
        
        progressCell.lblPopUpText.text = "When you have created a pot, select 'Add Money' and then you will be asked to add your debit card details to 'Add Card'. \n If you wish, you can also send funds directly to your Peculi account using the unique bank details provided under 'Edit Profile' from the side menu bar."
        
       
        progressCell.tierNameLbl.isHidden = true
        progressCell.btnAddNewTarget.isHidden = true
        progressCell.iconBtnAddTarget.isHidden = true
        progressCell.lblBtnAddTarget.isHidden = true
        progressCell.mainView.backgroundColor = UIColor.clear
        progressCell.backgroundColor = UIColor.clear
        progressCell.yellowCircleImg.isHidden = true
        progressCell.btnOk.isHidden = false
        progressCell.lblPopUpText.isHidden = false
        
        progressCell.btnOkPressed = {
            
            
            self.okCompletionHandler?()
             
             self.dismiss(animated: true, completion: nil)
             
             print("call function")
        }
        
     return progressCell
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
}
