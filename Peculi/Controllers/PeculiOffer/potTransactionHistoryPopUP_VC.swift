//
//  potTransactionHistoryPopUP_VC.swift
//  Peculi
//
//  Created by PPI on 5/13/22.
//  Copyright © 2022 Stealth Tech. All rights reserved.
//

import UIKit

class potTransactionHistoryPopUP_VC: UIViewController {
    
    
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var popUpChildView: UIView!
    @IBOutlet var popUpSuperView: UIView!
    @IBOutlet weak var titleView: UIView!

    
    var voucherData = [VoucherTransactionModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(voucherData)
        

        myTableView.delegate = self
        myTableView.dataSource = self
        
        myTableView.register(UINib(nibName: "VoucherTransactionsTableViewCell", bundle: nil), forCellReuseIdentifier: "VoucherTransactionsTableViewCell")
        
        myTableView.reloadData()
        
        setUpviewCornerRadius()
        animateView()
        setupView()
        
    }
    
    
    func animateView() {
        popUpChildView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
            self.popUpChildView.transform = .identity
        }, completion: {(finished: Bool) -> Void in
            
        })
        
    }
    
    func setUpviewCornerRadius(){
        
        titleView.clipsToBounds = true
        titleView.layer.cornerRadius = 10
        titleView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        
        popUpChildView.clipsToBounds = true
        popUpChildView.layer.cornerRadius = 10
        popUpChildView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        
    }
    
    func setupView(){
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        popUpSuperView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        
    }
    
    
    @IBAction func btnCancel_Action(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        print("call cancel ")
        
    }
    
}


extension potTransactionHistoryPopUP_VC : UITableViewDelegate, UITableViewDataSource  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if voucherData.count == 0 {
            return 1
        }
        
        return voucherData.count
        
    }
    

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = myTableView.dequeueReusableCell(withIdentifier: "VoucherTransactionsTableViewCell") as! VoucherTransactionsTableViewCell
        if !voucherData.isEmpty {
            cell.titleLbl.text = String(format: "%@ %@", "Paid for", voucherData[indexPath.row].productDetails.name)
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let newDate = dateFormatter.date(from: voucherData[indexPath.row].created_at) ?? Date()
            
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let finalDate = dateFormatter.string(from: newDate)
            cell.dateLbl.text = finalDate
            
            cell.amountLbl.text = "\(GlobalVariables.currency)\(voucherData[indexPath.row].calculate_amount.format())"
            cell.defaultLbl.text = ""
            
            if (voucherData.count - 1) == indexPath.row {
                cell.seperatorView.isHidden = true
            }
            else {
                
                cell.seperatorView.isHidden = false
            }
        }
        else {
            
            cell.titleLbl.text = ""
            cell.dateLbl.text = ""
            cell.amountLbl.text = ""
            cell.seperatorView.isHidden = true
         }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80 //UITableView.automaticDimension
    }
    
}
