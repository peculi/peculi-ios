//
//  AccountViewController.swift
//  Peculi
//
//  Created by iApp on 12/05/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import UIKit
import SideMenu

class AccountViewController: UIViewController {
    
    //MARK:- outlets
    @IBOutlet weak var userprofileImageView: UIImageView!
    @IBOutlet weak var profileBackView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var cardHolderLbl: UILabel!
    @IBOutlet weak var WalletTextField: UITextField!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var superchildView: UIView!
    
    //MARK:- view did load
    override func viewDidLoad() {
        super.viewDidLoad()
        if checkIfGuest() == true{
            superchildView.isHidden = true
            popupToGuestRegister()
        }
        else{
            superchildView.isHidden = false
        UISetup()
        
        setValues()
        
        }
        
    }
    
    func popupToGuestRegister(){
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutAlertViewController") as? LogoutAlertViewController else {
            return
        }
        vc.setTransition()
        vc.titleLblData = "Message"
        vc.messageLblData = "Please complete your registration process to proceed further!"
        vc.okCompletionHandler = { [weak self] in
            
            self?.dismiss(animated: true) {
                
                guard let vc = self?.storyboard?.instantiateViewController(withIdentifier: "SignUpVc") as? SignUpVc else{
                    return
                }

                self?.navigationController?.pushViewController(vc, animated: true)
          
            }
        }
        vc.cancelCompletionHandler = { [weak self] in
            
            self?.dismiss(animated: false){
                self?.setRootHomeVC()
            }
        }
        
        self.present(vc, animated: true, completion: nil)

        
    }
    
    /****UI setup */
    func UISetup(){
        self.navigationController?.navigationBar.isHidden = true
        profileBackView.layer.masksToBounds = true
        profileBackView.layer.borderColor = #colorLiteral(red: 0.7960784314, green: 0.8588235294, blue: 0.9725490196, alpha: 1)
        profileBackView.layer.borderWidth = 2.0
        profileBackView.layer.cornerRadius = 8
        navigationView.layer.masksToBounds = false
        navigationView.layer.shadowRadius = 3.0
        navigationView.layer.shadowOpacity = 0.6
        navigationView.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        navigationView.layer.shadowOffset = CGSize(width: 0 , height: 3)
        
    }
    
    /**** Assigning values */
    func setValues(){
        
        if checkIfGuest() == false{
            var firstName = ""
            var lastName = ""
            if UserDefaults.exists(key: UserDefaultConstants.FirstName.rawValue){
                firstName = UserDefaults.standard.value(forKey: UserDefaultConstants.FirstName.rawValue) as! String
            }
            if UserDefaults.exists(key: UserDefaultConstants.LastName.rawValue){
                
                lastName = UserDefaults.standard.value(forKey: UserDefaultConstants.LastName.rawValue) as! String
                
            }
            
            nameTextField.text = "\(firstName.capitalizingFirstLetter()) \(lastName.capitalizingFirstLetter())"
            cardHolderLbl.text = "\(firstName.capitalizingFirstLetter()) \(lastName.capitalizingFirstLetter())"
            if UserDefaults.exists(key: UserDefaultConstants.UserEmail.rawValue){
                emailTextField.text = UserDefaults.standard.value(forKey: UserDefaultConstants.UserEmail.rawValue) as? String ?? ""
            }
            if let globalBal = UserDefaults.standard.string(forKey: UserDefaultConstants.GlobalBalance.rawValue){
                WalletTextField.text = "\(GlobalVariables.currency)\(globalBal)"
            }
        }
        else{
            nameTextField.text = "Guest User"
            cardHolderLbl.text = "Guest User"
            emailTextField.text = ""
            WalletTextField.text = String(format: "%@%@", GlobalVariables.currency, "0.00")
        }
        
        
    }
    
    //MARK:- button action
    @IBAction func sideBarBtnAction(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "menuLeftNavigationController") as! SideMenuNavigationController
        nextViewController.leftSide = true
        nextViewController.menuWidth = UIScreen.main.bounds.size.width - 100
        self.present(nextViewController, animated:true, completion:nil)
        
    }
    
    
}
