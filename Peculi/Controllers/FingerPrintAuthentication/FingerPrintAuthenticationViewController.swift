//
//  FingerPrintAuthenticationViewController.swift
//  Peculi
//
//  Created by iApp on 05/05/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import UIKit

class FingerPrintAuthenticationViewController: UIViewController {
    
    //MARK: - variables
    var fromVC: AllControllers?
    
    //MARK: - view did load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.newUpdateToken_Api()
        
    }
    
    //MARK: - view will appear
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
        navigationController?.navigationBar.barStyle = .black
        
        ///fingerprint authentication setup
        FingerprintAuth.share.accessAuth { (success, error, type) in
            if success{
                
                self.setpushFlow()
                
            }
            else {
                
                
                if type == FingerprintConstants.authenticationError{
                    
                    DispatchQueue.main.async {
                        Toast.show(message: error?.errorMessage ?? "", controller: self)
                    }
                    
                    
                    
                }
                else if type == FingerprintConstants.none || type == FingerprintConstants.authenticationFailed{
                    
                    self.showPopup()
                    
                }
                else if type == FingerprintConstants.cancel{
                    
                    DispatchQueue.main.async {
                        
                        if self.navigationController?.viewControllers.count ?? 1 > 1{
                            self.navigationController?.popViewController(animated: true)
                        }
                        else{
                            
                            exit(0)
                        }
                    }
                    
                }
                
                
                
                
                
                
            }
        }
    }
    
    /*** push to Home ViewController */
    func setpushFlow(){
        
        UserDefaults.standard.set("3", forKey: UserDefaultConstants.LoginStatus.rawValue)
        UserDefaults.standard.set("finger", forKey: UserDefaultConstants.Auth.rawValue)
        self.setRootHomeVC()
        
    }
    
    /***** popup setup */
    func showPopup() {
        
        
        let sb = UIStoryboard(name: "Main", bundle:nil)
        let logAlert = sb.instantiateViewController(withIdentifier: "LogoutAlertViewController") as! LogoutAlertViewController
        
        logAlert.providesPresentationContextTransitionStyle = true
        logAlert.definesPresentationContext = true
        logAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        logAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        logAlert.messageLblData = "Biometric authentication is not enrolled on your device."
        logAlert.titleLblData = "Message"
        logAlert.okCompletionHandler = { [weak self] in
            
            self?.clearUserDefault()
            
        }
        logAlert.cancelCompletionHandler = {
            
            exit(0)
            
        }
        
        DispatchQueue.main.async {
            
            
            self.present(logAlert, animated: true, completion: nil)
        }
        
        
        
    }
    
    
    func newUpdateToken_Api(){
        
//self.addPeculiPopup(view: self.view)
        
        let params:[String:Any] = ["userId": UserDefaults.standard.value(forKey: UserDefaultConstants.UserID.rawValue) ?? ""
                                   
        ]

        print(params)
        
        DataService.sharedInstance.postApiCall(endpoint: constantApis.newUpdateToken_Api, params: params) { (response, error) in
            
           // self.hidePeculiPopup(view: self.view)
       
            print(response)
            
            if response != nil {
                
                print(response ?? [:])
                
                let codeInt = response?["code"] as? Int ?? 0
                let codeString = response?["code"] as? String ?? ""
                let message = response?["message"] as? String ?? ""
                
                if codeInt == 200 || codeString == "200" {
                    
                    print("edit api call 1===========>>>>>>>>>>>")
                    
                    let token = response?["token"] as? String ?? ""
                    let monthlyLimit = response?["monthlyLimit"] as? String ?? ""
                    let dayLimit = response?["dayLimit"] as? String ?? ""
                    let yearlyLimit = response?["yearlyLimit"] as? String ?? ""
                    let maxLoad = response?["maxLoad"] as? String ?? ""
                    let transactionCount = response?["transactionCount"] as? String ?? ""
                    let transactionLimit = response?["transactionLimit"] as? String ?? ""
                    let transactionSum = response?["transactionSum"] as? String ?? ""
                     
                   
                    UserDefaults.standard.set(token, forKey: UserDefaultConstants.jwToken.rawValue)
                    
                    UserDefaults.standard.set(monthlyLimit, forKey: UserDefaultConstants.monthlyLimit.rawValue)
                    UserDefaults.standard.set(dayLimit, forKey: UserDefaultConstants.dayLimit.rawValue)
                    UserDefaults.standard.set(yearlyLimit, forKey: UserDefaultConstants.yearlyLimit.rawValue)
                    UserDefaults.standard.set(maxLoad, forKey: UserDefaultConstants.maxLoad.rawValue)
                    UserDefaults.standard.set(transactionCount, forKey: UserDefaultConstants.transactionCount.rawValue)
                    UserDefaults.standard.set(transactionLimit, forKey: UserDefaultConstants.transactionLimit.rawValue)
                    UserDefaults.standard.set(transactionSum, forKey: UserDefaultConstants.transactionSum.rawValue)
                    
                    
                } else {
                   
                 //   self.customAlertDismiss(title: "Alert", message: message)
                    
                }
                
            }
            else {
                
//                self.customAlertDismiss(title: "Alert", message: "Something went wrong!")
//
//                self.dismiss(animated: true, completion: nil)
            }
        }
        
    }
    
    
    
    
    
    
}
