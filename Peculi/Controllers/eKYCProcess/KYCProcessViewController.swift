//
//  KYCProcessViewController.swift
//  Peculi
//
//  Created by iApp on 08/07/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import UIKit
import SideMenu

class KYCProcessViewController: UIViewController {
    
    //MARK: - outlets
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var leftBarButton: UIButton!
    
    
    
    //MARK:- variables
    var showleftArrow = false
    
    
    //MARK:- view did load
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    //MARK:- View will appear
    override func viewWillAppear(_ animated: Bool) {
        
        navigationController?.navigationBar.isHidden = true
        navView.layer.masksToBounds = false
        navView.layer.shadowRadius = 3.0
        navView.layer.shadowOpacity = 0.6
        navView.layer.shadowColor = UIColor.clear.cgColor//#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        navView.layer.shadowOffset = CGSize(width: 0 , height: 3)
        
        if showleftArrow == false{
            leftBarButton.setImage(UIImage(named: "icSideMenu"), for: .normal)
        }
        else{
            leftBarButton.setImage(UIImage(named: "btnBack_Img"), for: .normal)
        }
    }
    
    //MARK:- button actions
    
    /**** side menu action setup */
    @IBAction func sideMenuAction(_ sender: Any) {
        if showleftArrow == false{
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "menuLeftNavigationController") as! SideMenuNavigationController
        nextViewController.leftSide = true
        nextViewController.menuWidth = UIScreen.main.bounds.size.width - 100
        self.present(nextViewController, animated:true, completion:nil)
        }
        else {
            popVc()
        }
    }
    
    /**** start KYC process action */
    
    @IBAction func startKYCAction(_ sender: Any) {
        
   //  KYCProcess.share.getKYCUuidApiCall()
        
    }
    

}
