//
//  VoucherTransactionDetailsViewController.swift
//  Peculi
//
//  Created by iApp on 22/04/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import UIKit

class VoucherTransactionDetailsViewController: UIViewController {
    
    //MARK: - outlets
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var transactionDateLbl: UILabel!
    @IBOutlet weak var amountImageView: UIImageView!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var expireImageView: UIImageView!
    @IBOutlet weak var expireLbl: UILabel!
    @IBOutlet weak var calculatedAmountImageView: UIImageView!
    @IBOutlet weak var calculatedAmountLbl: UILabel!
    @IBOutlet weak var currencyCodeImageView: UIImageView!
    @IBOutlet weak var currencyLbl: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    
    //MARK: - variables
    var fetchedData: VoucherTransactionModel?
    
    //MARK: - view did load
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    //MARK: - view will appear
    override func viewWillAppear(_ animated: Bool) {
        navigartionSetup()
        updateUI()
    }
    
    //MARK: - Navigation Setup
    func navigartionSetup(){
        navigationController?.navigationBar.isHidden = false
        title = "Transaction Details"
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.07450980392, green: 0.1019607843, blue: 0.1529411765, alpha: 1)
        self.navigationController?.navigationBar.isTranslucent = false
        
        
        //Navigation LeftBar Button: -
        let leftBarButton = UIBarButtonItem(image: UIImage(named: "icBackArrowWhite"), style: .done, target: self, action: #selector(icBackButtonAction))
        self.navigationItem.leftBarButtonItem  = leftBarButton
    }
    
    @objc func icBackButtonAction(){
        popVc()
    }
    
    /**** UI setup */
    func updateUI(){
        
        if let discount = fetchedData?.discount{
            
            let discountDouble = (discount as NSString).doubleValue
            
            let discountString = String(format: "%@%@", discountDouble.format(), "%")
            self.discountLbl.text = discountString
        }
        
   
        
        if let imageURL = fetchedData?.productDetails.logo_image_url{
//            if imageURL == ""{
//
//                userImageView.image = UIImage(named: "imgpsh_fullsize_anim")
//                userImageView.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1)
//            }
//            else{
            
            userImageView.sd_setImage(with: URL(string:imageURL), placeholderImage:UIImage(named: "placeholder_Img"), completed: { (image, error, cacheType, url) -> Void in
                if ((error) != nil) {
                    // set the placeholder image here
                    //cell.imgVw.image = #imageLiteral(resourceName: "doctorAvtar_Img")
                } else {
                    // success ... use the image
                }
            })
//            }
            
            
        }
        
        if let name = fetchedData?.productDetails.name{
            userNameLbl.text = name
        }
        //        userNameLbl.text = String(format: "%@ %@", firstName,lastName)
        
        if let date = fetchedData?.created_at{
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            guard let newDate = dateFormatter.date(from: date) else { return }
            
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let finalDate = dateFormatter.string(from: newDate)
            
            transactionDateLbl.text = finalDate
        }
        
        if let descriptionData = fetchedData?.productDetails.description{
            
            self.descriptionLbl.text = descriptionData
            
        }
        
        let setColor: UIColor = .white
        if let amount = fetchedData?.amount{
            let amountDoubleValue = (amount as NSString).doubleValue
            let amountWithCurrency = String(format: "%@%@", GlobalVariables.currency, amountDoubleValue.format())
            let amountLblData = String(format: "%@: %@%@", "Coupon Value",GlobalVariables.currency, amountDoubleValue.format())
            
            let amountString = changeSubstringColor(amountLblData, amountWithCurrency, setColor)
            self.amountLbl.attributedText = changeFont(amountString, "Montserrat-Medium", 12, NSRange(location: amountLblData.count - amountWithCurrency.count, length: amountWithCurrency.count))
            
        }
        
        if let calculatedAmount = fetchedData?.calculate_amount{
            
            
            
            let amountWithCurrency = String(format: "%@%@", GlobalVariables.currency, calculatedAmount.format())
            let calculatedAmountLblData = String(format: "%@: %@%@", "Amount Paid",GlobalVariables.currency, calculatedAmount.format())
            
            let calculatedAmountString = changeSubstringColor(calculatedAmountLblData, amountWithCurrency, setColor)
            self.calculatedAmountLbl.attributedText = changeFont(calculatedAmountString
                , "Montserrat-Medium", 12, NSRange(location: calculatedAmountLblData.count - amountWithCurrency.count, length: amountWithCurrency.count))
            
        }
        
        if let expireData = fetchedData?.expiry_date{
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            guard let newDate = dateFormatter.date(from: expireData) else { return }
            
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let finalDate = dateFormatter.string(from: newDate)
            
            let expireLblData = String(format: "%@: %@", "Expire",finalDate)
            let coloredExpireString = changeSubstringColor(expireLblData, finalDate, setColor)
            self.expireLbl.attributedText = changeFont(coloredExpireString, "Montserrat-Medium", 12, NSRange(location: coloredExpireString.string.count - finalDate.count, length: finalDate.count))
            
            
            
            
        }
        
        if let currencyCode = fetchedData?.product_code{
            
            
            let currencyLblData = String(format: "%@: %@", "Code",currencyCode)
            let coloredCurrencyString = changeSubstringColor(currencyLblData, currencyCode, setColor)
            self.currencyLbl.attributedText = changeFont(coloredCurrencyString, "Montserrat-Medium", 12, NSRange(location: coloredCurrencyString.string.count - currencyCode.count, length: currencyCode.count))
            
        }
        
    }
    
    
    
    
    func changeSubstringColor( _ text: String, _ subString: String, _ color: UIColor) -> NSMutableAttributedString{
        
        let dataString = NSMutableAttributedString(string: text)
        dataString.setColorForText(subString, with: color)
        return dataString
        
    }
    
    func changeFont(_ text: NSMutableAttributedString, _ fontName: String, _ size: CGFloat, _ range: NSRange) -> NSMutableAttributedString{
        
        let data = text
        data.addAttributes([
            .font: UIFont(name: fontName, size: size)!], range: range)
        
        return data
    }
    
    
    
}
