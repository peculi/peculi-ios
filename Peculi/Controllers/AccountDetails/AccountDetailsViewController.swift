//
//  AccountDetailsViewController.swift
//  Peculi
//
//  Created by iApp on 31/03/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import UIKit

class AccountDetailsViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var cardSuperView: UIView!
    
    //MARK: - ViewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSetup()
        
    }
    
    /**** UI setup */
    func viewSetup(){
        
        self.cardSuperView.layer.cornerRadius = 6
        self.cardSuperView.layer.masksToBounds = false
        self.cardSuperView.layer.shadowColor = UIColor.black.cgColor
        self.cardSuperView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.cardSuperView.layer.shadowOpacity = 1
        
    }
    
    //MARK: - back button action
    
    @IBAction func backBtnTapped(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
}
