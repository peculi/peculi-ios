//
//  SwaprificeViewController.swift
//  Peculi
//
//  Created by apple on 02/11/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit
import SideMenu

class SwaprificeViewController: UIViewController, UITextFieldDelegate {
    
    //MARK: - Outlets
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchToolView: UIView!
    @IBOutlet weak var globalBalanceView: UIView!
    @IBOutlet weak var clickandIconView: UIView!
    @IBOutlet weak var swaprificePostCollectionView: UICollectionView!
    @IBOutlet weak var walletSuperView: UIView!
    @IBOutlet weak var swapificationView: UIView!
    @IBOutlet weak var swaprificationPotSuperView: UIView!
    @IBOutlet weak var addNewPotView: UIView!
    @IBOutlet weak var scrollViewScrollHeight: NSLayoutConstraint!
    @IBOutlet weak var globalBalancelbl: UILabel!
    
    
    //MARK: - variables
    var potNameArray = ["Electronics","Food","Fashion","Travel","Leisure","Culture"]
    var potImage = ["icSwaprificeElectronics","icSwaprificeFood","icSwaprificeFashion","icSwaprificeTravel","icSwaprificeLeisure","icSwaprificeCulture"]
        
    var dataUser:[UserCategoriesInfo] = []
    var searchedArray = [UserCategoriesInfo]()
    
    
    //MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        searchTextField.delegate = self
        if let globalBal = UserDefaults.standard.string(forKey: UserDefaultConstants.GlobalBalance.rawValue){
            self.globalBalancelbl.text = "\(GlobalVariables.currency)\(globalBal)"
        }
        setupCollectionView()
       
    }
    
    
    //MARK: - ViewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
        setupUI()
        if UserDefaults.exists(key: UserDefaultConstants.UserID.rawValue){
            callAllCategories_Api(UserDefaults.standard.value(forKey: UserDefaultConstants.UserID.rawValue) as! String)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0) {
            self.globalBalanceView.addDashedBorder(conerRadius: 8)
        }
    }
    
    
    /* MARK: - Setup UI */
    func setupUI() {
        searchTextField.placeHolderColor = #colorLiteral(red: 0.5568627451, green: 0.5568627451, blue: 0.5764705882, alpha: 1)
        searchToolView.layer.masksToBounds = false
        searchToolView.layer.shadowRadius = 3.0
        searchToolView.layer.shadowOpacity = 0.6
        searchToolView.layer.shadowColor = UIColor.clear.cgColor //#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        searchToolView.layer.shadowOffset = CGSize(width: 0 , height: 3)
        clickandIconView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.2)
        clickandIconView.layer.borderWidth = 1
    }
    
    ///API call
    func callAllCategories_Api(_ user_id : String) {
        
        GetUserCategoriesModel().fetchInfo(user_id){ (userinfo) in
            //print(userinfo)
            if userinfo.count > 0{
                if userinfo[0].statuscheck == "success"{
                    self.dataUser.removeAll()
                    self.searchedArray.removeAll()
                    for i in 0..<userinfo.count{
                        self.dataUser.append(userinfo[i])
                        self.searchedArray.append(userinfo[i])
                    }
                    print(self.dataUser)
                    self.swaprificePostCollectionView.reloadData()
                    //self.showAlert("", userinfo[0].message)
                } else {
//                    AlertView.simpleViewAlert(title: "", userinfo[0].message, self)
                }
            } else {
//                AlertView.simpleViewAlert(title: "", userinfo[0].message, self)
            }
        }
        
        
    }
    
    /* MARK: - UIButton Actions*/
    @IBAction func icSIdeMenuButtonAction(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "menuLeftNavigationController") as! SideMenuNavigationController
        nextViewController.leftSide = true
        nextViewController.menuWidth = UIScreen.main.bounds.size.width - 100
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    //MARK: - CollectionView Layout Function:-
    func setupCollectionView() {
        let layout = UICollectionViewFlowLayout()
        let cellSize = UIScreen.main.bounds.width / 2
        layout.itemSize = CGSize(width: cellSize, height: 150)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        swaprificePostCollectionView.collectionViewLayout = layout
    }
    
    /****add new pot action*/
    @IBAction func btnAddNewPot(_ sender: Any) {
        
        
    }
}

//MARK: - collectionview delegates
extension SwaprificeViewController : UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataUser.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SwaprificeCollectionViewCell", for: indexPath) as! SwaprificeCollectionViewCell
        if dataUser[indexPath.row].data1.category_name == ""{
            
            cell.potProductNameLbl.text = String(format: "%@ Pot", dataUser[indexPath.row].data1.categorydata.new_name.capitalizingFirstLetter())
            
        }
        else{
            
            cell.potProductNameLbl.text = String(format: "%@ %@", dataUser[indexPath.row].data1.category_name.capitalizingFirstLetter(), "Pot")
        }
        var originalString = String()
        originalString = dataUser[indexPath.row].data1.categorydata.image
        let ImageUrl = String(format: "%@%@", AppConfig.ImageUrl,originalString)
        cell.potProductImageView.sd_setImage(with: URL(string:ImageUrl), placeholderImage:UIImage(named: "placeholder_Img"), completed: { (image, error, cacheType, url) -> Void in
            if ((error) != nil) {
                // set the placeholder image here
                //cell.imgVw.image = #imageLiteral(resourceName: "doctorAvtar_Img")
            } else {
                // success ... use the image
            }
        })
        let heightZero = potNameArray.count
        let heightOne = walletSuperView.frame.size.height+swapificationView.frame.size.height
        let heightTwo = swaprificationPotSuperView.frame.size.height+addNewPotView.frame.height
        scrollViewScrollHeight.constant = CGFloat(heightZero)+heightOne+heightTwo+180
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //        vc.potName = dataUser[indexPath.row].name
        //        vc.potImage = dataUser[indexPath.row].image
        //        vc.categoryId = dataUser[indexPath.row]._id
        
        cellSelected(index: indexPath.row)
        
    }
    
    /****did select action */
    func cellSelected(index: Int){
        
//        if UserDefaults.standard.string(forKey: UserDefaultConstants.personalDetailsStatus.rawValue) != "1" {
//          //  popupPersonalDetails()
//        }
//        else {
            
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddAmountSwaprificeVC") as! AddAmountSwaprificeVC
        
        if dataUser[index].data1.category_name == "" {
            
            vc.potName = String(format: "%@ Pot",dataUser[index].data1.categorydata.new_name)
            
        }
        else {
            
            vc.potName = String(format: "%@ %@", dataUser[index].data1.category_name, "Pot")
        }
        
        vc.catIdForUpdateTransctonStatus = dataUser[index].data1._id
        vc.potImage = dataUser[index].data1.categorydata.image
        vc.categoryId = dataUser[index].data1.categorydata._id
        
        self.navigationController?.pushViewController(vc, animated: true)
      //  }
        
    }
    
    func popupPersonalDetails(){
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutAlertViewController") as? LogoutAlertViewController else{return}
        vc.setTransition()
        vc.titleLblData = "Message"
        vc.messageLblData = "Please complete your personal details to add funds in Pot."
        vc.okCompletionHandler = {
            self.dismiss(animated: false) {
                let personalVc = PersonalDetailsViewController.instantiateFromAppStoryboard(appStoryboard: .main)
                self.pushViewController(viewController: personalVc)
            }
        }
        vc.cancelCompletionHandler = {
            self.dismiss(animated: true, completion: nil)
        }
        self.present(vc, animated: true, completion: nil)
    }

}

//search bar setup
extension SwaprificeViewController {
    
    ///textfield delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        searchTextField.resignFirstResponder()
        if (searchTextField.text?.count)! != 0 {
            self.dataUser.removeAll()
            for str in searchedArray {
                var potNameData = String()
                if str.data1.category_name == ""{
                    
                    potNameData = String(format: "%@ Pot",str.data1.categorydata.new_name)
                    
                }
                else{
                    potNameData = String(format: "%@ %@", str.data1.category_name, "Pot")
                }
                
                let range = potNameData.lowercased().range(of: textField.text!, options: .caseInsensitive, range: nil, locale: nil)
                if range != nil {
                    self.dataUser.append(str)
                }
            }
        }
        
        swaprificePostCollectionView.reloadData()
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        searchTextField.resignFirstResponder()
        searchTextField.text = ""
        self.dataUser.removeAll()
        for str in searchedArray {
            dataUser.append(str)
        }
        swaprificePostCollectionView.reloadData()
        return false
    }
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if (searchTextField.text?.count)! != 0 {
            self.dataUser.removeAll()
            for str in searchedArray {
                var potNameData = String()
                if str.data1.category_name == ""{
                    
                    potNameData = String(format: "%@ Pot",str.data1.categorydata.new_name)
                    
                }
                else {
                    
                    potNameData = String(format: "%@ %@", str.data1.category_name, "Pot")
                }
                let range = potNameData.lowercased().range(of: textField.text!, options: .caseInsensitive, range: nil, locale: nil)
                if range != nil {
                    self.dataUser.append(str)
                }
            }
        } else if (searchTextField.text?.count)! == 0{
            searchTextField.resignFirstResponder()
            searchTextField.text = ""
            self.dataUser.removeAll()
            for str in searchedArray {
                dataUser.append(str)
            }
            swaprificePostCollectionView.reloadData()
        }
        swaprificePostCollectionView.reloadData()
        
        
    }
    
}
