//
//  TransferModePopUp_VC.swift
//  Peculi
//
//  Created by PPI on 5/5/22.
//  Copyright © 2022 Stealth Tech. All rights reserved.
//

import UIKit



var potTransferValue = ""

class TransferModePopUp_VC: UIViewController {
    
    @IBOutlet weak var popUpChildView: UIView!
    @IBOutlet var popUpSuperView: UIView!
    @IBOutlet weak var titleView: UIView!
    
    var okCompletionHandler: (() -> Void)?
    
    var okCompletionHandler1: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
   
        self.setUpviewCornerRadius()
        self.setupView()
        self.animateView()

    }
    
    override func viewWillAppear(_ animated: Bool) {

      
        
    }
    
  
    // MARK: - FUNCTION
    
    /**** UI setup */
    
    func setUpviewCornerRadius(){
        
        titleView.clipsToBounds = true
        titleView.layer.cornerRadius = 10
        titleView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        
        popUpChildView.clipsToBounds = true
        popUpChildView.layer.cornerRadius = 10
        popUpChildView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        
    }
    
    func setupView(){
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        popUpSuperView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        
    }
     
     //animation setup
    
     func animateView() {
         popUpChildView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
         UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
             self.popUpChildView.transform = .identity
         }, completion: {(finished: Bool) -> Void in
             
         })
         
     }
    
    
    @IBAction func btnCancel_Action(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        print("call cancel ")
        
    }

    
    
    @IBAction func btnBankAcountTransfre_Action(_ sender: Any) {
        
        print("btn pressed acount transfer")
        potTransferValue = "0"
        self.okCompletionHandler1?()
        self.dismiss(animated: true, completion: nil)
        
        Toast.show(message: "In Progress", controller: self)
        
    }

    
    @IBAction func btnPotTransfer_Action(_ sender: Any) {
        
        potTransferValue = "1"
        print("btn pressed pot transfer")
        self.okCompletionHandler?()
        self.dismiss(animated: true, completion: nil)
        
        
    }
    
}
