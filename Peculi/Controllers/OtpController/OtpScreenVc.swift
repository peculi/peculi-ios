//
//  OtpScreenVc.swift
//  Peculi
//
//  Created by Stealth on 29/10/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit
import SVProgressHUD

class OtpScreenVc: UIViewController ,UITextFieldDelegate{
    
    //MARK: - outlets
    
    @IBOutlet weak var firstTextField: UITextField!
    @IBOutlet weak var firstLbl: UILabel!
    @IBOutlet weak var secondTextField: UITextField!
    @IBOutlet weak var secondLbl: UILabel!
    @IBOutlet weak var thirdTextField: UITextField!
    @IBOutlet weak var thirdLbl: UILabel!
    @IBOutlet weak var fourthTextField: UITextField!
    @IBOutlet weak var fourthLbl: UILabel!
    @IBOutlet weak var resetCodeLbl: UILabel!
    @IBOutlet weak var fifthTextField: UITextField!
    @IBOutlet weak var fifthLbl: UILabel!
    @IBOutlet weak var sixthTextField: UITextField!
    @IBOutlet weak var sixthLbl: UILabel!
    
    //MARK: - variables
    
    var otp = String()
    var email = String()

    //MARK: - did load method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    //MARK: - view will appear
    
    override func viewWillAppear(_ animated: Bool) {
        updateUi()
    }
    
    /*** setup UI */
    
    func updateUi(){
        
        firstTextField.delegate = self
        secondTextField.delegate = self
        thirdTextField.delegate = self
        fourthTextField.delegate = self
        fifthTextField.delegate = self
        sixthTextField.delegate = self
        firstLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        secondLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
        thirdLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
        fourthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
        fifthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
        sixthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
        firstTextField.becomeFirstResponder()
        let resetAction = UITapGestureRecognizer(target: self, action: #selector(self.forgetAction(_:)))
        resetCodeLbl?.isUserInteractionEnabled = true
        resetCodeLbl?.addGestureRecognizer(resetAction)
        
    }
    
    
    @objc func forgetAction(_ sender: UITapGestureRecognizer) {
        
        emptyAllTextfield()
        resendOTPApiCall()
        
    }
    
    /****Resend OTP*/
//    func resendOTPApiCall(){
//        SVProgressHUD.show()
//        let params:[String:Any] = ["email": self.email]
//        DataService.sharedInstance.postApiCall(endpoint: constantApis.resendOTP, params: params) { (response, error) in
//            SVProgressHUD.dismiss()
//            if response != nil{
//
//                let code = response?["code"] as? Int
//                let msg = response?["message"] as? String
//                if code == 200{
//
//                    self.emptyAllTextfield()
//
//                }
//                else{
//                    self.customAlertDismiss(title: "", message: msg ?? "")
//                }
//
//            }
//            else{
//                self.customAlertDismiss(title: "", message: "Something went wrong!")
//            }
//        }
//
//    }
    
    func resendOTPApiCall() {
        
        SVProgressHUD.show()
        let params:[String: Any] = ["email": self.email,
                                    "id" : UserDefaults.standard.string(forKey: UserDefaultConstants.UserID.rawValue) ?? "" ]
        DataService.sharedInstance.postApiCall(endpoint: constantApis.resendOTP, params: params) { (response, error) in
            
            SVProgressHUD.dismiss()
            
            print(response)
            
            if response != nil {
                
                Toast.show(message: "OTP sent on your email", controller: self)
                self.emptyAllTextfield()
               
            }
            else {
             
                self.customAlertDismiss(title: "Message", message: "Something went wrong!")
            }
            
        }
        
    }

    /**** Empty all textFields */
    
    func emptyAllTextfield(){
        
        firstTextField.text = ""
        secondTextField.text = ""
        thirdTextField.text = ""
        fourthTextField.text = ""
        fifthTextField.text = ""
        sixthTextField.text = ""
        firstLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        secondLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
        thirdLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
        fourthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
        fifthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
        sixthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
        firstTextField.becomeFirstResponder()
        
    }
    

    ///textfield delegate methods
     
    @IBAction func textFieldEditingChanged(_ sender: UITextField) {
        
        let count = sender.text?.count
        
        if count == 1 {
            switch sender {
            case firstTextField:
                firstLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                secondLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                thirdLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fourthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fifthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                sixthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                secondTextField.becomeFirstResponder()
               
            case secondTextField:
                firstLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                secondLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                thirdLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                fourthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fifthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                sixthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                thirdTextField.becomeFirstResponder()
                
            case thirdTextField:
                firstLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                secondLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                thirdLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fourthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                fifthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                sixthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fourthTextField.becomeFirstResponder()
                
            case fourthTextField:
                firstLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                secondLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                thirdLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fourthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fifthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                sixthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fifthTextField.becomeFirstResponder()
            case fifthTextField:
                firstLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                secondLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                thirdLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fourthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fifthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                sixthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                sixthTextField.becomeFirstResponder()
            case sixthTextField:
                firstLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                secondLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                thirdLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fourthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fifthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                sixthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                sixthTextField.becomeFirstResponder()
                
            default:
                print("default")
            }
        }
        
    }

    @IBAction func textFieldEditingBegin(_ sender: UITextField) {
        sender.text = " "
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange, replacementString string: String) -> Bool {
       
       // textField.text = string
        textField.text = ""
        
        if string == "" {
            
            if textField == sixthTextField {
                firstLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                secondLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                thirdLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fourthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fifthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                sixthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fifthTextField?.becomeFirstResponder()
            }
            else if textField == fifthTextField {
                firstLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                secondLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                thirdLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fourthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                fifthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                sixthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fourthTextField?.becomeFirstResponder()
            }
            else if textField == fourthTextField {
                firstLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                secondLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                thirdLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                fourthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fifthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                sixthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                thirdTextField?.becomeFirstResponder()
            }
            else if textField == thirdTextField {
                firstLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                secondLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                thirdLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fourthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fifthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                sixthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                secondTextField?.becomeFirstResponder()
            }
            else if textField == secondTextField {
                firstLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                secondLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                thirdLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fourthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fifthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                sixthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                firstTextField?.becomeFirstResponder()
            }
            else if textField == firstTextField {
                firstLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                secondLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                thirdLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fourthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                fifthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                sixthLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05)
                firstTextField?.becomeFirstResponder()
            }
           
            textField.text = ""
            return false
        }

        return true
    }
    
    //MARK: - UIButton actions
    
    /*** back button */
    @IBAction func btnBackAction(_ sender: Any) {
        popVc()
    }
    
    /*** continue button */
    
    @IBAction func btnContinueAction(_ sender: Any) {
        
        otp = String(format: "%@%@%@%@%@%@", firstTextField.text ?? "", secondTextField.text ?? "", thirdTextField.text ?? "", fourthTextField.text ?? "", fifthTextField.text ?? "", sixthTextField.text ?? "")
        otp = otp.trimmingCharacters(in: .whitespaces)
        if otp.count == 6 {
            emptyAllTextfield()
            //callOtpApi(email,otp)
            callOtpApi(otp)
            
        } else {
            AlertView.simpleViewAlert(title: "", "Please enter the One time passcode", self)
        }
    }
    
    //verify otp API call
//    func callOtpApi(_ email : String,_ otp : String){
//        
//        OtpModel().fetchInfo(email,otp){ (userinfo) in
//            
//            print(userinfo)
//            if userinfo.count > 0{
//                if userinfo[0].statuscheck == "success" {
//                    self.emptyAllTextfield()
//                    self.moveToResetPasswordVc()
//
//                } else {
//                    self.emptyAllTextfield()
//                    self.customAlertDismiss(title: "", message: userinfo[0].message)
//                }
//                
//            }
//            else{
//                self.emptyAllTextfield()
//                self.customAlertDismiss(title: "", message: "Something went wrong!")
//            }
//            
//        }
//    }
    
    
    func callOtpApi(_ otp : String) {
        
        SVProgressHUD.show()
        let params:[String: Any] = ["user_id": UserDefaults.standard.string(forKey: UserDefaultConstants.UserID.rawValue) ?? "",
                                    "otp" : otp
        ]
        
        DataService.sharedInstance.postApiCall(endpoint: constantApis.otpForgetVerify, params: params) { (response, error) in
            
            SVProgressHUD.dismiss() // OtpApi
            
            print(response)
            
            if response != nil {
                
                self.emptyAllTextfield()
                self.moveToResetPasswordVc()
               
            }
            else {
                self.emptyAllTextfield()
                self.customAlertDismiss(title: "Message", message: "Something went wrong!")
            }
       
        }
        
    }
    
    
    
    
    
    func moveToResetPasswordVc()
    {
        let vc = ResetPasswordVc.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.email = email
        self.pushViewController(viewController: vc)
    }
    
    
    
    
}
