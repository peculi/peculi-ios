//
//  PreviewTodaysOfferViewController.swift
//  Peculi
//
//  Created by iApp on 26/04/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import UIKit

class PreviewTodaysOfferViewController: UIViewController {
    
    
    //MARK:- outlets
    @IBOutlet weak var superView: UIView!
    @IBOutlet weak var offerLbl: UILabel!
    @IBOutlet weak var previewOfferTableView: UITableView!
    
    
    //MARK:- variables
    var potlist:[UserCategoriesInfo] = []
    var todaysRewardData = [AllTiersModel]()
    var tierDiscount = Double()
    
    //MARK:- view did load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if potlist.count<=1{
            offerLbl.text = "This offer is applicable for below Pot. Please select this pot to view offer details."
        }
        else{
            offerLbl.text = "This offer is applicable for below Pots. Please select any pot to view offer details."
        }
        
        
    }
    
    //MARK:- view will appear
    override func viewWillAppear(_ animated: Bool) {
        navigartionSetup()
    }
    
    //MARK:- Navigation Setup
    func navigartionSetup(){
        
        navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.07450980392, green: 0.1019607843, blue: 0.1529411765, alpha: 1)
        self.navigationController?.navigationBar.isTranslucent = false
        
        //MARK:- Navigation LeftBar Button:-
        let leftBarButton = UIBarButtonItem(image: UIImage(named: "icBackArrowWhite"), style: .done, target: self, action: #selector(icBackButtonAction))
        self.navigationItem.leftBarButtonItem  = leftBarButton
    }
    
    @objc func icBackButtonAction(){
        popVc()
    }
    
    
    
    
    
    
}

//MARK:- table view delegate methods
extension PreviewTodaysOfferViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return potlist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = previewOfferTableView.dequeueReusableCell(withIdentifier: "PreviewTodaysOfferTableViewCell") as? PreviewTodaysOfferTableViewCell else{
            return UITableViewCell()
        }
        
        if potlist[indexPath.row].data1.category_name == ""{
            cell.potNameLbl.text = String(format: "%@ Pot", potlist[indexPath.row].data1.categorydata.new_name.capitalizingFirstLetter())
        }
        else{
            cell.potNameLbl.text = "\(potlist[indexPath.row].data1.category_name.capitalizingFirstLetter()) Pot"
        }
        let price = potlist[indexPath.row].total_amount.format()
        cell.potValueLbl.text = "\(GlobalVariables.currency)\(price)"
        
        var originalString = String()
        
        originalString = potlist[indexPath.row].data1.categorydata.image
        let ImageUrl = String(format: "%@%@", AppConfig.ImageUrl,originalString)
        
        cell.potImageView.sd_setImage(with: URL(string:ImageUrl), placeholderImage:UIImage(named: "placeholder_Img"), completed: { (image, error, cacheType, url) -> Void in
            if ((error) != nil) {
                // set the placeholder image here
                //cell.imgVw.image = #imageLiteral(resourceName: "doctorAvtar_Img")
            } else {
                // success ... use the image
            }
        })
        if indexPath.row == potlist.count - 1 {
            cell.seperatorView.isHidden = true
        }
        else {
            cell.seperatorView.isHidden = false
        }
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RedeemNowVC") as! RedeemNowVC
        vc.logoImageURL = todaysRewardData[0].logoImage
        vc.allAvailbleDenomination = todaysRewardData[0].denominationData
        vc.discreption = todaysRewardData[0].description
        vc.discount = todaysRewardData[0].percentDiscount
        vc.navName = todaysRewardData[0].name
        vc.catId = potlist[indexPath.row].data1.category_id
        vc.productCode = todaysRewardData[0].code
        vc.currencyCode = todaysRewardData[0].currencyCode
        vc.type = todaysRewardData[0].denominationType
        vc.termsAndConditions = todaysRewardData[0].termsAndConditions
        vc.potBalance = potlist[indexPath.row].total_amount.format()
        vc.tierVal = "\(tierDiscount)"
        let originalString = potlist[indexPath.row].data1.categorydata.image
        let ImageUrl = String(format: "%@%@", AppConfig.ImageUrl,originalString)
        vc.potImageURL = ImageUrl
        if todaysRewardData[0].denominationType == "fixed"{
            vc.price = todaysRewardData[0].max
        }
        else{
            let percentageIntValue = Double(todaysRewardData[0].percentDiscount) ?? 0.00
            
            var value = Double()
            
            value = potlist[indexPath.row].total_amount + (potlist[indexPath.row].total_amount * (percentageIntValue / 100))
            //            let value: Double = potlist[indexPath.row].total_amount * 100.00/(100.00 - percentageIntValue)
            print(value)
            
            vc.price = String(format: "%.2f", value)
            
            
        }
        
        vc.minimumVal = todaysRewardData[0].min
        vc.maximumVal = todaysRewardData[0].max
        
        
        
        
        
        self.navigationController?.pushViewController(vc, animated: true)
        
        
        
    }
    
    
    
    
}
