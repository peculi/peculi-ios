//
//  LogoutAlertViewController.swift
//  Peculi
//
//  Created by iApp on 06/05/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import UIKit



class LogoutAlertViewController: UIViewController {
    
    //MARK: - outlet
    
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var messageLbl: UILabel!
    
    @IBOutlet weak var okBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet var popUpSuperView: UIView!
    @IBOutlet weak var lblLineShow: UILabel!
    
    //MARK: - variables
    
    var titleLblData = ""
    var messageLblData = ""
    var okBtnTitle = "Ok"
    var cancelBtnTitle = "Cancel"
    var okCompletionHandler: (()->Void)?
    var cancelCompletionHandler: (()->Void)?
    var showhideValue = 0
    
    
    //MARK: - view did load
    
    override func viewDidLoad() {
        super.viewDidLoad()

        titleLbl.text = titleLblData
        messageLbl.text = messageLblData
        okBtn.setTitle(okBtnTitle, for: .normal)
        cancelBtn.setTitle(cancelBtnTitle, for: .normal)
        setupView()
        
        if showhideValue == 0 {
            
            okBtn.isHidden = false
            cancelBtn.isHidden = false
            titleLbl.isHidden = false
            lblLineShow.isHidden = false
            
        } else {
            
            showhideValue = 0
            okBtn.isHidden = true
            cancelBtn.isHidden = true
            titleLbl.isHidden = true
            lblLineShow.isHidden = true
            
        }

    }

    /**** UI Setup */
    
    func setupView(){
        
        if checkIfGuest() == true {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
            popUpSuperView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        }
        else {
            
            if showhideValue == 0 {
                
                self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
                popUpSuperView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
                
            } else {
                
               //self.view.backgroundColor = UIColor.black.withAlphaComponent(0.2)
                
                popUpSuperView.backgroundColor = UIColor.black.withAlphaComponent(0.95)
                
            }
  
        }
    }
    
    
    
    //MARK: - button actions
    @IBAction func okBtnAction(_ sender: Any) {
   
        okCompletionHandler?()
        
    }
    
    
    @IBAction func cancelBtnAction(_ sender: Any) {

        cancelCompletionHandler?()

    }

    
}
