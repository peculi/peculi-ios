//
//  SettingsViewController.swift
//  Peculi
//
//  Created by apple on 02/11/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit
import SideMenu

class SettingsViewController: UIViewController {
    
    //MARK: - Outltes
    
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var navigationBackButton: UIButton!
    @IBOutlet weak var navigationTitleLbl: UILabel!
    @IBOutlet weak var settingTableView: UITableView!
    
    
    //MARK: - Define Array
    
    var settingNameArray = ["Change Password", "Change PIN Number", "Privacy Policy", "Terms & Conditions", "Help", "FAQs", "Issuer Statement"]
    var settingNameArrayForFinger = ["Change Password", "Privacy Policy", "Terms & Conditions", "Help", "FAQs", "Issuer Statement"]
    
    var settingIconArray = ["ic_password", "ic_pin_lock", "ic_privacy_icon", "ic_t&p_icon", "ic_help_icon", "ic_faq_icon", "ic_faq_icon"]
    
    var settingIconArrayForFinger = ["ic_password", "ic_privacy_icon", "ic_t&p_icon", "ic_help_icon", "ic_faq_icon", "ic_faq_icon"]
    var guestSettingNameArray = [ "Privacy Policy", "Terms & Conditions", "Help", "FAQs", "Issuer Statement"]
    var guestSettingIconArray = [ "ic_privacy_icon", "ic_t&p_icon", "ic_help_icon", "ic_faq_icon", "ic_faq_icon"]
    
    var checkIfFingerAuth = false
    
    //MARK: - ViewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UserDefaults.exists(key: UserDefaultConstants.Auth.rawValue){
            if UserDefaults.standard.string(forKey: UserDefaultConstants.Auth.rawValue) == "finger"{
                checkIfFingerAuth = true
            }
            else{
                checkIfFingerAuth = false
            }
        }
        
    }
    
    //MARK: - ViewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        navigartionSetup()
    }
    
    
    //MARK: - Navigation Setup
    func navigartionSetup(){
        
        navigationController?.navigationBar.isHidden = true
        title = "Settings"
        
        navigationView.layer.masksToBounds = false
        navigationView.layer.shadowColor = UIColor.clear.cgColor
        //#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        navigationView.layer.shadowOpacity = 0.6
        navigationView.layer.shadowOffset = CGSize(width: 0, height: 3)
        navigationView.layer.shadowRadius = 3.0
        
        //MARK: - Navigation LeftBar Button:-
        
        let leftBarButton = UIBarButtonItem(image: UIImage(named: "icBackArrowWhite"), style: .done, target: self, action: #selector(icBackButtonAction))
        self.navigationItem.leftBarButtonItem  = leftBarButton
    }
    
    //MARK: - Object Function Button Action:-
    
    @objc func icBackButtonAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func navigationBackButtonAction(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "menuLeftNavigationController") as! SideMenuNavigationController
        nextViewController.leftSide = true
        nextViewController.menuWidth = UIScreen.main.bounds.size.width - 100
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    
}

/* MARK:- UITableView Delegate & DatsSource Method */
extension SettingsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.checkIfGuest() == false{
            if checkIfFingerAuth == false{
                return settingNameArray.count
            }
            else{
                return settingNameArrayForFinger.count
            }
        }
        else{
            return guestSettingNameArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsTableViewCell") as! SettingsTableViewCell
        if self.checkIfGuest() == false{
            if checkIfFingerAuth == false{
                cell.settingsNameLbl.text = settingNameArray[indexPath.row]
                cell.settingIconImageView.image = UIImage(named: settingIconArray[indexPath.row])
            }
            else{
                cell.settingsNameLbl.text = settingNameArrayForFinger[indexPath.row]
                cell.settingIconImageView.image = UIImage(named: settingIconArrayForFinger[indexPath.row])
            }
        }
        else{
            cell.settingsNameLbl.text = guestSettingNameArray[indexPath.row]
            cell.settingIconImageView.image = UIImage(named: guestSettingIconArray[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let isGuestUser = self.checkIfGuest()
        if isGuestUser == false{
            if checkIfFingerAuth == false{
                switch indexPath.row {
                case 0:
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                case 1:
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "EnterPinViewController") as! EnterPinViewController
                    vc.commingFromScreen = "settings"
                    self.navigationController?.pushViewController(vc, animated: true)
                case 2:
                    let vc = ContentVc.instantiateFromAppStoryboard(appStoryboard: .main)
                    vc.contentType = "privacy"
                    vc.titleData = "Privacy Policy"
                    self.pushViewController(viewController: vc)
                    
                    print("\(indexPath.row)")
                case 3:
                    let vc = ContentVc.instantiateFromAppStoryboard(appStoryboard: .main)
                    vc.contentType = "term"
                    vc.titleData = "Terms & Conditions"
                    self.pushViewController(viewController: vc)
                    print("\(indexPath.row)")
                case 4:
                    let vc = ContentVc.instantiateFromAppStoryboard(appStoryboard: .main)
                    vc.contentType = "help"
                    vc.titleData = "Help"
                    self.pushViewController(viewController: vc)
                    print("\(indexPath.row)")
                case 5:
                    let vc = ContentVc.instantiateFromAppStoryboard(appStoryboard: .main)
                    vc.contentType = "faq"
                    vc.titleData = "FAQs"
                    self.pushViewController(viewController: vc)
                    print("\(indexPath.row)")
                    
                case 6:
                    let vc = ContentVc.instantiateFromAppStoryboard(appStoryboard: .main)
                    vc.contentType = "issue"
                    vc.titleData = "Issuer statement"
                    self.pushViewController(viewController: vc)
                    print("\(indexPath.row)")
                default:
                    break
                }
            }
            else{
                
                switch indexPath.row {
                case 0:
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                case 1:
                    let vc = ContentVc.instantiateFromAppStoryboard(appStoryboard: .main)
                    vc.contentType = "privacy"
                    vc.titleData = "Privacy Policy"
                    
                    self.pushViewController(viewController: vc)
                    
                    print("\(indexPath.row)")
                case 2:
                    let vc = ContentVc.instantiateFromAppStoryboard(appStoryboard: .main)
                    vc.contentType = "term"
                    vc.titleData = "Terms & Conditions"
                    
                    self.pushViewController(viewController: vc)
                    print("\(indexPath.row)")
                case 3:
                    let vc = ContentVc.instantiateFromAppStoryboard(appStoryboard: .main)
                    vc.contentType = "help"
                    vc.titleData = "Help"
                    
                    self.pushViewController(viewController: vc)
                    print("\(indexPath.row)")
                case 4:
                    let vc = ContentVc.instantiateFromAppStoryboard(appStoryboard: .main)
                    vc.contentType = "faq"
                    vc.titleData = "FAQs"
                    
                    self.pushViewController(viewController: vc)
                    print("\(indexPath.row)")
                    
                case 5:
                    let vc = ContentVc.instantiateFromAppStoryboard(appStoryboard: .main)
                    vc.contentType = "issue"
                    vc.titleData = "Issuer statement"
                    self.pushViewController(viewController: vc)
                    print("\(indexPath.row)")
                    
                default:
                    break
                }
                
            }
        }
        else{
            
            switch indexPath.row {
            case 0:
                let vc = ContentVc.instantiateFromAppStoryboard(appStoryboard: .main)
                vc.contentType = "privacy"
                vc.titleData = "Privacy Policy"
                
                self.pushViewController(viewController: vc)
                
                print("\(indexPath.row)")
            case 1:
                let vc = ContentVc.instantiateFromAppStoryboard(appStoryboard: .main)
                vc.contentType = "term"
                vc.titleData = "Terms & Conditions"
                
                self.pushViewController(viewController: vc)
                print("\(indexPath.row)")
            case 2:
                let vc = ContentVc.instantiateFromAppStoryboard(appStoryboard: .main)
                vc.contentType = "help"
                vc.titleData = "Help"
                
                self.pushViewController(viewController: vc)
                print("\(indexPath.row)")
            case 3:
                let vc = ContentVc.instantiateFromAppStoryboard(appStoryboard: .main)
                vc.contentType = "faq"
                vc.titleData = "FAQs"
                
                self.pushViewController(viewController: vc)
                print("\(indexPath.row)")
                
            case 4:
                let vc = ContentVc.instantiateFromAppStoryboard(appStoryboard: .main)
                vc.contentType = "issue"
                vc.titleData = "Issuer statement"
                self.pushViewController(viewController: vc)
                print("\(indexPath.row)")
                
                
            default:
                break
            }
            
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
}
