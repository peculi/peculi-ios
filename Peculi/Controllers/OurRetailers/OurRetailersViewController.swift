//
//  OurRetailersViewController.swift
//  Peculi
//
//  Created by iApp on 29/06/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import UIKit
import SideMenu
import SVProgressHUD

class OurRetailersViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    //MARK:- outlets
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var retailerCollectionView: UICollectionView!
    
    //MARK:- variables
    let retailers = ["icCurrys","icFlashOfferSky","icFlashOfferTescoMobile", "amazon", "icCurrys","icFlashOfferSky", "icCurrys","icFlashOfferSky","icFlashOfferTescoMobile", "amazon","icFlashOfferTescoMobile", "amazon", "icCurrys","icFlashOfferSky","icFlashOfferTescoMobile", "amazon", "icCurrys","icFlashOfferSky","icFlashOfferTescoMobile", "amazon", "icCurrys","icFlashOfferSky","icFlashOfferTescoMobile", "amazon", "icCurrys","icFlashOfferSky","icFlashOfferTescoMobile", "amazon", "icCurrys","icFlashOfferSky", "icCurrys","icFlashOfferSky","icFlashOfferTescoMobile", "amazon","icFlashOfferTescoMobile", "amazon", "icCurrys", "icCurrys","icFlashOfferSky","icFlashOfferTescoMobile", "amazon","icFlashOfferSky","icFlashOfferTescoMobile", "amazon", "icCurrys","icFlashOfferSky","icFlashOfferTescoMobile", "amazon", "icCurrys","icFlashOfferSky","icFlashOfferTescoMobile", "amazon", "icCurrys","icFlashOfferSky","icFlashOfferTescoMobile", "amazon"]
    var retailerList = [OurRetailersModel]()
    
    //MARK:- view did load
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        navView.layer.masksToBounds = false
        navView.layer.shadowRadius = 3.0
        navView.layer.shadowOpacity = 0.6
        navView.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        navView.layer.shadowOffset = CGSize(width: 0 , height: 3)
        setupCollectionView()
        
    }
    
    //MARK:- view will appear
    override func viewWillAppear(_ animated: Bool) {
        fetchRetailerImages()
    }
    
    //MARK:- setup collection view
    func setupCollectionView(){
        let layout = UICollectionViewFlowLayout()
        let cellSize = UIScreen.main.bounds.width / 3 - 5
        layout.itemSize = CGSize(width: cellSize, height: 70)
        layout.minimumInteritemSpacing = 5
        layout.minimumLineSpacing = 5
        retailerCollectionView.collectionViewLayout = layout
        
    }
    
    //side menu action
    @IBAction func sideMenuAction(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "menuLeftNavigationController") as! SideMenuNavigationController
        nextViewController.leftSide = true
        nextViewController.menuWidth = UIScreen.main.bounds.size.width - 100
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    //MARK:- collection view delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return retailerList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = retailerCollectionView.dequeueReusableCell(withReuseIdentifier: "RetailersCollectionViewCell", for: indexPath) as? RetailersCollectionViewCell else{
            return UICollectionViewCell()
        }
        cell.retailerImageView.sd_setImage(with: URL(string:retailerList[indexPath.row].logo_image_url), placeholderImage:UIImage(named: ""), completed: { (image, error, cacheType, url) -> Void in
            if ((error) != nil) {
                // set the placeholder image here
                //cell.imgVw.image = #imageLiteral(resourceName: "doctorAvtar_Img")
            } else {
                // success ... use the image
            }
        })
        
        if retailerList[indexPath.row].logo_image_url == "https://playground.wegift.io/static/product_assets/MOVE/MOVE-logo.png"{
            cell.retailerImageView.backgroundColor = #colorLiteral(red: 0.1254901961, green: 0.1529411765, blue: 0.1960784314, alpha: 1)
        }
        else{
            cell.retailerImageView.backgroundColor = .clear
        }
        return cell
    }
    //
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = retailerCollectionView.bounds.width / 3 - 5
        
        return CGSize(width: width, height: 70)
    }
    
    /**** fetch images from API */
    func fetchRetailerImages(){
        SVProgressHUD.show()
        retailerList.removeAll()
        RetailersData().fetchRetailersDetails { (list, error) in
            SVProgressHUD.dismiss()
            if error == ""{
                
                self.retailerList = list
                self.retailerCollectionView.reloadData()
                
            }
        }
        
    }
    
    
    
    
    
}
