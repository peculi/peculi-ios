//
//  PotOfferBottomXibViewController.swift
//  Peculi
//
//  Created by apple on 05/11/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit

class PotOfferBottomXibViewController: UIViewController {

    //MARK:- Outltes
    @IBOutlet var superView: UIView!
    @IBOutlet weak var managePotsView: UIView!
    @IBOutlet weak var depositNowView: UIView!
    @IBOutlet weak var peculiPremiumView: UIView!
    @IBOutlet weak var managePotsButton: UIButton!
    @IBOutlet weak var depositNowButton: UIButton!
    @IBOutlet weak var peculiPremiumButton: UIButton!
    
    //MARK:- ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }

    //MARK:- View Did layout subviews
    override func viewDidLayoutSubviews() {
         super.viewDidLayoutSubviews()
         
         superView.layer.cornerRadius = 24
         superView.layer.masksToBounds = true
         superView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
         view.layer.cornerRadius = 24
         view.layer.masksToBounds = true
         view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        managePotsView.layer.borderColor = #colorLiteral(red: 0.5921568627, green: 0.5921568627, blue: 0.5921568627, alpha: 1)
        managePotsView.layer.borderWidth = 1
        depositNowView.layer.borderColor = #colorLiteral(red: 0.5921568627, green: 0.5921568627, blue: 0.5921568627, alpha: 1)
        depositNowView.layer.borderWidth = 1
        peculiPremiumView.layer.borderColor = #colorLiteral(red: 0.5921568627, green: 0.5921568627, blue: 0.5921568627, alpha: 1)
        peculiPremiumView.layer.borderWidth = 1
         
     }
    
    //MARK:- UIButton Actions
    @IBAction func icCrossButtonAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

