//
//  BottomXIBViewController.swift
//  Peculi
//
//  Created by apple on 05/11/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit
import NBBottomSheet


protocol ClickHere {
    func clickedAction(flag: Int)
}

class BottomXIBViewController: UIViewController {
    
    //MARK:- Outltes
    @IBOutlet weak var superView: UIView!
    @IBOutlet weak var offerTypeLbl: UILabel!
    @IBOutlet weak var offerImageView: UIImageView!
    @IBOutlet weak var offerdescriptionLbl: UILabel!
    @IBOutlet weak var clickHereButton: UIButton!
    @IBOutlet weak var saveButtonView: UIView!
    
    //MARK:- variables
    var offerdescription = ""
    var offerImage = ""
    var offerType = ""
    var offerData = SpecialOfferModel()
    var totalAmount = Double()
    var clickDelegate: ClickHere!
    var from = ""
    var offerName = ""
    
    
    var configuration = NBBottomSheetConfiguration()
    
    //MARK:- ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    //MARK:- View will appear
    override func viewWillAppear(_ animated: Bool) {
        offerTypeLbl.text = offerType
        
        if offerImage == ""{
            
            offerImageView.image = UIImage(named: "imgpsh_fullsize_anim")
            offerImageView.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1)
        }
        else{
            
            
            let placeHolder = UIImage(named: "imgpsh_fullsize_anim")
            let imageURL = URL(string: offerImage)
            offerImageView.sd_setImage(with: imageURL, placeholderImage: placeHolder)
            offerImageView.contentMode = .scaleToFill

        }
        
     
                
        let textString = NSMutableAttributedString(string: offerdescription)
        textString.setColorForText(offerName, with: #colorLiteral(red: 1, green: 0.6588235294, blue: 0.231372549, alpha: 1))
        
        offerdescriptionLbl.attributedText = textString
    }
    
   
    //MARK:- View did layout subviews
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        superView.layer.cornerRadius = 24
        superView.layer.masksToBounds = true
        superView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        view.layer.cornerRadius = 24
        view.layer.masksToBounds = true
        view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
    }
    
    
    //MARK:- button actions
    
    @IBAction func icCrossButttonAction(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func clickHereButtonAction(_ sender: Any) {
        
        print("dergwr")
        dismiss(animated: false) {
            
            if self.from == "Home"{
                self.clickDelegate.clickedAction(flag: 2)
            }
            else if self.from == "Notification"{
                
                
                self.clickDelegate.clickedAction(flag: 1)
            }
        }
        
        
        
        
    }
    
    @IBAction func saveButtonAction(_ sender: Any) {
        
    }
    
}
