//
//  VoucherDetailsViewController.swift
//  Peculi
//
//  Created by iApp on 11/02/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import UIKit

class VoucherDetailsViewController: UIViewController {
    
    //MARK: - outlets
    
    @IBOutlet weak var CardImageView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var VoucherImageView: UIImageView!
    @IBOutlet weak var voucherLbl: UILabel!
    @IBOutlet weak var expirylbl: UILabel!
    @IBOutlet weak var voucherDetailTableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollViewHeight: NSLayoutConstraint!
    @IBOutlet weak var DiscountSetLbl: UILabel!
    
    @IBOutlet weak var viewBarCodeImg: UIView!
    @IBOutlet weak var dashBorderView: UIView!
    @IBOutlet weak var lblBarCodeCopy: UILabel!
    @IBOutlet weak var lblPinNo: UILabel!
    
    @IBOutlet weak var viewBarCodeGenerate: UIView!
    @IBOutlet weak var lblPinBarCodeNo: UILabel!
    @IBOutlet weak var lblShowBarCodeNo: UILabel!
    @IBOutlet weak var imgBarCodeGenerate: UIImageView!
    
    @IBOutlet weak var viewClickRedeem: UIView!
    
    
    //MARK: - variables
    
    
    var amount: String = ""
    var expiryDate: String = ""
    var name: String = ""
    var logo_image_url: String = ""
    var Description: String = ""
    var cardImageURL: String = ""
    var code: String = ""
    var currencyCode: String = ""
    var denomenationType: String = ""
    var expiryPolicy: String = ""
    var expiryMonths: String = ""
    var maxValue: String = ""
    var minValue: String = ""
    var percentDiscount: String = ""
    var redeemInstructionsHTML: String = ""
    var termsAndConditionsHTML: String = ""
    var  voucherUrl = ""
    var clickRedeemCode = ""
    
    var yourViewBorder = CAShapeLayer()
    
    //MARK: - view did load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        navigartionSetup()
        updateUI()
        let urlforApi = "\(voucherUrl)" + "?format=JSON"
        
        print("urlforApi===>>>>>  \(urlforApi)")
       
        getUrlResponse(getUrlResponse:urlforApi)
        
        dashViewBorder()
        viewHide()
 
    }
    
    
    
    /**** setup UI */
    
    func viewHide() {
        
        viewBarCodeImg.isHidden = true
        viewBarCodeGenerate.isHidden = true
        lblShowBarCodeNo.isHidden = true
        viewClickRedeem.isHidden = true
        viewBarCodeGenerate.backgroundColor = UIColor.clear
        
    }
    
    func dashViewBorder() {
        
        yourViewBorder.strokeColor = UIColor.white.cgColor
        yourViewBorder.lineDashPattern = [6, 6]
        yourViewBorder.frame = dashBorderView.bounds
        yourViewBorder.fillColor = nil
        yourViewBorder.path = UIBezierPath(rect: dashBorderView.bounds).cgPath
        dashBorderView.layer.addSublayer(yourViewBorder)
  
    }
    
    
    func updateUI(){
        
        voucherLbl.text = String(format: "From %@", name)
        DiscountSetLbl.text = "\(GlobalVariables.currency)\(amount)"
        print(convertExpiryDate(expiryDate))
        let myString:NSString = "Expires \(convertExpiryDate(expiryDate))" as NSString
        var myMutableString = NSMutableAttributedString()
        myMutableString = NSMutableAttributedString(string: myString as String)
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(displayP3Red: 15.0/255.0, green: 131.0/255.0, blue: 217.0/255.0, alpha: 1.0), range: NSRange(location:0,length:7))
        
        expirylbl.attributedText = myMutableString
        
        VoucherImageView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        VoucherImageView.layer.borderWidth = 1 
        
        let images = cardImageURL

        if images != "" {
            CardImageView.sd_setImage(with: URL(string: images ), placeholderImage:UIImage(named: ""), completed: { (image, error, cacheType, url) -> Void in
                if ((error) != nil) {
                    // set the placeholder image here
                    //cell.imgVw.image = #imageLiteral(resourceName: "doctorAvtar_Img")
                } else {
                    // success ... use the image
                }
            })
        }
        
        let image = logo_image_url
        
        if image != "" {
            VoucherImageView.sd_setImage(with: URL(string: image ), placeholderImage:UIImage(named: ""), completed: { (image, error, cacheType, url) -> Void in
                if ((error) != nil) {
                    // set the placeholder image here
                    //cell.imgVw.image = #imageLiteral(resourceName: "doctorAvtar_Img")
                } else {
                    // success ... use the image
                }
            })
        }
        else {
            
            VoucherImageView.image = UIImage(named: "")
        }

    }
    
    
    //MARK: - Navigation Setup
    
    func navigartionSetup(){
        navigationController?.navigationBar.isHidden = false
        title = ""
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2203302979, green: 0.1600231826, blue: 0.3218392134, alpha: 1)
        self.navigationController?.navigationBar.isTranslucent = true
        
        //MARK: - Navigation LeftBar Button:-
        
        let leftBarButton = UIBarButtonItem(image: UIImage(named: "icBackArrowWhite"), style: .done, target: self, action: #selector(icBackButtonAction))
        self.navigationItem.leftBarButtonItem  = leftBarButton
    }
    
    
    ///convert required date

    func convertExpiryDate(_ dateStr : String) -> String {
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        let newDate = dateFormatterGet.date(from: dateStr) ?? Date()
        let dateformat = DateFormatter()
        dateformat.dateFormat = "MMM d, yyyy"
        return dateformat.string(from: newDate)
        
    }
    
    // BAR CODE GENERATE
    
    func generateBarcode(from string: String) -> UIImage? {
        
        let data = string.data(using: String.Encoding.ascii)
           if let filter = CIFilter(name: "CICode128BarcodeGenerator")
           {
               filter.setValue(data, forKey: "inputMessage")
               let transform = CGAffineTransform(scaleX: 3, y: 3)
               if let output = filter.outputImage?.transformed(by: transform)
               {
                   return UIImage(ciImage: output)
               }
           }
        
           return nil
    }
    
    
    func characterSpacingLblTxt(lbltxt:String) {

        let attributedString = NSMutableAttributedString(string:lbltxt)
        attributedString.addAttribute(NSAttributedString.Key.kern, value: CGFloat(5.0), range: NSRange(location: 0, length: attributedString.length))
        lblShowBarCodeNo.attributedText = attributedString
        
    }

    //MARK: - Object Function Button Action
    
    @objc func icBackButtonAction(){
        popVc()
    }
    
    
    @IBAction func btnCopyTxt_Action(_ sender: Any) {

        UIPasteboard.general.string = lblBarCodeCopy.text
        Toast.show(message: "Copied", controller: self)
    }
    
    
    @IBAction func btnClickRedeem_Action(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "voucherWebView_VC") as! voucherWebView_VC
        vc.urlData = clickRedeemCode
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
}

//MARK: - tableview delegates

extension VoucherDetailsViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.voucherDetailTableView.dequeueReusableCell(withIdentifier: "VoucherDetailsTableViewCell", for: indexPath) as! VoucherDetailsTableViewCell
        
        if indexPath.row == 0 {
            
            cell.lblTitle.text = "Redeem Instructions"
            cell.descriptionTextView.text = redeemInstructionsHTML.htmlToString
            
        } else {

            cell.lblTitle.text = "Terms & Conditions"
            cell.descriptionTextView.text = termsAndConditionsHTML.htmlToString

        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if let lastVisibleIndexPath = tableView.indexPathsForVisibleRows?.last {
            
            if indexPath == lastVisibleIndexPath {
                
                var tableViewHeight: CGFloat {
                    voucherDetailTableView.layoutIfNeeded()
                    return voucherDetailTableView.contentSize.height
                }
                scrollViewHeight.constant = tableViewHeight+236
            }
        }
    }
}


extension VoucherDetailsViewController {
    
   
    func getUrlResponse(getUrlResponse:String) {
        self.addPeculiPopup(view: self.view)
        
        guard let url = URL(string:getUrlResponse ) else { return }

        let task = URLSession.shared.dataTask(with: url) { [self] data, response, error in
           

                guard let data = data, error == nil else { return }

                do {

                    // make sure this JSON is in the format we expect
                    // convert data to json
                    
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        // try to read out a dictionary
                        
                        DispatchQueue.main.async {
                            self.hidePeculiPopup(view: self.view)
                        }
                        print(json)
                        
                        if let data = json["e_code"] as? NSDictionary {
                            print(data)
                            
                            let barcode_format = data["barcode_format"] as? String ?? ""
                            let barcode_string = data["barcode_string"] as? String ?? ""
                            let code = data["code"] as? String ?? ""
                            let pin = data["pin"] as? String ?? "null"
                            
                            print(barcode_format)
                            print(barcode_string)
                            print(code)
                            print(pin)
                            
                            if barcode_format == "" && barcode_string == "" {
                                
                                if code.contains("https://") {
                                    
                                    print(code)
                                    
                                    DispatchQueue.main.async {
                                        
                                        self.viewClickRedeem.isHidden = false
                                        self.viewBarCodeImg.isHidden = true
                                        self.viewBarCodeGenerate.isHidden = true
                                        lblShowBarCodeNo.isHidden = true
                                        self.clickRedeemCode = code

                                    }
                                    
                                } else {
                                    
                                    print(code)
                                    DispatchQueue.main.async {
                                        
                                        self.viewClickRedeem.isHidden = true
                                        self.viewBarCodeImg.isHidden = false
                                        self.viewBarCodeGenerate.isHidden = true
                                        lblShowBarCodeNo.isHidden = true
                                        
                                        self.lblBarCodeCopy.text = code
                                        self.lblPinNo.text = "Pin " + "\(pin)"
                                    }
                                    
                                }
                                
                            } else {
                                
                                print(code)
                                
                                DispatchQueue.main.async {
                                    
                                    self.viewClickRedeem.isHidden = true
                                    self.viewBarCodeImg.isHidden = true
                                    self.viewBarCodeGenerate.isHidden = false
                                    lblShowBarCodeNo.isHidden = false
                                    
                                    self.lblPinBarCodeNo.text = "Pin " + "\(pin)"
                                    
                                    characterSpacingLblTxt(lbltxt:barcode_string)
                                    
                                    let image = generateBarcode(from: barcode_string)
                                    self.imgBarCodeGenerate.image = image
                                    
                                }
 
                            }

                        }
                    }
                    
                } catch let error as NSError {
                    
                    DispatchQueue.main.async {
                        self.hidePeculiPopup(view: self.view)
                    }
                    print("Failed to load: \(error.localizedDescription)")
                }
        }
        
        DispatchQueue.main.async {
            self.hidePeculiPopup(view: self.view)
        }

            task.resume()
        
    }

}
