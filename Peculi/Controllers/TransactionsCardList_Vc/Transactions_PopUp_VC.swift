//
//  Transactions_PopUp_VC.swift
//  Peculi
//
//  Created by PPI on 4/27/22.
//  Copyright © 2022 Stealth Tech. All rights reserved.
//

import UIKit

class Transactions_PopUp_VC: UIViewController {
    
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var popUpChildView: UIView!
    @IBOutlet var popUpSuperView: UIView!
    @IBOutlet weak var titleView: UIView!
    
    
    var selectCatId = ""
    var transactionModel = [TransactionPopUp_Model]()
    var clickTransctionsHistory = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        myTableView.delegate = self
        myTableView.dataSource = self
        
        self.setUpviewCornerRadius()
        self.setupView()
        self.animateView()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if clickTransctionsHistory == "1" {
            
            clickTransctionsHistory = "0"
            
        } else {
            
            recurrenceTransaction_Api(selectCatId:selectCatId)
        }
 
    }
    
  
    // MARK: - FUNCTION
    
    /**** UI setup */
    
    func setUpviewCornerRadius(){
        
        titleView.clipsToBounds = true
        titleView.layer.cornerRadius = 10
        titleView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        
        popUpChildView.clipsToBounds = true
        popUpChildView.layer.cornerRadius = 10
        popUpChildView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        
    }
    
    func setupView(){
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        popUpSuperView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        
    }
     
     //animation setup
    
     func animateView() {
         
         popUpChildView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
         UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
             self.popUpChildView.transform = .identity
         }, completion: {(finished: Bool) -> Void in
             
         })
         
     }
    
    @IBAction func btnCancel_Action(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        print("call cancel ")
    }

}


extension Transactions_PopUp_VC   {

    func recurrenceTransaction_Api(selectCatId:String){
    
    self.addPeculiPopup(view: self.view)
    
    let params:[String:Any] = ["catId" : selectCatId ]
    
    print(params)
    
    DataService.sharedInstance.postApiCall(endpoint: constantApis.recurrenceTransaction_Api, params: params) { (response, error) in
        
        self.hidePeculiPopup(view: self.view)
        
        if response != nil {
            
            print(response ?? [:])
            let codeInt = response?["code"] as? Int ?? 0
            let codeString = response?["code"] as? String ?? ""
            let message = response?["message"] as? String ?? ""
            if codeInt == 200 || codeString == "200" {

                if let result = response?["result"] as? [NSDictionary] {
                   
                    self.transactionModel.removeAll()
                    
                    for allData in result {

                        let createdAt = allData["createdAt"] as? String ?? ""
                        let time = allData["time"] as? String ?? ""
                        let amount =  allData["amount"]  as? Double ?? 0.00
                        let transactionType = allData["transactionType"] as? String ?? ""
                        let categoryImage = allData["categoryImage"] as? String ?? ""
                        
                        self.transactionModel.append(TransactionPopUp_Model.init(categoryImage: categoryImage, createdAt: createdAt, time: time, amount: amount, transactionType: transactionType))
                        
                    }
                    
                    self.myTableView.reloadData()
                   
                }

               let message = response?["message"] as? String ?? "0"
                Toast.show(message: message, controller: self)
               
              
            } else {
               
                self.customAlertDismiss(title: "Alert", message: message)
                self.dismiss(animated: true, completion: nil)
            }
            
        }
        else {
            
            self.customAlertDismiss(title: "Alert", message: "Something went wrong!")
            self.dismiss(animated: true, completion: nil)
        }
    }
}
}



extension Transactions_PopUp_VC : UITableViewDelegate, UITableViewDataSource  {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if clickTransctionsHistory == "1" {
            
            return 1
            
        } else {
            return transactionModel.count
        }
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                
        if clickTransctionsHistory == "1" {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "transactionsPopUp_TVCell") as! transactionsPopUp_TVCell
            cell.selectionStyle = .none

            return cell
            
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "transactionsPopUp_TVCell") as! transactionsPopUp_TVCell
            
            cell.lblCardPayment.text = "transectionvNotvYet"
            cell.cardImg.isHidden = true
            cell.selectionStyle = .none
        
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd"

            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "d MMM yyyy"

            let date: NSDate? = dateFormatterGet.date(from: transactionModel[indexPath.row].createdAt) as NSDate?
            
            print(dateFormatterPrint.string(from: date! as Date))
            
            let convertDate = (dateFormatterPrint.string(from: date! as Date))
            
            cell.lblDate.text = "\(convertDate)" + " at " + "\(transactionModel[indexPath.row].time)"
            cell.lblDepositValue.text = "£" + "\(transactionModel[indexPath.row].amount)" + ".00"
            
           var transactionType = ""
            transactionType = transactionModel[indexPath.row].transactionType
            
            if transactionType == "recurrence" {
                cell.lblCardPayment.text = "Regular Deposits"
            } else {
                cell.lblCardPayment.text = "One Time Deposits"
            }
            
            var originalString = String()
            
            originalString = transactionModel[indexPath.row].categoryImage
            let ImageUrl = String(format: "%@%@", AppConfig.ImageUrl,originalString)
            
            cell.cardImg.sd_setImage(with: URL(string:ImageUrl), placeholderImage:UIImage(named: "placeholder_Img"), completed: { (image, error, cacheType, url) -> Void in
                if ((error) != nil) {
                    // set the placeholder image here
                    //cell.imgVw.image = #imageLiteral(resourceName: "doctorAvtar_Img")
                } else {
                    // success ... use the image
                }
            })
            
            print("convertDate ===>>>> \(convertDate)")
 
                return cell
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60

    }

}


