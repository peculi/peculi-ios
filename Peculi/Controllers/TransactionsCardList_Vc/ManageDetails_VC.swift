//
//  ManageDetails_VC.swift
//  Peculi
//
//  Created by PPI on 4/26/22.
//  Copyright © 2022 Stealth Tech. All rights reserved.
//

import UIKit
import DropDown

class ManageDetails_VC: UIViewController {
    
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var myTblHeight: NSLayoutConstraint!
    
    @IBOutlet weak var btnRegularDeposit: UIButton!
    @IBOutlet weak var btnSubscription: UIButton!
    
    let yellowColor = UIColor(red: 245/255.0, green: 176/255.0, blue: 69/255.0, alpha: 1)
    
    let GrayColor = UIColor(red: 152/255.0, green: 152/255.0, blue: 152/255.0, alpha: 1)
    
    let dropDown = DropDown()
    var getRecurranceData = [GetRecurrance_Model]()
    var getSubscriptionData = [GetSubcription_Model]()
    
    var selectTranasactionId = ""
    var btnClickValue = ""
    var subscriptionType = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        firstTimeCall()
        
        navigartionSetup()
        myTableView.delegate = self
        myTableView.dataSource = self
        
  
//               DropDown.appearance().backgroundColor = #colorLiteral(red: 0.1209298894, green: 0.1362816691, blue: 0.1861090362, alpha: 1)//UIColor.gray
//                DropDown.appearance().selectionBackgroundColor = UIColor.black
//                DropDown.appearance().textColor = UIColor.white
//                DropDown.appearance().selectedTextColor = UIColor.white
//                DropDown.appearance().cornerRadius = 4
//
      
        
        
    }

    
    
    func navigartionSetup(){

        //icBackArrowWhite
        
        navigationController?.navigationBar.isHidden = false
        title = "Manage Details"
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2203302979, green: 0.1600231826, blue: 0.3218392134, alpha: 1)
        self.navigationController?.navigationBar.isTranslucent = true

        //MARK: - Navigation LeftBar Button:-

        let leftBarButton = UIBarButtonItem(image: UIImage(named: "icBackArrowWhite"), style: .done, target: self, action: #selector(icBackButtonAction))
        self.navigationItem.leftBarButtonItem  = leftBarButton
        
    }
    
    func popUpTransactionDetailAlert() {
            
        
            let sb = UIStoryboard(name: "Main", bundle:nil)
            let addTranasactionsAlert = sb.instantiateViewController(withIdentifier: "Transactions_PopUp_VC") as! Transactions_PopUp_VC

        addTranasactionsAlert.selectCatId = selectTranasactionId
        
       // customAlert.textFieldData = navName.capitalizingFirstLetter()
        //  addCardAlert.categoryId = CategoryInfo[0].userData.categoryDetails._id
        
        addTranasactionsAlert.providesPresentationContextTransitionStyle = true
        addTranasactionsAlert.definesPresentationContext = true
        addTranasactionsAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        addTranasactionsAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        
        self.present(addTranasactionsAlert, animated: true, completion: nil)
        
    }
        
    
    //MARK: - Object Function Button Action
    
    @objc func icBackButtonAction(){
        
        self.navigationController?.popViewController(animated: true)
        
    }

        
    
    func firstTimeCall() {
        btnSubscription.backgroundColor = GrayColor
        btnRegularDeposit.backgroundColor = yellowColor
        btnClickValue = "0"
        
        self.getRecurrance_Api()
    }
    
    
    
    
    
// MARK: - BUTTON ACTION
    
    @IBAction func btnRegularDeposit_Action(_ sender: Any) {
        
        btnSubscription.backgroundColor = GrayColor
        btnRegularDeposit.backgroundColor = yellowColor
        btnClickValue = "0"
        
        self.getRecurrance_Api()
        
    }
    
    
    @IBAction func btnSubscriptions(_ sender: Any) {
        
        btnSubscription.backgroundColor = yellowColor
        btnRegularDeposit.backgroundColor = GrayColor
        btnClickValue = "1"
        
        
        getSubscription_Api()
        
    }
    
    
  
}




extension ManageDetails_VC : UITableViewDelegate, UITableViewDataSource  {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if btnClickValue == "0" {
            return getRecurranceData.count
        } else {
            return getSubscriptionData.count
        }
      
        

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                    
            let cell = tableView.dequeueReusableCell(withIdentifier: "ManageDetailTV_Cell") as! ManageDetailTV_Cell
        
        if btnClickValue == "0" {
            
            cell.bgView.isHidden = false
            cell.subscriptionView.isHidden = true
            
            cell.selectionStyle = .none
            cell.lblCardName.text = getRecurranceData[indexPath.row].cardName
            cell.lblPotName.text = getRecurranceData[indexPath.row].categoryName
            
            cell.lblTarget.text =  getRecurranceData[indexPath.row].targetAmount
            cell.lblFrequency.text =  getRecurranceData[indexPath.row].targetType
            
            cell.lblAmountSaved.text = getRecurranceData[indexPath.row].amountPaid
            cell.lblDueDate.text = getRecurranceData[indexPath.row].dueDate
            
            // CELL BUTTON ACTION
               
            cell.btnDropDownPressed = {


                print("Mohit Rana")

                self.dropDown.dataSource = ["Cancel", "Details"]
                self.dropDown.anchorView = cell.btnDropDown
                self.dropDown.textColor = .black
                self.dropDown.bottomOffset = CGPoint(x: -60, y: cell.btnDropDown.frame.size.height)
                self.dropDown.backgroundColor = .white
                self.dropDown.show()
                self.dropDown.direction = .bottom
                self.dropDown.width = 100
                self.dropDown.cornerRadius = 8
                
                self.dropDown.selectionAction = { [weak self] (index:Int, item:String) in
                    
                     guard let _ = self else { return }
                    
                    
                    if item == "Cancel" {
                        
                        let recurranceId = self?.getRecurranceData[indexPath.row]._id ?? ""
                        self?.cancelRecurrance_Api(recurranceId:recurranceId)
                        
                        print("recurranceId==>\(recurranceId)")
                        print("Cancel")
                       
                    } else {

                        self?.selectTranasactionId = self?.getRecurranceData[indexPath.row].categoryId ?? ""
                        
                        self?.popUpTransactionDetailAlert()

                        print("Details")
                        print("categoryId==>\(self?.getRecurranceData[indexPath.row].categoryId ?? "")")
                    }
                  
                     }
                   }
            
            
        } else {
            
            cell.bgView.isHidden = true
            cell.subscriptionView.isHidden = false
            
//            let type = getSubscriptionData[indexPath.row].subscriptionId
//            let substring1 = type.dropFirst(7)
//
//            print("accountNumber==>>\(substring1)")

            cell.subType.text = getSubscriptionData[indexPath.row].subscriptionId //"\(substring1)"
            
            cell.subAmount.text = getSubscriptionData[indexPath.row].price
            
            var dateTo = Date()
            

            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let newDate = dateFormatter.date(from: getSubscriptionData[indexPath.row].expiryDate) ?? Date()
            
            dateFormatter.locale =  NSLocale(localeIdentifier: "en") as Locale?
            let finalDate = dateFormatter.string(from: newDate)
            cell.subExpiryDate.text = finalDate
            
            
            
            cell.btnDropDownPressedS = {
                
                print("Mohit Rana")
                
                self.dropDown.dataSource = ["Cancel"]
                self.dropDown.anchorView = cell.btnDropDown
                self.dropDown.textColor = .black
                self.dropDown.bottomOffset = CGPoint(x: -60, y: cell.btnDropDownS.frame.size.height)
                self.dropDown.backgroundColor = .white
                self.dropDown.show()
                self.dropDown.direction = .bottom
                self.dropDown.width = 100
                self.dropDown.cornerRadius = 8
                
                self.dropDown.selectionAction = { [weak self] (index:Int, item:String) in
                    
                     guard let _ = self else { return }
                    
                    
                    if item == "Cancel" {
                 
                        print("Cancel")
                        
                        
                        if self?.getSubscriptionData[indexPath.row].deviceType == "android" {
                            
                            UIApplication.shared.openURL(URL(string: "https://play.google.com/store/account/subscriptions")!)
                            
                            
                        } else {
                            
//                            if let url = URL(string: "itms-apps://apps.apple.com/account/subscriptions") {
//                                if UIApplication.shared.canOpenURL(url) {
//                                    UIApplication.shared.open(url, options: [:])
//                                }
//                            }

                           UIApplication.shared.openURL(URL(string: "https://support.apple.com/en-us/HT202039")!)
                       
                        }
                        
                        
                        
                        
                        
                        
                       
                        
                       
                    } else {

              
                    }
                  
                     }
                   }
            
            
        }
        
        

            return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        if btnClickValue == "0" {
            return 280
        } else {
            return 140
        }
        
        
    }
    
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//
//
//    }


    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        if let lastVisibleIndexPath = myTableView.indexPathsForVisibleRows?.last {

            if indexPath == lastVisibleIndexPath {

                var glanceAtIEPtableViewHeight: CGFloat {

                    myTableView.layoutIfNeeded()
                    return myTableView.contentSize.height

                }
                myTblHeight.constant = glanceAtIEPtableViewHeight + 50
            }
        }

    }

    
}


extension ManageDetails_VC   {

func getRecurrance_Api(){
    
    self.addPeculiPopup(view: self.view)
    
    let params:[String:Any] = [ : ]
    
    print(params)
    
    DataService.sharedInstance.postApiCall(endpoint: constantApis.getRecurrance_Api, params: params) { (response, error) in
        
        self.hidePeculiPopup(view: self.view)
        
        if response != nil {
            print(response ?? [:])
            let codeInt = response?["code"] as? Int ?? 0
            let codeString = response?["code"] as? String ?? ""
            let message = response?["message"] as? String ?? ""
            if codeInt == 200 || codeString == "200" {

                if let result = response?["result"] as? [NSDictionary] {
                
                    self.getRecurranceData.removeAll()
                    
                    for allData in result {
                        
                        let _id = allData["_id"] as? String ?? ""
                        let cancelStatus = allData["cancelStatus"] as? Bool ?? true
                        let targetType =  allData["targetType"] as? String ?? ""
                        let targetAmount = allData["targetAmount"] as? String ?? ""
                        let amountPaid = allData["amountPaid"] as? String ?? ""
                        let startDate = allData["startDate"] as? String ?? ""
                        let dueDate = allData["dueDate"] as? String ?? ""
                        let categoryId = allData["categoryId"] as? String ?? ""
                        let categoryName = allData["categoryName"] as? String ?? ""
                        let cardName = allData["cardName"] as? String ?? ""
                        
                        if cancelStatus == false {
                            
                            self.getRecurranceData.append(GetRecurrance_Model.init(cancelStatus: cancelStatus, _id: _id, targetType: targetType, targetAmount: targetAmount, amountPaid: amountPaid, startDate: startDate, dueDate: dueDate, categoryId: categoryId, categoryName: categoryName, cardName: cardName))

                        } else {
                            print("cancelStatus == True")
                        }
                    }
                    
                    self.myTableView.reloadData()
                    print(self.getRecurranceData)
                }
                
//               let message = response?["message"] as? String ?? "0"
//                Toast.show(message: message, controller: self)
               
              
            } else {
               
                self.customAlertDismiss(title: "Alert", message: message)
                self.dismiss(animated: true, completion: nil)
            }
            
        }
        else {
            
            self.customAlertDismiss(title: "Alert", message: "Something went wrong!")
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}
    
    
    func cancelRecurrance_Api(recurranceId:String){
        
        self.addPeculiPopup(view: self.view)
        
        let params:[String:Any] = ["recurranceId" :recurranceId ]
        
        print(params)
        
        DataService.sharedInstance.postApiCall(endpoint: constantApis.cancelRecurrance_Api, params: params) { (response, error) in
            
            self.hidePeculiPopup(view: self.view)
            
            if response != nil {
                print(response ?? [:])
                let codeInt = response?["code"] as? Int ?? 0
                let codeString = response?["code"] as? String ?? ""
                let message = response?["message"] as? String ?? ""
                if codeInt == 200 || codeString == "200" {

                    self.getRecurrance_Api()
                   //let message = response?["message"] as? String ?? "0"
                    //Toast.show(message: message, controller: self)
                   
                  
                } else {
                   
                    self.customAlertDismiss(title: "Alert", message: message)
                    self.dismiss(animated: true, completion: nil)
                }
                
            }
            else {
                
                self.customAlertDismiss(title: "Alert", message: "Something went wrong!")
                self.dismiss(animated: true, completion: nil)
            }
        }
        
    }
    
    
    
    
    func getSubscription_Api(){
        
        self.addPeculiPopup(view: self.view)
        
        let params:[String:Any] = [ : ]
        
        print(params)
        
        DataService.sharedInstance.getApiCall(endpoint: constantApis.getSubscriptions_Api, params: params) { (response, error) in
            
            self.hidePeculiPopup(view: self.view)
            print(response)
            
            if response != nil {
                print(response ?? [:])
                let codeInt = response?["code"] as? Int ?? 0
                let codeString = response?["code"] as? String ?? ""
                let message = response?["message"] as? String ?? ""
                if codeInt == 200 || codeString == "200" {

                    self.getSubscriptionData.removeAll()
                   
                    
                    if let result = response?["result"] as? NSDictionary {
                    
                        print(result)
                        
                        let expiryDate = result["expiryDate"] as? String ?? ""
                        let price = result["price"] as? String ?? ""
                       let subscriptionId = result["subscriptionId"] as? String ?? ""
                        let deviceType = result["deviceType"] as? String ?? ""
                        
                      
                        if subscriptionId.contains("monthly") {
                            self.subscriptionType = "monthly"
                        } else {
                            self.subscriptionType = "yearly"
                        }
                        
                        
                        
                        self.getSubscriptionData.append(GetSubcription_Model.init(expiryDate: expiryDate, price: price, subscriptionId: self.subscriptionType, deviceType: deviceType))

                   
                  self.myTableView.reloadData()
                       
                    }
                    
//                   let message = response?["message"] as? String ?? "0"
//                    Toast.show(message: message, controller: self)
//
                  
                } else {
                   
                    self.customAlertDismiss(title: "Alert", message: message)
                    self.dismiss(animated: true, completion: nil)
                }
                
            }
            else {
                
                self.customAlertDismiss(title: "Alert", message: "Something went wrong!")
                self.dismiss(animated: true, completion: nil)
            }
        }
        
    }
    
    
}
