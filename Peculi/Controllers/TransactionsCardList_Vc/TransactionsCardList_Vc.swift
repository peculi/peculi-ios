//
//  TransactionsCardList_Vc.swift
//  Peculi
//
//  Created by PPI on 4/18/22.
//  Copyright © 2022 Stealth Tech. All rights reserved.
//

import UIKit
import SideMenu
import DropDown

class TransactionsCardList_Vc: UIViewController {
    
    
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var allTransactionTableView: UITableView!
    
    @IBOutlet weak var transactionTblHeight: NSLayoutConstraint!
    @IBOutlet weak var myTblviewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblTransactionType: UILabel!
    @IBOutlet weak var btnTransactionDropDown: UIButton!
    
    
    var selectedRows:[IndexPath] = []
    var getCardModel = [GetCArd_ModelStructure]()
    var cardSelectId = ""
    var TapCell = false
    
    var transactionModel = [TransactionPopUp_Model]()
    var potTransactionHistryModel = [potTransactionHistory_Model]()
    var bankTransactionHistryModel = [bankTransactionHistory_Model]()
    var DirectCreditHistoryModel = [DirectCreditHistory_Model]()
    var dropdwnTarget = DropDown()
    var setDropDownArr = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        TapCell = false

        myTableView.delegate = self
        myTableView.dataSource = self
        allTransactionTableView.delegate = self
        allTransactionTableView.dataSource = self

        GetCard_Api()

        setUpDropDown()
        
        
        
        allTransactionTableView.register(UINib(nibName: "potTransactionHistoryTV_Cell", bundle: nil), forCellReuseIdentifier: "potTransactionHistoryTV_Cell")
        
    }
    
    //MARK: - ViewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        navigartionSetup()
        setNeedsStatusBarAppearanceUpdate()
 
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    

    //MARK: - Navigation Setup
    
    
    func navigartionSetup(){

        //icBackArrowWhite
        navigationController?.navigationBar.isHidden = false
        title = "Transactions"
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2203302979, green: 0.1600231826, blue: 0.3218392134, alpha: 1)
        self.navigationController?.navigationBar.isTranslucent = true

        //MARK: - Navigation LeftBar Button:-

        let leftBarButton = UIBarButtonItem(image: UIImage(named: "icSideMenu"), style: .done, target: self, action: #selector(icBackButtonAction))
        self.navigationItem.leftBarButtonItem  = leftBarButton
    }
        
    
    
    func setupTargetDropDown() {
     
            dropdwnTarget.anchorView = btnTransactionDropDown
        dropdwnTarget.bottomOffset = CGPoint(x: -120 , y: btnTransactionDropDown.bounds.height + 5 )//
            dropdwnTarget.dataSource = setDropDownArr
            dropdwnTarget.selectionAction = { [weak self] (index, item) in
                
                self?.lblTransactionType.text = item
                
                if item == "One Time Deposits" {
          
                    self?.transactionHistory_Api(type: "onetime")
                    
                } else if item == "Pot to Pot" {
               
                    self?.transactionHistory_Api(type: "topot")
                    
                } else if item == "Regular Deposits" {
                 
                    self?.transactionHistory_Api(type: "recurrence")
                    
                } else if item == "Direct Credit" {
                    
                    self?.transactionHistory_Api(type: "directcredit")
                    
                } else {

                    self?.transactionHistory_Api(type: "tobank")
                }

            }

        }
    
    
    func setUpDropDown() {

        setDropDownArr = ["One Time Deposits", "Pot to Pot", "Regular Deposits", "Withdrawals", "Direct Credit"]

        lblTransactionType.text = "One Time Deposits"
        
        self.transactionHistory_Api(type: "onetime")
        
        DropDown.appearance().backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)  //UIColor.gray
       // DropDown.appearance().selectionBackgroundColor = UIColor.black
       // DropDown.appearance().textColor = UIColor.white
       // DropDown.appearance().selectedTextColor = UIColor.white
        DropDown.appearance().textColor = UIColor.black
        DropDown.appearance().cornerRadius = 4
        dropdwnTarget.direction = .bottom
        self.dropdwnTarget.width = 150
        dropdwnTarget.shadowColor = UIColor.clear
        
    }
    

    //MARK: - Object Function Button Action
    
    @objc func icBackButtonAction(){
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "menuLeftNavigationController") as! SideMenuNavigationController
        nextViewController.leftSide = true
        nextViewController.menuWidth = UIScreen.main.bounds.size.width - 100
        self.present(nextViewController, animated:true, completion:nil)
    }

    //MARK: -  Button Action
    
    
    @IBAction func btnManageDeposit_Action(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ManageDetails_VC") as! ManageDetails_VC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    @IBAction func btnTransactionDroDown_Action(_ sender: Any) {
        setupTargetDropDown()
        dropdwnTarget.show()
    }
   
}


extension TransactionsCardList_Vc : UITableViewDelegate, UITableViewDataSource  {


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == myTableView {
            
            return getCardModel.count
            
        } else {
            
            if lblTransactionType.text == "One Time Deposits" || lblTransactionType.text == "Regular Deposits" {
                
                return transactionModel.count
                
            } else if lblTransactionType.text == "Pot to Pot" {
                
                return potTransactionHistryModel.count
                
            } else if lblTransactionType.text == "Direct Credit" {
                
                return DirectCreditHistoryModel.count
                
            } else {
                
                return bankTransactionHistryModel.count
                
            }
            
            
        }

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if tableView == myTableView {


            let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionsCardListTv_Cell") as! TransactionsCardListTv_Cell

            cell.cardLbl.text = getCardModel[indexPath.row].cardName
            cell.lblPrimaryCard.isHidden = true

            cell.selectionStyle = .none

            if TapCell == false {

                if getCardModel[indexPath.row].primaryCard == true {

                    cell.circleImg.image = UIImage.init(named: "Ic_ChangePrimaryStatus")
                    cell.lblPrimaryCard.isHidden = false

                } else {
                    
                    cell.lblPrimaryCard.isHidden = true
                    cell.circleImg.image = UIImage.init(named: "Ic_UnselectCircle-1")
                }

            } else {

                if selectedRows.contains(indexPath) {

                    cell.circleImg.image = UIImage.init(named: "Ic_ChangePrimaryStatus")
                    cell.lblPrimaryCard.isHidden = false

                } else {

                    cell.lblPrimaryCard.isHidden = true
                    cell.circleImg.image = UIImage.init(named: "Ic_UnselectCircle-1")
                }

            }

            cell.backgroundColor = UIColor.clear

            return cell

        } else {
            
            
            if lblTransactionType.text == "One Time Deposits" || lblTransactionType.text == "Regular Deposits" {
                
                let cell1 = tableView.dequeueReusableCell(withIdentifier: "AllTransactionTV_Cell") as! AllTransactionTV_Cell
                
                cell1.oneTimeTransactionView.isHidden = false
                cell1.potTransactionView.isHidden = true
                cell1.bankTransactionView.isHidden = true
                
                let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "yyyy-MM-dd"

                let dateFormatterPrint = DateFormatter()
                dateFormatterPrint.dateFormat = "d MMM yyyy"

                let date: NSDate? = dateFormatterGet.date(from: transactionModel[indexPath.row].createdAt) as NSDate?

               // print(dateFormatterPrint.string(from: date? as Date))

                let convertDate = (dateFormatterPrint.string(from: date! as Date))

                cell1.lblDate.text = "\(convertDate)" + " at " + "\(transactionModel[indexPath.row].time)"
                
                let priceConvert = transactionModel[indexPath.row].amount
                let price = priceConvert.format()

                cell1.lblDepositValue.text = "\(GlobalVariables.currency)\(price)" //"£" + "\(transactionModel[indexPath.row].amount)"

                var transactionType = ""
                 transactionType = transactionModel[indexPath.row].transactionType
                
                 if transactionType == "recurrence" {
                     cell1.lblCardPayment.text = "Regular Deposits"
                     
                 } else {
                     
                     cell1.lblCardPayment.text = "One Time Deposits"
                 }

                 var originalString = String()

                 originalString = transactionModel[indexPath.row].categoryImage
                 let ImageUrl = String(format: "%@%@", AppConfig.ImageUrl,originalString)

                 cell1.cardImg.sd_setImage(with: URL(string:ImageUrl), placeholderImage:UIImage(named: "placeholder_Img"), completed: { (image, error, cacheType, url) -> Void in
                     if ((error) != nil) {
                         // set the placeholder image here
                         //cell.imgVw.image = #imageLiteral(resourceName: "doctorAvtar_Img")
                     } else {
                         
                         // success ... use the image
                         
                     }
                 })
 
                cell1.selectionStyle = .none
               
                return cell1

                
            } else if lblTransactionType.text == "Direct Credit" {
                
                let cell1 = tableView.dequeueReusableCell(withIdentifier: "AllTransactionTV_Cell") as! AllTransactionTV_Cell

                cell1.oneTimeTransactionView.isHidden = false
                cell1.potTransactionView.isHidden = true
                cell1.bankTransactionView.isHidden = true
                
                
                let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"

                let dateFormatterPrint = DateFormatter()
                dateFormatterPrint.dateFormat = "d MMM yyyy HH:mm a"

                let date: NSDate? = dateFormatterGet.date(from: DirectCreditHistoryModel[indexPath.row].createdAt) as NSDate?

               // print(dateFormatterPrint.string(from: date? as Date))

                let convertDate = (dateFormatterPrint.string(from: date! as Date))
                
                cell1.lblDate.text = "\(convertDate)"
                
                cell1.lblCardPayment.text = "Direct Deposits"
                
                let amount = Double(DirectCreditHistoryModel[indexPath.row].paymentAmount)
                let priceConvert = amount?.format()
                cell1.lblDepositValue.text = "£" + "\(priceConvert ?? "")" //"£" + "\(DirectCreditHistoryModel[indexPath.row].paymentAmount)" + ".00"
                
                var originalString = String()

                originalString = DirectCreditHistoryModel[indexPath.row].categoryImage
                let ImageUrl = String(format: "%@%@", AppConfig.ImageUrl,originalString)

                cell1.cardImg.sd_setImage(with: URL(string:ImageUrl), placeholderImage:UIImage(named: "placeholder_Img"), completed: { (image, error, cacheType, url) -> Void in
                    if ((error) != nil) {
                        
                        // set the placeholder image here
                        
                        //cell.imgVw.image = #imageLiteral(resourceName: "doctorAvtar_Img")
                        
                    } else {
                        
                        // success ... use the image
                        
                    }
                })
                
                
                
                cell1.selectionStyle = .none
               
                return cell1
                
            } else if lblTransactionType.text == "Pot to Pot" {
                
                
                let cell1 = tableView.dequeueReusableCell(withIdentifier: "AllTransactionTV_Cell") as! AllTransactionTV_Cell
                print("None")
                
                cell1.oneTimeTransactionView.isHidden = true
                cell1.potTransactionView.isHidden = false
                cell1.bankTransactionView.isHidden = true
                
                
                let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"

                let dateFormatterPrint = DateFormatter()
                dateFormatterPrint.dateFormat = "d MMM yyyy HH:mm a"

                let date: NSDate? = dateFormatterGet.date(from: potTransactionHistryModel[indexPath.row].createdAt) as NSDate?

               print(dateFormatterPrint.string(from: date! as Date))

                let convertDate = (dateFormatterPrint.string(from: date! as Date))
                
                cell1.lblDate1.text = convertDate
             
                cell1.tolblName.text = potTransactionHistryModel[indexPath.row].toCategoryName
                cell1.fromLbl.text = potTransactionHistryModel[indexPath.row].fromCategoryName
                
                let priceConvert = Double("\(potTransactionHistryModel[indexPath.row].amount)") ?? 0.0
                let price = priceConvert.format()
                cell1.lblAmount.text = "\(GlobalVariables.currency)\(price)"
                

                var fromCategoryImage = String()
                var toCategoryImg = String()

                fromCategoryImage = potTransactionHistryModel[indexPath.row].fromCategoryImage
                let ImageUrl = String(format: "%@%@", AppConfig.ImageUrl,fromCategoryImage)

                cell1.fromImg.sd_setImage(with: URL(string:ImageUrl), placeholderImage:UIImage(named: "placeholder_Img"), completed: { (image, error, cacheType, url) -> Void in
                    if ((error) != nil) {
                        // set the placeholder image here
                        //cell.imgVw.image = #imageLiteral(resourceName: "doctorAvtar_Img")
                    } else {
                        // success ... use the image
                    }
                })

                toCategoryImg = potTransactionHistryModel[indexPath.row].toCategoryImage
                let ImageUrl1 = String(format: "%@%@", AppConfig.ImageUrl,toCategoryImg)

                cell1.toImg.sd_setImage(with: URL(string:ImageUrl1), placeholderImage:UIImage(named: "placeholder_Img"), completed: { (image, error, cacheType, url) -> Void in
                    if ((error) != nil) {
                        // set the placeholder image here
                        //cell.imgVw.image = #imageLiteral(resourceName: "doctorAvtar_Img")
                    } else {
                        // success ... use the image
                    }
                })

                return cell1
            }
            
            else {
                
                let cell1 = tableView.dequeueReusableCell(withIdentifier: "AllTransactionTV_Cell") as! AllTransactionTV_Cell
                
                print("None")
                
                cell1.oneTimeTransactionView.isHidden = true
                cell1.potTransactionView.isHidden = true
                cell1.bankTransactionView.isHidden = false
                
                let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"

                let dateFormatterPrint = DateFormatter()
                dateFormatterPrint.dateFormat = "d MMM yyyy"

                let date: NSDate? = dateFormatterGet.date(from: bankTransactionHistryModel[indexPath.row].createdAt) as NSDate?

               print(dateFormatterPrint.string(from: date! as Date))

                let convertDate = (dateFormatterPrint.string(from: date! as Date))
                
                cell1.bankDateCreate.text = convertDate

                var toCategoryImg = String()

                toCategoryImg = bankTransactionHistryModel[indexPath.row].toCategoryImage
                let ImageUrl1 = String(format: "%@%@", AppConfig.ImageUrl,toCategoryImg)

                cell1.bankToImg.sd_setImage(with: URL(string:ImageUrl1), placeholderImage:UIImage(named: "placeholder_Img"), completed: { (image, error, cacheType, url) -> Void in
                    
                    if ((error) != nil) {
                        
                        // set the placeholder image here
                        //cell.imgVw.image = #imageLiteral(resourceName: "doctorAvtar_Img")
                        
                    } else {
                        // success ... use the image
                        
                
                    }
                })
                
                
                cell1.bankTolbl.text = bankTransactionHistryModel[indexPath.row].toCategoryName
                cell1.bankFirstName.text = "\(bankTransactionHistryModel[indexPath.row].firstName)" + " \(bankTransactionHistryModel[indexPath.row].lastName)"
                let bankAccount = bankTransactionHistryModel[indexPath.row].accountIdentifier
                
                var substringReplace =  bankAccount
                let endIndex = substringReplace.index(substringReplace.startIndex,
                                                      offsetBy: 3)
                substringReplace.replaceSubrange(...endIndex,
                                                 with: "****")

                print(substringReplace)
                
                cell1.bankAcoountNo.text = substringReplace
                
                let priceConvert = Double(bankTransactionHistryModel[indexPath.row].paymentAmount)
                let price = priceConvert?.format()
                cell1.lblaymentAmount.text = "\(GlobalVariables.currency)\(price ?? "0.00")"

                return cell1
            }

        }


    }
    
    
    
        
    
    
 


    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        if tableView == myTableView {
            return 60.0

        } else {
            
            if lblTransactionType.text == "One Time Deposits" || lblTransactionType.text == "Regular Deposits" {
                return 90
            } else {
                return 100
            }
           
        }

    }


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if tableView == myTableView {

            let selectedIndexPath = IndexPath(row: indexPath.row, section: 0)

            self.selectedRows.removeAll()

            if self.selectedRows.contains(selectedIndexPath) {

                self.selectedRows.remove(at: self.selectedRows.index(of: selectedIndexPath)!)
    

            } else {

                self.TapCell = true
                self.selectedRows.append(selectedIndexPath)
                cardSelectId = getCardModel[indexPath.row]._id
                print("cardSelectId===>>>> \(cardSelectId)")
                self.UpdateCardStatus_Api(cardSelectId:cardSelectId)
                
            }

            myTableView.reloadData()

        } else {

            
            
        }

    }


    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        if tableView == myTableView {

            if let lastVisibleIndexPath = myTableView.indexPathsForVisibleRows?.last {

                if indexPath == lastVisibleIndexPath {

                    var glanceAtIEPtableViewHeight: CGFloat {

                        myTableView.layoutIfNeeded()
                        return myTableView.contentSize.height
                    }
                    
                    myTblviewHeight.constant = glanceAtIEPtableViewHeight + 30
                }
            }


        } else {


            if let lastVisibleIndexPath = allTransactionTableView.indexPathsForVisibleRows?.last {

                if indexPath == lastVisibleIndexPath {

                    var glanceAtIEPtableViewHeight: CGFloat {

                        allTransactionTableView.layoutIfNeeded()
                        return allTransactionTableView.contentSize.height

                    }
                    transactionTblHeight.constant = glanceAtIEPtableViewHeight + 30
                }
            }
        }

    }

    func GetCard_Api(){


        self.addPeculiPopup(view: self.view)

        DataService.sharedInstance.getCard_Api{ (resultDict, errorMsg) in

            self.hidePeculiPopup(view: self.view)

            print(resultDict as Any)

            if errorMsg == nil {

                if let result = resultDict?["result"] as? [NSDictionary] {

                    self.getCardModel.removeAll()

                    for allData in result {
                        let _id = allData["_id"] as? String ?? ""

                        let isEnable = allData["isEnable"] as? Bool ?? true

                        let cardName =  allData["cardName"] as? String ?? ""
                        let primaryCard = allData["primaryCard"] as? Bool ?? true
                        let merchentToken = allData["merchentToken"] as? String ?? ""
                        let userId = allData["userId"] as? String ?? ""
                        let createdDate = allData["createdDate"] as? String ?? ""
                        let updatedDate = allData["updatedDate"] as? String ?? ""
                        let __v = allData["__v"] as? Int ?? 0

                        self.getCardModel.append(GetCArd_ModelStructure.init(isEnable: isEnable, _id: _id, cardName: cardName, primaryCard: primaryCard, merchentToken: merchentToken, userId: userId, createdDate: createdDate, updatedDate: updatedDate, __v: __v))
                        
                    }

                    print("getCardModel========>>>>>>>>>>>>>> \(self.getCardModel)")
                        self.myTableView.reloadData()
                    // self.transactionHistory_Api()

                       } else {
                           self.transactionHistory_Api(type: "onetime")

                           let message = resultDict?["message"] as? String
                             Toast.show(message: message ?? "Something went wrong", controller: self)
               }

            }

            else {
               self.transactionHistory_Api(type: "onetime")
                Toast.show(message: "Something went wrong", controller: self)
                 //let message = result["message"] as? String
                 // Toast.show(message: message ?? "Something went wrong", controller: self)

            }
        }
//            self.hidePeculiPopup(view: self.view)
//            Toast.show(message: "Something went wrong", controller: self)
    }



    func UpdateCardStatus_Api(cardSelectId:String){

        self.addPeculiPopup(view: self.view)

        let params:[String:Any] = [ "cardId" : cardSelectId
        ]

        print(params)

        DataService.sharedInstance.postApiCall(endpoint: constantApis.changeCardStatus_Api, params: params) { (response, error) in

            self.hidePeculiPopup(view: self.view)

            if response != nil {
                
                print(response ?? [:])
                let codeInt = response?["code"] as? Int ?? 0
                let codeString = response?["code"] as? String ?? ""
                let message = response?["message"] as? String ?? ""
                if codeInt == 200 || codeString == "200" {

                    let message = response?["message"] as? String ?? "0"
                    Toast.show(message: message, controller: self)
                    self.GetCard_Api()

                } else {

                    self.customAlertDismiss(title: "Alert", message: message)
                    self.dismiss(animated: true, completion: nil)
                }
                
            }
            else {

                self.customAlertDismiss(title: "Alert", message: "Something went wrong!")
                self.dismiss(animated: true, completion: nil)
            }
        }

    }


    func transactionHistory_Api(type:String) {

    self.addPeculiPopup(view: self.view)

    let params:[String:Any] = ["type" : type ]

    print(params)

    DataService.sharedInstance.postApiCall(endpoint: constantApis.transactionHistory_Api, params: params) { (response, error) in

        self.hidePeculiPopup(view: self.view)

        if response != nil {

            print(response ?? [:])
            let codeInt = response?["code"] as? Int ?? 0
            let codeString = response?["code"] as? String ?? ""
            let message = response?["message"] as? String ?? ""
            if codeInt == 200 || codeString == "200" {

                if let result = response?["result"] as? [NSDictionary] {

                    self.transactionModel.removeAll()
                    self.potTransactionHistryModel.removeAll()
                    self.DirectCreditHistoryModel.removeAll()
                    self.bankTransactionHistryModel.removeAll()
                    
                    for allData in result {
                        
      
                        

                        let createdAt = allData["createdAt"] as? String ?? ""
                        let time = allData["time"] as? String ?? ""
                        let amount =  allData["amount"] as? Double ?? 0.00
                        
                        let amountPot =  allData["amount"] as? String ?? ""
                        
                        let transactionType = allData["transactionType"] as? String ?? ""
                        let categoryImage = allData["categoryImage"] as? String ?? ""
                        
                        let toCategoryName = allData["toCategoryName"] as? String ?? ""
                        let toCategoryImage = allData["toCategoryImage"] as? String ?? ""
                        let fromCategoryName = allData["fromCategoryName"] as? String ?? ""
                        let fromCategoryImage = allData["fromCategoryImage"] as? String ?? ""
                        
                        let paymentAmount = allData["paymentAmount"] as? String ?? ""
                        let firstName = allData["firstName"] as? String ?? ""
                        let lastName = allData["lastName"] as? String ?? ""
                        let accountIdentifier = allData["accountIdentifier"] as? String ?? ""
                        
                        if transactionType == "recurrence" || transactionType == "onetime"  {

                            self.transactionModel.append(TransactionPopUp_Model.init(categoryImage: categoryImage, createdAt: createdAt, time: time, amount: amount, transactionType: transactionType))
                            
                        } else if self.lblTransactionType.text == "Pot to Pot" {
               
                            self.potTransactionHistryModel.append(potTransactionHistory_Model.init(createdAt: createdAt, amount: amountPot, toCategoryName: toCategoryName, toCategoryImage: toCategoryImage, fromCategoryName: fromCategoryName, fromCategoryImage: fromCategoryImage))
                            
                        } else if self.lblTransactionType.text == "Direct Credit" {
                            
                            self.DirectCreditHistoryModel.append(DirectCreditHistory_Model.init(categoryImage: categoryImage, createdAt: createdAt, paymentAmount: paymentAmount))
                            
                            
                        } else {
                            
                            self.bankTransactionHistryModel.append(bankTransactionHistory_Model.init(createdAt: createdAt, toCategoryName: toCategoryName, toCategoryImage: toCategoryImage, firstName: firstName, lastName: lastName, accountIdentifier: accountIdentifier, paymentAmount: paymentAmount))
                            
                        }
                        
                    }

                    self.allTransactionTableView.reloadData()
                }

//               let message = response?["message"] as? String ?? "0"
//                Toast.show(message: message, controller: self)

            } else {
                
                self.customAlertDismiss(title: "Alert", message: message)
                self.dismiss(animated: true, completion: nil)
            }

        }
        else {

            self.customAlertDismiss(title: "Alert", message: "Something went wrong!")
            
            self.dismiss(animated: true, completion: nil)
        }
    }
}

}



