//
//  FingerprintVc.swift
//  Peculi
//
//  Created by Stealth on 29/10/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit

class FingerprintVc: UIViewController {
    
    //MARK:- outlets
    @IBOutlet weak var useFingerprintVw: UIView!
    @IBOutlet weak var usePinVw: UIView!
    
    //MARK:- variables
    var commingFromScreen = ""
    var fromVC: AllControllers?
    
    //MARK:- view did load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateUi()
        
    }
    
    /**** setup UI  */
    func updateUi(){
        
        self.navigationController?.navigationBar.isHidden = true
        usePinVw.layer.borderWidth = 1
        usePinVw.layer.borderColor = UIColor.white.cgColor
        useFingerprintVw.layer.borderWidth = 1
        useFingerprintVw.layer.borderColor = UIColor.white.cgColor
        
        
    }
    
    
    //MARK:- button actions
    
    /*** use fingerprint action */
    @IBAction func useFingerprintBtnAction(_ sender: Any) {
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "FingerPrintAuthenticationViewController") as? FingerPrintAuthenticationViewController else{return}
        vc.fromVC = fromVC
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    /*** use pin action */
    
    @IBAction func btnUsePinAction(_ sender: Any) {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        
        if #available(iOS 13.0, *) {
            
            let vc = storyBoard.instantiateViewController(identifier: "EnterPinViewController") as EnterPinViewController
            
            vc.commingFromScreen = commingFromScreen
            vc.fromVC = fromVC
            
            self.navigationController?.pushViewController(vc, animated: true)
            
            
        } else {
            
            let vc = storyBoard.instantiateViewController(withIdentifier: "EnterPinViewController") as! EnterPinViewController
            vc.commingFromScreen = commingFromScreen
            vc.fromVC = fromVC
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
    /*** back action */
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
