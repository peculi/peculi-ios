//
//  RedeemNowVC.swift
//  Peculi
//
//  Created by Stealth on 1/21/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import UIKit
import DropDown
import SVProgressHUD
class RedeemNowVC: UIViewController,UITextFieldDelegate {
    
    //MARK: - outlets
    
    @IBOutlet weak var dropDownBtnOutlet: UIButton!
    @IBOutlet weak var checkBoxBtnOutlet: UIButton!
    @IBOutlet weak var dropDownView: UIView!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var addMinusStackView: UIStackView!
    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var rangeTxtField: UITextField!
    @IBOutlet weak var selectCoupanView: UIView!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var totalDiscountLbl: UILabel!
    @IBOutlet weak var minusImageView: UIImageView!
    @IBOutlet weak var plusImageView: UIImageView!
    @IBOutlet weak var termsConditionsLbl: UILabel!
    @IBOutlet weak var selectCouponLbl: UILabel!
    @IBOutlet weak var offerDescriptionLbl: UILabel!
    @IBOutlet weak var dropDownImageView: UIImageView!
    @IBOutlet weak var offerDescriptionTop: NSLayoutConstraint!
    @IBOutlet weak var lblMinus: UILabel!
    @IBOutlet weak var lblPlus: UILabel!
    
    //MARK: - variables
    var logoImageURL = String()
    var allAvailbleDenomination = [String]()
    var dropGender = DropDown()
    var price = ""
    var discreption = ""
    var discount = ""
    var navName = ""
    var catId = ""
    var amount = ""
    var currencyCode = ""
    var productCode = ""
    var type = ""
    var tierVal = ""
    var counter = 1.0
    var potBalance = ""
    var totalDiscountValue = 0.0
    var shortDescriptionData = ""
    var termsAndConditions = ""
    var minimumVal = ""
    var maximumVal = ""
    var rangeValue = "0.0"
    var reversedDenomination = [String]()
  
    var potImageURL = ""
    var catIdForUpdateTransctonStatus = ""
    var voucher_Id = ""
    
    //MARK: -  view did load
    
    override func viewDidLoad() {
        super.viewDidLoad()

        offerDescriptionLbl.text = shortDescriptionData
        
        
        
        if UserDefaults.standard.value(forKey: UserDefaultConstants.showPopup_redeemScreen.rawValue) as? String ?? "" == "0" {
            
            UserDefaults.standard.set("1", forKey: UserDefaultConstants.showPopup_redeemScreen.rawValue)
            self.redeem_ScreenPopUp()
        }
        
        
        if type == "fixed" {
            
            self.lblMinus.isHidden = true
            self.lblPlus.isHidden = true
            plusBtn.isHidden = true
            minusBtn.isHidden = true
            plusImageView.isHidden = true
            minusImageView.isHidden = true
            dropDownImageView.isHidden = false
            rangeTxtField.isUserInteractionEnabled = false
            dropDownBtnOutlet.isHidden = false
            self.view.bringSubviewToFront(dropDownView)
            self.offerDescriptionLbl.sizeToFit()
            self.offerDescriptionLbl.removeFromSuperview()
            selectCouponLbl.topAnchor.constraint(equalTo: descriptionLbl.bottomAnchor, constant: 15).isActive = true
            
            self.view.layoutIfNeeded()
            
        } else {
            
            plusBtn.isHidden = false
            minusBtn.isHidden = false
            self.lblMinus.isHidden = false
            self.lblPlus.isHidden = false
            plusImageView.isHidden = false
            minusImageView.isHidden = false
            dropDownBtnOutlet.isHidden = true
            dropDownImageView.isHidden = true
            rangeTxtField.isUserInteractionEnabled = true
            self.view.sendSubviewToBack(dropDownView)
            self.offerDescriptionLbl.isHidden = false
            self.offerDescriptionTop.constant = 15
            self.offerDescriptionLbl.sizeToFit()
            self.offerDescriptionLbl.frame.size.height = self.offerDescriptionLbl.bounds.height
            self.view.layoutIfNeeded()
            
        }

        let discountDouble = (discount as NSString).doubleValue
        
        discountLbl.text = "With \(discountDouble.format())% Reward, You have to Pay"
        self.descriptionLbl.text = self.discreption.convertHtmlToNSAttributedString?.string.maxLength(length: 150)
        let readmoreFont = UIFont(name: "Montserrat-Regular", size: 12.0)
        let readmoreFontColor = #colorLiteral(red: 0.05882352941, green: 0.5137254902, blue: 0.8509803922, alpha: 1)
        
        DispatchQueue.main.async {
            self.descriptionLbl.addTrailing(with: "... ", moreText: "Read All", moreTextFont: readmoreFont!, moreTextColor: readmoreFontColor)
        }
        
        let potBal = (potBalance as NSString).doubleValue
        let maxVal = (maximumVal as NSString).doubleValue
        var totalDiscount = Double()
        if potBal <= maxVal{
            totalDiscount = getDiscount(totalAmount: price, discount: discount)
        }
        else {
            totalDiscount = getDiscount(totalAmount: maximumVal, discount: discount)
        }
        
        amount = String(totalDiscount)
        
        totalDiscountLbl.text = "\(GlobalVariables.currency)" + String(format: "%.2f", totalDiscount)
        //        amount_to_pay = amount_in_edit_text_fild / ( (100+dicount_persontage)/100 );
        if potBal <= maxVal{
            rangeTxtField.text = price
            rangeValue = price
        }
        else {
            rangeTxtField.text = maxVal.format()
            rangeValue = maxVal.format()
        }
        
        self.counter = (price as NSString).doubleValue
        
//        if logoImageURL == ""{
//            
//            logoImage.image = UIImage(named: "imgpsh_fullsize_anim")
//            logoImage.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1)
//        }
//        else{
            let ImageUrl = URL(string:logoImageURL)
            
            
            logoImage.sd_setImage(with: ImageUrl, placeholderImage:UIImage(named: "placeholder_Img"), completed: { (image, error, cacheType, url) -> Void in
                if ((error) != nil) {
                    // set the placeholder image here
                    //cell.imgVw.image = #imageLiteral(resourceName: "doctorAvtar_Img")
                } else {
                    // success ... use the image
                }
            })
//        }
        
       
        
        
        var totalamt = Double()
        if potBal > maxVal{
            totalamt = maxVal
        }
        else{
            totalamt = potBal
        }
        let descriptionText = String(format: "%@ are offering %@% % reward, for %@%@ of your savings they will offer a %@%@ voucher", navName,discountDouble.format()+"%",GlobalVariables.currency,totalDiscount.format(),GlobalVariables.currency,totalamt.format())
        
        offerDescriptionLbl.text = descriptionText
        
        
        rangeTxtField.delegate = self
        rangeTxtField.attributedPlaceholder = NSAttributedString(string: "£130",
                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        selectCoupanView.layer.borderWidth = 1.0
        selectCoupanView.layer.borderColor =  #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        selectCoupanView.layer.cornerRadius = 8.0
        
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.black
        DropDown.appearance().textColor = UIColor.black
        DropDown.appearance().selectedTextColor = UIColor.white
        DropDown.appearance().cornerRadius = 4
        DropDown.appearance().frame.size.width = selectCoupanView.frame.width
        
        ///change color
        let termsTextAttributed = NSMutableAttributedString(string: "Read Terms & Conditions")
        termsTextAttributed.setColorForText("Terms & Conditions", with: #colorLiteral(red: 0.9568627451, green: 0.6588235294, blue: 0.231372549, alpha: 1))
        termsConditionsLbl.attributedText = termsTextAttributed
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        navigartionSetup()
       // setNeedsStatusBarAppearanceUpdate()
        
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    
    func redeem_ScreenPopUp() {
            
            let sb = UIStoryboard(name: "Main", bundle:nil)
            let addCardAlert = sb.instantiateViewController(withIdentifier: "redeemScreen_PopUp") as! redeemScreen_PopUp
            //     customAlert.textFieldData = navName.capitalizingFirstLetter()
            
            //addCardAlert.categoryId = CategoryInfo[0].userData.categoryDetails._id
        
       
            addCardAlert.okCompletionHandler = {
                print("call Add Card PopUp")
                
                self.dismiss(animated: true, completion: nil)
       
            }
            
            addCardAlert.providesPresentationContextTransitionStyle = true
            addCardAlert.definesPresentationContext = true
            addCardAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            addCardAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            
            self.present(addCardAlert, animated: true, completion: nil)
        }
    
    
    //MARK: - Navigation Setup
    
    func navigartionSetup(){
//        navigationController?.navigationBar.isHidden = false
//        title = navName
//        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2122195363, green: 0.16875422, blue: 0.3097219765, alpha: 1)
//        self.navigationController?.navigationBar.isTranslucent = true
        
        navigationController?.navigationBar.isHidden = false
        title = navName
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2203302979, green: 0.1600231826, blue: 0.3218392134, alpha: 1)
        self.navigationController?.navigationBar.isTranslucent = true
        
        
        
        
        //MARK: - Navigation LeftBar Button:-
        
        let leftBarButton = UIBarButtonItem(image: UIImage(named: "icBackArrowWhite"), style: .done, target: self, action: #selector(icBackButtonAction))
        self.navigationItem.leftBarButtonItem  = leftBarButton
        
    }
    
    
    //MARK: - button actions-
    
    @objc func icBackButtonAction(){
        popVc()
    }
    
    @IBAction func dropDownBtn(_ sender: Any) {
        setupGenderDropDown()
        dropGender.show()
    }
    
    func setupGenderDropDown() {
        dropGender.anchorView = selectCoupanView
        dropGender.bottomOffset = CGPoint(x: 0, y: selectCoupanView.bounds.height)
        dropGender.width = selectCoupanView.frame.width
        
        dropGender.dataSource = allAvailbleDenomination
        dropGender.selectionAction = { [weak self] (index, item) in
            
            let totalDiscount = self?.getDiscount(totalAmount: item, discount: self?.discount ?? "0")
            if let discountedAmount = totalDiscount{
                self?.totalDiscountValue = discountedAmount
                self?.amount = String(discountedAmount )
                self?.totalDiscountLbl.text = "\(GlobalVariables.currency)\(discountedAmount.format())"
            }
            self?.rangeValue = item
            self?.rangeTxtField.text = item
            print(item)
        }
    }
    
    @IBAction func redeemNowBtn(_ sender: Any) {
        
        
        let minVal = (minimumVal as NSString).doubleValue
        let maxVal = (maximumVal as NSString).doubleValue
        
        let rangeValueUpdate = (self.rangeTxtField.text! as NSString).doubleValue
        
        
        
        if checkBoxBtnOutlet.isSelected == false {
            
            AlertView.simpleViewAlert(title: "", "Please accept terms and conditions!", self)
            
        } else {
            
            let potPriceInt = (potBalance as NSString).doubleValue

            if potPriceInt == 0.00 {
                
                guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "WalletEmptyAlertViewController") as? WalletEmptyAlertViewController else{return}
                vc.setTransition()
                vc.okCompletionHandler = { [weak self] in
                    guard let vc = self?.storyboard?.instantiateViewController(withIdentifier: "AddMoneyViewController") as? AddMoneyViewController else {return}
                    vc.categoryId = self?.catId ?? ""
                    vc.potImage = self?.potImageURL ?? ""//self?.logoImageURL ?? ""
                    vc.potName = self?.navName ?? ""
                    vc.catIdForUpdateTransctonStatus = self?.catIdForUpdateTransctonStatus ?? ""
                    GlobalVariables.moveAfterTransactions = "0"
                    self?.navigationController?.pushViewController(vc, animated: true)
                    
                }
                
                self.present(vc, animated: true, completion: nil)
                
            } else if rangeValueUpdate < minVal || rangeValueUpdate > maxVal {
                
                let messageData = "Please enter Coupen amount \(GlobalVariables.currency)\(minVal.format()), - \(GlobalVariables.currency)\(maxVal.format())."
                self.customAlertDismiss(title: "", message: messageData)
       
            } else if potPriceInt >= (self.amount as NSString).doubleValue{
                confirmPopup()
            }
            else {
                
                guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "WalletEmptyAlertViewController") as? WalletEmptyAlertViewController else{return}
                vc.setTransition()
                vc.okCompletionHandler = { [weak self] in
                    guard let vc = self?.storyboard?.instantiateViewController(withIdentifier: "AddMoneyViewController") as? AddMoneyViewController else {return}
                    vc.categoryId = self?.catId ?? ""
                    vc.potImage = self?.potImageURL ?? ""
                    vc.potName = self?.navName ?? ""
                    vc.catIdForUpdateTransctonStatus = self?.catIdForUpdateTransctonStatus ?? ""
                    GlobalVariables.moveAfterTransactions = "0"
                    
                    self?.navigationController?.pushViewController(vc, animated: true)
                }
                self.present(vc, animated: true, completion: nil)
            }
        }
        
        
    }
    @IBAction func checkBoxBtn(_ sender: UIButton) {
        if (checkBoxBtnOutlet.isSelected == true)
        {
            checkBoxBtnOutlet.setBackgroundImage(UIImage(named: "Path 1"), for: UIControl.State.normal)
            
            checkBoxBtnOutlet.isSelected = false;
        }
        else
        {
            checkBoxBtnOutlet.setBackgroundImage(UIImage(named: "empty-checkbox"), for: UIControl.State.normal)
            
            checkBoxBtnOutlet.isSelected = true;
        }
    }
    @IBAction func readAllBtnAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PotDescriptionVC") as! PotDescriptionVC
        vc.descriptionData = self.discreption
        vc.navName = navName
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func plusBtnAction(_ sender: Any) {
        
        
        let rangeValueUpdate = (self.rangeTxtField.text! as NSString).doubleValue + 1
        
        
        let maxVal = (maximumVal as NSString).doubleValue
        
        if rangeValueUpdate <= maxVal {
            rangeValue = rangeValueUpdate.format()
            self.rangeTxtField.text = rangeValueUpdate.format()
            let totalDiscount = getDiscount(totalAmount: self.rangeTxtField.text ?? "0.00", discount: self.discount).format()
            
            amount = totalDiscount
            
            self.totalDiscountLbl.text = String(format: "%@%@", GlobalVariables.currency, totalDiscount)
        }
        else {
            
            let messageData = "Voucher maximum limit is \(GlobalVariables.currency)\(maxVal.format()), please select amount equal or less than \(GlobalVariables.currency)\(maxVal.format())."
            self.customAlertDismiss(title: "", message: messageData)
            
        }
        
      }
    
    @IBAction func minusBtn(_ sender: Any) {
        
        let minVal = (minimumVal as NSString).doubleValue
        
        
        let rangeValueUpdate = (self.rangeTxtField.text! as NSString).doubleValue - 1
        
        if rangeValueUpdate >= minVal {
            
            rangeValue = rangeValueUpdate.format()
            self.rangeTxtField.text = rangeValueUpdate.format()
            
            let totalDiscount = getDiscount(totalAmount: self.rangeTxtField.text ?? "0.00", discount: self.discount).format()
            
            amount = totalDiscount
            
            self.totalDiscountLbl.text = String(format: "%@%@", GlobalVariables.currency, totalDiscount)
        }
        else {
            
            let messageData = "Voucher minimum limit is \(GlobalVariables.currency)\(minVal.format()), please select amount equal or more than \(GlobalVariables.currency)\(minVal.format())."
            self.customAlertDismiss(title: "", message: messageData)
            
   
        }
        
 
    }

    
    @IBAction func readTermsAndConditionsTapped(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TermsAndConditionsViewController") as! TermsAndConditionsViewController
        vc.data = termsAndConditions
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
 
    
    //calculate discount
    func getDiscount(totalAmount: String, discount: String) -> Double{
        
        let totalAmountDouble = (totalAmount as NSString).doubleValue
        
        let discountDouble = (discount as NSString).doubleValue
        //        amount_to_pay = amount_in_edit_text_fild / ( (100+dicount_persontage)/100 );
        
        let discountedAmount = totalAmountDouble / ((100 + discountDouble) / 100)
        
        return discountedAmount

        
    }
    
    //textfield delegates
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == rangeTxtField {
            print("rangeEntered--->",textField.text ?? "avcadvaa")
            let maxVal = (maximumVal as NSString).doubleValue
            let minVal = (minimumVal as NSString).doubleValue
            let textFieldDouble = (textField.text! as NSString).doubleValue
            
            if textFieldDouble <= maxVal && textFieldDouble >= minVal{
                
                let totalDiscount = getDiscount(totalAmount: self.rangeTxtField.text ?? "0.00", discount: self.discount).format()
                
                amount = totalDiscount
                rangeValue = rangeTxtField.text ?? "0.0"
                
                self.totalDiscountLbl.text = String(format: "%@%@", GlobalVariables.currency, totalDiscount)
            }
            else {
                self.rangeTxtField.text = rangeValue
                if textFieldDouble > maxVal{
                    
                    //                    guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "WalletEmptyAlertViewController") as? WalletEmptyAlertViewController else{return}
                    //                    vc.setTransition()
                    let message = "Voucher maximum limit is \(GlobalVariables.currency)\(maxVal.format()), please select amount equal or less than \(GlobalVariables.currency)\(maxVal.format())."
                    //                    self.present(vc, animated: true, completion: nil)
                    
                    self.customAlertDismiss(title: "", message: message)
                    
                }
                else if textFieldDouble < minVal {
                    
                    //                    guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "WalletEmptyAlertViewController") as? WalletEmptyAlertViewController else{return}
                    //                    vc.setTransition()
                    let messageData = "Voucher minimum limit is \(GlobalVariables.currency)\(minVal.format()), please select amount equal or more than \(GlobalVariables.currency)\(minVal.format())."
                    self.customAlertDismiss(title: "", message: messageData)
                    //                    self.present(vc, animated: true, completion: nil)
                    
                }
            }
            
        }
        
    }
    
    
}
extension RedeemNowVC {
    
    ///Redeem voucher API call
    func saveRedeemVoucher() {
        
        SVProgressHUD.show()
        
        DataService.sharedInstance.redeemVoucher(categoryId: catId, productCode: productCode, amount: rangeTxtField.text ?? "", calculate_amount: amount ,voucher_discount: tierVal,discount: discount, currencyCode: currencyCode,voucherId: voucher_Id, selectedPotId:catIdForUpdateTransctonStatus) { (resultDict, errorMsg) in
            SVProgressHUD.dismiss()
            
            print(resultDict as Any)
            
            if errorMsg == nil {
                
                let message = resultDict?["message"] as? String
                
                if let result = resultDict?["code"] as? Int {
                    
                    if result == 200 {
                        
                        self.alertView(title: "Success", "Voucher redeemed successfully", self)
                        
                    } else if result == 400 {
                        self.alertView(title: "Success", "Request is submitted, your order will be completed soon.", self)
                        
                    }  else {
                        
                        self.customAlertDismiss(title: "Error", message: message ?? "")
                    }
            
    
                }
                else {
                    
                    if let message = resultDict?["message"] as? String {
                        
                        self.customAlertDismiss(title: "Error", message: message)
                        
                    }
                }
                
            } else {
                
                self.customAlertDismiss(title: "Error", message: "Something went wrong!")
                
            }
        }
    }
    
    
    //MARK:- alertview setup
    func alertView(title:String, _ message:String, _ view:UIViewController){
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "CustomDefaultAlertViewController") as? CustomDefaultAlertViewController else{return}
        
        vc.setTransition()
        vc.titleVal = title
        vc.messageVal = message
        vc.completionHandler = { [weak self] in
            
            view.dismiss(animated: true) {
                
                self?.setRootHomeVC()
                
            }
            
        }

        view.present(vc, animated: true, completion: nil)
        
    }
    
    func confirmPopup(){
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutAlertViewController") as? LogoutAlertViewController else{return}
        vc.setTransition()
        vc.titleLblData = "Message"
        vc.messageLblData = "Please note the purchase price will be deducted from your chosen Pot immediately to buy this voucher."
        vc.okBtnTitle = "Confirm"
        vc.okCompletionHandler = { [weak self] in
            self?.dismiss(animated: false) {
                
                self?.saveRedeemVoucher()
                
            }
            
        }
        
        vc.cancelCompletionHandler = { [weak self] in
            self?.dismiss(animated: true, completion: nil)
        }
        
        self.present(vc, animated: true, completion: nil)
    }
}



