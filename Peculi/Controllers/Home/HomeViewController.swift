//
//  HomeViewController.swift
//  Peculi
//
//  Created by apple on 30/10/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit
import SideMenu
import NBBottomSheet
import SVProgressHUD

class HomeViewController: UIViewController {
    
    //MARK: - Outlets
    
    @IBOutlet weak var globalBalanceView: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var ProfileBackView: UIView!
    @IBOutlet weak var topWelcomView: UIView!
    @IBOutlet weak var welcomeBackLbl: UILabel!
    @IBOutlet weak var peculiAddLbl: UILabel!
    @IBOutlet weak var globelPriceLbl: UILabel!
    @IBOutlet weak var tableVw: UITableView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var rewardOfferImageView: UIImageView!
    @IBOutlet weak var rewardOfferLbl: UILabel!
    @IBOutlet weak var rewardView: UIView!
    @IBOutlet weak var addPotView: UIView!
    @IBOutlet weak var addNewPotBtn: UIButton!
    @IBOutlet weak var addMorePotView: UIView!
    
    
    //MARK: - Define Vairable
    
    var dataUser:[UserCategoriesInfo] = []
    var superView = UIView()
    var checkAPI = false
    var todaysRewardData = [AllTiersModel]()
    var todayallDenomination = [String]()
    var todaydiscountVal = 0
    var todaycatId = ""
    var categories = [String]()
    var slugCategories: [UserCategoriesInfo] = []
    var offerFetchedData = SpecialOfferModel()
    var totalAmount = Double()
    var premiumStatus = "false"
    var acstatus = 0
    var balanceText = "0"
    
    // MARK: - Define Vairable for Get color
    
   
   // var topLeft = [CGPoint]()
    
    //MARK: did load method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSideMenu()
        
        self.rewardView.isHidden = true
        self.addPotView.isHidden = true
        self.addMorePotView.isHidden = false
        
        self.globelPriceLbl.text = "\(GlobalVariables.currency)0.00"
        
        tableVw.register(UINib(nibName: "EmptyDataTableViewCell", bundle: nil), forCellReuseIdentifier: "EmptyDataTableViewCell")

        
        if UserDefaults.standard.value(forKey: UserDefaultConstants.showPopupHomeScreen.rawValue) as? String ?? "" == "0" {
            
            UserDefaults.standard.set("1", forKey: UserDefaultConstants.showPopupHomeScreen.rawValue)
            self.homescreen_popUpAlert()
        }
        
       
  
    }
    
    
    //MARK: - view will appear
    
    override func viewWillAppear(_ animated: Bool) {
        
        navigationSetup()
        
        lblTextColorChange()
        
    
        
        UISetup()
        
        
       
        
        //fetch global balance from API
        
//        DataService.sharedInstance.getGlobalPrice { (success, error) in
//            let globalBalace = UserDefaults.standard.value(forKey: UserDefaultConstants.GlobalBalance.rawValue) as? String
//            if globalBalace != nil {
//                self.globelPriceLbl.text = String(format: "%@%@",GlobalVariables.currency,globalBalace!)
//            }
//            else {
//                self.globelPriceLbl.text = "\(GlobalVariables.currency)0.0"
//            }
//        }

        if checkIfGuest() == true {
            
            lblName.text = "Guest User"
            
        }
        else {
            
            var firstName = ""
            var lastName = ""
            if UserDefaults.exists(key: UserDefaultConstants.FirstName.rawValue){
                firstName = UserDefaults.standard.value(forKey: UserDefaultConstants.FirstName.rawValue) as! String
            }
            if UserDefaults.exists(key: UserDefaultConstants.LastName.rawValue){
                
                lastName = UserDefaults.standard.value(forKey: UserDefaultConstants.LastName.rawValue) as! String
                
            }
            
            lblName.text = "\(firstName.capitalizingFirstLetter())\("!") " //\(lastName.capitalizingFirstLetter())"
            
        }
        
        
        if UserDefaults.standard.value(forKey: UserDefaultConstants.bottomSheetValue.rawValue) != nil {
            if let value = UserDefaults.standard.value(forKey: UserDefaultConstants.bottomSheetValue.rawValue){
                if value as! Int == 1 {
                    getSpecialOfferByUserIDApiCall()
                }
            }
        }
        
//        if UserDefaults.exists(key: UserDefaultConstants.UserID.rawValue){
//
//
//            callGetuser_Api(UserDefaults.standard.value(forKey: UserDefaultConstants.UserID.rawValue) as! String)
//
//        }
//        else {
//            checkAPI = true
//        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0) {
            self.globalBalanceView.addDashedBorder(conerRadius: 8)
            
        }
        
    }

    /**** Set text color */
    
    func lblTextColorChange() {
        
        var firstName = ""
        var lastName = ""
        
        if UserDefaults.exists(key: UserDefaultConstants.FirstName.rawValue){
            firstName = UserDefaults.standard.value(forKey: UserDefaultConstants.FirstName.rawValue) as! String
        }
        
        if UserDefaults.exists(key: UserDefaultConstants.LastName.rawValue) {
            
            lastName = UserDefaults.standard.value(forKey: UserDefaultConstants.LastName.rawValue) as! String
        }
        
        
        lblName.text = "\(firstName.capitalizingFirstLetter())\("!") " //\(lastName.capitalizingFirstLetter())"
        
        let string = NSMutableAttributedString(string: "Welcome Back,")
        
        string.setColorForText("Back", with: #colorLiteral(red: 0.05882352941, green: 0.5137254902, blue: 0.8509803922, alpha: 1))
        
        // string.setColorForText("Privacy policy", with: #colorLiteral(red: 0.9843137255, green: 0.4941176471, blue: 0.1294117647, alpha: 1))
        
        welcomeBackLbl.attributedText = string

    }
    
    //Setup SideMenu
    private func setupSideMenu() {
        
        // Define the menus
        
        SideMenuManager.default.leftMenuNavigationController = storyboard?.instantiateViewController(withIdentifier:
                                                                                                        "menuLeftNavigationController") as? SideMenuNavigationController
    }
    
    func UISetup(){
        
        ProfileBackView.layer.masksToBounds = true
        ProfileBackView.layer.borderColor = #colorLiteral(red: 0.7960784314, green: 0.8588235294, blue: 0.9725490196, alpha: 1)
        ProfileBackView.layer.borderWidth = 2.0
        ProfileBackView.layer.cornerRadius = 8
        topWelcomView.layer.masksToBounds = false
        topWelcomView.layer.shadowRadius = 3.0
        topWelcomView.layer.shadowOpacity = 0.6
        topWelcomView.layer.shadowColor = UIColor.clear.cgColor
        //#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        topWelcomView.layer.shadowOffset = CGSize(width: 0 , height: 3)
        
       // getUserProfile_APICall()
        
        self.newUpdateToken_Api()
        
    }
    
    //MARK: - Navigation Setup
    
    func navigationSetup(){
        
        navigationController?.navigationBar.isHidden = true
        navigationController?.navigationBar.barStyle = .black
        
    }
    
    
    ///get user pots API call
    func callGetuser_Api(_ user_id : String){
        
        GetUserCategoriesModel().fetchInfo(user_id){ (userinfo) in
            
            print(userinfo)
            self.dataUser.removeAll()
            
            if userinfo.count > 0 {
                
                if userinfo[0].statuscheck == "success" {
                    
                    for i in 0..<userinfo.count {
                        self.dataUser.append(userinfo[i])
                    }
                    
                    print(self.dataUser)
                    print(self.slugCategories)
                    self.todayOfferApiCall()
            
                } else {
                    
                    
                }
                self.checkAPI = true
                
                self.tableVw.reloadData()
                
            }
            else {
                
                self.checkAPI = true
                self.tableVw.reloadData()
                
            }

            if self.dataUser.count > 0 {

                self.addPotView.isHidden = true
                self.addNewPotBtn.isUserInteractionEnabled = false
                self.addMorePotView.isHidden = false

            } else {
                
                self.addPotView.isHidden = false
                self.addNewPotBtn.isUserInteractionEnabled = true
                self.addMorePotView.isHidden = true
            }
            
        }
    }
    
    
    //PopUp Function
    
    func homescreen_popUpAlert() {
            
            
            let sb = UIStoryboard(name: "Main", bundle:nil)
            let addCardAlert = sb.instantiateViewController(withIdentifier: "Home_ScreenPopUp_VC") as! Home_ScreenPopUp_VC
            //     customAlert.textFieldData = navName.capitalizingFirstLetter()
            
            //addCardAlert.categoryId = CategoryInfo[0].userData.categoryDetails._id
        
        addCardAlert.balanceText = balanceText
            addCardAlert.okCompletionHandler = {
                print("call Add Card PopUp")
                
                self.dismiss(animated: true, completion: nil)
       
            }
            
            addCardAlert.providesPresentationContextTransitionStyle = true
            addCardAlert.definesPresentationContext = true
            addCardAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            addCardAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            
            self.present(addCardAlert, animated: true, completion: nil)
        }
    
    
    
    
    
    
    
    //MARK: - button actions
    
    /*** side menu button */
    
    @IBAction func btnAddNewPots_Action(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddPotViewController") as! AddPotViewController

        self.navigationController?.pushViewController(vc, animated: true)
     
    }
 
    @IBAction func icMenuButtonAction(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "menuLeftNavigationController") as! SideMenuNavigationController
        
        nextViewController.leftSide = true
        nextViewController.menuWidth = UIScreen.main.bounds.size.width - 100
        
        self.present(nextViewController, animated:true, completion:nil)
        
    }
    
    /*** add new pot button action*/
    
    @IBAction func addNewPotBtnAction(_ sender: Any) {
        
        AddPotViewController.setRootVC()
 
    }
    
    /*** profile button action */
    @IBAction func profileButtonAction(_ sender: Any) {
        
        if checkIfGuest() == true {
            self.popupToRegister()
        }
        else {
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewProfile_VC") as! NewProfile_VC
            self.navigationController?.pushViewController(vc, animated: true)
            
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewEditProfile_VC") as! NewEditProfile_VC
//            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    @IBAction func globalBalanceButtonAction(_ sender: Any) {
        
    }
    
    
    @IBAction func peculiArRewardButtonAcrtion(_ sender: Any) {
        
        if checkIfGuest() == true {
            
            self.popupToRegister()
            
        }
        else {
            
            peculiRewardAction()
            
        }

    }
    
    func peculiRewardAction(){
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "PreviewTodaysOfferViewController") as? PreviewTodaysOfferViewController else {
            
            return
        }

        vc.title = todaysRewardData[0].name
        vc.potlist = slugCategories
        vc.todaysRewardData = todaysRewardData
        vc.tierDiscount = Double(self.todaydiscountVal)
        self.navigationController?.pushViewController(vc, animated: true)
        
    }

}

//MARK: - UITableView Delegate & DataSource Method

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if checkAPI == true {
            
            if dataUser.count == 0 {
                
                return 1
            }
            else {
                
                return dataUser.count
                
            }
            
        }
        else {
            
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if checkAPI == true {
            
            if dataUser.count == 0 {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyDataTableViewCell") as! EmptyDataTableViewCell
                
                cell.defaultImageView.image = UIImage(named: "Group 41")
                cell.defaultFirstLbl.textColor = .white
                cell.defaultFirstLbl.font = cell.defaultFirstLbl.font.withSize(20)
                
                let text = "Use My Pots & Add More Pots....."
                let dataString = NSMutableAttributedString(string: text)
                dataString.setColorForText("My Pots", with: #colorLiteral(red: 0.9568627451, green: 0.6588235294, blue: 0.231372549, alpha: 1))
                
                cell.defaultFirstLbl.attributedText = dataString
                
                if  cell.defaultSecondLbl != nil {
                    cell.defaultSecondLbl.removeFromSuperview()
                }
                
                if cell.addNewPot != nil {
                    cell.addNewPot.removeFromSuperview()
                }
                
                cell.defaultFirstLbl.bottomAnchor.constraint(equalTo: cell.contentView.bottomAnchor, constant: -15).isActive = true
                
                return cell
                
            }
            else {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell") as! HomeTableViewCell
                
                if dataUser[indexPath.row].data1.category_name == "" {
                    cell.productNameLbl.text = String(format: "%@ Pot", dataUser[indexPath.row].data1.categorydata.new_name.capitalizingFirstLetter())
                }
                else {

                    cell.productNameLbl.text = "\(dataUser[indexPath.row].data1.category_name.capitalizingFirstLetter())" //Pot
                }
//                let price = dataUser[indexPath.row].total_amount.format()
//                cell.productPrizeLbl.text = "\(GlobalVariables.currency)\(price)"
                
                let priceConvert = Double(dataUser[indexPath.row].data1.categoryBalance)
                let price = priceConvert?.format()
                cell.productPrizeLbl.text = "\(GlobalVariables.currency)\(price ?? "")"
                
                var originalString = String()
                
                originalString = dataUser[indexPath.row].data1.categorydata.image
                let ImageUrl = String(format: "%@%@", AppConfig.ImageUrl,originalString)
                
                cell.productImageView.sd_setImage(with: URL(string:ImageUrl), placeholderImage:UIImage(named: "placeholder_Img"), completed: { (image, error, cacheType, url) -> Void in
                    if ((error) != nil) {
                        // set the placeholder image here
                        //cell.imgVw.image = #imageLiteral(resourceName: "doctorAvtar_Img")
                    } else {
                        // success ... use the image

                    }
                })
                
                
//                var topLeft = CGPoint(x: ((cell.productImageView.frame.origin.x) - (20)), y:((cell.productImageView.frame.origin.y) - (90)) )
//
//
//                  //let getLocation = topLeft[indexPath.row]
//
//                  let greenColour = cell.productImageView.image?.pixelColor(atLocation: topLeft)
//
//                 var redval: CGFloat = 0
//                 var greenval: CGFloat = 0
//                 var blueval: CGFloat = 0
//                 var alphaval: CGFloat = 0
//
//                 greenColour?.getRed(&redval, green: &greenval, blue: &blueval, alpha: &alphaval)
//
//                  cell.productPrizeLbl.textColor = greenColour

                if indexPath.row == dataUser.count - 1 {
                    cell.sepratorLine.isHidden = true
                }
                else {
                    
                    cell.sepratorLine.isHidden = false
                }
                
                return cell
            }
        }
        else {
            
            return UITableViewCell()
        }
                
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if dataUser.count > 0 {

            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PeculiOfferViewController") as! PeculiOfferViewController
            
           // let price = String(format: "%.2f", self.dataUser[indexPath.row].total_amount)
          //  let price = String(format: "%.2f", self.dataUser[indexPath.row].data1.categoryBalance)
            
            let priceConvert = Double(dataUser[indexPath.row].data1.categoryBalance)
            let price = priceConvert?.format()
     
            //self.dataUser[indexPath.row].data1.categoryBalance //
            
            print(price ?? "")
            vc.setPrice = price ?? ""
            
            let tier1Val = (self.dataUser[indexPath.row].data1.categorydata.tier1 as NSString).doubleValue
            let tier2Val = (self.dataUser[indexPath.row].data1.categorydata.tier2 as NSString).doubleValue
            let tier3Val = (self.dataUser[indexPath.row].data1.categorydata.tier3 as NSString).doubleValue
            
            vc.targetValue = (self.dataUser[indexPath.row].data1.target as NSString).doubleValue.format()
            vc.potImage = self.dataUser[indexPath.row].data1.categorydata.image
            vc.slugName = self.dataUser[indexPath.row].data1.categorydata.slug
            vc.tier1 = tier1Val.format()
            vc.tier2 = tier2Val.format()
            vc.tier3 = tier3Val.format()
            vc.categoryNameId = self.dataUser[indexPath.row].data1.categorydata._id
            vc.catIdForUpdateTransctonStatus = self.dataUser[indexPath.row].data1._id
          vc.premiumStatus = premiumStatus
            
   print("catIdForUpdateTransctonStatus==>>\(vc.catIdForUpdateTransctonStatus)")
            
            vc.delegate = self
            
            if self.dataUser[indexPath.row].data1.category_name == "" {
                vc.navigationName = dataUser[indexPath.row].data1.categorydata.new_name
            }
            else {
                
                vc.navigationName = dataUser[indexPath.row].data1.category_name
            }
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
         
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    
    
    
    func showlogoutPop(){
        
        let sb = UIStoryboard(name: "Main", bundle:nil)
        let logAlert = sb.instantiateViewController(withIdentifier: "LogoutAlertViewController") as! LogoutAlertViewController
        let rootViewController = UIApplication.shared.keyWindow!.rootViewController!
   logAlert.setTransitionSpecificVC()
        logAlert.messageLblData = "Oops! Your details are currently being verified through our KYC process. This process may take up to 72 hours. An email confirmation will come through to confirm when the due diligence process has been undertaken. Please email contact@peculi.co.uk if you have any further queries."
        logAlert.titleLblData = "Message"
        
        logAlert.showhideValue = 1
        
        //                        logAlert.from = "logout"
        logAlert.okCompletionHandler = { [weak self] in
            rootViewController.dismiss(animated: true, completion: nil)
        }
        
        logAlert.cancelCompletionHandler = {
            
            rootViewController.dismiss(animated: true, completion: nil)
        }

        DispatchQueue.main.async {
            rootViewController.present(logAlert, animated: true, completion: nil)
        }
          
    }
    

}

extension HomeViewController {
    
    
    func todayOfferApiCall() {
        
        let userID = UserDefaults.standard.value(forKey: UserDefaultConstants.UserID.rawValue) ?? ""
        DataService.sharedInstance.todayOfferApiCall(userId: userID as! String) { (response, error) in
            
            if response != nil{
                
                print(response)
                
                let code = response?["code"] as? Int ?? 0
                
                let codeString = response?["code"] as? String ?? ""
                
                if code == 200 || codeString == "200" {
                    
                    self.todaysRewardData.removeAll()
                    self.categories.removeAll()
                    self.slugCategories.removeAll()
                    
                    if let result = response?["result"] as? NSArray {
                        
                        if let data = result[0] as? [String: Any]{
                            
                            if let voucherDetails = data["voucher_details"] as? [String: Any] {
                                
                                self.todaycatId = voucherDetails["_id"] as? String ?? ""
                                let max = voucherDetails["maximum_value"] as? String ?? ""
                                let min = voucherDetails["minimum_value"] as? String ?? ""
                                let name = voucherDetails["name"] as? String ?? ""
                                //let percentDiscount = voucherDetails["percent_discount"] as? String ?? ""
                                
//                                let percentageDouble = Double(percentDiscount )
//                                let percentageInt = Int(percentageDouble ?? 0)
//
//                                print(percentageInt)
                                
                                let denominationType = voucherDetails["denomination_type"] as? String
                                let logoImageURL = voucherDetails["logo_image_url"] as? String
                                let description = voucherDetails["description"] as? String
                                if let slugCategories = voucherDetails["categories"] as? [String] {
                                    
                                    self.categories = slugCategories
                                }
                                
                                let currencyCode = voucherDetails["currency_code"] as? String
                                let code = voucherDetails["code"] as? String
                                let id = voucherDetails["_id"] as? String
                                let termsAndConditions = voucherDetails["terms_and_conditions_html"] as? String

                                self.todayallDenomination.removeAll()
                                
                                if denominationType == "fixed" {
                                    if let availabledenomination = voucherDetails["available_denominations"] as? NSArray {
                                        for allData in availabledenomination {
                                            self.todayallDenomination.append(allData as? String ?? "")
                                        }
                                        self.todayallDenomination = self.todayallDenomination.reversed()
                                    }
                                }

                                if let voucherDetails1 = data["voucher_retailer_details"] as? [String: Any] {
                                
                                    let percentDiscount1 = voucherDetails1["premium"] as? Double ?? 0.0
                                    
                                    let percentDiscount = "\(percentDiscount1)"
                                    
                                    let percentageDouble = Double(percentDiscount )
                                    let percentageInt = Int(percentageDouble ?? 0)
                                    
                                    print(percentageInt)
                                
                                self.todaysRewardData.append(AllTiersModel.init(max: max , min: min , name: name, percentDiscount: percentDiscount , denominationType: denominationType ?? "", logoImage: logoImageURL ?? "", denominationData: self.todayallDenomination, description: description ?? "", currencyCode: currencyCode ?? "",code: code ?? "",id : id ?? "", termsAndConditon: termsAndConditions ?? "",voucher_Id: "", voucherDiscountValue: 0.00))
                            }
                        }
                            
                        }
                    }
                    else {
                        
                    }
                    
                    let imageURL = self.todaysRewardData[0].logoImage
                    let name = self.todaysRewardData[0].name
                    let percentAmount = (self.todaysRewardData[0].percentDiscount as NSString).doubleValue
                    let minVal = (self.todaysRewardData[0].min as NSString).doubleValue
                    let maxVal = (self.todaysRewardData[0].max as NSString).doubleValue
                    
//                    let descriptionText = "\(name) are offering an \(percentAmount.format())% reward on any amount b/w \(GlobalVariables.currency)\(minVal.format()) to \(GlobalVariables.currency)\(maxVal.format())"
                    
                    let descriptionText = "\(name) are offering an exclusive reward of \(percentAmount.format())% today"

//                    if imageURL == "" {
//
//                        self.rewardOfferImageView.image = UIImage(named: "imgpsh_fullsize_anim")
//                        self.rewardOfferImageView.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1)
//
//                    }
//                    else {
                        
                        let url = URL(string: imageURL )
                        let placeHolder = UIImage(named: "imgpsh_fullsize_anim")
                        self.rewardOfferImageView.sd_setImage(with: url, placeholderImage: placeHolder)
                        self.rewardOfferImageView.contentMode = .scaleToFill
//                    }

                    self.rewardOfferLbl.text = descriptionText
                    
                    if !self.todaysRewardData.isEmpty {
                        
                        if self.dataUser.isEmpty {

                            self.rewardView.isHidden = true
                        }
                        else if self.categories.isEmpty {
                            
                            self.rewardView.isHidden = true
                        }

                        else {
                            self.slugCategories.removeAll()
                            
                            for i in 0..<self.dataUser.count {
                                for index in self.categories {
                                    
                                    if self.dataUser[i].data1.categorydata.slug == index {
                                        
                                        self.slugCategories.append(self.dataUser[i])
                                        break
                                    }
                                }
                            }
                            if self.slugCategories.isEmpty{
                                self.rewardView.isHidden = true
                            }
                            else {
                                
                                self.rewardView.isHidden = false
                            }
                            
                        }
                    }
                    else {
                        
                        self.rewardView.isHidden = true
                    }
                }
            }
            else {
                print("error---->",error)
            }
            
        }
        
    }

    func getSpecialOfferByUserIDApiCall(){
        
        var configuration = NBBottomSheetConfiguration()
                
        let userID = UserDefaults.standard.value(forKey: UserDefaultConstants.UserID.rawValue) ?? ""
        DataService.sharedInstance.getSpecialOfferByUserIDApiCall(userId: userID as! String) { (response, error) in
            
            print(response ?? [:])
            
            UserDefaults.standard.set(0, forKey: UserDefaultConstants.bottomSheetValue.rawValue)
            UserDefaults.standard.synchronize()
            
            if response != nil{
                
                if let status = response?["status"] as? String{
                    
                    if status == "success" {
                        
                        self.totalAmount = response?["category_balance"] as? Double ?? 0.00
                        
                        if let result = response?["result"] as? NSArray {
                            
                            if let resultData = result[0] as? Dictionary<String, Any> {
                                
                                self.offerFetchedData.deserilize(values: resultData)
                                
                            }
                        }
                        
                        let imageURL = self.offerFetchedData.voucherDetails.logo_image_url
                        let name = self.offerFetchedData.voucherDetails.name
                        let percentAmount = (self.offerFetchedData.percentage as NSString).doubleValue
                        let minVal = (self.offerFetchedData.voucherDetails.minimum_value as NSString).doubleValue
                        let maxVal = (self.offerFetchedData.voucherDetails.maximum_value as NSString).doubleValue
                        
 
                        let descriptionText = "\(name) are offering an \(percentAmount.format())% reward on any amount b/w \(GlobalVariables.currency)\(minVal.format()) to \(GlobalVariables.currency)\(maxVal.format())"
                        
                        DispatchQueue.main.asyncAfter(deadline: .now()) {
                            
                            configuration = NBBottomSheetConfiguration(sheetSize: .fixed(340))
                            let vc  = BottomXIBViewController()
                            vc.from = "Home"
                            vc.offerdescription = descriptionText
                            vc.offerType = "Reward of the Day"
                            vc.offerImage = imageURL
                            vc.offerData = self.offerFetchedData
                            vc.totalAmount = self.totalAmount
                            vc.offerName = name
                            vc.clickDelegate = self
                            let bottomSheetController = NBBottomSheetController(configuration: configuration)
                            bottomSheetController.present(vc, on: self)
                        }
 
                    }
                    
                }
                
            }
  
        }
     
    }
    
    
    
    func getUserProfile_APICall() {

        self.addPeculiPopup(view: self.view)

        let params:[String: Any] = [ "premiumCheck" : "true" ]

        print(params)
        
        DataService.sharedInstance.postApiCall(endpoint: constantApis.getUserProfile_Api, params: params) { (response, error) in
            self.hidePeculiPopup(view: self.view)

            print(response)

            if response != nil {

                let status = response?["premiumStatus"] ?? false
                let acstatus = response?["acstatus"] as? Int ?? 0
                
                self.premiumStatus = "\(status)"
                self.acstatus = acstatus
                print("premiumStatus========>>> \(self.premiumStatus)")
                print("acstatus========>>> \(acstatus)")
                
                if let result = response?["result"] as? [String: Any] {

                    print(result)
                    
                    let kycStatus = result["kycStatus"] as? String ?? ""
                    
                    UserDefaults.standard.set(kycStatus, forKey: UserDefaultConstants.kycStatus.rawValue)
                }
                
                
                
                if  self.checkIfGuest() == true {
                    
                    
                
                    
                    
                    
                    DataService.sharedInstance.getGlobalPrice { (success, error) in
                        let globalBalace = UserDefaults.standard.value(forKey: UserDefaultConstants.GlobalBalance.rawValue) as? String
                        if globalBalace != nil {
                            self.globelPriceLbl.text = String(format: "%@%@",GlobalVariables.currency,globalBalace!)
                            
                            self.balanceText =  self.globelPriceLbl.text ?? "0.00"
                        }
                        else {
                            self.globelPriceLbl.text = "\(GlobalVariables.currency)0.0"
                            self.balanceText =  self.globelPriceLbl.text ?? "0.00"
                        }
                        
                    }
                    
                    
                    if UserDefaults.exists(key: UserDefaultConstants.UserID.rawValue){
                        
                        self.callGetuser_Api(UserDefaults.standard.value(forKey: UserDefaultConstants.UserID.rawValue) as! String)
                        
                    }
                    else {
                        
                        self.checkAPI = true
                    }
                    
  
                }
                else {
                    
                    if acstatus == 0 {
                        
                        self.showlogoutPop()
                        
                        
                        
                    } else {
                        
                        
   
                        
                        DataService.sharedInstance.getGlobalPrice { (success, error) in
                            let globalBalace = UserDefaults.standard.value(forKey: UserDefaultConstants.GlobalBalance.rawValue) as? String
                            if globalBalace != nil {
                                self.globelPriceLbl.text = String(format: "%@%@",GlobalVariables.currency,globalBalace!)
                            }
                            else {
                                self.globelPriceLbl.text = "\(GlobalVariables.currency)0.0"
                            }
                            
                        }
                        
                        if UserDefaults.exists(key: UserDefaultConstants.UserID.rawValue){
                            
                            self.callGetuser_Api(UserDefaults.standard.value(forKey: UserDefaultConstants.UserID.rawValue) as! String)
                            
                        }
                        else {
                            
                            self.checkAPI = true
                        }
                        
                    }
                   
                  
                }
                
                

               
            }
            else {

                self.customAlertDismiss(title: "Message", message: "\(error ?? "Something went wrong!")")
//
//                DataService.sharedInstance.getGlobalPrice { (success, error) in
//                    let globalBalace = UserDefaults.standard.value(forKey: UserDefaultConstants.GlobalBalance.rawValue) as? String
//                    if globalBalace != nil {
//                        self.globelPriceLbl.text = String(format: "%@%@",GlobalVariables.currency,globalBalace!)
//                    }
//                    else {
//                        self.globelPriceLbl.text = "\(GlobalVariables.currency)0.0"
//                    }
//                }
//
//                if UserDefaults.exists(key: UserDefaultConstants.UserID.rawValue){
//
//                    self.callGetuser_Api(UserDefaults.standard.value(forKey: UserDefaultConstants.UserID.rawValue) as! String)
//
//                }
//                else {
//
//                    self.checkAPI = true
//                }
            }
        }
        
    }
        
    
    func newUpdateToken_Api() {
        
//self.addPeculiPopup(view: self.view)
        
        let params:[String:Any] = ["userId": UserDefaults.standard.value(forKey: UserDefaultConstants.UserID.rawValue) ?? ""
                                   
        ]

        print(params)
        
        DataService.sharedInstance.postApiCall(endpoint: constantApis.newUpdateToken_Api, params: params) { (response, error) in
            
           // self.hidePeculiPopup(view: self.view)
       
            print(response)
            
            if response != nil {
                
                print(response ?? [:])
                
                let codeInt = response?["code"] as? Int ?? 0
                let codeString = response?["code"] as? String ?? ""
                let message = response?["message"] as? String ?? ""
                
                if codeInt == 200 || codeString == "200" {
                    
                    print("edit api call 1===========>>>>>>>>>>>")
                    
                    let token = response?["token"] as? String ?? ""
                    let monthlyLimit = response?["monthlyLimit"] as? String ?? ""
                    let dayLimit = response?["dayLimit"] as? String ?? ""
                    let yearlyLimit = response?["yearlyLimit"] as? String ?? ""
                    let maxLoad = response?["maxLoad"] as? String ?? ""
                    let transactionCount = response?["transactionCount"] as? String ?? ""
                    let transactionLimit = response?["transactionLimit"] as? String ?? ""
                    let transactionSum = response?["transactionSum"] as? String ?? ""
                     
                    UserDefaults.standard.set(token, forKey: UserDefaultConstants.jwToken.rawValue)
                    
                    UserDefaults.standard.set(monthlyLimit, forKey: UserDefaultConstants.monthlyLimit.rawValue)
                    UserDefaults.standard.set(dayLimit, forKey: UserDefaultConstants.dayLimit.rawValue)
                    UserDefaults.standard.set(yearlyLimit, forKey: UserDefaultConstants.yearlyLimit.rawValue)
                    UserDefaults.standard.set(maxLoad, forKey: UserDefaultConstants.maxLoad.rawValue)
                    UserDefaults.standard.set(transactionCount, forKey: UserDefaultConstants.transactionCount.rawValue)
                    UserDefaults.standard.set(transactionLimit, forKey: UserDefaultConstants.transactionLimit.rawValue)
                    UserDefaults.standard.set(transactionSum, forKey: UserDefaultConstants.transactionSum.rawValue)
                    
                    self.getUserProfile_APICall()
                    
                } else {
                   
                    self.getUserProfile_APICall()
                    
                 //   self.customAlertDismiss(title: "Alert", message: message)
                    
                }
                
            }
            else {
                
//                self.customAlertDismiss(title: "Alert", message: "Something went wrong!")
//
//                self.dismiss(animated: true, completion: nil)
            }
        }
        
    }
    
    
    
    
}

extension HomeViewController: CheckIfBack, ClickHere{

    func clickedAction(flag: Int) {
        
        if flag == 2 {
            
            let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
            
            let vc = storyBoard.instantiateViewController(withIdentifier: "RedeemNowVC") as! RedeemNowVC
            
            vc.logoImageURL = self.offerFetchedData.voucherDetails.logo_image_url
            
            let denominationArray = self.offerFetchedData.voucherDetails.available_denominations
            var array = [String]()
            
            denominationArray.reversed().forEach{array.append($0)}
            
            vc.allAvailbleDenomination = array
            
            //            vc.allAvailbleDenomination = self.offerFetchedData.voucherDetails.available_denominations
            
            vc.discreption = self.offerFetchedData.voucherDetails.description
            vc.discount = self.offerFetchedData.percentage
            vc.navName = self.offerFetchedData.voucherDetails.name
            vc.catId = self.offerFetchedData.category_id
            vc.productCode = self.offerFetchedData.voucherDetails.code
            vc.currencyCode = self.offerFetchedData.voucherDetails.currency_code
            vc.type = self.offerFetchedData.voucherDetails.denomination_type
            vc.termsAndConditions = self.offerFetchedData.voucherDetails.terms_and_conditions_html
            vc.potBalance = totalAmount.format()
            vc.tierVal = self.offerFetchedData.percentage
            if self.offerFetchedData.voucherDetails.denomination_type == "fixed" {
                vc.price = self.offerFetchedData.voucherDetails.maximum_value
            }
            else {

                
                let percentageIntValue = (self.offerFetchedData.percentage as NSString).doubleValue
                //            if let potValue = potlist[indexPath.row].total_amount {
                let value: Double = totalAmount + (totalAmount * (percentageIntValue / 100))
                //                let value: Double = totalAmount * 100.00/(100.00 - percentageIntValue)
                print(value)
                
                vc.price = String(format: "%.2f", value)
            }
            
            
            vc.minimumVal = offerFetchedData.voucherDetails.minimum_value
            vc.maximumVal = offerFetchedData.voucherDetails.maximum_value
            
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
    }
    
    
    func backAction(_ flag: Int) {
        if flag == 1 {
            if UserDefaults.exists(key: UserDefaultConstants.UserID.rawValue) {
                
                callGetuser_Api(UserDefaults.standard.value(forKey: UserDefaultConstants.UserID.rawValue) as! String)
                
            }
        }
    }

    
}


extension UIImage {
    
    func pixelColor(atLocation point: CGPoint) -> UIColor? {
        
        guard let cgImage = cgImage, let pixelData = cgImage.dataProvider?.data else { return nil }

        let data: UnsafePointer<UInt8> = CFDataGetBytePtr(pixelData)

        let bytesPerPixel = cgImage.bitsPerPixel / 8

        let pixelInfo: Int = ((cgImage.bytesPerRow * Int(point.y)) + (Int(point.x) * bytesPerPixel))

        let b = CGFloat(data[pixelInfo]) / CGFloat(255.0)
        let g = CGFloat(data[pixelInfo+1]) / CGFloat(255.0)
        let r = CGFloat(data[pixelInfo+2]) / CGFloat(255.0)
        let a = CGFloat(data[pixelInfo+3]) / CGFloat(255.0)
        
        return UIColor(red: r, green: g, blue: b, alpha: a)
        
    }
}
