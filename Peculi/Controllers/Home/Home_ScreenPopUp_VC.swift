//
//  Home_ScreenPopUp_VC.swift
//  Peculi
//
//  Created by Mohit Rana on 25/08/22.
//  Copyright © 2022 Stealth Tech. All rights reserved.
//

import UIKit

class Home_ScreenPopUp_VC: UIViewController {
    
    // MARK: - OUTLETS
   
    @IBOutlet weak var popUpChildView: UIView!
    @IBOutlet var popUpSuperView: UIView!
    @IBOutlet weak var globalBalanceView: UIView!
    @IBOutlet weak var globelPriceLbl: UILabel!
    
   
    
    //MARK: - declare Variables
        
        var okCompletionHandler: (() -> Void)?
        var balanceText = ""
    
    // MARK: - VIEW LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()

        DispatchQueue.main.asyncAfter(deadline: .now() + 0) {
            self.globalBalanceView.addDashedBorder1(conerRadius: 8)
        }
        
        DataService.sharedInstance.getGlobalPrice { (success, error) in
            let globalBalace = UserDefaults.standard.value(forKey: UserDefaultConstants.GlobalBalance.rawValue) as? String
            if globalBalace != nil {
                self.globelPriceLbl.text = String(format: "%@%@",GlobalVariables.currency,globalBalace!)
            }
            else {
                self.globelPriceLbl.text = "\(GlobalVariables.currency)0.0"
            }
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupView()
        animateView()
   
       
    
    }
    
    
    // MARK: - FUNCTION
    
    /**** UI setup */
    func setupView(){
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        popUpSuperView.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        
    }
     
     ///animation setup
     func animateView() {
         popUpChildView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
         UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
             self.popUpChildView.transform = .identity
         }, completion: {(finished: Bool) -> Void in
             
         })
         
     }
    
    
    // MARK: - BUTTON ACTION
    
    @IBAction func btnCancel_Action(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        print("call cancel ")
    }

    
    @IBAction func btnContinue_Action(_ sender: Any) {
        

        
       self.okCompletionHandler?()
        
        self.dismiss(animated: true, completion: nil)
        
        print("call function")
      
        
    }

}
