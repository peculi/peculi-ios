//
//  swipeDlt_Popup_VC.swift
//  Peculi
//
//  Created by PPI on 6/6/22.
//  Copyright © 2022 Stealth Tech. All rights reserved.
//

import UIKit


class swipeDlt_Popup_VC: UIViewController {
    
    //MARK: - outlet
    
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var messageLbl: UILabel!
    
    @IBOutlet weak var okBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet var popUpSuperView: UIView!
    
    //MARK: - variables
    
    var titleLblData = ""
    var messageLblData = ""
    var comingVc = ""
    
    var voucherId = ""
    
    var okCompletionHandler: (()->Void)?
    var cancelCompletionHandler: (()->Void)?
    
//    var okBtnTitle = "Ok"
//    var cancelBtnTitle = "Cancel"

    
    
    //MARK: - view did load
    
    override func viewDidLoad() {
        super.viewDidLoad()

        titleLbl.text = titleLblData
        messageLbl.text = messageLblData
//        okBtn.setTitle(okBtnTitle, for: .normal)
//        cancelBtn.setTitle(cancelBtnTitle, for: .normal)
        
        setupView()

    }

    /**** UI Setup */
    
    func setupView(){
        
        if checkIfGuest() == true {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
            popUpSuperView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        }
        else {
            
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        popUpSuperView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
            
        }
        
        
    }
    
    
    
    //MARK: - button actions
    @IBAction func okBtnAction(_ sender: Any) {
        if comingVc == "0"{
            
            
            self.okCompletionHandler?()
            self.dismiss(animated: true, completion: nil)
            
        } else {
            
            deleteVouchers_Api()
        }
   
       
        
    }
    
    
    @IBAction func cancelBtnAction(_ sender: Any) {

        cancelCompletionHandler?()

    }
    
    
    func deleteVouchers_Api() {

        self.addPeculiPopup(view: self.view)

        let params:[String: Any] = ["voucherId": voucherId,
                                     ]

        print(params)

        DataService.sharedInstance.postApiCall(endpoint: constantApis.deleteVouchers_Api, params: params) { (response, error) in
            self.hidePeculiPopup(view: self.view)

         print(response)

            if response != nil {
                
                self.okCompletionHandler?()
                self.dismiss(animated: true, completion: nil)
                self.customAlertDismiss(title: "Message", message: "Sucessfully")

            }
            else {

                self.customAlertDismiss(title: "Message", message: "Something went wrong!")
            }
        }

    }
    
    
}
