//
//  MyPotDetailsViewController.swift
//  Peculi
//
//  Created by iApp on 03/06/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import UIKit
import SVProgressHUD

class MyPotDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CustomTableView {
    
    //MARK: - outlets
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var backbtn: UIButton!
    @IBOutlet weak var navTitleLbl: UILabel!
    @IBOutlet weak var DetailsTableView: UITableView!
    @IBOutlet weak var editPotNameBtn: UIButton!
    @IBOutlet weak var navViewHeight: NSLayoutConstraint!
    
    //MARK: - variables
    
    var navName = ""
    var potCatId = String()
    var CategoryInfo = [GetCategoryInfoByIdModel]()
    var voucherTransactionData = [VoucherTransactionModel]()
    var progressValue = Double()
    var transactionTableHeight:CGFloat = 70
    var didreceiveTableViewHeight = false
    
    //MARK: - view did load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /**** register table view cells*/
        DetailsTableView.register(UINib(nibName: "ProgressViewTableViewCell", bundle: nil), forCellReuseIdentifier: "ProgressViewTableViewCell")
        
        DetailsTableView.register(UINib(nibName: "PeculiTransactionsTableViewCell", bundle: nil), forCellReuseIdentifier: "PeculiTransactionsTableViewCell")
        
        DetailsTableView.register(UINib(nibName: "PeculiTargetSetTableViewCell", bundle: nil), forCellReuseIdentifier: "PeculiTargetSetTableViewCell")
        
    }
    
    //MARK: - view will appear
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navTitleLbl.text =  String(format: "%@ Pot", navName.capitalizingFirstLetter()).maxLength(length: 20)
        
        self.navigationController?.navigationBar.isHidden = true
        navView.layer.masksToBounds = false
        navView.layer.shadowRadius = 3.0
        navView.layer.shadowOpacity = 0.6
        navView.layer.shadowColor = UIColor.clear.cgColor
            //#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        navView.layer.shadowOffset = CGSize(width: 0 , height: 3)
        if UIScreen.main.bounds.size.height <= 736 {
            navViewHeight?.constant = 80
            self.view.layoutIfNeeded()
        }
        else {

            navViewHeight?.constant = 100
            self.view.layoutIfNeeded()
        }

        getCategoryInfoByIDApiCall()
        
    }
    
    //MARK: - button actions
    
    @IBAction func backBtnAction(_ sender: Any) {
        
        self.popVc()
    }
    
    @IBAction func editPotNameBtnAction(_ sender: Any) {
        
        popupCustomAlert()
        
    }
    
    @IBAction func deletePotBtnAction(_ sender: Any) {
        
        
        deletePotAction()
        
    }
    
    /****Assigning values */
    func setupValues() {
        
        let setPrice = String(format: "%.2f", CategoryInfo[0].totalAmount)
        let value  = (setPrice as NSString).doubleValue
        let tier1 = (CategoryInfo[0].userData.categoryDetails.tier1 as NSString).doubleValue
        let tier2 = (CategoryInfo[0].userData.categoryDetails.tier2 as NSString).doubleValue
        let tier3 = (CategoryInfo[0].userData.categoryDetails.tier3 as NSString).doubleValue
        
        let price = Double(setPrice) ?? 0.0
        if price == 0.0 || value < tier1{
            self.progressValue = 0
        }
        
        else if value >= tier1 && value < tier2 {
            let getPer = tier2 - tier1
            let getPer1 = price - tier1
            
            self.progressValue =  ((getPer1 * 100) / getPer) / 100
        }
        else if value >= tier2 && value < tier3  {
            let getPer = tier3 - tier2
            let getPer1 = price - tier2
            self.progressValue =  ((getPer1 * 100) / getPer) / 100
        }
        else {
            
            self.progressValue =  1
        }
    }
    
    //MARK: - alert setup

    func popupCustomAlert() {
        if CategoryInfo.count > 0 {
            
            let sb = UIStoryboard(name: "Main", bundle:nil)
            let customAlert = sb.instantiateViewController(withIdentifier: "CustomAlertViewController") as! CustomAlertViewController
            customAlert.textFieldData = navName.capitalizingFirstLetter()
            
            customAlert.categoryId = CategoryInfo[0].userData.categoryDetails._id
            
            customAlert.okCompletionHandler = {
                self.popVc()
            }
            customAlert.providesPresentationContextTransitionStyle = true
            customAlert.definesPresentationContext = true
            customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            
            self.present(customAlert, animated: true, completion: nil)
        }
    }
    
    func deletePotAction(){
        if CategoryInfo.count > 0{
            let price = String(format: "%.2f", CategoryInfo[0].totalAmount)
            var potName = String()
            if CategoryInfo[0].userData.categoryName == ""{
                
                potName = CategoryInfo[0].userData.categoryDetails.newName
            }
            else{
                potName = CategoryInfo[0].userData.categoryName
            }
            if Double(price) == 0.000{
                
                guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutAlertViewController") as? LogoutAlertViewController else {
                    return
                }
                
                vc.titleLblData = "Delete Pot"
                vc.messageLblData = String(format: "Are you sure you want to delete %@ Pot?", potName.capitalizingFirstLetter())
                vc.okCompletionHandler = { [weak self] in

                    self?.dismiss(animated: true) {
                    self?.deleteCategoryApi()
                        
                    }
                }
                
                vc.cancelCompletionHandler = { [weak self] in
                    self?.dismiss(animated: true, completion: nil)
                }
                
                vc.setTransition()
                
                self.present(vc, animated: true, completion: nil)
  
            }
            else {
                self.customAlertDismiss(title: "Alert", message: "You cannot delete this pot!")
            }
        }
    }
    
    ///API call
    func deleteCategoryApi(){
        
        SVProgressHUD.show()
        let userId = UserDefaults.standard.value(forKey: UserDefaultConstants.UserID.rawValue) as! String
        
        DataService.sharedInstance.deleteCategoryApiCall(userID: userId, categoryId: CategoryInfo[0].userData.categoryDetails._id) { (response, error) in
            
            print(response)
            
            if response != nil{
                SVProgressHUD.dismiss()
                if let code = response?["code"] as? Int{
                    if code == 200{
                        
                        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "CustomDefaultAlertViewController") as? CustomDefaultAlertViewController else {
                            return
                        }
                        vc.titleVal = "Message"
                        var potName = String()
                        if self.CategoryInfo[0].userData.categoryName == "" {
                            
                            potName = self.CategoryInfo[0].userData.categoryDetails.newName
                        }
                        else {
                            potName = self.CategoryInfo[0].userData.categoryName
                        }
                        
                        vc.messageVal = String(format: "%@ Pot has been deleted!", potName.capitalizingFirstLetter())
                        
                        vc.completionHandler = {
                            self.dismiss(animated: false) {
                                self.popToRoot()
                            }
                        }
                        
                        vc.setTransition()
                        self.present(vc, animated: true, completion: nil)
                        
                    }
                    else {
                        AlertView.simpleViewAlert(title: "Error", response?["message"] as! String, self)
                    }
                }
                
            }
            else {
                SVProgressHUD.dismiss()
                AlertView.simpleViewAlert(title: "Error","Somthing went wrong", self)
            }
            
        }
        
    }
    
    
    //MARK: - tableView delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if CategoryInfo.count > 0 {
            switch indexPath.row {
            case 0:
                let targetValue = (CategoryInfo[0].userData.target as NSString).doubleValue.format()
                let setPrice = String(format: "%.2f", CategoryInfo[0].totalAmount)
                let value  = (setPrice as NSString).doubleValue
                let tier1 = (CategoryInfo[0].userData.categoryDetails.tier1 as NSString).doubleValue
                let tier2 = (CategoryInfo[0].userData.categoryDetails.tier2 as NSString).doubleValue
                let tier3 = (CategoryInfo[0].userData.categoryDetails.tier3 as NSString).doubleValue
                
                let progressCell = Bundle.main.loadNibNamed("ProgressViewTableViewCell", owner: self, options: nil)?.first as! ProgressViewTableViewCell
                let titleString = String(format: "%@ Pot %@%@ target", navName.capitalizingFirstLetter(),GlobalVariables.currency, targetValue)
                let addString = NSMutableAttributedString(string: titleString)
                addString.setColorForText("\(GlobalVariables.currency)\(targetValue)", with: #colorLiteral(red: 1, green: 0.6588235294, blue: 0.231372549, alpha: 1))
                progressCell.potNameLbl.attributedText = addString
                progressCell.targetValueLbl.text = "\(GlobalVariables.currency)\(setPrice)"
                progressCell.toValue = progressValue
                
                progressCell.btnAddMoney.isHidden = true
                progressCell.btnAddNewTarget.isHidden = true
                progressCell.iconBtnAddMony.isHidden = true
                progressCell.iconBtnAddTarget.isHidden = true
                progressCell.lblBtnAddMony.isHidden = true
                progressCell.lblBtnAddTarget.isHidden = true
                
                if value == 0.00 || value < tier1 {
                    
                    progressCell.tierNameLbl.text = "Tier 1"
                }
                
                else if value >= tier1 && value < tier2 {
                    
                    progressCell.tierNameLbl.text = "Tier 1"
 
                } else if value >= tier2 && value < tier3  {
                    progressCell.tierNameLbl.text = "Tier 2"
                    
                    
                } else {
                    progressCell.tierNameLbl.text = "Tier 3"
                    
                }
                
                progressCell.handleProgressView()
                
                return progressCell
            case 1:
                let offerCell = Bundle.main.loadNibNamed("PeculiTargetSetTableViewCell", owner: self, options: nil)?.first as! PeculiTargetSetTableViewCell
                
                let currTargetString = String(format: "Current target %@%@",GlobalVariables.currency,CategoryInfo[0].userData.target)
                let modifiedString = NSMutableAttributedString(string: currTargetString)
                
                let coloredString = "Current target"
                modifiedString.setColorForText(coloredString, with: #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1))
                
                let weeklyTargetString = String(format: "Weekly limit %@%@",GlobalVariables.currency,CategoryInfo[0].userData.weeklyTarget)
                
                let modifiedWeeklyString = NSMutableAttributedString(string: weeklyTargetString)
                let coloredWeeklyString = "Weekly limit"
                
                modifiedWeeklyString.setColorForText(coloredWeeklyString, with: #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1))
                
                print(currTargetString.count - (currTargetString.count - coloredString.count))
                offerCell.currentLbl.attributedText = changeFont(modifiedString, "Montserrat-regular", 14.0, NSRange(location: 0, length: coloredString.count))
                
                offerCell.weeklyLbl.attributedText = changeFont(modifiedWeeklyString, "Montserrat-regular", 14.0, NSRange(location: 0, length: coloredWeeklyString.count))
                
                offerCell.addNewTargetBtn.addTarget(self, action: #selector(addNewTargetBtnAction(sender:)), for: .touchUpInside)
                offerCell.addMoneyBtn.addTarget(self, action: #selector(addMoneyBtnAction(sender:)), for: .touchUpInside)
                
                if CategoryInfo[0].userData.target == "" || CategoryInfo[0].userData.weeklyTarget == ""{
                    
                    offerCell.currentLbl.isHidden = true
                    offerCell.weeklyLbl.isHidden = true
                }

                else {
                    
                    offerCell.currentLbl.isHidden = false
                    offerCell.weeklyLbl.isHidden = false
                }
  
                return offerCell
                
            case 2:
                
                let transactionCell = Bundle.main.loadNibNamed("PeculiTransactionsTableViewCell", owner: self, options: nil)?.first as! PeculiTransactionsTableViewCell

                transactionCell.voucherData = voucherTransactionData
                transactionCell.heightDelegate = self
                transactionCell.viewController = self
                
                return transactionCell
                
            default:
                let cell = UITableViewCell()
                cell.backgroundColor = #colorLiteral(red: 0.07450980392, green: 0.1019607843, blue: 0.1529411765, alpha: 1)
                
                return cell
            }
        }
        else{
            let cell = UITableViewCell()
            cell.backgroundColor = #colorLiteral(red: 0.2122195363, green: 0.16875422, blue: 0.3097219765, alpha: 1)
            
            return cell
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 2 {
            
            print(transactionTableHeight)
            
            return  transactionTableHeight + 158
            
        } else {
            return UITableView.automaticDimension
        }
        
    }
    
    @objc func addNewTargetBtnAction(sender: UIButton) {
        
        if checkIfGuest() == true {
            
            self.popupToRegister()
    
        }
        else {
           // if UserDefaults.standard.string(forKey: UserDefaultConstants.personalDetailsStatus.rawValue) == "1"{
                addNewTarget()
           // }
           // else{
                
          //      popupPersonalDetails(showMessage: "Please complete your personal details to set new target.")
           // }
        }
        
    }
    
    @objc func addMoneyBtnAction(sender: UIButton) {
        
        if checkIfGuest() == true {
            
            self.popupToRegister()
        }
        else {
           // if UserDefaults.standard.string(forKey: UserDefaultConstants.personalDetailsStatus.rawValue) == "1"{
                pushToAddAmountVC()
          //  }
           // else{
               
           //     popupPersonalDetails(showMessage: "Please complete your personal details to add funds in Pot.")
           // }
        }
        
        
        
    }
    
    func popupPersonalDetails(showMessage: String){
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutAlertViewController") as? LogoutAlertViewController else{return}
        
        vc.setTransition()
        vc.titleLblData = "Message"
        vc.messageLblData = showMessage
        vc.okCompletionHandler = {
            self.dismiss(animated: false) {
                let personalVc = PersonalDetailsViewController.instantiateFromAppStoryboard(appStoryboard: .main)
                self.pushViewController(viewController: personalVc)
            }
            
        }
        vc.cancelCompletionHandler = {
            self.dismiss(animated: true, completion: nil)
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    
    func pushToAddAmountVC() {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddMoneyViewController") as! AddMoneyViewController
        
        if CategoryInfo[0].userData.categoryName == "" {
            vc.potName = String(format: "%@ Pot",CategoryInfo[0].userData.categoryDetails.newName)
        }
        else {
            vc.potName = String(format: "%@ %@", CategoryInfo[0].userData.categoryName, "Pot")
        }
        
        vc.potImage = CategoryInfo[0].userData.categoryDetails.image //String(format: "%@%@", AppConfig.ImageUrl,CategoryInfo[0].userData.categoryDetails.image)
        
        vc.categoryId = CategoryInfo[0].userData.categoryDetails._id
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
  
    func addNewTarget(){

        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "SetTargetForPotViewController") as? SetTargetForPotViewController else{return}

        vc.addNewTargetData = CategoryInfo[0]
        vc.from = .MyPotDetailsVC
        
        if CategoryInfo[0].userData.categoryName == "" {
            
            vc.potName = String(format: "%@ Pot",CategoryInfo[0].userData.categoryDetails.newName)
            
        }
        else {
            
            vc.potName = String(format: "%@ %@", CategoryInfo[0].userData.categoryName, "Pot")
        }
        
        
       // vc.potImage = CategoryInfo[0].userData.categoryDetails.image //String(format: "%@%@", AppConfig.ImageUrl,CategoryInfo[0].userData.categoryDetails.image)
        
        vc.categoryId = CategoryInfo[0].userData.categoryDetails._id
        
        self.pushViewController(viewController: vc)
        
    }
    
    func changeFont(_ text: NSMutableAttributedString, _ fontName: String, _ size: CGFloat, _ range: NSRange) -> NSMutableAttributedString{

        let data = text
        data.addAttributes([
                            .font: UIFont(name: fontName, size: size)!], range: range)
        return data
    }
    
    func fetchTableViewHeight(height: CGFloat) {
        
        print("newwww--->", height)
        
        //        if height != 70 {
        //            if didreceiveTableViewHeight == false{
        //                print("new height-->", height)
        //                let index = IndexPath(item: 2, section: 0)
        //                DetailsTableView.reloadRows(at: [index], with: .automatic)
        //                didreceiveTableViewHeight = true
        //            }
        //        }
        
        if transactionTableHeight != height {
            
            transactionTableHeight = height
            let index = IndexPath(item: 2, section: 0)
            DetailsTableView.reloadRows(at: [index], with: .automatic)

        }

    }
    
    //transaction history api call
    
    func voucherTransactionsByCatIdApiCall(){
        
        
        SVProgressHUD.show()
        let userId = UserDefaults.standard.value(forKey: UserDefaultConstants.UserID.rawValue) as! String
        
        DataService.sharedInstance.voucherTransactionsByCatIdApiCall(userId: userId, categoryId: CategoryInfo[0].userData.categoryDetails._id) { (response, error) in
            
            print(response)
            
            if response != nil {
                
                SVProgressHUD.dismiss()
                if let code = response?["code"] as? String{
                    if code == "200" {
                        
                        if let result = response?["result"] as? NSArray{
                            
                            let listedTransactions = result
                            var voucherDict = Dictionary<String,Any>()
                            self.voucherTransactionData.removeAll()
                            for index in listedTransactions {

                                var sessionData = VoucherTransactionModel()
                                voucherDict = index as! Dictionary<String,Any>
                                sessionData.deserilize(values: voucherDict)
                                self.voucherTransactionData.append(sessionData)
                                
                            }
                            
                            self.DetailsTableView.reloadData()

                        }
                    }
                    else {
                        //                        AlertView.simpleViewAlert(title: "Error", response?["message"] as! String, self)
                    }
                }
            }
            else {
                
                SVProgressHUD.dismiss()
                //                AlertView.simpleViewAlert(title: "Error","Somthing went wrong", self)
            }

        }
        
    }
        
    ///API call
    
    
    func getCategoryInfoByIDApiCall() {
        SVProgressHUD.show()
        DataService.sharedInstance.getCategoryInfoByID(catID: potCatId) { (response, error) in
            
            print(response)
            
            SVProgressHUD.dismiss()
            self.CategoryInfo.removeAll()
            if response != nil{
                let message = response?["message"] as? String
                if let status = response?["status"] as? String{
                    
                    if status == "success" {
                        
                        if let result = response?["result"] as? NSArray{
                            if let data = result[0] as? Dictionary<String, Any> {
                                
                                var session = GetCategoryInfoByIdModel()
                                session.deserilize(values: data)
                                self.CategoryInfo.append(session)
                                
                            }
                            
                        }
                        
                    }
                    else {
                        
                        self.customAlertDismiss(title: "Message", message: message ?? "")
                    }
                    
                    self.setupValues()
                    
                    if self.CategoryInfo[0].userData.categoryDetails._id != "" {
                        
                        self.voucherTransactionsByCatIdApiCall()
                    }
                    
                    self.DetailsTableView.reloadData()
                    
                }
            }
            else {
                self.customAlertDismiss(title: "Message", message: "Something went wrong!")
            }
            
        }
    }
    
    
}


