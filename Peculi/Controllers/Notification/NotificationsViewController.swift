//
//  NotificationsViewController.swift
//  Peculi
//
//  Created by apple on 02/11/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit

class NotificationsViewController: UIViewController {
    
    //MARK: - Outlets
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var notificationTableView: UITableView!
    
    //MARK: - Define Array
    
    var notificationArray = ["sdhfbsdhfbvhjsevbsdhjbvsdhbvhjsdbfvhsbdhvbdshvbsdhjbvhjdsbvhjsdb", "sdhfbsdhfbvhjsevbsdhjbvsdhbvhjsdbfvhsbdhvbdshvbsdhjbvhjdsbvhjsdbsdsdfbvdfxbvdsfbvdsf", "sdhfbsdhfbvhjsevbsdhjbvsdhbvhjsdbfvhsbdhvbdshvbsdhjbvhjdsbvhjsdb"]
    
    var notificationDateLbl = ["20-10-2020", "20-10-2020","20-10-2020"]
    
    var notificationTimeLbl = ["11:15 AM ","11:15 AM ","Yesterday"]
    
    //MARK: - ViewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    //MARK: - ViewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        navigartionSetup()
    }
    
    
    //MARK: - Navigation Setup
    
    func navigartionSetup(){
        
        navigationController?.navigationBar.isHidden = true
        
        navView.layer.masksToBounds = false
        navView.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        navView.layer.shadowOpacity = 0.6
        navView.layer.shadowOffset = CGSize(width: 0, height: 3)
        navView.layer.shadowRadius = 3.0
       
    }
    
   /****Back action */
    @IBAction func backBtnAction(_ sender: Any) {
        popVc()
    }
    
}

//MARK: - tableview delegates

extension NotificationsViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell") as! NotificationTableViewCell
        cell.notificaitonLbl.text = notificationArray[indexPath.row]
        cell.notificationDateLbl.text = notificationDateLbl[indexPath.row]
        cell.notificaitonTimeLbl.text = notificationTimeLbl[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
