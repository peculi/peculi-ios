//
//  SignUpVc.swift
//  Peculi
//
//  Created by Stealth on 29/10/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Foundation
import SVProgressHUD
import DropDown
import MKToolTip

class SignUpVc: UIViewController , UITextFieldDelegate, UITextViewDelegate {
    
    //MARK: - outlets
    @IBOutlet weak var superChildView: UIView!
    @IBOutlet weak var profileImgVw: UIImageView!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var dobTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var dropDownImg: UIImageView!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var chooseImgVw: UIView!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtGender: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var postCodeTextField: UITextField!
    @IBOutlet weak var addressLine2Textfield: UITextField!
    @IBOutlet weak var confirmEmailTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var passwordEyeBtn: UIButton!
    @IBOutlet weak var confirmPasswordEyeBtn: UIButton!
    @IBOutlet weak var address1TextView: UITextView!
    @IBOutlet weak var address1Height: NSLayoutConstraint!
    @IBOutlet weak var addressLine2TextView: UITextView!
    @IBOutlet weak var addressLine2Height: NSLayoutConstraint!
    
    @IBOutlet weak var genderTextField: UITextField!
    @IBOutlet weak var newDobTxtField: UITextField!
    @IBOutlet weak var genderBtn: UIButton!
    
    
    //MARK: - variables
    var selectedDate = String()
    var dateTo = Date()
    var allCountries = [String]()
    var allStates = [String]()
    var allCity = [String]()
    var allCurrency = [String]()
    var shortName = [String]()
    var countryShortName = "GB"
    var countryName = String()
    var currencyName = "GBP"
    var dropGender = DropDown()
    var dropCountries = DropDown()
    var dropState = DropDown()
    var dropCity = DropDown()
    var newGender = DropDown()
    var fromVC: AllControllers?
    var placeHolderImage = #imageLiteral(resourceName: "ic_user_icon")
    let minTextViewHeight: CGFloat = 32
    let maxTextViewHeight: CGFloat = 64
    let addressLine1Placeholder = "Enter House Name/Number,Street Name"
    let addressLine2Placeholder = "Enter Apartment, suite, unit, building, floor etc.."
    var addressLine2Data = ""
    var otp = ""
    var email = ""
    var responseData = Dictionary<String,AnyObject>()
    var showGuestPopup = false
    
    //MARK: - view did load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if showGuestPopup == false{
            superChildView.isHidden = false
        
        }
        else{
            superChildView.isHidden = true
            popupToGuestRegister()
        }
        updateUi()
        
    }
    
    /**** setup UI */
    func updateUi(){
        
        self.navigationController?.navigationBar.isHidden = true
        navigationController?.navigationBar.barStyle = .black
        
        firstNameTextField.delegate = self
        firstNameTextField.attributedPlaceholder = NSAttributedString(string: "First Name",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        lastNameTextField.delegate = self
        lastNameTextField.attributedPlaceholder = NSAttributedString(string: "Last Name",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
              

        genderTextField.delegate = self
               genderTextField.attributedPlaceholder = NSAttributedString(string: "Select Gender",
                                                                          attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        newDobTxtField.delegate = self
        newDobTxtField.attributedPlaceholder = NSAttributedString(string: "Date of Birth",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])

        emailTextField.delegate = self
        emailTextField.attributedPlaceholder = NSAttributedString(string: "Email",
                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])

        txtPhoneNumber.delegate = self
        txtPhoneNumber.attributedPlaceholder = NSAttributedString(string: "Phone Number",
                                                                  attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])

        txtPassword.delegate = self
        txtPassword.attributedPlaceholder = NSAttributedString(string: "Password",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        confirmPasswordTextField.delegate = self
        confirmPasswordTextField.attributedPlaceholder = NSAttributedString(string: "Confirm Password",
                                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])

    }
    
    
    func setupDropDwn(SetForView: UIView, list: [String], callback: @escaping ((String)-> Void)) {
        
        newGender.anchorView = SetForView
        newGender.bottomOffset = CGPoint(x: 0, y: SetForView.bounds.height)
        newGender.dataSource = list
        newGender.selectionAction = { (index, item) in
            
            callback(item)
        }
    
    }
    
    
    func dobPicker() {
        
        let alert = UIAlertController(style: .alert, title: "")
        let date = Calendar.current.date(byAdding: .year, value: 0, to: Date())
        dateTo = date ?? Date()
        alert.addDatePicker(mode: .date, date: Date(), minimumDate: nil, maximumDate: Date()) { date in
            self.dateTo = date
        }
        alert.addAction( title: "OK", style: .default, isEnabled: true) { (action) in
            let formatter1 = DateFormatter()
            formatter1.dateFormat = "yyyy-MM-dd" //"dd-MM-yyyy"
            
            formatter1.locale = NSLocale(localeIdentifier: "en_US") as Locale?
            self.newDobTxtField.text = formatter1.string(from: self.dateTo)
        }
        alert.addAction(title: "Cancel", style: .cancel){ (action) in
            
        }
        alert.show()
    }
    
    
    
    
    //MARK: - button actions
    
    /*** Back button*/
    @IBAction func btnBackAction(_ sender: Any) {
        
        ///check if guest user or not
        let ifGuest = self.checkIfGuest()
        if ifGuest == true{
            popVc()
        }
        else {
            WelcomeScreenVc.setRootVC()
        }
        
    }
    
    
    /****  Select Gender */
    
      @IBAction func selectGenderAction(_ sender: Any) {
          
          setupDropDwn(SetForView: genderBtn, list: ["Male","Female","Other"]) {[weak self] (selectedValue) in
              self?.genderTextField.text = selectedValue
          }

          newGender.show()

      }
    
    
    
    @IBAction func selectDOBAction(_ sender: Any) {
        
        dobPicker()
        
    }
    
    /**** unhide password */
    
    @IBAction func passwordEyeBtnAction(_ sender: Any) {
        
        if txtPassword.tag == 0 {
            
            txtPassword.tag = 1
            passwordEyeBtn.setImage(UIImage(named: "ic_eye"), for: .normal)
            txtPassword.isSecureTextEntry = false
            
        }
        else {
            
            txtPassword.tag = 0
            passwordEyeBtn.setImage(UIImage(named: "ic_eyeclose1"), for: .normal)
            txtPassword.isSecureTextEntry = true
        }
        
    }
    
    /*** unhide confirm password */
    @IBAction func confirmPasswordEyeBtnAction(_ sender: Any) {
        
        if confirmPasswordTextField.tag == 0 {
            confirmPasswordTextField.tag = 1
            confirmPasswordEyeBtn.setImage(UIImage(named: "ic_eye"), for: .normal)
            confirmPasswordTextField.isSecureTextEntry = false
        }
        else {
            confirmPasswordTextField.tag = 0
            confirmPasswordEyeBtn.setImage(UIImage(named: "ic_eyeclose1"), for: .normal)
            confirmPasswordTextField.isSecureTextEntry = true
        }
        
    }
    
    
    @IBAction func btnToolTip_Action(_ sender: Any) {
        
        let preference = ToolTipPreferences()
        preference.drawing.bubble.color = UIColor(red: 0.937, green: 0.964, blue: 1.000, alpha: 1.000)
        preference.drawing.bubble.spacing = 10
        preference.drawing.bubble.cornerRadius = 5
        preference.drawing.bubble.inset = 15
        preference.drawing.bubble.border.color = UIColor(red: 0.768, green: 0.843, blue: 0.937, alpha: 1.000)
        preference.drawing.bubble.border.width = 1
        preference.drawing.arrow.tipCornerRadius = 5
        preference.drawing.message.color = UIColor(red: 0.200, green: 0.200, blue: 0.200, alpha: 1.000)
        preference.drawing.message.font = UIFont.systemFont(ofSize: 13, weight: .bold)
        preference.drawing.button.color = UIColor(red: 0.074, green: 0.231, blue: 0.431, alpha: 1.000)
        preference.drawing.button.font = UIFont.systemFont(ofSize: 13, weight: .bold)
        (sender as AnyObject).showToolTip(identifier: "DOB", message: "Peculi accounts are only available to over 18’s.", button: "", arrowPosition: .left, preferences: preference, delegate: nil) //
        
    }
    
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard range.location == 0 else {
            return true
        }

        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string) as NSString
        return newString.rangeOfCharacter(from: CharacterSet.whitespacesAndNewlines).location != 0
    }

    
    /**** signup */
    @IBAction func btnSignUpAction(_ sender: Any) {
        
            let str = firstNameTextField.text
            let trimmed = str?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        let str1 = lastNameTextField.text
        let trimmed1 = str1?.trimmingCharacters(in: .whitespacesAndNewlines)
        
//        let dateOfBirth = datePicker.date
//
//           let today = NSDate()
//
//           let gregorian = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
//
//           let age = gregorian.components([.Year], fromDate: dateOfBirth, toDate: today, options: [])
//
//           if age.year < 18 {
//               // user is under 18
//           }
//
        if newDobTxtField.text ?? "" == ""  {
            
            AlertView.simpleViewAlert(title: "", "Please enter All details!", self)
            return
            
        } else {
            
            
            let dob : String = newDobTxtField.text ?? "" //"05-31-1996"

            // Format Date
         
            var myFormatte = DateFormatter()
            
            myFormatte.dateFormat = "yyyy-MM-dd" //"MM-dd-yyyy"

            // Convert DOB to new Date

            var finalDate : Date = myFormatte.date(from: dob)!

            // Todays Date

            let now = Date()
            
            // Calender
       
            let calendar = Calendar.current
            
            // Get age Components
            

            let ageComponents = calendar.dateComponents([.year], from: finalDate, to: now)

            print("Age is \(ageComponents.year!)") // Output 21
            
            
            if trimmed == "" {
                
                AlertView.simpleViewAlert(title: "", "Please enter first name!", self)
                return
            }
            else if trimmed1 == "" {
                
                AlertView.simpleViewAlert(title: "", "Please enter Last Name!", self)
                return
            }
            
            else if emailTextField.text == "" {
                AlertView.simpleViewAlert(title: "", "Please enter email!", self)
                return
                
            } else if ageComponents.year! < 18 {
                
                AlertView.simpleViewAlert(title: "", "Peculi accounts are only available to over 18’s.", self)
                return
                
            } else if newDobTxtField.text == "" {
                AlertView.simpleViewAlert(title: "", "Please enter date of birth!", self)
                return
            }
//            else if genderTextField.text == "" {
//                AlertView.simpleViewAlert(title: "", "Please enter gender!", self)
//                return
//            }
//            else if txtPhoneNumber.text == "" {
//                AlertView.simpleViewAlert(title: "", "Please enter phone number!", self)
//                return
//            }

            else if txtPassword.text == "" {
                AlertView.simpleViewAlert(title: "", "Please enter password!", self)
                return
            }
            
    //        else if confirmPasswordTextField.text?.count == 0{
    //            AlertView.simpleViewAlert(title: "", "Please confirm your password!", self)
    //        }
            
            else if !(emailTextField.text?.EmailValidation())! {
                AlertView.simpleViewAlert(title: "", "Please enter valid email!", self)
                return
            }
            
//            else if txtPhoneNumber.text?.count ?? 0 < 6 {
//                
//                AlertView.simpleViewAlert(title: "", "Please enter valid phone number!", self)
//                
//            }

            else if !isValidPassword(txtPassword.text!) || txtPassword.text!.count < 8 {
                AlertView.simpleViewAlert(title: "", "Minimum Password length is 8 character includes a lower case letter , an upper case letter, a special character and no whitespace allowed. e.g (Peculi@1234!!)", self)
                return
            }
            
    //        else if txtPassword.text != confirmPasswordTextField.text {
    //            AlertView.simpleViewAlert(title: "", "Password and confirm password does not match!", self)
    //            return
    //        }
            
            else {
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpDetail_VC") as! SignUpDetail_VC

                let deviceid = UIDevice.current.identifierForVendor?.uuidString ?? ""
                let userID = UserDefaults.standard.string(forKey: UserDefaultConstants.UserID.rawValue) ?? "0"
     
                vc.firstname = firstNameTextField.text ?? ""
                vc.lastname = lastNameTextField.text ?? ""
                vc.email = emailTextField.text ?? ""
                vc.password = txtPassword.text ?? ""
               // vc.phone = txtPhoneNumber.text ?? ""
                vc.newDob = newDobTxtField.text ?? ""
                //vc.newGender = genderTextField.text ?? ""
                vc.device_id = deviceid
                vc.device_token = GlobalVariables.device_token
                vc.user_id = userID
                
                self.navigationController?.pushViewController(vc, animated: true)
                
         //   addNewEvents(placeHolderImage)
                
            }
            
        }
        
       // let calculateAge = Int(ageComponents.year!)
        
      //  print(calculateAge)
        
    }
    

    // guest register popup
    func popupToGuestRegister(){
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutAlertViewController") as? LogoutAlertViewController else {
            return
        }
        vc.setTransition()
        vc.titleLblData = "Message"
        vc.messageLblData = "Please complete your registration process to proceed further!"
        vc.okCompletionHandler = { [weak self] in
            
            self?.dismiss(animated: true) {
                
                self?.superChildView.isHidden = false
          
            }
        }
        vc.cancelCompletionHandler = { [weak self] in
            
            self?.dismiss(animated: false){
                self?.popVc()
            }
        }
        
        self.present(vc, animated: true, completion: nil)
    }
    
    
    func isValidPassword(_ password: String) -> Bool {
        let passwordRegex = "^(?=.{8,}$)(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9])(?=.*?\\W).*$"
        
        
        let passPred = NSPredicate(format:"SELF MATCHES %@", passwordRegex)
        return passPred.evaluate(with: password)
    }
    
    /*** sign in action*/
    @IBAction func btnSignInAction(_ sender: Any) {
        let vc = SignInVc.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.fromVC = fromVC
        self.pushViewController(viewController: vc)
    }
    
    
    //Signup API setup
    func signUpApiCallSetup() {
        
        let deviceid = UIDevice.current.identifierForVendor?.uuidString ?? ""
        let userID = UserDefaults.standard.string(forKey: UserDefaultConstants.UserID.rawValue) ?? ""
        
        let params:[String: Any] = ["firstname": firstNameTextField.text ?? "",
                                    "lastname": lastNameTextField.text ?? "",
                                    "email": emailTextField.text ?? "",
                                    "password": txtPassword.text ?? "",
                                    "phone": txtPhoneNumber.text ?? "",
                                    "device_id": deviceid,
                                    "device_token": GlobalVariables.device_token,
                                    "device_type": "ios",
                                    "user_id": userID]
        
        SignupApiModel().signUpApiCall(param: params) { (response) in
            print(response)
            guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "EmailVerificationViewController") as? EmailVerificationViewController else {
                
                return
                
            }
            
            self.pushViewController(viewController: vc)
            
        }
        
    }
    
    
    
        //multipart API setup
        func postComplexPictures(url:URL, params:[String:Any], imageToUpload : Data, finish: @escaping ((message:String, list:[String: Any],isSuccess:Bool)) -> Void) {
            var result:(message:String, list:[String: Any],isSuccess:Bool) = (message: "Something went wrong!", list:[:],isSuccess : false)
            SVProgressHUD.show()
            
            //        let headers: HTTPHeaders
            //        headers = ["Content-Type":"application/json"]
            
            AF.upload(multipartFormData: { (multipartFormData) in
//                let jpegData = imageToUpload
                //let jsonData = try! JSONSerialization.data(withJSONObject: params, options: [])
                // multipartFormData.append(jsonData, withName: "" )
    
//                multipartFormData.append(jpegData, withName: "image",fileName: "file.jpg", mimeType: "image/jpg")
                
                for (key, value) in params {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            }, to: url, usingThreshold: UInt64.init(), method: .post, headers: nil).responseJSON{ response in
    
                print(response)
                if((response.error == nil))
                {
                    do
                    {
                        if let jsonData = response.data
                        {
                            SVProgressHUD.dismiss()
                            let parsedData = try JSONSerialization.jsonObject(with: jsonData) as! Dictionary<String, AnyObject>
                            self.responseData = parsedData
                            let status = parsedData["status"] as? String ?? ""
                            let msg = parsedData["message"] as? String ?? ""
                            if let result = parsedData["result"] as? NSDictionary {
                                let userEmail = result["email"] as? String ?? ""
                                self.otp = result["otp"] as? String ?? ""
                                self.email = userEmail
                            }
                            print("parsedData=", parsedData)
                            if(status=="success") {
                                if let jsonArray = parsedData["result"] as? [String: Any] {
                                    result.isSuccess = true
                                    result.list = jsonArray
                                }
                            } else {
                                result.isSuccess = false
                                result.message=msg
                            }
    
                        }
                        SVProgressHUD.dismiss()
                        finish(result)
                    } catch {
                        SVProgressHUD.dismiss()
                        finish(result)
                    }
                } else {
                    
                    SVProgressHUD.dismiss()
                    print("Resonse.error",response.error?.localizedDescription as Any)
                    finish(result)
                    
                }
            }
        }

    
    func addNewEvents(_ img: UIImage) {
        
        let deviceid = UIDevice.current.identifierForVendor?.uuidString ?? ""
        let userID = UserDefaults.standard.string(forKey: UserDefaultConstants.UserID.rawValue) ?? ""
        
        let params:[String: Any] = ["firstname": firstNameTextField.text ?? "",
                                    "lastname": lastNameTextField.text ?? "",
                                    "email": emailTextField.text ?? "",
                                    "password": txtPassword.text ?? "",
                                    "phone": txtPhoneNumber.text ?? "",
                                    "device_id": deviceid,
                                    "device_token": GlobalVariables.device_token,
                                    "device_type": "ios",
                                    "user_id": userID]
        
        print(params)
        
        let pickedImage = img
        
        let Url = AppConfig.BaseUrl + constantApis.signup
        var jpegData = Data()
        
        if img.cgImage != nil {
            if pickedImage.jpegData(compressionQuality: 0.2) != nil {
                jpegData = pickedImage.jpegData(compressionQuality: 0.2)!
            }
            
        }
        
        //        else{
        //
        //            if image.jpegData(compressionQuality: 0.2) != nil{
        //                           jpegData = image.jpegData(compressionQuality: 0.2)!
        //                       }
        //        }
        
        
        
        self.postComplexPictures(url:NSURL.init(string: Url)! as URL , params: params, imageToUpload: jpegData) { (arg0) in
            let (_, list, isSuccess) = arg0
            if(isSuccess) {
                print(list)
                //                    self.showAlert("","Sign Up Successfully")
                self.pressed()
                
            } else {
                print(list)
                self.customAlertDismiss(title: "", message: arg0.message)
                //
            }
        }
    }
    
    //MARK: -  Alert Function
    
    func showAlert( _ title:String, _ message:String) {
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "CustomDefaultAlertViewController") as? CustomDefaultAlertViewController else{return}
        vc.setTransition()
        vc.titleVal = title
        vc.messageVal = message
        vc.completionHandler = { [weak self] in
            self?.dismiss(animated: true) {
                self?.pressed()
            }
        }
        self.present(vc, animated: true, completion: nil)
        
    }
    
    func pressed() {
        let vc = EmailVerificationViewController.instantiateFromAppStoryboard(appStoryboard: .main)
//        vc.commingFromScreen = "Signup"
//        vc.fromVC = fromVC
       // vc.otp = self.otp
     //   vc.email = self.email
       // vc.response = self.responseData
        self.pushViewController(viewController: vc)
    }
    
}


