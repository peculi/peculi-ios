

//
//  SignUpVc.swift
//  Peculi
//
//  Created by Stealth on 29/10/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.


import UIKit
import Alamofire
import SwiftyJSON
import Foundation
import SVProgressHUD
import DropDown




var latLongStatus = "0"




class SignUpDetail_VC: UIViewController , UITextFieldDelegate, UITextViewDelegate {

    var firstname = ""
    var lastname = ""
    var email = ""
    var password = ""
    var phone = ""
    var newDob = ""
    var newGender = ""
    var device_id = ""
    var device_token = ""
    var user_id = ""

    var currentLat : Double?
    var currentlong : Double?
    
    var cityNameStr = ""
  //  var locationManager = CLLocationManager()
    var signupModel = [Signup_ModelStructure]()
    
    var allPostCodeData1 = [allPostCodeData]()

    var arrPostCodeAddress = [String]()
    

    //MARK: - outlets
    
    
    @IBOutlet weak var superChildView: UIView!
    @IBOutlet weak var profileImgVw: UIImageView!
 
    @IBOutlet weak var dobtxtField: UITextField!
    @IBOutlet weak var addressTxtField: UITextField!
    @IBOutlet weak var zipcodeTxtField: UITextField!
    @IBOutlet weak var cityTxtField: UITextField!
    @IBOutlet weak var countryCodeTxtField: UITextField!
    
    

    @IBOutlet weak var dropDownImg: UIImageView!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var chooseImgVw: UIView!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtGender: UITextField!
  
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var postCodeTextField: UITextField!
    @IBOutlet weak var addressLine2Textfield: UITextField!
    @IBOutlet weak var confirmEmailTextField: UITextField!

    @IBOutlet weak var passwordEyeBtn: UIButton!
    @IBOutlet weak var confirmPasswordEyeBtn: UIButton!
    @IBOutlet weak var address1TextView: UITextView!
    @IBOutlet weak var address1Height: NSLayoutConstraint!
    @IBOutlet weak var addressLine2TextView: UITextView!
    @IBOutlet weak var addressLine2Height: NSLayoutConstraint!
    
    @IBOutlet weak var btnSelectOccupation: UIButton!
    ////mmmmmmmm
    
    @IBOutlet weak var occuptionProfsionView: UIView!
    @IBOutlet weak var occuptionView: UIView!
    @IBOutlet weak var monthlyIncomeView: UIView!
    
    @IBOutlet weak var occuptionProfessionTxtField: UITextField!
    @IBOutlet weak var selectOccuptionTxtField: UITextField!
    @IBOutlet weak var monthlyIncomeTxtField: UITextField!
    @IBOutlet weak var btnMonthlyIncome: UIButton!
    
    @IBOutlet weak var btnSelectAddress: UIButton!
    
    var allOccupationProfsionArr = [String]()
    var selectOccuptionArr = [String]()
    var selectMonthlyIncomeArr = [String]()

    //MARK: - variables
    
    
    var selectedDate = String()
    var dateTo = Date()
    var allCountries = [String]()
    var allStates = [String]()
    var allCity = [String]()
    var allCurrency = [String]()
    var shortName = [String]()
    var countryShortName = "GB"
    var countryName = String()
    var currencyName = "GBP"
    var dropGender = DropDown()
    var dropCountries = DropDown()
    var dropState = DropDown()
    var dropCity = DropDown()
    var commonDropDown = DropDown()
    var fromVC: AllControllers?
    var placeHolderImage = #imageLiteral(resourceName: "ic_user_icon")
    let minTextViewHeight: CGFloat = 32
    let maxTextViewHeight: CGFloat = 64
    let addressLine1Placeholder = "Enter House Name/Number,Street Name"
    let addressLine2Placeholder = "Enter Apartment, suite, unit, building, floor etc.."
    var addressLine2Data = ""
    var otp = ""
  //  var email = ""
    var responseData = Dictionary<String,AnyObject>()
    var showGuestPopup = false
    
    
    var clickDropDown = ""
    //MARK: - view did load
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpDropDown()

        if showGuestPopup == false {
            superChildView.isHidden = false
        }
        else {
            superChildView.isHidden = true
            popupToGuestRegister()
        }
        updateUi()
 
    }
    
    /**** setup UI */
    func updateUi(){
        
        self.navigationController?.navigationBar.isHidden = true
        navigationController?.navigationBar.barStyle = .black
        
       
        
        occuptionProfessionTxtField.delegate = self
        occuptionProfessionTxtField.attributedPlaceholder = NSAttributedString(string: "Select Profession",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
     
        
        selectOccuptionTxtField.delegate = self
        selectOccuptionTxtField.attributedPlaceholder = NSAttributedString(string: "Select Occupation",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
     
        
        monthlyIncomeTxtField.delegate = self
        monthlyIncomeTxtField.attributedPlaceholder = NSAttributedString(string: "Select Monthly Income",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
       
        
        dobtxtField.delegate = self
        dobtxtField.attributedPlaceholder = NSAttributedString(string: "Date of Birth",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        addressTxtField.delegate = self
        addressTxtField.attributedPlaceholder = NSAttributedString(string: "Address",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        cityTxtField.delegate = self
        cityTxtField.attributedPlaceholder = NSAttributedString(string: "City",
                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        
        zipcodeTxtField.delegate = self
        zipcodeTxtField.attributedPlaceholder = NSAttributedString(string: "Post Code",
                                                                  attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        

        countryCodeTxtField.delegate = self
        countryCodeTxtField.attributedPlaceholder = NSAttributedString(string: "Country",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
   
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        getCurrentLocation()
//        locationManager.requestAlwaysAuthorization()
//        locationManager.startUpdatingLocation()
//        locationManager.requestLocation()
//
//        if CLLocationManager.locationServicesEnabled() {
//            locationManager.startUpdatingLocation() // start location manager
//
//        }
        
     
        
        
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            print("countryCode===>>>>>\(countryCode)")
            
            self.countryCodeTxtField.text = countryCode
        }
        
        
        
    }

    //MARK: - button actions
    
    /*** Back button*/
    
    @IBAction func btnBackAction(_ sender: Any) {
        
        //check if guest user or not

        
//        let ifGuest = self.checkIfGuest()
//        if ifGuest == true{
//            popVc()
//        }
//        else {
//
//            WelcomeScreenVc.setRootVC()
//        }
        
        popVc()
        
    }

    @IBAction func btnOccupsionProfession_Action(_ sender: Any) {
        
  
        
        clickDropDown = "0"
        setupCommonDropDown()
        commonDropDown.show()
        btnSelectOccupation.isUserInteractionEnabled = true
        //btnMonthlyIncome.isUserInteractionEnabled = true
        
        
    }
    
    
    @IBAction func btnOccupsion_Action(_ sender: Any) {

        if self.occuptionProfessionTxtField.text == "Retired" {
            
            btnSelectOccupation.isUserInteractionEnabled = false
            self.occuptionProfessionTxtField.isUserInteractionEnabled = false
            
        } else {
            
            self.occuptionProfessionTxtField.isUserInteractionEnabled = false
            btnSelectOccupation.isUserInteractionEnabled = true
          btnMonthlyIncome.isUserInteractionEnabled = true
            clickDropDown = "1"
            setupCommonDropDown()
            commonDropDown.show()
            
        }

    }
    
    
    @IBAction func btnMonthlyIncome_Action(_ sender: Any) {
        
        if self.occuptionProfessionTxtField.text == "Retired" {
            
            clickDropDown = "2"
            setupCommonDropDown()
            commonDropDown.show()
            btnSelectOccupation.isUserInteractionEnabled = true
          btnMonthlyIncome.isUserInteractionEnabled = true
            btnSelectOccupation.isUserInteractionEnabled = false
           
        } else {

            clickDropDown = "2"
            setupCommonDropDown()
            commonDropDown.show()
            btnSelectOccupation.isUserInteractionEnabled = true
          btnMonthlyIncome.isUserInteractionEnabled = true
        }
        
       
    }
    
    /**** unhide password */
    @IBAction func passwordEyeBtnAction(_ sender: Any) {
        
 
        
        
    }
    
    /*** unhide confirm password */
    
    @IBAction func confirmPasswordEyeBtnAction(_ sender: Any) {

        
    }
    
    /**** Select DOB */
    @IBAction func selectDOBAction(_ sender: Any) {
        
        self.dobPicker()
        
    }
    
    
    
    @IBAction func btnSelectAddress_Action(_ sender: Any) {
        
        if zipcodeTxtField.text == "" {
            
        } else {
            postalCodeSearchNew_Api()
        }
            

        clickDropDown = "3"
  
        setupCommonDropDown()
        commonDropDown.show()
        
    }
    
    func setupCommonDropDown() {
        
        if clickDropDown == "0" {
            
            commonDropDown.anchorView = occuptionProfsionView
            commonDropDown.bottomOffset = CGPoint(x: 0, y: occuptionProfsionView.bounds.height + 5 )//
            commonDropDown.dataSource = allOccupationProfsionArr
            commonDropDown.selectionAction = { [weak self] (index, item) in

                self?.occuptionProfessionTxtField.text = item
                
                if item == "Retired" {
                    self?.selectOccuptionTxtField.text = ""
           //         self?.monthlyIncomeTxtField.text = ""
                    
                    self?.selectOccuptionTxtField.attributedPlaceholder = NSAttributedString(string: "Select Occupation",
                                                                           attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
                    
                } else {
                    
                    self?.selectOccuptionTxtField.attributedPlaceholder = NSAttributedString(string: "Select Occupation",
                                                                           attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
                    
                }
                
                
               
         
            }
            
            
        } else if clickDropDown == "1" {
            commonDropDown.anchorView = occuptionView
            commonDropDown.bottomOffset = CGPoint(x: 0, y: occuptionView.bounds.height + 5)
            commonDropDown.dataSource = selectOccuptionArr
            commonDropDown.selectionAction = { [weak self] (index, item) in
                self?.selectOccuptionTxtField.text = item
            }
            
        } else if clickDropDown == "2" {
            
            commonDropDown.anchorView = monthlyIncomeView
            commonDropDown.bottomOffset = CGPoint(x: 0, y: monthlyIncomeView.bounds.height + 5)
            commonDropDown.dataSource = selectMonthlyIncomeArr
            commonDropDown.selectionAction = { [weak self] (index, item) in
                self?.monthlyIncomeTxtField.text = item
                
            }
            
        }  else if clickDropDown == "3" {
            
            commonDropDown.anchorView = btnSelectAddress
            commonDropDown.bottomOffset = CGPoint(x: 0, y: btnSelectAddress.bounds.height + 5)
        
            commonDropDown.dataSource = arrPostCodeAddress
           
            commonDropDown.selectionAction = { [weak self] (index, item) in
                self?.addressTxtField.text = item
                self?.cityTxtField.text = self?.allPostCodeData1[index].town_or_city
               // self?.countryCodeTxtField.text = self?.allPostCodeData1[index].country
                
            }
        }
    }
    
    
    func setUpDropDown() {
        
        clickDropDown = "0"
        allOccupationProfsionArr = ["Director / Owner","Executive","Manager","Employee / Worker","Self employed","Student","Retired","Unemployed"]
        
         selectOccuptionArr = ["Public servant / Police / Military","Agriculture","Craftwork / Trade","Arts / Culture / Sport","Banking / Insurance / Finance / Auditing","Construction / Publicworks","Education","Manufacturing / maintenance","Medical / Paramedical","Food industry / Work from home / Hospitali","Services / IT","Social Security / NGO","Politician / Elected Member of Parliament"]
        
         selectMonthlyIncomeArr = ["LESS THAN 500 GBP","500 to 1000 GBP","1001 to 1500 GBP","1501 to 2000 GBP","2001 to 3000 GBP","MORE THAN 3000 GBP"]
       
        //self.ShowDropDown()
        
        DropDown.appearance().backgroundColor = #colorLiteral(red: 0.1209298894, green: 0.1362816691, blue: 0.1861090362, alpha: 1)//UIColor.gray
        DropDown.appearance().selectionBackgroundColor = #colorLiteral(red: 0.1209298894, green: 0.1362816691, blue: 0.1861090362, alpha: 1)///UIColor.black
        DropDown.appearance().textColor = UIColor.white
        DropDown.appearance().selectedTextColor = UIColor.white
        DropDown.appearance().cornerRadius = 4
        commonDropDown.direction = .bottom
        commonDropDown.backgroundColor = #colorLiteral(red: 0.1846595407, green: 0.1497857869, blue: 0.2699982524, alpha: 1)
        commonDropDown.shadowColor = UIColor.clear
        
    }
    
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == self.dobtxtField {
            return false
        }
        return true
        
    }
    

    /**** signup */
    @IBAction func btnSignUpAction(_ sender: Any) {
        
//        if dobtxtField.text?.count == 0 {
//            AlertView.simpleViewAlert(title: "", "Please enter dob!", self)
//            return
//        }
//        else


        if addressTxtField.text?.count == 0 {
            AlertView.simpleViewAlert(title: "", "Please enter address!", self)
            return
        }

        else if cityTxtField.text?.count == 0 {
            AlertView.simpleViewAlert(title: "", "Please enter city!", self)
            return
        }

        else if zipcodeTxtField.text?.count == 0 {
            
            AlertView.simpleViewAlert(title: "", "Please enter Post code!", self)
            return
        }
        else if countryCodeTxtField.text?.count == 0 {
            AlertView.simpleViewAlert(title: "", "Please enter country!", self)
            return
        } else if occuptionProfessionTxtField.text?.count == 0 {
            AlertView.simpleViewAlert(title: "", "Please select Occuption Profession!", self)
            return
        }
        
        
//        else if selectOccuptionTxtField.text?.count == 0 {
//            AlertView.simpleViewAlert(title: "", "Please select Occuption!", self)
//            return
//        } else if monthlyIncomeTxtField.text?.count == 0 {
//            AlertView.simpleViewAlert(title: "", "Please select Monthly income!", self)
//            return
//        }

        else {
            
            signUpApiCallSetup()
            
       // addNewEvents(placeHolderImage)
            
        }

    }
    
    
    // guest register popup
    func popupToGuestRegister(){
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutAlertViewController") as? LogoutAlertViewController else {
            return
        }
        vc.setTransition()
        vc.titleLblData = "Message"
        vc.messageLblData = "Please complete your registration process to proceed further!"
        vc.okCompletionHandler = { [weak self] in
            
            self?.dismiss(animated: true) {
                
                self?.superChildView.isHidden = false
          
            }
        }
        vc.cancelCompletionHandler = { [weak self] in
            
            self?.dismiss(animated: false){
                self?.popVc()
            }
        }

        self.present(vc, animated: true, completion: nil)

    }
    
    
    func isValidPassword(_ password: String) -> Bool {
        let passwordRegex = "^(?=.{8,}$)(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9])(?=.*?\\W).*$"
        
        
        let passPred = NSPredicate(format:"SELF MATCHES %@", passwordRegex)
        return passPred.evaluate(with: password)
    }
    
    
    
    func dobPicker() {
        
        let alert = UIAlertController(style: .alert, title: "")
        let date = Calendar.current.date(byAdding: .year, value: 0, to: Date())
        dateTo = date ?? Date()
        alert.addDatePicker(mode: .date, date: Date(), minimumDate: nil, maximumDate: Date()) { date in
            self.dateTo = date
        }
        alert.addAction( title: "OK", style: .default, isEnabled: true) { (action) in
            let formatter1 = DateFormatter()
            formatter1.dateFormat = "yyyy-MM-dd" //"dd-MM-yyyy"
            
            formatter1.locale = NSLocale(localeIdentifier: "en_US") as Locale?
            self.dobtxtField.text = formatter1.string(from: self.dateTo)
        }
        alert.addAction(title: "Cancel", style: .cancel){ (action) in
            
        }
        alert.show()
    }
    
    /*** sign in action*/
    @IBAction func btnSignInAction(_ sender: Any) {
        
        let vc = SignInVc.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.fromVC = fromVC
        self.pushViewController(viewController: vc)
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard range.location == 0 else {
            return true
        }

        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string) as NSString
        return newString.rangeOfCharacter(from: CharacterSet.whitespacesAndNewlines).location != 0
    }
    
 
    
    

    
 
    
    
    
    func signUpApiCallSetup(){

       // "user_id":0"
        
        
        SVProgressHUD.show()
        DataService.sharedInstance.signUp(firstname: firstname, lastName: lastname, email: email, password: password, gender: newGender, dob: newDob, phone: phone, city: cityTxtField.text!, countryCode: countryCodeTxtField.text!, zipcode: zipcodeTxtField.text!, address: addressTxtField.text!, deviceId: device_id, deviceToken: device_token, deviceType: "ios", userId: user_id,profession:occuptionProfessionTxtField.text ?? "",occupation:selectOccuptionTxtField.text ?? "", income:monthlyIncomeTxtField.text ?? "") { (resultDict, errorMsg) in
            
            SVProgressHUD.dismiss()
            
            print(resultDict as Any)
            
            if errorMsg == nil {
                
                if let result = resultDict?["result"] as? NSDictionary {
                   
                    self.responseData = result as! [String : AnyObject]
                    
                    if let id = result["_id"] as? String {

                        let otp = result["otp"] as? Int
                        let firstname =  result["firstname"] as? String
                        let lastname = result["lastname"] as? String
                        let status = result["status"] as? Int
                        let gender = result["gender"] as? String
                        let dob = result["dob"] as? String

                        let email = result["email"] as? String
                        let phone = result["phone"] as? String
                        let address = result["address"] as? String
                       
                        let countrycode = result["countrycode"] as? String
                        let city = result["city"] as? String
                        let zipcode = result["zipcode"] as? String

                        // let __v = result["__v"] as? Int
                        let jwtToken = result["jwtToken"] as? String
//                        let otp_status = result["otp_status"] as? String
//                        let username = result["username"] as? String
//                        let currency = result["currency"] as? String
//                        let device_id = result["device_id"] as? String
                     //  device_token = result["device_token"] as? String
//                        let device_type = result["device_type"] as? String

                        UserDefaults.standard.set(otp, forKey: "SAVEOTP")
                        UserDefaults.standard.set(id, forKey: "SAVEID")
                        
                        UserDefaults.standard.set(firstname, forKey: UserDefaultConstants.FirstName.rawValue)
                        UserDefaults.standard.set(lastname, forKey: UserDefaultConstants.LastName.rawValue)
                        UserDefaults.standard.set(status, forKey: UserDefaultConstants.LoginStatus.rawValue)
                        UserDefaults.standard.setValue(id, forKey: UserDefaultConstants.UserID.rawValue)
                        
                        UserDefaults.standard.set(gender, forKey: UserDefaultConstants.userGender.rawValue)
                        //    //                    UserDefaults.standard.set(userinfo[0].image, forKey: "UserImage")
                        UserDefaults.standard.set(dob, forKey: UserDefaultConstants.UserDob.rawValue)
                        UserDefaults.standard.set(email, forKey: UserDefaultConstants.UserEmail.rawValue)
                        //
                        UserDefaults.standard.set(phone, forKey: UserDefaultConstants.PhoneNumber.rawValue)
                        
                        UserDefaults.standard.set(address, forKey: "Address")
                        UserDefaults.standard.set(countrycode, forKey: "country")
                        
                        // UserDefaults.standard.set(state, forKey: "state")
                        
                        UserDefaults.standard.set(city, forKey: UserDefaultConstants.city.rawValue)
                        UserDefaults.standard.set(city, forKey: UserDefaultConstants.countryName.rawValue)
                        
                        UserDefaults.standard.set(zipcode, forKey: UserDefaultConstants.postcode.rawValue)
                        UserDefaults.standard.set(true, forKey:  UserDefaultConstants.isguestLogin.rawValue)
                     
                        // "device_token"

//                        UserDefaults.standard.set(address1, forKey: UserDefaultConstants.Address1.rawValue)
//                        UserDefaults.standard.set(address2, forKey: UserDefaultConstants.Address2.rawValue)
//                        UserDefaults.standard.set(personalDetailsStatus, forKey: UserDefaultConstants.personalDetailsStatus.rawValue)
//                        UserDefaults.standard.set(otp, forKey: UserDefaultConstants.otp.rawValue)

                        UserDefaults.standard.set(jwtToken, forKey: UserDefaultConstants.jwToken.rawValue)
                        
//                        UserDefaults.standard.set(kycStatus, forKey: UserDefaultConstants.kycStatus.rawValue)
                        
                        UserDefaults.standard.set("0", forKey: UserDefaultConstants.showPopupHomeScreen.rawValue)
                        UserDefaults.standard.set("0", forKey: UserDefaultConstants.showPopup_PeculiOfferScreen.rawValue)
                        UserDefaults.standard.set("0", forKey: UserDefaultConstants.showPopup_redeemScreen.rawValue)
                        

                    self.signupModel.append(Signup_ModelStructure.init(signUpId: id ?? "", signupOtp: otp ?? 0))
                        
                    self.pressed()

                       } else {
                           
                           let message = result["message"] as? String
                             Toast.show(message: message ?? "Something went wrong", controller: self)
                           
               }
                    
            }
                
            else {
                
                Toast.show(message: "Something went wrong", controller: self)
            }
        }
            SVProgressHUD.dismiss()
            Toast.show(message: "\(errorMsg)" , controller: self) //"Something went wrong"
    }

    }
    
    

    
        //multipart API setup
    
        func postComplexPictures(url:URL, params:[String:Any], imageToUpload : Data, finish: @escaping ((message:String, list:[String: Any],isSuccess:Bool)) -> Void) {
            var result:(message:String, list:[String: Any],isSuccess:Bool) = (message: "Something went wrong!", list:[:],isSuccess : false)
            SVProgressHUD.show()
            //        let headers: HTTPHeaders
            //        headers = ["Content-Type":"application/json"]
            AF.upload(multipartFormData: { (multipartFormData) in
//                let jpegData = imageToUpload
                //let jsonData = try! JSONSerialization.data(withJSONObject: params, options: [])
                // multipartFormData.append(jsonData, withName: "" )
    
//                multipartFormData.append(jpegData, withName: "image",fileName: "file.jpg", mimeType: "image/jpg")
                
                for (key, value) in params {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            }, to: url, usingThreshold: UInt64.init(), method: .post, headers: nil).responseJSON{ response in
    
                print(response)
                
                if((response.error == nil))
                    
                {
                    do
                    {
                        if let jsonData = response.data
                        {
                            SVProgressHUD.dismiss()
                            let parsedData = try JSONSerialization.jsonObject(with: jsonData) as! Dictionary<String, AnyObject>
                            self.responseData = parsedData
                            let status = parsedData["status"] as? String ?? ""
                            let msg = parsedData["message"] as? String ?? ""
                            if let result = parsedData["result"] as? NSDictionary {
                                let userEmail = result["email"] as? String ?? ""
                                self.otp = result["otp"] as? String ?? ""
                                self.email = userEmail
                                
                            }
                            
                            print("parsedData=", parsedData)
                            if(status=="success") {
                                if let jsonArray = parsedData["result"] as? [String: Any] {
                                    result.isSuccess = true
                                    result.list = jsonArray
                                }
                            } else {
                                result.isSuccess = false
                                result.message=msg
                            }
    
                        }
                        
                        SVProgressHUD.dismiss()
                        finish(result)
                        
                    } catch {
                        SVProgressHUD.dismiss()
                        finish(result)
                    }
                    
                } else {
                     
                    SVProgressHUD.dismiss()
                    print("Resonse.error",response.error?.localizedDescription as Any)
                    finish(result)
                }
            }
        }

    
    //MARK: -  Alert Function
    
    func showAlert( _ title:String, _ message:String) {
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "CustomDefaultAlertViewController") as? CustomDefaultAlertViewController else {return}
        vc.setTransition()
        vc.titleVal = title
        vc.messageVal = message
        vc.completionHandler = { [weak self] in
            self?.dismiss(animated: true) {
                self?.pressed()
            }
        }
        self.present(vc, animated: true, completion: nil)
        
    }
    
    func pressed() {
        
        let vc = EmailVerificationViewController.instantiateFromAppStoryboard(appStoryboard: .main)
        
    //    vc.commingFromScreen = "Signup"
     //   vc.fromVC = fromVC
       // vc.otp = UserDefaults.standard.value(forKey: "SAVEOTP") as? Int ?? 0
        
        vc.idOtp = UserDefaults.standard.value(forKey: "SAVEID") as? String ?? ""  //self.otp
        vc.email = self.email
         vc.response = self.responseData
        print("self.responseData========>>>>>>  \(self.responseData)")
        self.pushViewController(viewController: vc)
        
    }
    
}

/*
extension SignUpDetail_VC: CLLocationManagerDelegate {
    
    func getCurrentLocation() {
        
//        locationManager.delegate = self
//         locationManager.requestWhenInUseAuthorization()
//         locationManager.desiredAccuracy = kCLLocationAccuracyBest
//         locationManager.startUpdatingLocation()
//         locationManager.startMonitoringSignificantLocationChanges()
         // Here you can check whether you have allowed the permission or not.
//        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
//
//         }
        
        
          locationManager = CLLocationManager()
           locationManager.delegate = self
           locationManager.desiredAccuracy = kCLLocationAccuracyBest
           locationManager.requestAlwaysAuthorization()

           if CLLocationManager.locationServicesEnabled(){
               locationManager.startUpdatingLocation()
           }


    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {

        if CLLocationManager.locationServicesEnabled()
        {
            
 
            let authorizationStatus: CLAuthorizationStatus

            if #available(iOS 14, *) {
                authorizationStatus = self.locationManager.authorizationStatus
            } else {
                authorizationStatus = CLLocationManager.authorizationStatus()
            }
           
            switch authorizationStatus {
                
            case .authorizedAlways, .authorizedWhenInUse:
                
                print("Authorize.")
                
                if latLongStatus == "1" {
                    
                    latLongStatus = "0"
                    
                    let valuelat = UserDefaults.standard.value(forKey: "SAVELATITUDE") as? String ?? ""
                    let valuelong = UserDefaults.standard.value(forKey: "SAVELONGITUDE") as? String ?? ""
                    
                    currentLat = Double("\(valuelat)") ?? 0.0
                    currentlong = Double("\(valuelong)") ?? 0.0
                    
                    
                } else {
                    
                    
                    let setCurrentLat = self.locationManager.location?.coordinate.latitude
                    let setCurrentLong = self.locationManager.location?.coordinate.longitude
                    
                    currentLat = Double("\(setCurrentLat ?? 0.00)") ?? 0.00 //Double("\(String(describing: (self.locationManager.location?.coordinate.latitude)))") ?? 0.0
                    currentlong = Double("\(setCurrentLong ?? 0.00)") ?? 0.00//Double("\(String(describing: (self.locationManager.location?.coordinate.longitude)))") ?? 0.0
                    
                }
                
//                let latitude: CLLocationDegrees = (self.locationManager.location?.coordinate.latitude)!
//                let longitude: CLLocationDegrees = (self.locationManager.location?.coordinate.longitude)!
                
                let location = CLLocation(latitude: currentLat ?? 0.0, longitude: currentlong ?? 0.0) //changed!!!
                CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
                    if error != nil {
                        return
                    } else if let country = placemarks?.first?.country,
                        let city = placemarks?.first?.locality {
                        
                        let address = placemarks?.first?.name ?? ""
                        let state = placemarks?.first?.subAdministrativeArea ?? ""
                        
                        let countryCode = placemarks?.first?.isoCountryCode ?? ""
                        let postCode = placemarks?.first?.postalCode ?? ""
                       
                        print(country)
                        self.cityNameStr = "\(address),\(city),\(state)"
                        
                        self.addressTxtField.text = "\(address),\(city),\(state)"
                        self.countryCodeTxtField.text = countryCode
                        self.cityTxtField.text = city
                        self.zipcodeTxtField.text = postCode
                        
                      //  self.tbleView.reloadData()
                    }
                    else {
                        
                    }
                })
                
                break

            case .notDetermined:
                print("Not determined.")
//                self.showAlertMessage(messageTitle: "AAC", withMessage: "Location service is disabled!!")
                break

            case .restricted:
                print("Restricted.")
//                self.showAlertMessage(messageTitle: "AAC", withMessage: "Location service is disabled!!")
                break
            
            case .denied:
                print("Denied.")
           
            }
            }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

            if locations.first != nil {
                print("location:: (location)")
                                
            }

        }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
             print("error:: \(error.localizedDescription)")
        }
    
    
    func showAlertMessage(messageTitle: NSString, withMessage: NSString) ->Void  {
        let alertController = UIAlertController(title: messageTitle as String, message: withMessage as String, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in

        }
        
        alertController.addAction(cancelAction)

        let OKAction = UIAlertAction(title: "Settings", style: .default) { (action:UIAlertAction!) in
            if let url = URL(string: "App-Prefs:root=Privacy&path=LOCATION/com.company.AppName") {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                    
                }
            }
        }
        
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion:nil)
    }
    
}
 */

extension SignUpDetail_VC {
    
   
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        
        if textField == zipcodeTxtField {
            latLongStatus = "1"
            
            postalCodeSearchNew_Api()
            
           // getCurrentLocation()
          //  postalCodeSearch_Api()

//            if CLLocationManager.locationServicesEnabled() {
//                locationManager.startUpdatingLocation() // start location manager
//
//            }

       }
    }
    
    
//    func textFieldShouldReturn(textField: UITextField) -> Bool {
//
//        print("TextField should return method called")
//        if textField == self.occuptionProfessionTxtField {
//            if self.occuptionProfessionTxtField.text == "Retired" {
//                textField.resignFirstResponder();
//                return false;
//
//            } else {
//
//
//            }
//        }
//        textField.resignFirstResponder();
//        return true;
//    }
    
    

    
    func postalCodeSearch_Api() {
        
        SVProgressHUD.show()
        
        DataService.sharedInstance.postalCodeSearch(postCode: zipcodeTxtField.text!) { (resultDict, errorMsg) in
            
            SVProgressHUD.dismiss()

            print(resultDict as Any)

            if errorMsg == nil {

                if (((resultDict!["result"] as? NSDictionary)?.isKind(of: NSDictionary.self)) != nil) {
                    
                    if ((resultDict!["result"] as? NSDictionary)!.value(forKey: self.zipcodeTxtField.text ?? "") as! NSArray).count > 1  {
                        
                        AlertView.simpleViewAlert(title: "", "Invalid Post Code", self)
                        self.countryCodeTxtField.text = ""
                        self.cityTxtField.text = ""
                        self.addressTxtField.text = ""
                        
                    } else {
                        
                        let allPostCodeGet = ((resultDict!["result"] as? NSDictionary)!.value(forKey: self.zipcodeTxtField.text ?? "") as! NSArray)
                    
                        for saveData in allPostCodeGet {
                            
                            let city = ((saveData as! NSDictionary).value(forKey: "city"))
                            let country_code = ((saveData as! NSDictionary).value(forKey: "country_code"))
                            
                            let lat = ((saveData as! NSDictionary).value(forKey: "latitude"))
                            
                            let long = ((saveData as! NSDictionary).value(forKey: "longitude"))
                            
                            
                            UserDefaults.standard.setValue(lat, forKey: "SAVELATITUDE")
                            
                            UserDefaults.standard.setValue(long, forKey: "SAVELONGITUDE")
                            
                            self.countryCodeTxtField.text = country_code as? String ?? ""
                            self.cityTxtField.text = city as? String ?? ""
                            
                            
                        }
                    }
                    
                    
                } else {
                    
                    AlertView.simpleViewAlert(title: "", "Invalid Post Code", self)
                    self.countryCodeTxtField.text = ""
                    self.cityTxtField.text = ""
                    self.addressTxtField.text = ""

                }
                

            } else {
                Toast.show(message: "Something went wrong", controller: self)
            }
        }
    }
    

    func postalCodeSearchNew_Api() {
        
        SVProgressHUD.show()
        
        DataService.sharedInstance.postalCodeSearchNew(postCode: zipcodeTxtField.text!) { (resultDict, errorMsg) in
            
            SVProgressHUD.dismiss()

            print(resultDict as Any)

            if errorMsg == nil {
                
                self.allPostCodeData1.removeAll()
                self.arrPostCodeAddress.removeAll()
         
                
                if let result = resultDict?["result"] as? NSDictionary {
                    
                    print("result===>>>> \(result)")
                    
                    if let addresses = result["addresses"] as? [NSDictionary] {
                        
                        
                        print("addresses===>>>> \(addresses)")
                        for i in 0..<addresses.count {
                            
                            let data = addresses[i]
                            
                            let line_1 = data["line_1"] as? String ?? ""
                            let line_2 = data["line_2"] as? String ?? ""
                            let line_3 = data["line_3"] as? String ?? ""
                            let line_4 = data["line_4"] as? String ?? ""
                            let locality = data["locality"] as? String ?? ""
                            let town_or_city = data["town_or_city"] as? String ?? ""
                            let county = data["county"] as? String ?? ""
                            let country = data["country"] as? String ?? ""
                            
                            var allAddress = ""
                            
                            
                            if line_1 != "" {
                                allAddress += line_1 + ", "
                            }

                            if line_2 != "" {
                                allAddress += line_2 + ", "
                            
                            }
                            if line_3 != "" {
                                allAddress += line_3 + ", "
                                
                            }
                            if line_4 != "" {
                                allAddress += line_4 + ", "
                                
                            }
                            
                            allAddress += "\(locality + ", " + town_or_city + ", " + county  )"
                            
                            self.allPostCodeData1.append(allPostCodeData.init(line_1: allAddress, town_or_city: town_or_city, country: country))
                            
                        }
                        
                        
                        let count = self.allPostCodeData1.count
                        for i in 0..<count {
                            let a = self.allPostCodeData1[i].line_1
                            self.arrPostCodeAddress.append(a)
 
                        }
                        
                        if self.clickDropDown == "3"{
                          
                            self.setupCommonDropDown()
                            self.commonDropDown.show()
                            
                        }
                       
                        
                        print("allPostCodeData1===>>>> \(self.allPostCodeData1)")
                        
                        print("allPostCodeData1===>>>> \(self.allPostCodeData1.count)")
                        
                    }
                
                }
                
                
            } else {
                Toast.show(message: "Something went wrong", controller: self)
            }
        }
    }
    
    
    
    
}
 
