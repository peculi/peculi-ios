//
//  LeftSideMenuViewController.swift
//  Peculi
//
//  Created by apple on 31/10/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit
import SideMenu
//import Netverify


protocol LogoutTapped {
    func actionBtn(flag: Int)
}

class LeftSideMenuViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var crossButton: UIButton!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var editProfileButton: UIButton!
    @IBOutlet weak var sideMenuTableView: UITableView!
    
    
    //MARK: - Define Array
    var menuNameArray = ["Home", "Peculi Wallet", "My Pots", "Settings", "Transactions","Swaprifice", "Retailers", "Logout"] //Delete My Account",
    var menuIconArray = ["icHome", "icPeculiWallet-1", "icMyPosts-1", "icSettings-1", "Ic_transactions-1", "icPeculiWallet-1","icRetailer-1", "icLogout"] //,"deleteAccount"
    var logoutDelegate: LogoutTapped!
    
    
    //MARK: - ViewDidLoad
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        //        if UserDefaults.exists(key: "UserImage"){
        //            var originalString = String()
        //
        //            originalString = UserDefaults.standard.value(forKey: "UserImage") as! String
        //                   let ImageUrl = String(format: "%@%@", AppConfig.userProfileImageUrl,originalString)
        //
        //                   userImageView.sd_setImage(with: URL(string:ImageUrl), placeholderImage:UIImage(named: "placeholder_Img"), completed: { (image, error, cacheType, url) -> Void in
        //                                          if ((error) != nil) {
        //                                              // set the placeholder image here
        //                                           //cell.imgVw.image = #imageLiteral(resourceName: "doctorAvtar_Img")
        //                                          } else {
        //                                              // success ... use the image
        //                                          }
        //                                      })
        //
        //        }
        
//        userImageView.layer.masksToBounds = true
//        //        userImageView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
//        //        userImageView.layer.borderWidth = 2.0
//        userImageView.layer.cornerRadius = 8
//
//        if checkIfGuest() == true {
//
//            userNameLbl.text = "Guest User"
//
//        }
//        else {
//
//            var firstName = ""
//            var lastName = ""
//            if UserDefaults.exists(key: UserDefaultConstants.FirstName.rawValue){
//                firstName = UserDefaults.standard.value(forKey: UserDefaultConstants.UserID.rawValue) as! String
//            }
//            if UserDefaults.exists(key: UserDefaultConstants.LastName.rawValue) {
//
//                lastName = UserDefaults.standard.value(forKey: UserDefaultConstants.LastName.rawValue) as! String
//
//            }
//
//            userNameLbl.text = "\(firstName.capitalizingFirstLetter()) \(lastName.capitalizingFirstLetter())"
//        }
 
        
    }
    
    //MARK: - view will appear
    override func viewWillAppear(_ animated: Bool) {
 
        navigationController?.navigationBar.isHidden = true
        navigationController?.navigationBar.barStyle = .black
        
        userImageView.layer.masksToBounds = true
        //        userImageView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        //        userImageView.layer.borderWidth = 2.0
        userImageView.layer.cornerRadius = 8
        
        if checkIfGuest() == true {
            userNameLbl.text = "Guest User"
        }
        else {
            
            var firstName = ""
            var lastName = ""
            if UserDefaults.exists(key: UserDefaultConstants.FirstName.rawValue){
                firstName = UserDefaults.standard.value(forKey: UserDefaultConstants.FirstName.rawValue) as! String
            }
            if UserDefaults.exists(key: UserDefaultConstants.LastName.rawValue) {
                
                lastName = UserDefaults.standard.value(forKey: UserDefaultConstants.LastName.rawValue) as! String
                
            }
            
            userNameLbl.text = "\(firstName.capitalizingFirstLetter()) \(lastName.capitalizingFirstLetter())"
        }

        
    }
    
    
    @IBAction func editProfileButtonAction(_ sender: Any) {
        
        if checkIfGuest() == true{
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpVc") as! SignUpVc
            vc.showGuestPopup = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        else{
            
//            let vc = EditProfileViewController.instantiateFromAppStoryboard(appStoryboard: .main)
//            self.pushViewController(viewController: vc)
            
            let vc = NewProfile_VC.instantiateFromAppStoryboard(appStoryboard: .main)
            self.pushViewController(viewController: vc)
            
        }
        
        
        
        
    }
    
    @IBAction func crossButtonAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}


//MARK: - UITableview Delegate & DataSource Method
extension LeftSideMenuViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuNameArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeftSideMenuTableViewCell") as! LeftSideMenuTableViewCell
        cell.iconNameLbl.text = menuNameArray[indexPath.row]
        cell.iconImageView.image = UIImage(named: menuIconArray[indexPath.row])
        
        if indexPath.row == 7{
            cell.iconNameLbl.textColor = #colorLiteral(red: 0.2431372549, green: 0.1921568627, blue: 0.337254902, alpha: 1)
            cell.iconImageView.isHidden = true
            if UserDefaults.exists(key: UserDefaultConstants.isguestLogin.rawValue){
                
                if UserDefaults.standard.bool(forKey: UserDefaultConstants.isguestLogin.rawValue) == true{
                    
                    cell.iconNameLbl.text = "Login / Register"
                    //                    cell.iconNameLbl.textColor = #colorLiteral(red: 1, green: 0.6588235294, blue: 0.231372549, alpha: 1)
                    
                }
 
            }
            
        }
        
        return cell
    }
    
    fileprivate func extractedFunc(_ indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            setRootHomeVC()
        case 1:
            PeculiWalletViewController.setRootVC()
        case 2:
            MyPotsViewController.setRootVC()
        case 3:
            SettingsViewController.setRootVC()
        case 4:
            TransactionsCardList_Vc.setRootVC()
//            if checkIfGuest() == true{
//                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpVc") as! SignUpVc
//                vc.showGuestPopup = true
//                self.navigationController?.pushViewController(vc, animated: true)
//
//            }
//            else{
//                AccountViewController.setRootVC()
//            }
            
        case 5:
            SwaprificeViewController.setRootVC()
        case 6:
            OurRetailersViewController.setRootVC()
      
        case 7:
            self.dismiss(animated: false) {
                self.logoutAction()
            }
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        extractedFunc(indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    
    func logoutAction(){
        
        if  checkIfGuest() == true {
            
            WelcomeScreenVc.setRootVC()
            
        }
        else {
            showlogoutPop()
            
        }
        
    }
    
    
    func deleteAction() {
        
        if  checkIfGuest() == true {
            
            WelcomeScreenVc.setRootVC()
            
        }
        else {
            showDeletePopUp()
        }
        
    }

    
    func showlogoutPop(){
        
        let sb = UIStoryboard(name: "Main", bundle:nil)
        let logAlert = sb.instantiateViewController(withIdentifier: "LogoutAlertViewController") as! LogoutAlertViewController
        let rootViewController = UIApplication.shared.keyWindow!.rootViewController!
        logAlert.setTransition()
        logAlert.messageLblData = "Are you sure you want to Logout?"
        logAlert.titleLblData = "Message"
        //                        logAlert.from = "logout"
        logAlert.okCompletionHandler = { [weak self] in
            self?.clearUserDefault()
        }
        
        logAlert.cancelCompletionHandler = {
            
            rootViewController.dismiss(animated: true, completion: nil)
        }

        DispatchQueue.main.async {
            rootViewController.present(logAlert, animated: true, completion: nil)
        }
          
    }

    
    func showDeletePopUp(){
        
        let sb = UIStoryboard(name: "Main", bundle:nil)
        let logAlert = sb.instantiateViewController(withIdentifier: "LogoutAlertViewController") as! LogoutAlertViewController
        let rootViewController = UIApplication.shared.keyWindow!.rootViewController!
        logAlert.setTransition()
        logAlert.messageLblData = "Are you sure you want to Delete your account?"
        logAlert.titleLblData = "Message"
        //                        logAlert.from = "logout"
        logAlert.okCompletionHandler = { [weak self] in
            self?.clearUserDefault()
        }
        
        logAlert.cancelCompletionHandler = {
            
            rootViewController.dismiss(animated: true, completion: nil)
        }
        
        DispatchQueue.main.async {
            
            rootViewController.present(logAlert, animated: true, completion: nil)
            
        }
        
    }
    
    
    
}



