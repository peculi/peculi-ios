//
//  DltMyAccountPopUp_VC.swift
//  Peculi
//
//  Created by Mohit Rana on 20/07/22.
//  Copyright © 2022 Stealth Tech. All rights reserved.
//

import UIKit

class DltMyAccountPopUp_VC: UIViewController {
    
    // MARK: - OUTLETS
   
    @IBOutlet weak var popUpChildView: UIView!
    @IBOutlet var popUpSuperView: UIView!
    
    @IBOutlet weak var popUpHeight: NSLayoutConstraint!
    @IBOutlet weak var btnAgree: UIButton!
    
    //MARK: - declare Variables
        
        var okCompletionHandler: (() -> Void)?
    var hideForButton = ""
    
    // MARK: - VIEW LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupView()
        animateView()
        
        if hideForButton == "0" {
            btnAgree.isHidden = false
            popUpHeight.constant = 320
        } else {
            btnAgree.isHidden = true
            popUpHeight.constant = 280
        }
    }
    
    
    // MARK: - FUNCTION
    
    /**** UI setup */
    func setupView(){
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        popUpSuperView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        
    }
     
     ///animation setup
     func animateView() {
         popUpChildView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
         UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
             self.popUpChildView.transform = .identity
         }, completion: {(finished: Bool) -> Void in
             
         })
         
     }
    
    
    // MARK: - BUTTON ACTION
    
    @IBAction func btnCancel_Action(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        print("call cancel ")
    }

    
    @IBAction func btnContinue_Action(_ sender: Any) {
        

        
        self.okCompletionHandler?()
        
        self.dismiss(animated: true, completion: nil)
        
        print("call function")
      
        
    }

}
