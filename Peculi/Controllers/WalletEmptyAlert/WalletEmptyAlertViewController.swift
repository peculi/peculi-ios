//
//  WalletEmptyAlertViewController.swift
//  Peculi
//
//  Created by iApp on 08/05/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import UIKit

class WalletEmptyAlertViewController: UIViewController {
    
    //MARK:- outlets
    @IBOutlet var superView: UIView!
    @IBOutlet weak var alertImageView: UIImageView!
    @IBOutlet weak var alertTitleLbl: UILabel!
    @IBOutlet weak var okBtn: UIButton!
    
    //MARK:- variables
    var messageData = ""
    var potImage = ""
    var potName = ""
    var categoryId = ""
    var okCompletionHandler: (()->Void)?
    
    //MARK:- view did load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        superView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        
        if messageData == ""{
            let textString = NSMutableAttributedString(string: "You do not have enough money to buy this coupon, please add more money in your pot.")
            textString.setColorForText("add more", with: #colorLiteral(red: 1, green: 0.6588235294, blue: 0.231372549, alpha: 1))
            alertTitleLbl.attributedText = textString
        }
        else{
            alertTitleLbl.text = messageData
        }
        
    }
    
    //MARK:- button action
    @IBAction func okBtnAction(_ sender: Any) {
        
        self.dismiss(animated: false) {
            self.okCompletionHandler?()
        }
        
    }
    
    
}
