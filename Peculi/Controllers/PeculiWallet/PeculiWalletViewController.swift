//
//  PeculiWalletViewController.swift
//  Peculi
//
//  Created by apple on 31/10/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit
import SideMenu
import SVProgressHUD
class PeculiWalletViewController: UIViewController {
    
    //MARK: - Outltes
    @IBOutlet weak var globalBalanceView: UIView!
    @IBOutlet weak var globalPriceLbl: UILabel!
    @IBOutlet weak var walletTableView: UITableView!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var navigationSideMenuButton: UIButton!
    @IBOutlet weak var navigationTitleLbl: UILabel!
    
    
    
    //MARK: - Define Array
    
    var walletSection = [""]
    var walletData = [WalletHistory]()
    var product_Data = [productData]()
    
    //MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        walletTableView.register(UINib(nibName: "EmptyDataTableViewCell", bundle: nil), forCellReuseIdentifier: "EmptyDataTableViewCell")
        
        if let globalBal = UserDefaults.standard.string(forKey: UserDefaultConstants.GlobalBalance.rawValue){
            self.globalPriceLbl.text = "\(GlobalVariables.currency)\(globalBal)"
        }
        
        getRedeemVoucherHistory()
        
    }
    
    //MARK: - ViewWillAppear
    
    override func viewWillAppear(_ animated: Bool) {
        navigationSetup()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0) {
            self.globalBalanceView.addDashedBorder(conerRadius: 8)
            // self.historyBtn.addDashedBorder(conerRadius: 30)
        }
    }
    
     
    //MARK: - Navigation Setup
    
    func navigationSetup(){
        navigationController?.navigationBar.isHidden = true
        navigationController?.navigationBar.barStyle = .black
        navigationTitleLbl.text = "Peculi Wallet"
        
        navigationView.layer.masksToBounds = false
        navigationView.layer.shadowColor = UIColor.clear.cgColor //#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        navigationView.layer.shadowOpacity = 0.6
        navigationView.layer.shadowOffset = CGSize(width: 0, height: 3)
        navigationView.layer.shadowRadius = 3.0
        
        // MARK: - Navigation LeftBar Button:-
        
        let leftBarButton = UIBarButtonItem(image: UIImage(named: "icSideMenu"), style: .done, target: self, action: #selector(icMenuButtonAction))
        self.navigationItem.leftBarButtonItem  = leftBarButton
    }
    
    // MARK: - Object Function Button Action:-
    
    @objc func icMenuButtonAction(){
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "menuLeftNavigationController") as! SideMenuNavigationController
        nextViewController.leftSide = true
        nextViewController.menuWidth = UIScreen.main.bounds.size.width - 100
        
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    @IBAction func navigationSideMenuButton(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "menuLeftNavigationController") as! SideMenuNavigationController
        nextViewController.leftSide = true
        nextViewController.menuWidth = UIScreen.main.bounds.size.width - 100
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    @objc func addnewPotAction(sender: UIButton)
    {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddPotViewController") as! AddPotViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}

//MARK: - tableview delegates

extension PeculiWalletViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return walletSection[section]
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
            return walletSection.count
        
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        view.tintColor = #colorLiteral(red: 0.07450980392, green: 0.1019607843, blue: 0.1529411765, alpha: 1)
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        header.textLabel?.textAlignment = .center
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        } else {
            return 30
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if walletData.count == 0 {
            return 1
        }
        else {
        return walletData.count
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if walletData.count == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyDataTableViewCell") as! EmptyDataTableViewCell
            cell.addNewPot.addTarget(self, action: #selector(addnewPotAction(sender:)), for: .touchUpInside)
            
            return cell
        }
        else {

            let cell = tableView.dequeueReusableCell(withIdentifier: "PeculiWalletTableViewCell") as! PeculiWalletTableViewCell
            cell.blackShadowEffectView.isHidden = true
            let amountDoubleValue = (walletData[indexPath.row].amount as NSString).doubleValue
            cell.cellAmount.text = String(format: "%@%@", GlobalVariables.currency,amountDoubleValue.format())
            cell.cellDescription.text = walletData[indexPath.row].product_Data[indexPath.row].description
            cell.nameLbl.text = String(format: "%@ %@", walletData[indexPath.row].categoryDetails.new_name.capitalizingFirstLetter(), "Pot")
            cell.nameLbl.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2)
//            cell.nameLbl.sizeToFit()
            cell.nameWidth.constant = cell.cellSideColorView.frame.height
            let images = walletData[indexPath.row].product_Data[indexPath.row].logo_image_url
            

            if images != "" {
                cell.cellImageView.sd_setImage(with: URL(string: images ), placeholderImage:UIImage(named: ""), completed: { (image, error, cacheType, url) -> Void in
                    if ((error) != nil) {
                        // set the placeholder image here
                        //cell.imgVw.image = #imageLiteral(resourceName: "doctorAvtar_Img")
                    } else {
                        // success ... use the image
                    }
                })
            }
            else {
                cell.cellImageView.image = UIImage(named: "")
            }

            return cell
            
//        }
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if walletData.count > 0 {
            
            
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VoucherDetailsViewController") as! VoucherDetailsViewController
            
        vc.logo_image_url = walletData[indexPath.row].product_Data[indexPath.row].logo_image_url
        vc.cardImageURL = walletData[indexPath.row].product_Data[indexPath.row].cardImageURL
        vc.name = walletData[indexPath.row].product_Data[indexPath.row].name
        vc.amount = (walletData[indexPath.row].amount as NSString).doubleValue.format()
        vc.expiryDate = walletData[indexPath.row].expiryDate
        vc.voucherUrl =  walletData[indexPath.row].voucherUrl
        vc.redeemInstructionsHTML = walletData[indexPath.row].product_Data[indexPath.row].redeemInstructionsHTML
        vc.termsAndConditionsHTML = walletData[indexPath.row].product_Data[indexPath.row].termsAndConditionsHTML
           
        self.navigationController?.pushViewController(vc, animated: true)
            
    }
      
    }
    
//  func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//
//
//        if editingStyle == .delete {
//
//            tableView.deleteRows(at: [indexPath,], with: .fade)
//
//        } else if editingStyle == .insert {
//
//
//        }
//
//    }

    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
          let rightAction = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
              print("rightAction tapped")
              let id = self.walletData[indexPath.row].voucherId
              self.swipeDelete_Action(voucherId1:id)
              success(true)
          })

        
        
          rightAction.image = UIImage(named: "swipeDlt")
          rightAction.backgroundColor = #colorLiteral(red: 0.2203302979, green: 0.1600231826, blue: 0.3218392134, alpha: 1) //UIColor.clear

          return UISwipeActionsConfiguration(actions: [rightAction])
      }

  //}
    
    
    
    func swipeDelete_Action(voucherId1:String) {
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "swipeDlt_Popup_VC") as? swipeDlt_Popup_VC else {
            return
        }
        
        vc.setTransition()
        vc.titleLblData = ""
         vc.voucherId = voucherId1
        vc.messageLblData = "Please make sure this voucher has been fully redeemed before deleting."
        vc.okCompletionHandler = { [weak self] in
            
            self?.dismiss(animated: true) {
                
                self?.getRedeemVoucherHistory()
                
            }
            
        }
        vc.cancelCompletionHandler = { [weak self] in
            
            self?.dismiss(animated: true, completion: nil)
        }
        
        self.present(vc, animated: true, completion: nil)
        
    }
    
    
}

///API calls


extension PeculiWalletViewController {
    
    func getRedeemVoucherHistory() {
        SVProgressHUD.show()
        DataService.sharedInstance.redeemVoucherHistory() { (resultDict, errorMsg) in
            SVProgressHUD.dismiss()
            print(resultDict as Any)
            if errorMsg == nil {
                if let result = resultDict?["result"] as? [NSDictionary] {
                    
                    print(result)
                   
                    
                    for index in result {
                        
                        let voucherId = index["_id"] as? String
                        let amount = index["amount"] as? String
                        let expiry_date = index["expiry_date"] as? String
                        let voucherUrl = index["voucherUrl"] as? String
                        
                        let  product_details = index["product_details"] as? [String:Any]
                        
                        let name = product_details?["name"] as? String
                        let logo_image_url = product_details?["logo_image_url"] as? String
                        let description = product_details?["description"] as? String
                        let card_image_url = product_details?["card_image_url"] as? String
                        let code = product_details?["code"] as? String
                        let currency_code = product_details?["currency_code"] as? String
                        let denomination_type = product_details?["denomination_type"] as? String
                        let expiry_date_policy = product_details?["expiry_date_policy"] as? String
                        let expiry_in_months = product_details?["expiry_in_months"] as? String
                        let maximum_value = product_details?["maximum_value"] as? String
                        let minValue = product_details?["minValue"] as? String
                        let percent_discount = product_details?["percent_discount"] as? String
                        let redeem_instructions_html = product_details?["redeem_instructions_html"] as? String
                        let terms_and_conditions_html = product_details?["terms_and_conditions_html"] as? String
                        
                        let categoryDetails = index["category_details"] as? [String: Any]
                        
                        let categoryID = categoryDetails?["_id"] as? String
                        let categoryName = categoryDetails?["name"] as? String
                        let categoryNewName = categoryDetails?["new_name"] as? String
                        
                        self.product_Data.append(productData.init(name: name ?? "", logo_image_url: logo_image_url ?? "", description: description ?? "", cardImageURL: card_image_url ?? "", code: code ?? "", currencyCode: currency_code ?? "", denomenationType: denomination_type ?? "", expiryPolicy: expiry_date_policy ?? "", expiryMonths: expiry_in_months ?? "", maxValue: maximum_value ?? "", minValue: minValue ?? "", percentDiscount: percent_discount ?? "", redeemInstructionsHTML: redeem_instructions_html ?? "", termsAndConditionsHTML: terms_and_conditions_html ?? ""))
                        
                        
                        self.walletData.append(WalletHistory.init(voucherId: voucherId ?? "", amount: amount ?? "", expiryDate: expiry_date ?? "", voucherUrl: voucherUrl ?? "", product_Data: self.product_Data, categoryData: WalletCategoryDetails.init(_id: categoryID ?? "", name: categoryName ?? "", newName: categoryNewName ?? "")))
                        
                    }
                    self.walletTableView.reloadData()
                    
                } else {
                    
                }
            } else {
                // Toast.show(message: "Something went wrong", controller: self)
            }
            
            
        }
    }
}




