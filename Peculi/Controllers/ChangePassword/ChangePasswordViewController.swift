//
//  ChangePasswordViewController.swift
//  Peculi
//
//  Created by apple on 02/11/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit
import SVProgressHUD

class ChangePasswordViewController: UIViewController {
    
    
    //MARK: - Outlet
    @IBOutlet weak var oldPasswordTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var oldPasswordButton: UIButton!
    @IBOutlet weak var newPasswordButton: UIButton!
    @IBOutlet weak var confirmPasswordButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var navView: UIView!
    
    
    
    //MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: - ViewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        navigartionSetup()
        UISetup()
    }
    
    
    func UISetup(){
        oldPasswordTextField.placeHolderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.4)
        newPasswordTextField.placeHolderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.4)
        confirmPasswordTextField.placeHolderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.4)
    }
    
    
    //MARK:- Navigation Setup
    func navigartionSetup(){
        
        navigationController?.navigationBar.isHidden = true
        
        navView.layer.masksToBounds = false
        navView.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        navView.layer.shadowOpacity = 0.6
        navView.layer.shadowOffset = CGSize(width: 0, height: 3)
        navView.layer.shadowRadius = 3.0
        
        
    }
    
    ///back button action
    @IBAction func backBtnAction(_ sender: Any) {
        popVc()
    }
    
    ///empty all texts
    func emptyAllTextfield(){
        oldPasswordTextField.text = ""
        newPasswordTextField.text = ""
        confirmPasswordTextField.text = ""
    }
    
    
    ///API call
//    func callChangePasswordApi(_ password : String,_ old_password : String, _ email : String){
//        ChangePasswordModel().fetchInfo(password,old_password,email){ (userinfo) in
//
//            print(userinfo)
//            if userinfo.count > 0{
//                if userinfo[0].code == 200{
//                    self.emptyAllTextfield()
//
//                    self.showAlert("Alert", userinfo[0].message)
//
//                }else{
//
//                    self.customAlertDismiss(title: "Alert", message: userinfo[0].message)
//                }
//
//            }
//            else {
//
//                self.customAlertDismiss(title: "Alert", message: "Something went wrong!")
//            }
//
//        }
//    }
    
    
    func callChangePasswordApi(_ password : String,_ old_password : String, _ email : String){
            
    
        SVProgressHUD.show()
    
        let params:[String: Any] = ["email": email,
                                    "old_password" : old_password,
                                    "password" : password ] //UserDefaults.standard.string(forKey: UserDefaultConstants.UserID.rawValue) ?? "" ]
    
    print(params)
        DataService.sharedInstance.postApiCall(endpoint: constantApis.ChangePasswordApi, params: params) { (response, error) in
    
            SVProgressHUD.dismiss()
    
            print(response)
    
            if response != nil {
                self.emptyAllTextfield()
                Toast.show(message: "Password Updated Successfully", controller: self)
               
    
            }
            else {
    
                self.customAlertDismiss(title: "Message", message: "Something went wrong!")
            }
    
        }
    
    }

    
    
    
    
    
    
    
    
    
    
    
    //MARK:- UIButton Action
    @IBAction func oldPasswordHideUnhideButtonAction(_ sender: Any) {
        if oldPasswordTextField.tag == 0 {
            oldPasswordTextField.tag = 1
            oldPasswordButton.setImage(UIImage(named: "ic_eye"), for: .normal)
            oldPasswordTextField.isSecureTextEntry = false
        }
        else {
            oldPasswordTextField.tag = 0
            oldPasswordButton.setImage(UIImage(named: "ic_eyeclose1"), for: .normal)
            oldPasswordTextField.isSecureTextEntry = true
        }
        
    }
    
    @IBAction func newPasswordHideUnhideButtonAction(_ sender: Any) {
        if newPasswordButton.tag == 0 {
            newPasswordButton.tag = 1
            newPasswordButton.setImage(UIImage(named: "ic_eye"), for: .normal)
            newPasswordTextField.isSecureTextEntry = false
        }
        else {
            newPasswordButton.tag = 0
            newPasswordButton.setImage(UIImage(named: "ic_eyeclose1"), for: .normal)
            newPasswordTextField.isSecureTextEntry = true
        }
    }
    
    @IBAction func confirmPasswordHideUnhideButtonAction(_ sender: Any) {
        if confirmPasswordButton.tag == 0 {
            confirmPasswordButton.tag = 1
            confirmPasswordButton.setImage(UIImage(named: "ic_eye"), for: .normal)
            confirmPasswordTextField.isSecureTextEntry = false
        }
        else {
            confirmPasswordButton.tag = 0
            confirmPasswordButton.setImage(UIImage(named: "ic_eyeclose1"), for: .normal)
            confirmPasswordTextField.isSecureTextEntry = true
        }
    }
    
    
    @IBAction func saveButtonAction(_ sender: Any) {
        if UserDefaults.exists(key: "UserEmail"){
            
            if oldPasswordTextField.text?.count == 0 {
                AlertView.simpleViewAlert(title: "", "Please enter the old password", self)
                return
            }
            
            else if newPasswordTextField.text?.count == 0 {
                AlertView.simpleViewAlert(title: "", "Please enter the password", self)
                return
            }
            else  if confirmPasswordTextField.text?.count == 0 {
                AlertView.simpleViewAlert(title: "", "Please re - enter the password", self)
                return
            }
            else if !(newPasswordTextField.text?.isValidPassword())!{
                
                AlertView.simpleViewAlert(title: "", "Minimum Password length is 8 character includes a lower case letter , an upper case letter, a special character and no whitespace allowed. e.g (Peculi@1234!!)", self)
                
            }
            else  if newPasswordTextField.text != confirmPasswordTextField.text {
                AlertView.simpleViewAlert(title: "", "Password and confirm password doesn't match!", self)
                return
            }
            
            else {
                
                callChangePasswordApi(newPasswordTextField.text ?? "",oldPasswordTextField.text ?? "",UserDefaults.standard.value(forKey: UserDefaultConstants.UserEmail.rawValue) as! String)
            }
            
        }
        
    }
    
    
    //MARK:-   Alert Function
    
    func showAlert( _ title:String, _ message:String  ){
        
        //            let myAlert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert);
        //            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:  { action in
        //                self.pressed()})
        //            myAlert.addAction(okAction)
        //            self.present(myAlert, animated: true, completion: nil)
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "CustomDefaultAlertViewController") as? CustomDefaultAlertViewController else {
            return
        }
        vc.setTransition()
        vc.titleVal = title
        vc.messageVal = message
        vc.completionHandler = { [weak self] in
            
            self?.dismiss(animated: true) {
                self?.pressed()
            }
            
        }
        
        self.present(vc, animated: true, completion: nil)
        
        
        
    }
    
    
    
    
    func pressed()
    {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
        guard let signVc = self.storyboard!.instantiateViewController(withIdentifier: "SignInVc") as? SignInVc else {
            return
        }
        guard let welcomeVc = self.storyboard!.instantiateViewController(withIdentifier: "WelcomeScreenVc") as? WelcomeScreenVc else {
            return
        }

        
        self.navigationController?.setViewControllers([welcomeVc, signVc], animated: true)
    }
    
    
    
    
    
    
    
}




//func resendOTPApiCall() {
//
//    SVProgressHUD.show()
//
//    let params:[String: Any] = ["email": self.email,
//                                "id" : idOtp ] //UserDefaults.standard.string(forKey: UserDefaultConstants.UserID.rawValue) ?? "" ]
//
//print(params)
//    DataService.sharedInstance.postApiCall(endpoint: constantApis.resendOTP, params: params) { (response, error) in
//
//        SVProgressHUD.dismiss()
//
//        print(response)
//
//        if response != nil {
//
//            Toast.show(message: "OTP sent on your email", controller: self)
//            self.clearTextFields()
//
//        }
//        else {
//
//            self.customAlertDismiss(title: "Message", message: "Something went wrong!")
//        }
//
//    }
//
//}
