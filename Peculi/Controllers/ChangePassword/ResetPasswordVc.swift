//
//  ResetPasswordVc.swift
//  Peculi
//
//  Created by Stealth on 18/11/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit
import SVProgressHUD

class ResetPasswordVc: UIViewController {
    
    //MARK: - Outlets
    
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var newPasswordButton: UIButton!
    @IBOutlet weak var confirmPasswordButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    var email = String()
    
    //MARK: - view did load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    //MARK: - ViewWillAppear
    
    override func viewWillAppear(_ animated: Bool) {
        navigartionSetup()
        UISetup()
    }
    
    /**** Setup UI */
    
    func UISetup(){
        
        newPasswordTextField.placeHolderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.4)
        confirmPasswordTextField.placeHolderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.4)
    }
    

    /**** Empty textfeilds */
    
    func emptyAllTextfield() {
        newPasswordTextField.text = ""
        confirmPasswordTextField.text = ""
    }
    
    
    
    func popToSpecificController(){
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: WelcomeScreenVc.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
 
    //MARK: - Navigation Setup
    
    
    func navigartionSetup(){
        navigationController?.navigationBar.isHidden = false
        title = "Reset Password"
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.07450980392, green: 0.1019607843, blue: 0.1529411765, alpha: 1)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.6
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.navigationController?.navigationBar.layer.shadowRadius = 3.0
        
        //MARK: - Navigation LeftBar Button:-
        
        let leftBarButton = UIBarButtonItem(image: UIImage(named: "icBackArrowWhite"), style: .done, target: self, action: #selector(icBackButtonAction))
        self.navigationItem.leftBarButtonItem  = leftBarButton
        
    }
    
    //MARK: - Object Function Button Action
    
    @objc func icBackButtonAction(){
        self.navigationController?.popViewController(animated: true)
    }

    
    @IBAction func newPasswordHideUnhideButtonAction(_ sender: Any) {
        if newPasswordButton.tag == 0 {
            newPasswordButton.tag = 1
            newPasswordButton.setImage(UIImage(named: "ic_eye"), for: .normal)
            newPasswordTextField.isSecureTextEntry = false
        }
        else {
            newPasswordButton.tag = 0
            newPasswordButton.setImage(UIImage(named: "ic_eyeclose1"), for: .normal)
            newPasswordTextField.isSecureTextEntry = true
        }
    }
    
    @IBAction func confirmPasswordHideUnhideButtonAction(_ sender: Any) {
        
        if confirmPasswordButton.tag == 0 {
            confirmPasswordButton.tag = 1
            confirmPasswordButton.setImage(UIImage(named: "ic_eye"), for: .normal)
            confirmPasswordTextField.isSecureTextEntry = false
        }
        else {
            confirmPasswordButton.tag = 0
            confirmPasswordButton.setImage(UIImage(named: "ic_eyeclose1"), for: .normal)
            confirmPasswordTextField.isSecureTextEntry = true
        }
    }
    
    
    @IBAction func saveButtonAction(_ sender: Any) {
        
        if newPasswordTextField.text?.count == 0 {
            AlertView.simpleViewAlert(title: "", "Please enter the password", self)
            return
        }
        else  if confirmPasswordTextField.text?.count == 0 {
            AlertView.simpleViewAlert(title: "", "Please re - enter the password", self)
            return
        }
        else if !(self.newPasswordTextField.text?.isValidPassword())!{
            
            AlertView.simpleViewAlert(title: "", "Minimum Password length is 8 character includes a lower case letter , an upper case letter, a special character and no whitespace allowed. e.g (Peculi@1234!!)", self)
            
        }
        else  if newPasswordTextField.text != confirmPasswordTextField.text {
            AlertView.simpleViewAlert(title: "", "Password and confirm password doesn't match!", self)
            return
        }
        else {
            callResetPasswordApi(email,newPasswordTextField.text ?? "")
        }
        
        
        
    }
    
    
    //API call
    
    func callResetPasswordApi(_ email : String,_ password : String){
        
        SVProgressHUD.show()
        let params:[String: Any] = ["email": email,
                                    "password": password
                                    ]
        
        print(params)
        
        DataService.sharedInstance.postApiCall(endpoint: constantApis.ResetPasswordApi, params: params) { (response, error) in
            
            SVProgressHUD.dismiss()
            
            
            print(response)
            
            if response != nil {
                
                self.emptyAllTextfield()
                self.popToSpecificController()

            }
            else {
                self.customAlertDismiss(title: "Message", message: "Something went wrong!")
            }
            
        }
        
        
    }
    
    
    
    

//    func callResetPasswordApi(_ email : String,_ password : String){
//        ResetPasswordModel().fetchInfo(email,password){ (userinfo) in
//
//            print(userinfo)
//            if userinfo.count > 0{
//                if userinfo[0].statuscheck == "success" {
//                    self.emptyAllTextfield()
//                    self.popToSpecificController()
//
//
//                } else {
//
//                    AlertView.simpleViewAlert(title: "", userinfo[0].message, self)
//                }
//
//            }
//            else{
//
//                AlertView.simpleViewAlert(title: "", userinfo[0].message, self)
//            }
//
//        }
//    }
}





