//
//  TermsAndConditionsViewController.swift
//  Peculi
//
//  Created by iApp on 23/03/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import UIKit
import WebKit

class TermsAndConditionsViewController: UIViewController, WKNavigationDelegate {
    
    //MARK:- outlets
    @IBOutlet weak var termsLbl: UITextView!
    @IBOutlet weak var termsWebView: WKWebView!
    
    //MARK:- variables
    var data = String()
    
    //MARK:- view did load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.termsWebView.navigationDelegate = self
        self.termsWebView.isOpaque = false
        self.termsWebView.backgroundColor = #colorLiteral(red: 0.07450980392, green: 0.1019607843, blue: 0.1529411765, alpha: 1)
        
        let HTMLString = "<html><body style='font-family:Montserrat-Regular;font-size:40px; color:#ffffff'><style>a:link { color: #0f83d9; }</style>\(data)</body></html>"
        termsWebView.loadHTMLString(HTMLString, baseURL: nil)
        
    }
    
    
    
}

