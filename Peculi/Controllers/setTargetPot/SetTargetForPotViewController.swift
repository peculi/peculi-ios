//
//  SetTargetForPotViewController.swift
//  Peculi
//
//  Created by iApp on 08/06/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import UIKit
import SideMenu
import DropDown


class SetTargetForPotViewController: UIViewController, UITextFieldDelegate {
    
    //MARK: - Outlets
    
    @IBOutlet var superView: UIView!
    @IBOutlet weak var superChildView: UIView!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var globalBalView: UIView!
    @IBOutlet weak var globalBalLbl: UILabel!
    @IBOutlet weak var globalBalChildView: UIView!
    @IBOutlet weak var potImage: UIImageView!
    @IBOutlet weak var potNameLbl: UILabel!
    @IBOutlet weak var potNameTitleLbl: UILabel!
    @IBOutlet weak var targetTextField: UITextField!
    @IBOutlet weak var weeklyTargetTextField: UITextField!
    @IBOutlet weak var saveAmountTxtField: UITextField!
    
    @IBOutlet weak var DropDownView: UIView!

    @IBOutlet weak var btnSelectTarget: UIButton!
    @IBOutlet weak var btnDepositFrequency: UIButton!
    @IBOutlet weak var btnAmountPerSave: UIButton!
    
    @IBOutlet weak var imgStopTarget: UIImageView!
    @IBOutlet weak var imgContinueTarget: UIImageView!
    @IBOutlet weak var myTableView: UITableView!
    
    @IBOutlet weak var myTblViewHeight: NSLayoutConstraint!
    
 

    //MARK: - Variables
    
    var from = AllControllers.none
    var categoryInfo = AllCategoriesInfo()
    var addNewTargetData = GetCategoryInfoByIdModel()
    var categoryId = String()
    var potName = String()
    var dropdwnTarget = DropDown()
    
    var selectedRows:[IndexPath] = []
    var getCardModel = [GetCArd_ModelStructure]()
    var TapCell = false
    var cardId = ""
    var cardName = ""
    var cardSelectId = ""
    var targetStatus = true
    
    var allTargetAmount = [String]()
    var clickDropDown = "0"
    var setDepositFrequency = [String]()
    var selectAmountSave = [String]()
    var catIdForUpdateTransctonStatus = ""
    
    var monthlyLimit = UserDefaults.standard.value(forKey: UserDefaultConstants.monthlyLimit.rawValue) ?? "0"
    var dayLimit = UserDefaults.standard.value(forKey: UserDefaultConstants.dayLimit.rawValue) ?? "0"
    var yearlyLimit = UserDefaults.standard.value(forKey: UserDefaultConstants.yearlyLimit.rawValue) ?? "0"
    var maxLoad = UserDefaults.standard.value(forKey: UserDefaultConstants.maxLoad.rawValue) ?? "0"
    var transactionCount = UserDefaults.standard.value(forKey: UserDefaultConstants.transactionCount.rawValue) ?? "0"
    var transactionLimit = UserDefaults.standard.value(forKey: UserDefaultConstants.transactionLimit.rawValue) ?? "0"
    var transactionSum = UserDefaults.standard.value(forKey: UserDefaultConstants.transactionSum.rawValue) ?? "0"
    
    
    
    //MARK: - view did load
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        self.setUpDropDown()
        self.SetTextFieldPlaceholder ()
       
        myTableView.delegate = self
        myTableView.dataSource = self
        
        imgStopTarget.image = UIImage.init(named: "Ic_UnselectCircle-1")
        imgContinueTarget.image = UIImage.init(named: "Ic_SelectCircle")
        targetStatus = true
        
        self.GetCard_Api()
        
        
        
     
    
        
       
    }
    
    //MARK: - view will appear
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = true
        navView.layer.masksToBounds = false
        navView.layer.shadowRadius = 3.0
        navView.layer.shadowOpacity = 0.6
        navView.layer.shadowColor = UIColor.clear.cgColor //#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        navView.layer.shadowOffset = CGSize(width: 0 , height: 3)
        
        if let globalBal = UserDefaults.standard.string(forKey: UserDefaultConstants.GlobalBalance.rawValue){
            self.globalBalLbl.text = "\(GlobalVariables.currency)\(globalBal)"
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0) {
            self.globalBalChildView.addDashedBorder(conerRadius: 8)
            // self.historyBtn.addDashedBorder(conerRadius: 30)
        }
        setValues()
    }
    
    func setUpDropDown() {
        
        clickDropDown = "0"
        allTargetAmount = ["50", "100", "150", "200", "250", "300", "350", "400", "450", "500"]
        setDepositFrequency = ["Weekly", "Monthly"]
       
        //self.ShowDropDown()
        
        DropDown.appearance().backgroundColor = #colorLiteral(red: 0.1846595407, green: 0.1497857869, blue: 0.2699982524, alpha: 1)    //UIColor.gray
        DropDown.appearance().selectionBackgroundColor = UIColor.black
        DropDown.appearance().textColor = UIColor.white
        DropDown.appearance().selectedTextColor = UIColor.white
        DropDown.appearance().cornerRadius = 4
        dropdwnTarget.direction = .bottom
      
        dropdwnTarget.shadowColor = UIColor.clear
        
    }
    
    func valueAppendArr() {
        
        if targetTextField.text == "50" {
            selectAmountSave = ["5", "10"]
            
        } else if targetTextField.text == "100" {
            
            selectAmountSave = ["5", "10", "20"]
            
        }  else if targetTextField.text == "150" {
            selectAmountSave = ["5", "10", "25"]
            
        } else if targetTextField.text == "200" {
            selectAmountSave = ["5", "10", "25"]
            
        } else  {

            selectAmountSave = ["5", "10", "25", "50"]
 
        }
    }

    
    func SetTextFieldPlaceholder () {
        
        targetTextField.delegate = self
        targetTextField.attributedPlaceholder = NSAttributedString(string: "Select Target",
                                                                   attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        weeklyTargetTextField.delegate = self
        weeklyTargetTextField.attributedPlaceholder = NSAttributedString(string: "Select Deposit Frequency",
                                                                         attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        
        
        saveAmountTxtField.delegate = self
        saveAmountTxtField.attributedPlaceholder = NSAttributedString(string: "Select Amount Per Save",
                                                                         attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        
    }
    
    /**** Assigning Values */
    func setValues() {
        
        if from == .AddPotVc {
            
            potNameLbl.text = String(format: "%@ Pot", categoryInfo.name.capitalizingFirstLetter())
            potNameTitleLbl.text = String(format: "Add %@ Pot Target", categoryInfo.name.capitalizingFirstLetter())
            var originalString = String()
            
            originalString = categoryInfo.image
            let ImageUrl = String(format: "%@%@", AppConfig.ImageUrl,originalString)
            potImage.sd_setImage(with: URL(string:ImageUrl), placeholderImage:UIImage(named: "placeholder_Img"), completed: { (image, error, cacheType, url) -> Void in
                if ((error) != nil) {
                    // set the placeholder image here
                    //cell.imgVw.image = #imageLiteral(resourceName: "doctorAvtar_Img")
                } else {
                    // success ... use the image
                }
            })
            categoryId = categoryInfo._id
        }
        else {
           
//            if addNewTargetData.userData.categoryName == ""{
//
//                potName = String(format: "%@ Pot",addNewTargetData.userData.categoryDetails.newName)
//
//            }
//            else {
//                potName = String(format: "%@ %@", addNewTargetData.userData.categoryName , "Pot")
//            }
            
            potNameLbl.text = potName.capitalizingFirstLetter()
            potNameTitleLbl.text = String(format: "Add %@ Target", potName.capitalizingFirstLetter())
            var originalString = String()
            originalString = addNewTargetData.userData.categoryDetails.image
            let ImageUrl = String(format: "%@%@", AppConfig.ImageUrl,originalString)
            potImage.sd_setImage(with: URL(string:ImageUrl), placeholderImage:UIImage(named: "placeholder_Img"), completed: { (image, error, cacheType, url) -> Void in
                if ((error) != nil) {
                    // set the placeholder image here
                    //cell.imgVw.image = #imageLiteral(resourceName: "doctorAvtar_Img")
                } else {
                    // success ... use the image
                }
            })
          //  categoryId = addNewTargetData.userData.categoryDetails._id
            
            
        }
    }
    
    
//    func ShowDropDown() {
//
//        let  dropDown = DropDown(frame: CGRect(x: 110, y: 140, width: 200, height: 30)) // set frame
//
//        // The list of array to display. Can be changed dynamically
//        dropDown.optionArray = ["Option 1", "Option 2", "Option 3"]
//        // Its Id Values and its optional
//        dropDown.optionIds = [1,23,54,22]
//        // Image Array its optional
//
//
//        // The the Closure returns Selected Index and String
//        dropDown.didSelect{(selectedText , index ,id) in
//       // self.valueLabel.text = "Selected String: \(selectedText) \n index: \(index)"
//            print("Selected String: \(selectedText) \n index: \(index)")
//            }
//        }
    
    
    //MARK: - Button actions
    @IBAction func sideMenuBtnAction(_ sender: Any) {
        
        popVc()
        
        
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "menuLeftNavigationController") as! SideMenuNavigationController
//        nextViewController.leftSide = true
//        nextViewController.menuWidth = UIScreen.main.bounds.size.width - 100
//        self.present(nextViewController, animated:true, completion:nil)
        
    }
    
    @IBAction func addpotBtnAction(_ sender: Any) {
        
        let setTargetDouble = Double(targetTextField.text ?? "") ?? 0.00//(targetTextField.text! as NSString).doubleValue
        //let targetAmount = (saveAmountTxtField.text! as NSString).doubleValue
        let saveAmountDouble = Double(saveAmountTxtField.text ?? "") ?? 0.00 //(weeklyTargetTextField.text! as NSString).doubleValue //weekTargetDouble
        
        
        
        
        //        var globalAmount = Double()
        //        if let globalBal = UserDefaults.standard.string(forKey: "GlobalBalance"){
        //            globalAmount = (globalBal as NSString).doubleValue
        //        }
        //        else {
        //            globalAmount = 0.00
        //        }
        //
        //        let totalAmount = weekTargetDouble + globalAmount
        
        let totalAmount = saveAmountDouble + (Double("\(transactionSum)") ?? 0.00) //weekTargetDouble + globalAmount
        
        
        if targetTextField.text == "" {
            AlertView.simpleViewAlert(title: "", "Please set the target", self)
        }
        else if  weeklyTargetTextField.text == "" {
            AlertView.simpleViewAlert(title: "", "please set the weekly target", self)
            
        } else if saveAmountTxtField.text == "" {
            
            AlertView.simpleViewAlert(title: "", "please set the Save Amount", self)
            
        } else if Double("\(transactionCount)") ?? 0.00 >= Double("\(transactionLimit)") ?? 0.00 {
            
            AlertView.simpleViewAlert(title: "", "Your today transactions limit is exceed", self)
            
            
        } else if Double("\(transactionSum)") ?? 0.00 >= Double("\(dayLimit)") ?? 0.00 || saveAmountDouble > (Double("\(maxLoad)") ?? 0.00) {
            
            if UserDefaults.standard.string(forKey: UserDefaultConstants.kycStatus.rawValue) == "1"{
                
                //confirmPopup()
                
                AlertView.simpleViewAlert(title: "", "Please enter amount less than or equal to" + (" \(maxLoad)"), self)
            }
            else {
                
                eKYCProcess()
            }
            
        } else if totalAmount > (Double("\(dayLimit)") ?? 0.00){
            
            if UserDefaults.standard.string(forKey: UserDefaultConstants.kycStatus.rawValue) == "1" {
                
                //confirmPopup()
                
                AlertView.simpleViewAlert(title: "", "Please select amount less than" + (" \(saveAmountDouble)"), self)
                
            }
            else {
                
                eKYCProcess()
            }
            
            
        }  else {
            
            confirmPopup()
        }
        
 
        
    }
    
    
    @IBAction func btnSelectTarget_Action(_ sender: Any) {
        clickDropDown = "0"
        setupTargetDropDown()
        dropdwnTarget.show()
 
    }
    
    
    
    @IBAction func btnDepositFrequency_Action(_ sender: Any) {
        clickDropDown = "1"
        setupTargetDropDown()
        dropdwnTarget.show()
        
    }
    

    @IBAction func btnAmountPerSave_Action(_ sender: Any) {
        clickDropDown = "2"
        
        self.valueAppendArr()
        setupTargetDropDown()
        dropdwnTarget.show()
    }
    
    
    @IBAction func btnStopTarget_Action(_ sender: Any) {
        
        targetStatus = false
        imgStopTarget.image = UIImage.init(named: "Ic_SelectCircle")
        imgContinueTarget.image = UIImage.init(named: "Ic_UnselectCircle-1")
    }
    
    
    @IBAction func btnContinueTarget_Action(_ sender: Any) {
        
        targetStatus = true
        imgStopTarget.image = UIImage.init(named: "Ic_UnselectCircle-1")
        imgContinueTarget.image = UIImage.init(named: "Ic_SelectCircle")
        
    }
    
    
    //MARK: - FUNCTIONS
    
    
    func setupTargetDropDown() {
        
        if clickDropDown == "0"{
            
            dropdwnTarget.anchorView = btnSelectTarget
            dropdwnTarget.bottomOffset = CGPoint(x: 0, y: btnSelectTarget.bounds.height + 10 )//
            dropdwnTarget.dataSource = allTargetAmount
            dropdwnTarget.selectionAction = { [weak self] (index, item) in
                self?.targetTextField.text = item
                
            }
            
        } else if clickDropDown == "1" {
            
            dropdwnTarget.anchorView = btnDepositFrequency
            dropdwnTarget.bottomOffset = CGPoint(x: 0, y: btnDepositFrequency.bounds.height + 10)
            dropdwnTarget.dataSource = setDepositFrequency
            dropdwnTarget.selectionAction = { [weak self] (index, item) in
                self?.weeklyTargetTextField.text = item
                
            }
            
        } else {
            
            dropdwnTarget.anchorView = btnAmountPerSave
            dropdwnTarget.bottomOffset = CGPoint(x: 0, y: btnAmountPerSave.bounds.height + 10)
            dropdwnTarget.dataSource = selectAmountSave
            dropdwnTarget.selectionAction = { [weak self] (index, item) in
                self?.saveAmountTxtField.text = item
                
            }
        }
        
        
    }
        
    /**** Confirm popup */
    func confirmPopup(){
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutAlertViewController") as? LogoutAlertViewController else{return}
        vc.setTransition()
        vc.titleLblData = "Message"
        vc.messageLblData = "Please note the weekly amount will be taken from your stored payment method immediately and each week there after until the target is reached."
        vc.okBtnTitle = "Confirm"
        vc.okCompletionHandler = { [weak self] in
            self?.dismiss(animated: false) {
                
//                if UserDefaults.exists(key: UserDefaultConstants.UserID.rawValue){
                    
                    self?.addRecurrance_ApiCall()
                
//                    self?.callAddCategories_Api(UserDefaults.standard.value(forKey: UserDefaultConstants.UserID.rawValue) as! String, self?.categoryId ?? "", self?.targetTextField.text ?? "", self?.weeklyTargetTextField.text ?? "")
              //  }
            }
      

        }
        vc.cancelCompletionHandler = { [weak self] in
            self?.dismiss(animated: true, completion: nil)
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    /**** Proceed for eKYC Process popup */
    func eKYCProcess() {
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutAlertViewController") as? LogoutAlertViewController else{return}
        vc.setTransition()
        vc.titleLblData = "Message"
        vc.messageLblData = "By adding this amount your Global Balance will exceed £" + "\(dayLimit)" + " and you will be required to complete the ‘Know your customer’ process, it’s free and only takes a couple of minutes. Otherwise please amend your deposit to maintain a balance below £" + "\(dayLimit) ."     //"To set weekly target amount more than £100, you need to complete your eKYC process."
        vc.okBtnTitle = "Proceed"
        vc.okCompletionHandler = { [weak self] in
            self?.dismiss(animated: false) {
                
                
                self?.kyc_ApiCall()
                
                
//                guard let eKYCVc = self?.storyboard?.instantiateViewController(withIdentifier: "KYCProcessViewController") as? KYCProcessViewController else{return}
//                eKYCVc.showleftArrow = true
//                self?.pushViewController(viewController: eKYCVc)
                

            }
            
        }
        vc.cancelCompletionHandler = { [weak self] in
            self?.dismiss(animated: true, completion: nil)
        }
        
        self.present(vc, animated: true, completion: nil)
    }
    
    
    ///API call
    func callAddCategories_Api(_ user_id : String,_ category_id : String,_ target : String,_ weekly_target : String){
        UserEditCategoryModel().fetchInfo(user_id,category_id,target,weekly_target){ (userinfo) in
            
            //print(userinfo)
            if userinfo.count > 0 {
                if userinfo[0].statuscheck == "success" {
                    
                    
                    guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "CustomDefaultAlertViewController") as? CustomDefaultAlertViewController else {
                        return
                    }
                    
                    vc.setTransition()
                    vc.titleVal = "Success"
                    vc.messageVal = "Target amount set successfully!"
                    vc.completionHandler = { [weak self] in
                        self?.dismiss(animated: true){
                            if self?.from == AllControllers.AddPotVc{
                                MyPotsViewController.setRootVC()
                            }
                            else if self?.from == AllControllers.MyPotDetailsVC {
                                self?.navigationController?.popViewController(animated: true)
                            }
                            
                        }
                    }
                    
                    self.present(vc, animated: true, completion: nil)
                    
                    
                } else {
                    
                    self.customAlertDismiss(title: "message", message: userinfo[0].message)
                    
                }
            }
            else {
                
                self.customAlertDismiss(title: "message", message: userinfo[0].message)
            }
        }
    }
    
    //MMMMMM
    
    func addRecurrance_ApiCall(){
        
        self.addPeculiPopup(view: self.view)
        let params:[String: Any] = [
                                    "catId": categoryId,
                                    "amount": saveAmountTxtField.text ?? "",
                                    "targetAmount" : "1", //targetTextField.text ?? "",
                                    "targetType" : weeklyTargetTextField.text ?? "",
                                    "targetStatus" : targetStatus,
                                    "cardId": cardSelectId]
        
        print(params)
        
        DataService.sharedInstance.postApiCall(endpoint: constantApis.addRecurrance_Api, params: params) { (response, error) in
            
            self.hidePeculiPopup(view: self.view)
            
            print(response)
            
            if response != nil {
                
                //self.popVc()
                //self.navigationController?.popVc()
                
                let transactionId = response?["transactionId"] as? String ?? ""
                let merchentToken =  response?["merchentToken"] as? String ?? ""
                
                print(transactionId)
                
                UserDefaults.standard.setValue(transactionId, forKey: UserDefaultConstants.Transaction_Id.rawValue) //merchentToken
                UserDefaults.standard.setValue(merchentToken, forKey: UserDefaultConstants.MerchentToken.rawValue)
                
                if let result = response?["result"] as? [String: Any]{
                    
                    if let body = result["body"] as? [String: Any]{
                        
                        let paymentURL = body["nextURL"] as? String ?? ""
                        
                        let string = paymentURL
                        
                        if string.contains("http") {
                            
                            let vc = PaymentViewController.instantiateFromAppStoryboard(appStoryboard: .main)
                            vc.urlData = string
                            vc.paymentType = "recurrence"
                            vc.PaymentCardId = self.cardSelectId
                            vc.transsctionsAmount = self.targetTextField.text ?? ""
                            vc.catIdForUpdateTransctonStatus = self.catIdForUpdateTransctonStatus
                            
                        } else {
                            
                            self.updateTransactionStatus()
                            
                        }
             
//                        let vc = PaymentViewController.instantiateFromAppStoryboard(appStoryboard: .main)
//                        vc.urlData = paymentURL
//                        vc.paymentType = "recurrence"
//                        vc.PaymentCardId = self.cardSelectId
//                        vc.transsctionsAmount = self.targetTextField.text ?? ""
                        //self.pushViewController(viewController: vc)
                        
                    }
                }
                
            }
            else {
                
                self.customAlertDismiss(title: "Message", message: "Something went wrong!")
            }
            
        }
    }
    
    
 
    
    func updateTransactionStatus(){
        
        self.addPeculiPopup(view: self.view)
        let params:[String: Any] = ["user_id": UserDefaults.standard.string(forKey: UserDefaultConstants.UserID.rawValue) ?? "",
                                    "transaction_id": UserDefaults.standard.string(forKey: UserDefaultConstants.Transaction_Id.rawValue) ?? "", //UserDefaults.standard.string(forKey: "SAVETRANSACTIONID") ?? "",
                                    "type" : "recurrence",
                                    "merchentToken" : UserDefaults.standard.string(forKey: UserDefaultConstants.MerchentToken.rawValue) ?? "",
                                    "amount" : self.targetTextField.text ?? "",
                                    "cardId" : cardSelectId,
                                    "catId" : catIdForUpdateTransctonStatus

                                    ]
        
    
        print(params)
        
        DataService.sharedInstance.postApiCall(endpoint: constantApis.updateTransactionStatus, params: params) { (response, error) in
            
            self.hidePeculiPopup(view: self.view)
            
            print(response)
            
            if response != nil{
                
                
                if GlobalVariables.moveAfterTransactions == "0" {
                    
                    //RedeemNowVC.setRootVC()
                    
                    
                    for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: RedeemNowVC.self) {
                            self.navigationController!.popToViewController(controller, animated: true)
                            break
                        }
                    }
                    
                    
                } else if GlobalVariables.moveAfterTransactions == "1" {
                    
                    //PeculiOfferViewController.setRootVC()
                    
                    for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: PeculiOfferViewController.self) {
                            self.navigationController!.popToViewController(controller, animated: true)
                            break
                        }
                    }
                    
                    
                    
                } else {
                    
                   // HomeViewController.setRootVC()
                    
                    for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: HomeViewController.self) {
                            self.navigationController!.popToViewController(controller, animated: true)
                            break
                        }
                    }
                }
                
                //HomeViewController.setRootVC()
            }
            else {
                self.customAlertDismiss(title: "Message", message: "Something went wrong!")
            }
            
        }
        
    }
    
    
    func GetCard_Api() {
        
        self.addPeculiPopup(view: self.view)
        
        DataService.sharedInstance.getCard_Api{ (resultDict, errorMsg) in
            
            self.hidePeculiPopup(view: self.view)
            
            print(resultDict as Any)
            
            if errorMsg == nil {
                
                if let result = resultDict?["result"] as? [NSDictionary] {
                    
                    self.getCardModel.removeAll()
                    
                    for allData in result {
                        
                        let _id = allData["_id"] as? String ?? ""
                        
                        let isEnable = allData["isEnable"] as? Bool ?? true
                        
                        let cardName =  allData["cardName"] as? String ?? ""
                        let primaryCard = allData["primaryCard"] as? Bool ?? true
                        let merchentToken = allData["merchentToken"] as? String ?? ""
                        let userId = allData["userId"] as? String ?? ""
                        let createdDate = allData["createdDate"] as? String ?? ""
                        let updatedDate = allData["updatedDate"] as? String ?? ""
                        let __v = allData["__v"] as? Int ?? 0
                        
                        self.getCardModel.append(GetCArd_ModelStructure.init(isEnable: isEnable, _id: _id, cardName: cardName, primaryCard: primaryCard, merchentToken: merchentToken, userId: userId, createdDate: createdDate, updatedDate: updatedDate, __v: __v))
                    }
                    
                    print("getCardModel========>>>>>>>>>>>>>> \(self.getCardModel)")
                    self.myTableView.reloadData()
                    
                } else {
                    
                    let message = resultDict?["message"] as? String
                    Toast.show(message: message ?? "Something went wrong", controller: self)
                }
                
            }
            
            else {
                
                Toast.show(message: "Something went wrong", controller: self)
                //let message = result["message"] as? String
                // Toast.show(message: message ?? "Something went wrong", controller: self)
                
            }
        }
        //            self.hidePeculiPopup(view: self.view)
        //            Toast.show(message: "Something went wrong", controller: self)
    }
    

    
    func kyc_ApiCall(){
        
        self.addPeculiPopup(view: self.view)
        let params:[String: Any] = [ "" : ""
                                    ]
        
        print(params)
        
        DataService.sharedInstance.postApiCall(endpoint: constantApis.userKyc_Api, params: params) { (response, error) in
            
            self.hidePeculiPopup(view: self.view)
            
            print(response)
            
            if response != nil {
                
                
              
               // UserDefaults.standard.setValue(merchentToken, forKey: UserDefaultConstants.MerchentToken.rawValue)
                
               let result = response?["code"] as? Int ?? 0
              let message = response?["message"] as? String ?? ""
                
                if result == 200 {
                    
                    self.customAlertDismiss(title: "Message", message: "\(message)" )
                    
                    let kycstatus = response?["kycStatus"] as? String ?? ""
                    let dayLimit = response?["dayLimit"] as? String ?? ""
                    let maxLoad = response?["maxLoad"] as? String ?? ""
                    let monthlyLimit = response?["monthlyLimit"] as? String ?? ""
                    let yearlyLimit = response?["yearlyLimit"] as? String ?? ""
                    let transactionLimit = response?["transactionLimit"] as? String ?? ""
                    
                    
                    UserDefaults.standard.setValue(kycstatus, forKey: UserDefaultConstants.kycStatus.rawValue)
                    UserDefaults.standard.setValue(dayLimit, forKey: UserDefaultConstants.dayLimit.rawValue)
                    UserDefaults.standard.setValue(maxLoad, forKey: UserDefaultConstants.maxLoad.rawValue)
                    UserDefaults.standard.setValue(monthlyLimit, forKey: UserDefaultConstants.monthlyLimit.rawValue)
                    UserDefaults.standard.setValue(yearlyLimit, forKey: UserDefaultConstants.yearlyLimit.rawValue)
                    UserDefaults.standard.setValue(transactionLimit, forKey: UserDefaultConstants.transactionLimit.rawValue)
   
                }
                    
                
            }
            else {
                
                self.customAlertDismiss(title: "Message", message: "Something went wrong!")
            }
            
        }
    

}

}

extension SetTargetForPotViewController : UITableViewDelegate, UITableViewDataSource  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getCardModel.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SetTargetTV_Cell") as! SetTargetTV_Cell
        
        cell.cardLbl.text = getCardModel[indexPath.row].cardName
        
        cell.selectionStyle = .none

        
        if TapCell == false {
            
            if getCardModel[indexPath.row].primaryCard == true {

                cell.circleImg.image = UIImage.init(named: "Ic_SelectCircle")
               cardSelectId = getCardModel[indexPath.row]._id
                
                print("cardSelectId===>>>> \(cardSelectId)")
        
            } else {
                
                cell.circleImg.image = UIImage.init(named: "Ic_UnselectCircle-1")
            }
            
        } else {
            
            if selectedRows.contains(indexPath) {
      
                cell.circleImg.image = UIImage.init(named: "Ic_SelectCircle")

            } else {

                cell.circleImg.image = UIImage.init(named: "Ic_UnselectCircle-1")
     
            }
            
        }
        
        
        cell.backgroundColor = UIColor.clear
        
        // Button Pressed inside cell
        
        cell.btnEditCardPressed = {
            
            print("Botton pressed")
            self.cardId = self.getCardModel[indexPath.row]._id
            self.cardName = self.getCardModel[indexPath.row].cardName
            
            self.popupEditCardNameAlert()
            
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let selectedIndexPath = IndexPath(row: indexPath.row, section: 0)

        self.selectedRows.removeAll()
        
        if self.selectedRows.contains(selectedIndexPath) {

            self.selectedRows.remove(at: self.selectedRows.index(of: selectedIndexPath)!)
  
        } else {
            
            TapCell = true
            self.selectedRows.append(selectedIndexPath)
            cardSelectId = getCardModel[indexPath.row]._id
            print("cardSelectId===>>>> \(cardSelectId)")
        }
        
        myTableView.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if let lastVisibleIndexPath = myTableView.indexPathsForVisibleRows?.last {
            
            if indexPath == lastVisibleIndexPath {
                
                var glanceAtIEPtableViewHeight: CGFloat {
                    myTableView.layoutIfNeeded()
                    return myTableView.contentSize.height
                }
                myTblViewHeight.constant = glanceAtIEPtableViewHeight + 50
            }
        }
    
    }
    //heightOfAnnouncementBackgroundVw
    
    func popupEditCardNameAlert(){
        
        
        let sb = UIStoryboard(name: "Main", bundle:nil)
        let editCardAlert = sb.instantiateViewController(withIdentifier: "EditCard_PopUp_Vc") as! EditCard_PopUp_Vc
        
        editCardAlert.cardId =  cardId
        editCardAlert.textFieldData = cardName
        
        editCardAlert.okCompletionHandler = {
            
            // DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            
            self.GetCard_Api()
            
            print("call editCardAlert PopUp")
            
            //  }
            
            
        }
        
        editCardAlert.providesPresentationContextTransitionStyle = true
        editCardAlert.definesPresentationContext = true
        editCardAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        editCardAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        
        self.present(editCardAlert, animated: true, completion: nil)
    }
    

}


extension UITableView {
    func lastIndexpath() -> IndexPath {
        let section = max(numberOfSections - 1, 0)
        let row = max(numberOfRows(inSection: section) - 1, 0)

        return IndexPath(row: row, section: section)
    }
}
