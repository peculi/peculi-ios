//
//  PotsDetailViewController.swift
//  Peculi
//
//  Created by apple on 31/10/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit
import NBBottomSheet
import SVProgressHUD
class PotsDetailViewController: UIViewController {
    
    //MARK: - Outlets
    
    @IBOutlet weak var globalBalanceSuperView: UIView!
    @IBOutlet weak var globalBalanceLbl: UILabel!
    @IBOutlet weak var globalBalanceView: UIView!
    @IBOutlet weak var potsDetailTableView: UITableView!
    @IBOutlet weak var reedemNowButton: UIButton!
    @IBOutlet weak var reedemButton: UIButton!
    @IBOutlet weak var offerValueSuperView: UIView!
    @IBOutlet weak var offerProductSuperView: UIView!
    @IBOutlet weak var scrollViewHeight: NSLayoutConstraint!
    @IBOutlet weak var potNameLbl: UILabel!
    @IBOutlet weak var potPrizeLbl: UILabel!
    @IBOutlet weak var potProductImageView: UIImageView!
    
    
    //MARK: - Define Variable
    
    var typeSaveValue = ""
    var potnametext = ""
    var selectedPotimage = ""
    var potPrice = ""
    var potimage = ""
    var slugName = ""
    var tierName = ""
    var voucherDiscount = ""
    var allDenomination = [String]()
    var configuration = NBBottomSheetConfiguration()
    var offerType = String()
    var discountVal = 0.00
    var voucherDiscountValue = 0.00
    var catId = ""
    var viewController: UIViewController!
    var allVouchers = [AllTiersModel]()
    var offer = Bool()
    var premiumStatus = ""
    
    var catIdForUpdateTransctonStatus = ""

     //MARK: - ViewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let globalBal = UserDefaults.standard.string(forKey: UserDefaultConstants.GlobalBalance.rawValue){
            self.globalBalanceLbl.text = "\(GlobalVariables.currency)\(globalBal)"
        }
        
        reedemNowButton.isHidden = true
        //getAllVoucher()
        
        
    }
    
    
    //MARK: - ViewWillAppear
    
    override func viewWillAppear(_ animated: Bool) {
        
        navigationSetup()
        
        getAllVoucher()

        potNameLbl.text = potnametext.capitalizingFirstLetter()
        potPrizeLbl.text =  String(format: "\(GlobalVariables.currency)%@",potPrice)
        
        var originalString = String()
        
        originalString = potimage
        
        let ImageUrl = String(format: "%@%@", AppConfig.ImageUrl,originalString)
        
        potProductImageView.sd_setImage(with: URL(string:ImageUrl), placeholderImage:UIImage(named: "placeholder_Img"), completed: { (image, error, cacheType, url) -> Void in
            if ((error) != nil) {
                
                // set the placeholder image here
                //cell.imgVw.image = #imageLiteral(resourceName: "doctorAvtar_Img")
            } else {
                // success ... use the image
            }
        })
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0) {
            
            self.globalBalanceView.addDashedBorder(conerRadius: 8)
            // self.historyBtn.addDashedBorder(conerRadius: 30)
        }
    }
    
    
    //MARK: - Navigation Setup
    
//    func navigationSetup(){
//        navigationController?.navigationBar.isHidden = false
//        //navigationController?.navigationBar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
//        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2122195363, green: 0.16875422, blue: 0.3097219765, alpha: 1)
//        navigationController?.navigationBar.isTranslucent = true
//        title = "Pot"
//        self.navigationController?.navigationBar.layer.masksToBounds = false
//        self.navigationController?.navigationBar.layer.shadowColor = UIColor.clear.cgColor//#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//        self.navigationController?.navigationBar.layer.shadowOpacity = 0.6
//        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 3)
//        self.navigationController?.navigationBar.layer.shadowRadius = 3.0
//
//
//
//
//        //MARK: - Navigation LeftBar Button:-
//
//        let leftBarButton = UIBarButtonItem(image: UIImage(named: "icBackArrowWhite"), style: .done, target: self, action: #selector(icMenuButtonAction))
//        self.navigationItem.leftBarButtonItem  = leftBarButton
//    }
    
    
    func navigationSetup(){

        //icBackArrowWhite
        
        navigationController?.navigationBar.isHidden = false
        title = "Pot"
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2203302979, green: 0.1600231826, blue: 0.3218392134, alpha: 1)
        self.navigationController?.navigationBar.isTranslucent = true

        //MARK: - Navigation LeftBar Button:-

        let leftBarButton = UIBarButtonItem(image: UIImage(named: "icBackArrowWhite"), style: .done, target: self, action: #selector(icBackButtonAction))
        self.navigationItem.leftBarButtonItem  = leftBarButton
    }
    
    
    
    
    
    //MARK: - Object Function Button Action:-
    
    @objc func icBackButtonAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: - UIButton Action
    
    @IBAction func globalBalanceButtonAction(_ sender: Any) {
        //        configuration = NBBottomSheetConfiguration(sheetSize: .fixed(200))
        //        viewController = PotOfferBottomXibViewController()
        //
        //        let bottomSheetController = NBBottomSheetController(configuration: configuration)
        //        bottomSheetController.present(viewController, on: self)
    }
    
    
    
}

//MARK: - UITableView Delegate & DataSource Method

extension PotsDetailViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allVouchers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PotsDetailTableViewCell") as! PotsDetailTableViewCell
        cell.retailerNameLbl.text = allVouchers[indexPath.row].name
        
        let percentageDoubleValue = (allVouchers[indexPath.row].percentDiscount as NSString).doubleValue
      //  let stringVal = "\(discountVal)"
        //let value = (stringVal as NSString).doubleValue
        print("percentageDoubleValue==============>>>>>>>>>>>>>\(percentageDoubleValue)")
        cell.peculiOfferLbl.text = "\(percentageDoubleValue.format())%" //"\(value.format())%"
        
        if typeSaveValue == allVouchers[indexPath.row].name {
            cell.retailerNameLbl.font = UIFont(name: "Montserrat-SemiBold", size: 16)
            cell.postValueLbl.font = UIFont(name: "Montserrat-SemiBold", size: 16)
            cell.peculiOfferLbl.font = UIFont(name: "Montserrat-SemiBold", size: 16)
        } else {
            cell.retailerNameLbl.font = UIFont(name: "Montserrat-SemiBold", size: 14)
            cell.postValueLbl.font = UIFont(name: "Montserrat-SemiBold", size: 14)
            cell.peculiOfferLbl.font = UIFont(name: "Montserrat-SemiBold", size: 14)
        }
        
        if allVouchers[indexPath.row].denominationType == "fixed" {
            cell.peculiOfferTitleLbl.text = "Range"
            cell.premiumImage.isHidden = false
            cell.postValueLbl.text = "\(GlobalVariables.currency)\(allVouchers[indexPath.row].min) to \(GlobalVariables.currency)\(allVouchers[indexPath.row].max)"
        } else {
            if (potPrice as NSString).doubleValue > (allVouchers[indexPath.row].max as NSString).doubleValue{
                
                cell.peculiOfferTitleLbl.text = "Range"
                cell.premiumImage.isHidden = true
                cell.postValueLbl.text = "\(GlobalVariables.currency)\(allVouchers[indexPath.row].min) to \(GlobalVariables.currency)\(allVouchers[indexPath.row].max)"
            }
            else {
                
                cell.peculiOfferTitleLbl.text = "Peculi Offer"
                cell.premiumImage.isHidden = true
                
                if let potValue = Double(potPrice){

                    let value: Double = potValue + (potValue * (percentageDoubleValue / 100))
                    
                    print(value)
                    //offer_amount = pot_amount + (pot_amount * (discount /100))
                    cell.postValueLbl.text = "\(GlobalVariables.currency)" + String(format: "%.2f", value)
                    
                }
            }

        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if offerType == "View Offer" {
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "RedeemNowVC") as! RedeemNowVC
            
            vc.logoImageURL = allVouchers[indexPath.row].logoImage
            vc.allAvailbleDenomination = allVouchers[indexPath.row].denominationData
            vc.discreption = allVouchers[indexPath.row].description
            vc.discount = allVouchers[indexPath.row].percentDiscount
            vc.navName = allVouchers[indexPath.row].name
            vc.catId = catId
            vc.productCode = allVouchers[indexPath.row].code
            vc.currencyCode = allVouchers[indexPath.row].currencyCode
            vc.type = allVouchers[indexPath.row].denominationType
            vc.termsAndConditions = allVouchers[indexPath.row].termsAndConditions
            vc.potBalance = potPrice
            vc.tierVal = "\(allVouchers[indexPath.row].voucherDiscountValue)" //"\(voucherDiscountValue)"
            vc.potImageURL = potimage
            vc.voucher_Id = allVouchers[indexPath.row].id
            vc.catIdForUpdateTransctonStatus = catIdForUpdateTransctonStatus
            
            if allVouchers[indexPath.row].denominationType == "fixed" {
                vc.price = allVouchers[indexPath.row].max
            }
            else {
                
                let percentageDoubleValue = (allVouchers[indexPath.row].percentDiscount as NSString).doubleValue
                if let potValue = Double(potPrice){
                    let value: Double = potValue + (potValue * (percentageDoubleValue / 100))
                    print(value)
                    
                    vc.price = String(format: "%.2f", value)
                }
            }
            
            vc.minimumVal = allVouchers[indexPath.row].min
            vc.maximumVal = allVouchers[indexPath.row].max
            
            
            
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else {
            
            //shoe pop up screen
            showInsufficientPopUp()
            
        }
        
        
//        else if premiumStatus  == "true" {
//
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "RedeemNowVC") as! RedeemNowVC
//
//            vc.logoImageURL = allVouchers[indexPath.row].logoImage
//            vc.allAvailbleDenomination = allVouchers[indexPath.row].denominationData
//            vc.discreption = allVouchers[indexPath.row].description
//            vc.discount = allVouchers[indexPath.row].percentDiscount
//            vc.navName = allVouchers[indexPath.row].name
//            vc.catId = catId
//            vc.productCode = allVouchers[indexPath.row].code
//            vc.currencyCode = allVouchers[indexPath.row].currencyCode
//            vc.type = allVouchers[indexPath.row].denominationType
//            vc.termsAndConditions = allVouchers[indexPath.row].termsAndConditions
//            vc.potBalance = potPrice
//            vc.tierVal = "\(voucherDiscountValue)"
//            vc.potImageURL = potimage
//            vc.catIdForUpdateTransctonStatus = catIdForUpdateTransctonStatus
//
//            if allVouchers[indexPath.row].denominationType == "fixed" {
//                vc.price = allVouchers[indexPath.row].max
//            }
//            else {
//
//                let percentageDoubleValue = (allVouchers[indexPath.row].percentDiscount as NSString).doubleValue
//                if let potValue = Double(potPrice){
//                    let value: Double = potValue + (potValue * (percentageDoubleValue / 100))
//                    print(value)
//
//                    vc.price = String(format: "%.2f", value)
//                }
//            }
//
//            vc.minimumVal = allVouchers[indexPath.row].min
//            vc.maximumVal = allVouchers[indexPath.row].max
//
//            self.navigationController?.pushViewController(vc, animated: true)
//
//        }
        

    }
    
    func showInsufficientPopUp(){
        
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "insufficientBlncPopUp_VC") as? insufficientBlncPopUp_VC else {
            return
        }
        
        vc.setTransition()
        
        vc.titleVal = "Success"
        vc.messageVal = "Insufficient balance, please add to funds."
        vc.completionHandler = { [weak self] in
            self?.dismiss(animated: false){
             
            }
        }
        
        self.present(vc, animated: false, completion: nil)
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let lastVisibleIndexPath = tableView.indexPathsForVisibleRows?.last {
            if indexPath == lastVisibleIndexPath {
                var tableViewHeight: CGFloat {
                    potsDetailTableView.layoutIfNeeded()
                    return potsDetailTableView.contentSize.height
                }
                
                scrollViewHeight.constant = tableViewHeight+globalBalanceSuperView.frame.height+offerProductSuperView.frame.height+offerValueSuperView.frame.height+130
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}


/****API calls */
extension PotsDetailViewController {
    func getAllVoucher() {
        SVProgressHUD.show()
        DataService.sharedInstance.getAllVouchers(categories: slugName, tierName: tierName) { (resultDict, errorMsg) in
            SVProgressHUD.dismiss()
            print(resultDict as Any)
            if errorMsg == nil {
                self.allVouchers.removeAll()
                if let result = resultDict?["result"] as? [NSDictionary] {
                    for allData in result {
                        
                        if self.tierName == "tier1" {
                            if let tier1 = allData["tier1"] as? Double {
                                self.discountVal = tier1
                            }
                            
                        }
                        else if self.tierName == "tier2" {
                            if let tier2 = allData["tier2"] as? Double {
                                self.discountVal = tier2
                            }
                        }
                        else if self.tierName == "tier3" {
                            if let tier3 = allData["tier3"] as? Double {
                                self.discountVal = tier3
                            }
                        }
                            else if self.tierName == "premium" {
                                if let premium = allData["premium"] as? Double {
                                    self.discountVal = premium
                                }
                            
                        }
                        
                        //let voucher_Id = allData["_id"] as? String ?? ""
                        
                        //self.voucherDiscountValue = allData["discount"] as? Double ?? 0.00
                        let voucherDiscountValue = allData["discount"] as? Double ?? 0.00
                        
                        print(self.discountVal)
                        
                        if let retailerDetails = allData["retailer_details"] as? [String: Any] {
                            
                            let max = retailerDetails["maximum_value"] as? String
                            let min = retailerDetails["minimum_value"] as? String
                            let name = retailerDetails["name"] as? String ?? ""
                            let percentDiscount = retailerDetails["percent_discount"] as? String
                            
                            let percentageDouble = Double(percentDiscount ?? "0")
                            let percentageInt = Int(percentageDouble ?? 0)
                            print(percentageInt)
                            let denominationType = retailerDetails["denomination_type"] as? String
                            let logoImageURL = retailerDetails["logo_image_url"] as? String
                            let description = retailerDetails["description"] as? String
                            //                        let discount = allData["percent_discount"] as? String
                            let currencyCode = retailerDetails["currency_code"] as? String
                            let code = retailerDetails["code"] as? String
                            let id = retailerDetails["_id"] as? String
                            let termsAndConditions = retailerDetails["terms_and_conditions_html"] as? String
                            self.allDenomination.removeAll()
                            if denominationType == "fixed" {
                                
                                if let availabledenomination = retailerDetails["available_denominations"] as? NSArray {
                                    for allData in availabledenomination {
                                        self.allDenomination.append(allData as? String ?? "")
                                    }
                                    self.allDenomination = self.allDenomination.reversed()
                                }
                            }
                            
                            self.allVouchers.append(AllTiersModel.init(max: max ?? "", min: min ?? "", name: name , percentDiscount: "\(self.discountVal)" , denominationType: denominationType ?? "", logoImage: logoImageURL ?? "", denominationData: self.allDenomination, description: description ?? "", currencyCode: currencyCode ?? "",code: code ?? "",id : id ?? "", termsAndConditon: termsAndConditions ?? "",voucher_Id: "",voucherDiscountValue: voucherDiscountValue))
                            
                        }
                        
                        print("allVouchers============>>>>>>>>>   \(self.allVouchers)")
                        
                        
                        
                    }
                    self.potsDetailTableView.reloadData()
                } else {
                    
                    self.customAlertDismiss(title: "Message", message: "Please try for another pot to get exciting offers.")
                }
            } else {
                
                self.customAlertDismiss(title: "Message", message: "Something went wrong")
                
            }
        }
    }
}



