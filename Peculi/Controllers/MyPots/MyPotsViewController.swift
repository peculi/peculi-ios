//
//  MyPotsViewController.swift
//  Peculi
//
//  Created by apple on 31/10/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit
import SideMenu

class MyPotsViewController: UIViewController, CheckIfBack {
    func backAction(_ flag: Int) {
        if flag == 1 {
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    
    //MARK: - Outltes
    @IBOutlet weak var globalBalanceSuperView: UIView!
    @IBOutlet weak var globalBalanceView: UIView!
    @IBOutlet weak var myPotTableView: UITableView!
    @IBOutlet weak var addNewPotView: UIView!
    @IBOutlet weak var scrollViewHeight: NSLayoutConstraint!
    @IBOutlet weak var globalBalanceLbl: UILabel!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var navigationSideMenuButton: UIButton!
    @IBOutlet weak var navigationTitleLbl: UILabel!

    //MARK: - variables
    
    var dataUser:[UserCategoriesInfo] = []
    
    
    //MARK: - Did load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let globalBal = UserDefaults.standard.string(forKey: UserDefaultConstants.GlobalBalance.rawValue){
            self.globalBalanceLbl.text = "\(GlobalVariables.currency)\(globalBal)"
        }
        
        myPotTableView.register(UINib(nibName: "EmptyDataTableViewCell", bundle: nil), forCellReuseIdentifier: "EmptyDataTableViewCell")
        
    }
    
    //MARK: - View will appear
    
    override func viewWillAppear(_ animated: Bool) {
        
        navigationSetup()
        
        navigationView.layer.masksToBounds = false
        navigationView.layer.shadowRadius = 3.0
        navigationView.layer.shadowOpacity = 0.6
        navigationView.layer.shadowColor = UIColor.clear.cgColor//#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        navigationView.layer.shadowOffset = CGSize(width: 0 , height: 3)
        
        
        if UserDefaults.exists(key: UserDefaultConstants.UserID.rawValue){
            
            callGetuser_Api(UserDefaults.standard.value(forKey: UserDefaultConstants.UserID.rawValue) as! String)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0) {
            self.globalBalanceView.addDashedBorder(conerRadius: 8)
            // self.historyBtn.addDashedBorder(conerRadius: 30)
        }
        
    }
    
    func navigationSetup(){
        navigationController?.navigationBar.isHidden = true
        title = "My Pots"
        
        //MARK: - Navigation LeftBar Button:-
        let leftBarButton = UIBarButtonItem(image: UIImage(named: "icSideMenu"), style: .done, target: self, action: #selector(icMenuButtonAction))
        self.navigationItem.leftBarButtonItem  = leftBarButton
    }
    
    func callGetuser_Api(_ user_id : String){
        GetUserCategoriesModel().fetchInfo(user_id){ (userinfo) in
            //print(userinfo)
            if userinfo.count > 0 {
                
                if userinfo[0].statuscheck == "success" {
                    self.dataUser.removeAll()
//                    for i in 0..<userinfo.count{
//                        self.dataUser.append(userinfo[i])
//                    }
                    self.dataUser = userinfo
                    print(self.dataUser)
                    
                } else {
//                    self.customAlertDismiss(title: "Error", message: userinfo[0].message)
                }
            } else {
                self.customAlertDismiss(title: "Error", message: "Something went wrong!")
                
            }
            self.myPotTableView.reloadData()
        }
    }
    
    
    
    //MARK: - Object Function Button Action:-
    @objc func icMenuButtonAction(){

        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "menuLeftNavigationController") as! SideMenuNavigationController
        nextViewController.leftSide = true
        nextViewController.menuWidth = UIScreen.main.bounds.size.width - 100
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    //MARK: - UIButton Action

    @IBAction func addNewPotButtonAction(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddPotViewController") as! AddPotViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func navigationSideMenuButtonAction(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "menuLeftNavigationController") as! SideMenuNavigationController
        nextViewController.leftSide = true
        nextViewController.menuWidth = UIScreen.main.bounds.size.width - 100
        self.present(nextViewController, animated:true, completion:nil)
    }
    
}

//MARK: - UItableView Delegate & DataSource
extension MyPotsViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataUser.count == 0{
            
            return 1
        }
        else{
            return dataUser.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if dataUser.count == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyDataTableViewCell") as! EmptyDataTableViewCell
            cell.defaultImageView.image = UIImage(named: "Group 41")
            if cell.defaultFirstLbl != nil{
                cell.defaultFirstLbl.removeFromSuperview()
            }
            if cell.defaultSecondLbl != nil{
                cell.defaultSecondLbl.removeFromSuperview()
            }
            if cell.addNewPot != nil{
                cell.addNewPot.removeFromSuperview()
            }
            
            cell.defaultImageView.bottomAnchor.constraint(equalTo: cell.contentView.bottomAnchor, constant: -15).isActive = true
            
            
            
            return cell
            
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyPotsTableViewCell") as! MyPotsTableViewCell
            
            if dataUser[indexPath.row].data1.category_name == "" {
                cell.productNameLbl.text = dataUser[indexPath.row].data1.categorydata.new_name.capitalizingFirstLetter()//) //String(format: "%@ Pot",
            }
            else {
                
                cell.productNameLbl.text =  dataUser[indexPath.row].data1.category_name.capitalizingFirstLetter()//)String(format: "%@ Pot",
            }
            
//            let price = String(format: "%.2f", dataUser[indexPath.row].total_amount)
//            cell.productPrizeLbl.text = "\(GlobalVariables.currency)\(price)"
            
            let priceConvert = Double(dataUser[indexPath.row].data1.categoryBalance)
            let price = priceConvert?.format()
            cell.productPrizeLbl.text = "\(GlobalVariables.currency)\(price ?? "")"

            var originalString = String()
            
            originalString = dataUser[indexPath.row].data1.categorydata.image
            let ImageUrl = String(format: "%@%@", AppConfig.ImageUrl,originalString)
            
            cell.productImageView.sd_setImage(with: URL(string:ImageUrl), placeholderImage:UIImage(named: "placeholder_Img"), completed: { (image, error, cacheType, url) -> Void in
                if ((error) != nil) {
                    // set the placeholder image here
                    //cell.imgVw.image = #imageLiteral(resourceName: "doctorAvtar_Img")
                } else {
                    // success ... use the image
                }
            })
            
            if indexPath.row == dataUser.count - 1 {
                cell.sepratorLineView.isHidden = true
            }
            else {
                cell.sepratorLineView.isHidden = false
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyPotDetailsViewController") as? MyPotDetailsViewController else  {return}
        
        if self.dataUser[indexPath.row].data1.category_name == "" {
            
            vc.navName = self.dataUser[indexPath.row].data1.categorydata.new_name
        }
        else {

            vc.navName = self.dataUser[indexPath.row].data1.category_name
        }
        
        vc.potCatId = self.dataUser[indexPath.row].data1.category_id
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let lastVisibleIndexPath = tableView.indexPathsForVisibleRows?.last {
            if indexPath == lastVisibleIndexPath {
                var tableViewHeight: CGFloat {
                    myPotTableView.layoutIfNeeded()
                    
                    return myPotTableView.contentSize.height
                }
                // print(tableViewHeight)
                scrollViewHeight.constant = tableViewHeight+globalBalanceSuperView.frame.height+addNewPotView.frame.height+navigationView.frame.height
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}



