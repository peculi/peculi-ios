//
//  AddPotViewController.swift
//  Peculi
//
//  Created by apple on 31/10/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit
import SideMenu
import SDWebImage
import Lottie

class AddPotViewController: UIViewController,UITextFieldDelegate {
    
    //MARK: - Outlets
    
    @IBOutlet weak var globalBalanceView: UIView!
    @IBOutlet weak var addPotCollectionView: UICollectionView!
    @IBOutlet weak var globalBalanceLbl: UILabel!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var navigationSideMenuButton: UIButton!
    @IBOutlet weak var navigationTitleLbl: UILabel!
    
    
   //MARK: - variables
    
    var dataUser:[AllCategoriesInfo] = []
    var categoryId = String()
    var navigationControllers: UINavigationController?
    var fromVC = AllControllers.none
    var selectedIndex = -1
    
    //MARK: - ViewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
//        let progressHUD = LottieProgressHUD.Shared //LottieProgressHUD.shared
//        progressHUD.animationFileName = "bouncing_ball" //name of the file
//        progressHUD.hudHeight = 100 //height of ProgressHUD
//        progressHUD.hudWidth = 100  //weight of ProgressHUD
//        progressHUD.hudBackgroundColor = UIColor.white //set background color of ProgressHUD
//        self.view.addSubview(progressHUD)  // add to view
//        progressHUD.show() // show ProgressHUD, to hide progressHUD.hide()

        
        setupCollectionView()
        
        navigationController?.navigationBar.isHidden = true
        
    }
    
    //MARK: - ViewWilAppear
    
    override func viewWillAppear(_ animated: Bool) {
        
//        navigartionSetup()
        
        navigationView.layer.masksToBounds = false
        navigationView.layer.shadowRadius = 3.0
        navigationView.layer.shadowOpacity = 0.6
        navigationView.layer.shadowColor = UIColor.clear.cgColor//#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        navigationView.layer.shadowOffset = CGSize(width: 0 , height: 1)
        
        if let globalBal = UserDefaults.standard.string(forKey: UserDefaultConstants.GlobalBalance.rawValue) {

            self.globalBalanceLbl.text = "\(GlobalVariables.currency)\(globalBal)"
            
        }

        
        if UserDefaults.exists(key: UserDefaultConstants.UserID.rawValue) {
            
            callAllCategories_Api(UserDefaults.standard.value(forKey: UserDefaultConstants.UserID.rawValue) as! String)
            
        }
        
   
        DispatchQueue.main.asyncAfter(deadline: .now() + 0) {
            
            self.globalBalanceView.addDashedBorder(conerRadius: 8)
            // self.historyBtn.addDashedBorder(conerRadius: 30)
        }
        
    }
    
    /**** navigation Setup */

    func navigartionSetup(){

        navigationController?.navigationBar.isHidden = true
        title = "Add Pot"
        
        //MARK: - Navigation LeftBar Button:-
        
        let leftBarButton = UIBarButtonItem(image: UIImage(named: "icSideMenu"), style: .done, target: self, action: #selector(icMenuButtonAction))
        self.navigationItem.leftBarButtonItem  = leftBarButton
        
    }
    
    
    /****API Calls */
    
    func callAllCategories_Api(_ user_id : String){
        
        self.addPeculiPopup(view : self.view)
        
        GetAllCategoriesModel().fetchInfo(user_id){ (userinfo) in
         
            //print(userinfo)
            if userinfo.count > 0 {
                
                if userinfo[0].statuscheck == "success" {
                    
                    self.hidePeculiPopup(view : self.view)
                    
                    self.dataUser.removeAll()
                    
                    for i in 0..<userinfo.count {
                        self.dataUser.append(userinfo[i])
                    }

                    print(self.dataUser)
                 
//                    self.dataUser = self.dataUser.sorted(by: {$0.name < $1.name})
//
//                    print(self.dataUser)
                   
                    self.addPotCollectionView.reloadData()
                    
                } else {
                    
                    self.hidePeculiPopup(view : self.view)
                    AlertView.simpleViewAlert(title: "", userinfo[0].message, self)
                }
            }
            else {

                self.hidePeculiPopup(view : self.view)
                AlertView.simpleViewAlert(title: "", userinfo[0].message, self)
            }
        }
    }
    
    func callAddCategories_Api(_ user_id : String,_ category_id : String,_ target : String,_ weekly_target : String,_ category_Name : String) {
        
        addPeculiPopup(view: self.view)
        
        GetUserAddedCategoryModel().fetchInfo(user_id,category_id,target,weekly_target,category_Name){ (userinfo) in
            
            print(userinfo)
            
            if userinfo.count > 0 {
                
                if userinfo[0].statuscheck == "success" {
                    
                    self.hidePeculiPopup(view : self.view)
                    
                    guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "PotAddPopUp_VC") as? PotAddPopUp_VC else {
                        return
                    }
                    
                    vc.setTransition()
                    
                    vc.titleVal = "Success"
                    vc.messageVal = "Pot successfully added."
                    vc.completionHandler = { [weak self] in
                        self?.dismiss(animated: false){
                            if self?.checkIfGuest() == false{
                                if UserDefaults.standard.string(forKey: UserDefaultConstants.personalDetailsStatus.rawValue) == "1" {
                                    self?.askToSetTargetPopUp(index: self?.selectedIndex ?? -1)
                                }
                                else {
                                   // MyPotsViewController.setRootVC() MMMMM
                                    HomeViewController.setRootVC()
                                }
                               
                            }
                            else {
                                
                                //MyPotsViewController.setRootVC()  mmmmmm
                                
                                HomeViewController.setRootVC()

                            }
                        }
                    }
                    
                    self.present(vc, animated: false, completion: nil)
                    
                } else {
                    
                    self.hidePeculiPopup(view : self.view)
                    
                    self.customAlertDismiss(title: "message", message: userinfo[0].message)
                    
                }
            }
            else {
                self.hidePeculiPopup(view : self.view)
                
                 self.customAlertDismiss(title: "message", message: userinfo[0].message)

            }
            
        }
    }
    
    //MARK: - Object Function Button Action:-
    
    @objc func icMenuButtonAction(){
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "menuLeftNavigationController") as! SideMenuNavigationController
        nextViewController.leftSide = true
        self.present(nextViewController, animated:true, completion:nil)
        
    }
    
    //MARK: - CollectionView Layout Function:-
    
    func setupCollectionView(){
        let layout = UICollectionViewFlowLayout()
        let cellSize = UIScreen.main.bounds.width / 3
        layout.itemSize = CGSize(width: cellSize, height: 170)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        addPotCollectionView.collectionViewLayout = layout
    }
    
    
    @IBAction func navigationSideMenuButtonAction(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "menuLeftNavigationController") as! SideMenuNavigationController
        nextViewController.leftSide = true
        nextViewController.menuWidth = UIScreen.main.bounds.size.width - 100
        self.present(nextViewController, animated:true, completion:nil)
        
    }

}

extension AddPotViewController : UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataUser.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddPotsCollectionViewCell", for: indexPath) as! AddPotsCollectionViewCell
        
        cell.potNameLbl.text = dataUser[indexPath.row].name.capitalizingFirstLetter()
        var originalString = String()
        
        originalString = dataUser[indexPath.row].image
        
        let ImageUrl = String(format: "%@%@", AppConfig.ImageUrl,originalString)
        
        cell.peculiProductImageView.sd_setImage(with: URL(string:ImageUrl), placeholderImage:UIImage(named: "placeholder_Img"), completed: { (image, error, cacheType, url) -> Void in
            
            if ((error) != nil) {
                
                // set the placeholder image here
                //cell.imgVw.image = #imageLiteral(resourceName: "doctorAvtar_Img")
                
            } else {
                
                // success ... use the image

            }
        })
        
        
        cell.addPotButtonAction.tag = indexPath.row
        cell.addPotButtonAction.addTarget(self, action: #selector(self.addPotButtonTapped(sender:)), for: .touchUpInside)
        
//        cell.cellBlurView.layer.masksToBounds = true
//        cell.cellBlurView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
//        cell.cellBlurView.layer.borderWidth = 1.0
        
        return cell
    }
    
    @objc func addPotButtonTapped(sender: UIButton) {
        
        if UserDefaults.exists(key: UserDefaultConstants.UserID.rawValue){
            selectedIndex = sender.tag
            confirmAddingPot(index: sender.tag)
                    }

    }
    
    func confirmAddingPot(index: Int) {
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutAlertViewController") as? LogoutAlertViewController else {return}

        vc.setTransition()
        vc.titleLblData = "Message"
        vc.messageLblData = "Are you sure you want to add this pot?"
        vc.okBtnTitle = "Yes"
        vc.cancelBtnTitle = "Cancel" //"No"
        vc.okCompletionHandler = { [weak self] in
            
            self?.dismiss(animated: false, completion: {

                self?.callAddCategories_Api(UserDefaults.standard.value(forKey: UserDefaultConstants.UserID.rawValue) as! String, self?.dataUser[index]._id ?? "" , "", "", self?.dataUser[index].name ?? "")
            })
            
        }
            vc.cancelCompletionHandler = {
            self.dismiss(animated: true, completion: nil)
        }
        
        self.present(vc, animated: true, completion: nil)
        
    }

    
    func askToSetTargetPopUp(index: Int) {
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutAlertViewController") as? LogoutAlertViewController else {
            return
        }
        
        vc.setTransition()
        vc.titleLblData = "Message"
        vc.messageLblData = "Do you want to set target now?"
        vc.okBtnTitle = "Yes"
        vc.cancelBtnTitle = "No"
        vc.okCompletionHandler = { [weak self] in
            
            self?.dismiss(animated: false) {
                
                guard let vc = self?.storyboard?.instantiateViewController(withIdentifier: "SetTargetForPotViewController") as? SetTargetForPotViewController else {return}
                vc.from = AllControllers.AddPotVc
                vc.categoryInfo = self?.dataUser[index] ?? AllCategoriesInfo()
                self?.pushViewController(viewController: vc)

            }
        }
        
        vc.cancelCompletionHandler = {
            
            self.dismiss(animated: false) {
                MyPotsViewController.setRootVC()
            }
            
        }
        
        self.present(vc, animated: true, completion: nil)
        
    }
    
    
    
    
}
