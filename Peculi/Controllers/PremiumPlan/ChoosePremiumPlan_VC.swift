//
//  ChoosePremiumPlan_VC.swift
//  Peculi
//
//  Created by PPI on 5/26/22.
//  Copyright © 2022 Stealth Tech. All rights reserved.
//

import UIKit

class ChoosePremiumPlan_VC: UIViewController {
    
#if DEBUG
    let verifyReceiptURL = "https://sandbox.itunes.apple.com/verifyReceipt"
#else
    let verifyReceiptURL = "https://buy.itunes.apple.com/verifyReceipt"
#endif


    //let verifyReceiptURL = "https://buy.itunes.apple.com/verifyReceipt"
    
    @IBOutlet weak var monthlyPlanView: UIView!
    @IBOutlet weak var yearlyPlanView: UIView!
    @IBOutlet weak var monthlyImg: UIImageView!
    @IBOutlet weak var yearlyImg: UIImageView!
    
    @IBOutlet weak var yearlyPrice: UILabel!
    @IBOutlet weak var monthlyPrice: UILabel!

    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var checkBoxBtn: UIButton!
    @IBOutlet weak var termsAndConditionsLbl: UILabel!
    
    
    @IBOutlet weak var lblTermCondition: UILabel!
    @IBOutlet weak var lblPrivacyPolicy: UILabel!
    @IBOutlet weak var lblTermsOfUse: UILabel!
    
    
    
    var checkBoxSelected = false
    var planAmount = ""
    var planSubscription = ""
   
    
    let YellowColor = UIColor(red: 243/255, green: 165/255, blue: 54/255, alpha: 1)
    
    let kInAppProductPurchasedNotification = "InAppProductPurchasedNotification"
    let kInAppProductPurchasingNotification = "InAppProductPurchasingNotification"

    
    let kInAppPurchaseFailedNotification   = "InAppPurchaseFailedNotification"
    let kInAppProductRestoredNotification  = "InAppProductRestoredNotification"
    let kInAppPurchasingErrorNotification  = "InAppPurchasingErrorNotification"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addObserver()
     //   trimString()
        setupView()
        
        tapLabel()
       
    }
    
    
    func tapLabel(){
        
        
        let guestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(lblTermCondition(_:)))
        lblTermCondition.addGestureRecognizer(guestureRecognizer)
        
        
        let guestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(lblPrivacyPolicy(_:)))
        lblPrivacyPolicy.addGestureRecognizer(guestureRecognizer1)
        
        let guestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(lblTermsOfUse(_:)))
        lblTermsOfUse.addGestureRecognizer(guestureRecognizer2)
        
        lblTermCondition.isUserInteractionEnabled = true
        lblPrivacyPolicy.isUserInteractionEnabled = true
        lblTermsOfUse.isUserInteractionEnabled = true
        
        
        let attributedString = NSMutableAttributedString.init(string: "Terms and conditions,")
        // Add Underline Style Attribute.
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: 1, range:
            NSRange.init(location: 0, length: attributedString.length));
        lblTermCondition.attributedText = attributedString
        
        
        let attributedString1 = NSMutableAttributedString.init(string: "Privacy policy & ")
        // Add Underline Style Attribute.
        attributedString1.addAttribute(NSAttributedString.Key.underlineStyle, value: 1, range:
            NSRange.init(location: 0, length: attributedString1.length));
        lblPrivacyPolicy.attributedText = attributedString1
        
        
        let attributedString2 = NSMutableAttributedString.init(string: "Terms of use")
        // Add Underline Style Attribute.
        attributedString2.addAttribute(NSAttributedString.Key.underlineStyle, value: 1, range:
            NSRange.init(location: 0, length: attributedString2.length));
        lblTermsOfUse.attributedText = attributedString2
        
        
    }
    
    
    
    @objc func lblTermCondition(_ sender: Any) {
            print("UILabel lblTermCondition")
        
        let vc = ContentVc.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.contentType = "term"
        vc.titleData = "Terms & Conditions"
        self.pushViewController(viewController: vc)
        
        
        }
    
    
    
    @objc func lblPrivacyPolicy(_ sender: Any) {
            print("UILabel lblPrivacyPolicy")
        
        
        let vc = ContentVc.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.contentType = "privacy"
        vc.titleData = "Privacy Policy"
        self.pushViewController(viewController: vc)
        
        
        
        }

    
    @objc func lblTermsOfUse(_ sender: Any) {
            print("UILabel lblTermsOfUse")
        
        if let url = URL(string: "https://www.apple.com/legal/internet-services/itunes/dev/stdeula/"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
        
        }
    
    
    
    func addObserver(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name(kInAppProductPurchasedNotification), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name(kInAppPurchaseFailedNotification), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name(kInAppProductRestoredNotification), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name(kInAppPurchasingErrorNotification), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfPurchasedSuccessfull(notification:)), name: Notification.Name(kInAppProductPurchasedNotification), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfPurchasingSuccessfull(notification:)), name: Notification.Name(kInAppProductPurchasingNotification), object: nil)
        
    }
    
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        self.hidePeculiPopup(view: self.view)
   
    }
    
    @objc func methodOfPurchasedSuccessfull(notification: Notification) {
        
       self.hidePeculiPopup(view: self.view)
        receiptValidation()
    }
    
    @objc func methodOfPurchasingSuccessfull(notification: Notification) {
        
       self.hidePeculiPopup(view: self.view)
       
    }
    
    func trimString(){
        
        let termsString = NSMutableAttributedString(string: "£1.29")
         termsString.setColorForText("£", with: #colorLiteral(red: 243/255, green: 165/255, blue: 54/255, alpha: 1))
        
        monthlyPrice.attributedText = termsString
        
        let termsString1 = NSMutableAttributedString(string: "£11.99")
         termsString1.setColorForText("£", with: #colorLiteral(red: 243/255, green: 165/255, blue: 54/255, alpha: 1))
        
        yearlyPrice.attributedText = termsString1
        
        let termsString2 = NSMutableAttributedString(string: "I agree to Terms and conditions, Privacy policy & Terms of use")
         termsString2.setColorForText("Terms and conditions,", with: #colorLiteral(red: 0.2901960784, green: 0.5647058824, blue: 0.8862745098, alpha: 1))
         termsString2.setColorForText("Privacy policy & Terms of use", with: #colorLiteral(red: 0.2901960784, green: 0.5647058824, blue: 0.8862745098, alpha: 1))
        
        termsAndConditionsLbl.numberOfLines = 0
        
         termsAndConditionsLbl.attributedText = termsString2
        
        
    }
    
    func setupView() {
       
        monthlyPlanView.layer.cornerRadius = 18
        yearlyPlanView.layer.cornerRadius = 18
        monthlyPlanView.layer.borderWidth = 1.0
        monthlyPlanView.layer.borderColor = YellowColor.cgColor
        yearlyPlanView.layer.borderWidth = 1.0
        yearlyPlanView.layer.borderColor = UIColor.lightGray.cgColor
        yearlyImg.isHidden = true
        monthlyImg.isHidden = false
        planAmount = "1.29"
        planSubscription = "0"
        
    }
    
    
    
    
    
    
    
    
    
    func confirmPopup(){
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutAlertViewController") as? LogoutAlertViewController else {return}
        
        vc.setTransition()
        vc.titleLblData = "Message"
        if planAmount == "1.29" {

            vc.messageLblData = "Please note the first monthly fee will be paid to Peculi immediately via your stored payment method and each month there after."
        }
        else if planAmount == "11.99" {
            vc.messageLblData = "Please note the annual fee will be paid to Peculi immediately via your stored payment method."
        }
        vc.okBtnTitle = "Confirm"
        vc.okCompletionHandler = { [weak self] in
            self?.dismiss(animated: false)  {
               
               // self?.customAlertDismiss(title: "Message", message: "Under development")
                
                
                if self?.planSubscription == "0" {
                    
                    
                    print("planSubscription====>>>>>.\(self?.planSubscription)")
                    self?.addPeculiPopup(view:self?.view ?? UIView())
                    InAppPurchase.sharedInstance.buyUnlockTestInAppPurchase_Monthly()

                } else {
                    
                    print("planSubscription====>>>>>.\(self?.planSubscription)")
                    self?.addPeculiPopup(view:self?.view ?? UIView())

                    InAppPurchase.sharedInstance.buyUnlockTestInAppPurchase_Yearly()
                    
                }
                
//                let vc = self?.storyboard?.instantiateViewController(withIdentifier: "AccountDetailsViewController") as! AccountDetailsViewController
//              self?.navigationController?.pushViewController(vc, animated: true)
                
            }
            
        }
        
        vc.cancelCompletionHandler = { [weak self] in
            self?.dismiss(animated: true, completion: nil)
        }
        
        self.present(vc, animated: true, completion: nil)
        
    }
    
    
    func popupSubscriptionAlert() {
        
        
        let sb = UIStoryboard(name: "Main", bundle:nil)
        let addCardAlert = sb.instantiateViewController(withIdentifier: "subscriptionPopUp_VC") as! subscriptionPopUp_VC
        //     customAlert.textFieldData = navName.capitalizingFirstLetter()
        
        //addCardAlert.categoryId = CategoryInfo[0].userData.categoryDetails._id
        
        addCardAlert.okCompletionHandler = {
            print("call Add Card PopUp")
            
            self.callSubscription()
            
        }
        
        addCardAlert.providesPresentationContextTransitionStyle = true
        addCardAlert.definesPresentationContext = true
        addCardAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        addCardAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        
        self.present(addCardAlert, animated: true, completion: nil)
    }
    
    
    
    
    
    
    func callSubscription() {
        
        
        if self.planSubscription == "0" {
            
            
            print("planSubscription====>>>>>.\(self.planSubscription)")
            self.addPeculiPopup(view:self.view ?? UIView())
            InAppPurchase.sharedInstance.buyUnlockTestInAppPurchase_Monthly()

        } else {
            
            print("planSubscription====>>>>>.\(self.planSubscription)")
            self.addPeculiPopup(view:self.view ?? UIView())
            InAppPurchase.sharedInstance.buyUnlockTestInAppPurchase_Yearly()
            
        }
        
        
    }
    
    
    @IBAction func btnMonthlyPlan_Action(_ sender: Any) {
//
        monthlyPlanView.layer.borderColor = YellowColor.cgColor
        yearlyPlanView.layer.borderColor = UIColor.lightGray.cgColor
        yearlyImg.isHidden = true
        monthlyImg.isHidden = false
        planAmount = "1.29"
        planSubscription = "0"

        
        
    }
    

    @IBAction func btnYearlyPlan_Action(_ sender: Any) {
  
        monthlyPlanView.layer.borderColor = UIColor.lightGray.cgColor
        yearlyPlanView.layer.borderColor = YellowColor.cgColor
        yearlyImg.isHidden = false
        monthlyImg.isHidden = true
        planAmount = "11.99"
        planSubscription = "1"
    }
    
    
    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    
    @IBAction func checkBoxBtnTapped(_ sender: Any) {
        
        if checkBoxBtn.imageView?.image == UIImage(named:"icSelectedCheckboxBlue") {

            checkBoxSelected = false
            checkBoxBtn.setImage(UIImage(named: "icUnselectedTermsPolicyCopy"), for: .normal)
            
        }
        else {
            
            checkBoxSelected = true
            checkBoxBtn.setImage(UIImage(named: "icSelectedCheckboxBlue"), for: .normal)
            
        }
      }
    
    
    //popupAddCardAlert()

    
    @IBAction func proceedToPayTapped(_ sender: Any) {
 
        if checkBoxSelected == true {
            
            if planAmount == "" {
                
                AlertView.simpleViewAlert(title: "Error", "Please Choose Premium Plan!", self)
                
            } else {
                
                
                self.popupSubscriptionAlert()
                
                //confirmPopup()
                
//                if self.planSubscription == "0" {
//
//
//                    print("planSubscription====>>>>>.\(self.planSubscription)")
//                    self.addPeculiPopup(view:self.view ?? UIView())
//                    InAppPurchase.sharedInstance.buyUnlockTestInAppPurchase_Monthly()
//
//                } else {
//
//                    print("planSubscription====>>>>>.\(self.planSubscription)")
//                    self.addPeculiPopup(view:self.view ?? UIView())
//
//                    InAppPurchase.sharedInstance.buyUnlockTestInAppPurchase_Yearly()
//
//                }
                
                
            }
        }
        else {
            
            AlertView.simpleViewAlert(title: "Error", "Please Agree to Terms and Conditions!", self)
        }
        
    }
    
    
    
    
    

}


extension ChoosePremiumPlan_VC {
    
    func receiptValidation() {
        
        self.addPeculiPopup(view: self.view)

        
        print("function Call receiptValidation ")
        
        //SVProgressHUD.show()
        
        let receiptFileURL = Bundle.main.appStoreReceiptURL
        let receiptData = try? Data(contentsOf: receiptFileURL!)
        
        print(receiptData)
        
       let recieptString = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        
        
       tokenReceipt = "\(recieptString ?? "")" //receiptData //recieptString
        
        let jsonDict: [String: AnyObject] = ["receipt-data" : recieptString! as AnyObject, "password" : "6f3d25df229e4a01b2293978f70da6f5" as AnyObject] //  dab3f8e770384d99ae7dda0096529a30      //fea1a171ce20451a8607943eee7a94cf shi h //6f3d25df229e4a01b2293978f70da6f5
       
     print("jsonDict testing===========>>>>>>> \(jsonDict)")
        
        
        self.hidePeculiPopup(view: self.view ?? UIView())
       // SVProgressHUD.dismiss()
        
        do {

            let requestData = try JSONSerialization.data(withJSONObject: jsonDict, options: JSONSerialization.WritingOptions.prettyPrinted)
            let storeURL = URL(string: verifyReceiptURL)!
            var storeRequest = URLRequest(url: storeURL)
            storeRequest.httpMethod = "POST"
            storeRequest.httpBody = requestData
            
            let session = URLSession(configuration: URLSessionConfiguration.default)
            
           
 
                let task = session.dataTask(with: storeRequest, completionHandler: { [weak self] (data, response, error) in
                    

                    print("response_origional=======>>>>>>>>>>>>>\(String(describing: response))")

                    do {
                        let jsonResponse = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                        
                        print("jsonResponse====>>>>>.......\(jsonResponse)")
                        
                        if let date = self?.getExpirationDateFromResponse(jsonResponse as! NSDictionary) {
                            //print(date)
                            print("date====>>>>>.......\(date)")

                        }
                        
            self?.addSubcription_Api()
                        
                        
                    } catch let parseError {
                        
                        //print(parseError)
                        print("parseError====>>>>>.......\(parseError)")
                    }
                })
                
                task.resume()
           
            
            
            
        } catch let parseError {
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: kInAppPurchasingErrorNotification), object: nil)
           // SVProgressHUD.dismiss()
            print(parseError)
            
        }
    }
    
    
    func addSubcription_Api() {
        
      // self.addPeculiPopup(view: self.view ?? UIView())
        
       // SVProgressHUD.show()
        
        
        let params:[String: Any] = ["orderId": orderId,
                                    "package": package,
                                    "subscriptionId":  subscriptionId,
                                    "token": tokenReceipt,
                                    "price" : price,
                                    "currency" : currency,
                                    "deviceType" : "ios"]

        print(params)
   
        DataService.sharedInstance.postApiCall(endpoint: constantApis.addSubcription_Api, params: params) { (response, error) in
            self.hidePeculiPopup(view: self.view)

         print(response)
            
           
           // SVProgressHUD.dismiss()
            
            if response != nil {
                
//               let Alert = alert()
//            Alert.msg(message: "Subscription Successfully")
//
               // popVc()
                
            // PeculiOfferViewController.setRootVC()
 
                for controller in self.navigationController!.viewControllers as Array {
                    
                    if controller.isKind(of: HomeViewController.self) {
                        
                        self.navigationController!.popToViewController(controller, animated: true)
                        break
                    }
                }
            }
            else {
                
               // SVProgressHUD.dismiss()
                self.hidePeculiPopup(view: self.view)
//                let Alert = alert()
//                Alert.msg(message: "Somthing went wrong")
                
            }
        }
        
    }
    
    func getExpirationDateFromResponse(_ jsonResponse: NSDictionary) -> Date? {
        
        if let receiptInfo: NSArray = jsonResponse["latest_receipt_info"] as? NSArray {

            let lastReceipt = receiptInfo.lastObject as! NSDictionary
            
            print("lastReceipt\(lastReceipt)")
            
            let transaction_Id = lastReceipt["transaction_id"] as? String ?? ""
            
            print(transaction_Id)
            
           orderId = "\(transaction_Id)"
            apiCall_func = "0"

            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss VV"
            
            if let expiresDate = lastReceipt["expires_date"] as? String {
                
                return formatter.date(from: expiresDate)
                
            }
            
            
            return nil
        }
        else {
            return nil
        }
        
    }
}
