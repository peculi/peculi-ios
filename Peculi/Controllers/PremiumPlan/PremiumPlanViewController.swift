//
//  PremiumPlanViewController.swift
//  Peculi
//
//  Created by iApp on 31/03/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import UIKit
import StoreKit


class PremiumPlanViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
   

    //MARK: - outlets
    
    @IBOutlet var superView: UIView!
    @IBOutlet weak var superchildView: UIView!
    @IBOutlet weak var plansTableView: UITableView!
    @IBOutlet weak var checkBoxBtn: UIButton!
    @IBOutlet weak var proceedToPayBtn: UIButton!
    @IBOutlet weak var termsAndConditionsLbl: UILabel!
    @IBOutlet weak var navView: UIView!
    
    //MARK: - variables
    
    var selectedIndex = 0
    var checkBoxSelected = false
    var fetchedData: [GetPremiumInfo] = []
    
    //MARK: - In App purchase variables

    //MARK: - view did load
    
    override func viewDidLoad() {
        super.viewDidLoad()

//      navigationController?.navigationBar.isHidden = true
        
       let termsString = NSMutableAttributedString(string: "I agree to Terms and conditions & Privacy policy")
        
        termsString.setColorForText("Terms and conditions", with: #colorLiteral(red: 0.2901960784, green: 0.5647058824, blue: 0.8862745098, alpha: 1))
        termsString.setColorForText("Privacy policy", with: #colorLiteral(red: 0.2901960784, green: 0.5647058824, blue: 0.8862745098, alpha: 1))

        termsAndConditionsLbl.attributedText = termsString

        callGetPremiumList()

    }
    
    //MARK: - button actions
    
    @IBAction func backBtnTapped(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    

    @IBAction func checkBoxBtnTapped(_ sender: Any) {
        
        if checkBoxBtn.imageView?.image == UIImage(named:"icSelectedCheckboxBlue"){
            
            checkBoxSelected = false
            checkBoxBtn.setImage(UIImage(named: "icUnselectedTermsPolicyCopy"), for: .normal)
            
        }
        else {
            
            checkBoxSelected = true
            checkBoxBtn.setImage(UIImage(named: "icSelectedCheckboxBlue"), for: .normal)
            
        }
  
    }
    

    @IBAction func proceedToPayTapped(_ sender: Any) {
 
        if checkBoxSelected == true {
            confirmPopup(planAmount: fetchedData[selectedIndex].price)
        }
        else {
            AlertView.simpleViewAlert(title: "Error", "Please Agree to Terms and Conditions!", self)
        }
        
    }
    

    

    ///API call
    func callGetPremiumList(){
        
        GetPremiumListModel().fetchInfo { (premiumList) in
            
            if premiumList.count > 0{
                if premiumList[0].statusCheck == "success"{
                    self.fetchedData.removeAll()
                    for i in 0..<premiumList.count{
                        self.fetchedData.append(premiumList[i])
                    }
                    print(self.fetchedData)

                } else {
                    AlertView.simpleViewAlert(title: "", premiumList[0].message, self)
                }
                self.plansTableView.reloadData()
                
            }
            else {
                self.plansTableView.reloadData()
                AlertView.simpleViewAlert(title: "", premiumList[0].message, self)
            }

        }
        
    }
    

//MARK: - tableview delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard  let cell = plansTableView.dequeueReusableCell(withIdentifier: "PremiumPlanTableViewCell") as? PremiumPlanTableViewCell else {
            return UITableViewCell()
        }
        
        
        var changeColor = UIColor()
        
        if selectedIndex == indexPath.row {
            
                  cell.checkBtn.isHidden = false
                  changeColor = #colorLiteral(red: 0.9568627451, green: 0.6588235294, blue: 0.231372549, alpha: 1)
        } else{
                  cell.checkBtn.isHidden = true
                  changeColor = #colorLiteral(red: 0.2901960784, green: 0.5647058824, blue: 0.8862745098, alpha: 1)
        }
        
        
        let price = "\(GlobalVariables.currency)\(fetchedData[indexPath.row].price)"
        cell.planTitleLbl.text = String(format: "%@%@", fetchedData[indexPath.row].name , " Plan")
        
        
        let descriptionString = String(format: "%@%@%@%@%@", "You need to pay ",price," for ",fetchedData[indexPath.row].plan_days," Days")
        let amountData = String(format: "%@%@%@", price , " / ", fetchedData[indexPath.row].plan_type)
        
        let amountSting = changeSubstringColor(amountData, price, changeColor)

        cell.amountLbl.attributedText = changeFont(amountSting, "Montserrat-Medium", 20.0, NSRange(location: 0, length: price.count))
        cell.descriptionLbl.attributedText = changeSubstringColor(descriptionString, price, changeColor)
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.selectedIndex = indexPath.row
        self.plansTableView.reloadData()
        
    }
    
    
    func changeSubstringColor( _ text: String, _ subString: String, _ color: UIColor) -> NSMutableAttributedString{
        
        let dataString = NSMutableAttributedString(string: text)
        dataString.setColorForText(subString, with: color)
        return dataString
        
    }
    
    func changeFont(_ text: NSMutableAttributedString, _ fontName: String, _ size: CGFloat, _ range: NSRange) -> NSMutableAttributedString{
        
        let data = text
        data.addAttributes([
        .font: UIFont(name: fontName, size: size)!], range: range)
        
        return data
    }
    
    func confirmPopup(planAmount: String){
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutAlertViewController") as? LogoutAlertViewController else {return}
        
        vc.setTransition()
        vc.titleLblData = "Message"
        
        if planAmount == "0.99" {
            
        vc.messageLblData = "Please note the first monthly fee will be paid to Peculi immediately via your stored payment method and each month there after."
        }
        else if planAmount == "9.99" {
            vc.messageLblData = "Please note the annual fee will be paid to Peculi immediately via your stored payment method."
        }
        vc.okBtnTitle = "Confirm"
        vc.okCompletionHandler = { [weak self] in
            self?.dismiss(animated: false)  {
               
                self?.customAlertDismiss(title: "Message", message: "Under development")
                
                InAppPurchase.sharedInstance.buyUnlockTestInAppPurchase_Monthly()
      
//                let vc = self?.storyboard?.instantiateViewController(withIdentifier: "AccountDetailsViewController") as! AccountDetailsViewController
//              self?.navigationController?.pushViewController(vc, animated: true)
                
            }
      
        }
        vc.cancelCompletionHandler = { [weak self] in
            self?.dismiss(animated: true, completion: nil)
        }
        self.present(vc, animated: true, completion: nil)
    }

}
