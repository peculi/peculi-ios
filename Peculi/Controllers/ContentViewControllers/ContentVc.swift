//
//  ContentVc.swift
//  Peculi
//
//  Created by Stealth on 20/11/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit
import WebKit

class ContentVc: UIViewController ,WKNavigationDelegate{
    
    //MARK: - outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var webVw: WKWebView!
    
    //MARK:- variables
    var contentType = String()
    var dataUser:[ContentInfo] = []
    var titleData = String()
    
    //MARK: - ViewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        uiUpdate()
        
    }
    
    
    /**** UI Setup */
    func uiUpdate(){
        
        self.navigationController?.navigationBar.isHidden = true
        lblTitle.text = titleData
        navView.layer.masksToBounds = false
        navView.layer.shadowRadius = 3.0
        navView.layer.shadowOpacity = 0.6
        navView.layer.shadowColor = UIColor.clear.cgColor //#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        navView.layer.shadowOffset = CGSize(width: 0 , height: 3)
        self.webVw.navigationDelegate = self
        self.webVw.isOpaque = false
        
        self.webVw.backgroundColor = #colorLiteral(red: 0.2117647059, green: 0.1725490196, blue: 0.3058823529, alpha: 1)
        self.webVw.tintColor = .white
        
        callGetContentApi(contentType)
        
    }
    
    // API call
 
    func callGetContentApi(_ contentType : String){
        
        GetContentModel().fetchInfo(contentType){ (userinfo) in
            
            if userinfo.count > 0 {
                if userinfo[0].statuscheck == "success" {
                    self.dataUser.removeAll()
                    for i in 0..<userinfo.count{
                        self.dataUser.append(userinfo[i])
                    }
                    print(self.dataUser)
                    
                    let HTMLString = "<html><body style='font-family:Montserrat-Regular;font-size:40px; color:#ffffff'><style>a:link { color: #0f83d9; }</style>\(self.dataUser[0].content)</body></html>"
                    self.webVw.loadHTMLString(HTMLString, baseURL: nil)
                    
                } else {
                    
                    AlertView.simpleViewAlert(title: "", userinfo[0].message, self)
                }
                
            }
            else {
                
                AlertView.simpleViewAlert(title: "", "Something Went Wrong", self)
            }
            
        }
    }
    
    
    ///back button action
    @IBAction func btnBackAction(_ sender: Any) {
        popVc()
    }
    
    
}

