//
//  AppConfig.swift
//  IMC
//
//  Created by vikram macbook pro on 26/08/19.
//  Copyright © 2019 dr.mac. All rights reserved.


import UIKit

//MARK: -   Base Api Model Class

class AppConfig: NSObject {
    
    //MARK: - Test Url
    
    static let BaseUrl = "https://peculi.com/nodeAPI/peculiWeb/"             //"http://3.10.182.123:3000/peculiWeb/"
    static let ImageUrl = "https://peculi.com/uploads/category/"            //"http://3.10.182.123/nodeAPI/public/uploads/category/"
    static let userProfileImageUrl = "https://peculi.com/uploads/users/"   //"http://3.10.182.123/nodeAPI/public/uploads/users/"
    
    //http://3.10.182.123/nodeAPI/public/uploads/category/
}

//MARK: -   Rest Api Model Class


class constantApis : NSObject {
    
    //MARK: - Production Url
    
    static let SignupApi = "userSignups" //"addUser"
    static let SigninApi = "usersLogin"//"userLogin"
    static let ForgetApi = "forgotPassword" //forgotPassword
    //static let OtpApi = "otpVerify" //otpVerify
    static let ResetPasswordApi = "resetPassword"
    static let ChangePasswordApi = "changePassword"
    static let GetUserCategoriesApi = "userGetCategory"
    static let GetAllCategoriesApi = "getAllCategories"
    static let GetAddCategoriesApi = "userAddCategory"
    static let GetContentApi = "getContent"
    static let updateUserInfoApi = "updateUserInfo2"
    static let updateUserInfo1 = "updateUserInfo"
    static let updateUserPin = "updateUserPin"
    static let Get_Tiers_Range = "allTiers"
    static let Get_All_Vouchers = "getAllVouchers"
    static let Get_All_Vouchers_History = "userVouchersHistory"
    static let Add_Swaprifice_Amount = "addSwapAmt"
    static let Get_Global_Price = "getGlobalBalance"
    static let Get_Category_Balance = "getCategoryBalance"
    static let Save_Redeem_Voucher = "redeemVoucher"
    static let Redeem_Voucher_History = "userRedeemVouchersHistory"
    static let Get_Countries = "getCountries"
    static let Get_States = "getStates"
    static let Get_Cities = "getCities"
    static let checkPostcode = "checkPostcode"
    static let userCategoryUpdate = "userCategoryUpdate"
    static let userCategoryDelete = "userCategoryDelete"
    static let getPremiumList = "getPremiumList"
    static let voucherTransactionsByCatId = "voucherTransactionsByCatId"
    static let todayOffer = "todayOffer_test"
    static let rewardOffer = "rewardOffer"
    static let updateToken = "updateToken" //updateToken
    static let getSpecialOfferByUserID = "getSpecialOfferByUserID"
    static let guestLogin = "guestLogins" //"guestLogin"
    static let userEditCategory = "userEditCategory"
    static let getCategoryInfoByID = "getCategoryInfoByID"
    static let getAllVouchersImages = "getAllVouchersImages"
    static let signup = "signup"
    static let otpVerify = "otpVerify" //"otpVerify"
    static let otpForgetVerify = "otpForgetVerify"
 
    static let personalDetailsRegister = "personalDetailsRegister"
    static let getKYC_uuid_ID = "getKYC_uuid_ID"
    static let addmoney = "addmoney"
    static let resendOTP = "resendOTP"//"resendOTP"
    static let postalCodeSearch = "postalCodeSearch"
    static let postalCodeSearchNew = "getPostCodes"
    static let updateTransactionStatus = "updateTransactionStatus"
    
   //MMMMM
    
    static let getCards_Api = "getCards"
    static let addNewCards_Api = "saveCards"
    static let editCardsName_Api = "updateCardName"
    static let changeCardStatus_Api = "changeCardStatus"
    static let addRecurrance_Api = "addRecurrance"
    static let getRecurrance_Api = "getRecurrance"
    static let cancelRecurrance_Api = "cancelRecurrance"
    static let getSubscriptions_Api = "getSubscriptions"
    static let recurrenceTransaction_Api = "recurrenceTransaction"
    static let transactionHistory_Api = "transactionHistory"
    static let getUserProfile_Api = "getUserProfile"
    static let updateProfileDetails_Api = "updateProfileDetails"
    static let uploadProfileImage_Api = "uploadProfileImage"
    static let getPots_Api = "getPots"
    static let potTransfer_Api = "potTransfer"
    static let bankTransfers_Api = "bankTransfers"
    static let addSubcription_Api = "addSubcription"
    static let deleteVouchers_Api = "deleteVouchers"
    
    static let newUpdateToken_Api = "updateToken"
    
    static let userKyc_Api = "userKyc"
    
    static let deleteAccount_Api = "deleteAccount"
    
    
    //https://peculi.com/nodeAPI/peculiWeb/deleteAccount
    

    //MARK: - Test Url

    //    static let DepartmentDoctorList =
    //    "http://vdh.noj.mybluehost.me/uat/en/doctorlist/"
    //    static let FindDoctorList = "http://vdh.noj.mybluehost.me/uat/en/alldoctor/"
    //    static let DepartmentDoctorDetailList = "http://vdh.noj.mybluehost.me/uat/en/doctordetail/"
    //    static let HealthCategoryTipsApi = "http://vdh.noj.mybluehost.me/uatdev/healthcategory"
    
}





    



