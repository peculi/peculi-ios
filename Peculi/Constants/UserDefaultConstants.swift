//
//  UserDefaultConstants.swift
//  Peculi
//
//  Created by iApp on 27/05/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import Foundation

enum UserDefaultConstants: String {

    case jwToken = "jwToken"
    case isguestLogin = "guestLogin"
    case kycStatus = "kycStatus"
    case otpStatus = "otpStatus"
    case personalDetailsStatus = "personalDetailsStatus"
    case userGender = "UserGender"
    case UserID = "UserID"
    case GlobalBalance = "GlobalBalance"
    case GlobalDeviceToken = "GlobalDeviceToken"
    case GlobalDeviceId = "GlobalDeviceId"
    case LoginStatus = "LoginStatus"
    case FirstName = "FirstName"
    case LastName = "LastName"
    case UserEmail = "UserEmail"
    case PhoneNumber = "PhoneNumber"
    case UserDob = "UserDob"
    case countryName = "countryName"
    case Address1 = "Address1"
    case Address2 = "Address2"
    case city = "city"
    case postcode = "postcode"
    case zipCode = "zipCode"
    case Auth = "Auth"
    case UserPin = "UserPin"
    case countryShortName = "countryShortName"
    case bottomSheetValue = "bottomSheetValue"
    case otp = "otp"
    case saveLat = "saveLat"
    case saveLong = "saveLong"
    case Transaction_Id = "Transaction_Id"
    case MerchentToken = "MerchentToken"
    
    case monthlyLimit = "monthlyLimit"
    case dayLimit = "dayLimit"
    case yearlyLimit = "yearlyLimit"
    case maxLoad = "maxLoad"
    case transactionCount = "transactionCount"
    case transactionLimit = "transactionLimit"
    case transactionSum = "transactionSum"
    
   case showPopupHomeScreen = "showPopupHomeScreen"
    case showPopup_PeculiOfferScreen = "showPopup_PeculiOfferScreen"
    case showPopup_redeemScreen = "showPopup_redeemScreen"
    
    
    
}


