 //
//  InAppPurchase.swift
//  ios_swift_in_app_purchases_sample
//
//  Created by Maxim Bilan on 7/27/15.
//  Copyright (c) 2015 Maxim Bilan. All rights reserved.


import Foundation
import StoreKit
import SVProgressHUD

var orderId = ""
var package = ""
var subscriptionId = ""
var tokenReceipt = ""
var price = ""
var currency = ""

var apiCall_func = ""
let kInAppProductPurchasedNotification = "InAppProductPurchasedNotification"
let kInAppProductPurchasingNotification = "InAppProductPurchasingNotification"
let kInAppPurchaseFailedNotification   = "InAppPurchaseFailedNotification"
let kInAppProductRestoredNotification  = "InAppProductRestoredNotification"
let kInAppPurchasingErrorNotification  = "InAppPurchasingErrorNotification"


class InAppPurchase : NSObject, SKProductsRequestDelegate, SKPaymentTransactionObserver {
	
	static let sharedInstance = InAppPurchase()
	
    
#if DEBUG
	let verifyReceiptURL = "https://sandbox.itunes.apple.com/verifyReceipt"
#else
	let verifyReceiptURL = "https://buy.itunes.apple.com/verifyReceipt"
#endif

    //let verifyReceiptURL = "https://buy.itunes.apple.com/verifyReceipt"
    
    let unlockTestInAppPurchase1ProductId = "com.peculi.app.monthlyNew_subscription" //"com.peculi.app_monthly_subscription" //"new.peculi_monthly" //
	let unlockTestInAppPurchase2ProductId = "com.peculi.app_yearly_subscription" //"new.peculi_yearly"  // 

//    let autorenewableSubscriptionProductId = "com.testing.autorenewablesubscription"
//	let nonrenewingSubscriptionProductId = "com.testing.nonrenewingsubscription"
    

	override init() {
		super.init()

        SKPaymentQueue.default().add(self)
        
	}
	   
    
	func buyProduct(_ product: SKProduct) {
     
		//print("Sending the Payment Request to Apple")
        
        print("function Call buyProduct 2 ")
        
		let payment = SKPayment(product: product)
		SKPaymentQueue.default().add(payment)
        
	}
	
	func restoreTransactions() {
        
        print("function Call restoreTransactions ")
		SKPaymentQueue.default().restoreCompletedTransactions()
        
	}
	
    
	func request(_ request: SKRequest, didFailWithError error: Error) {
		print("Error %@ \(error)")
        
        print("function Call request ")
        
		NotificationCenter.default.post(name: Notification.Name(rawValue: kInAppPurchasingErrorNotification), object: error.localizedDescription)
	}
    
	
	func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        
		//print("Got the request from Apple")
        print("function Call productsRequest 1")
        
        
		let count: Int = response.products.count
		if count > 0 {
			_ = response.products

            let validProduct: SKProduct = response.products[0]
          
            print(validProduct.localizedTitle)
			print(validProduct.localizedDescription)
			print(validProduct.price)
            print(validProduct.productIdentifier)
            print(validProduct.priceLocale)
            print(validProduct.description)
                        
            print(validProduct.introductoryPrice)
            
          //  en_IN@currency=INR (fixed)

            orderId = ""
            package = validProduct.localizedTitle
            subscriptionId = validProduct.productIdentifier
            price = "\(validProduct.price)"
            currency = "\(validProduct.priceLocale)"
            

            print("\(package),", "\(subscriptionId),","\(price),","\(currency),")
            
			buyProduct(validProduct);
            
		}
		else {
			print("No products")
		}
	}
	
	func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        
		//print("Received Payment Transaction Response from Apple");
        
        print("function Call paymentQueue 3 ")
		
		for transaction: AnyObject in transactions {
            
			if let trans: SKPaymentTransaction = transaction as? SKPaymentTransaction {
				switch trans.transactionState {
                    
                case .purchasing:
                    
                    print("Product purchasing")
                    NotificationCenter.default.post(name: Notification.Name(rawValue: kInAppProductPurchasingNotification), object: nil)
                    
                    break
				case .purchased:
                    
					print("Product Purchased")
					
					savePurchasedProductIdentifier(trans.payment.productIdentifier)
					SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    
					//NotificationCenter.default.post(name: Notification.Name(rawValue: kInAppProductPurchasedNotification), object: nil)
                    
                    NotificationCenter.default.post(name: Notification.Name(rawValue: kInAppProductPurchasedNotification), object: nil)
					
					//receiptValidation()

                    break
					
				case .failed:
                    
					print("Purchased Failed")
					SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
					NotificationCenter.default.post(name: Notification.Name(rawValue: kInAppPurchaseFailedNotification), object: nil)
					break
					
				case .restored:
                    
					print("Product Restored")
                    
					savePurchasedProductIdentifier(trans.payment.productIdentifier)
					SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
					NotificationCenter.default.post(name: Notification.Name(rawValue: kInAppProductRestoredNotification), object: nil)
					break
					
				default:
					break
				}
			}
			else {
				
			}
            
		}
	}
	
	func savePurchasedProductIdentifier(_ productIdentifier: String!) {
        
        
		UserDefaults.standard.set(productIdentifier, forKey: productIdentifier)
		UserDefaults.standard.synchronize()
        
        print("function Call savePurchasedProductIdentifier 4 ")
        
	}

	
	func unlockProduct(_ productIdentifier: String) {
        
		if SKPaymentQueue.canMakePayments() {
            let productID: NSSet = NSSet(object: productIdentifier )
            let productsRequest: SKProductsRequest = SKProductsRequest(productIdentifiers: productID as! Set<String>)
            productsRequest.delegate = self
            productsRequest.start()
            print("Fetching Products")
        }
		else {
            
			print("Сan't make purchases")
			NotificationCenter.default.post(name: Notification.Name(rawValue: kInAppPurchasingErrorNotification), object: NSLocalizedString("CANT_MAKE_PURCHASES", comment: "Can't make purchases"))
		}
	}
    
    
//    orderId = ""
//    var package = ""
//    var subscriptionId = ""
//    var token = ""
//    var price = ""
//    var currency = ""
	
    func buyUnlockTestInAppPurchase_Monthly() {
        unlockProduct(unlockTestInAppPurchase1ProductId)
	}
	
	func buyUnlockTestInAppPurchase_Yearly() {
		unlockProduct(unlockTestInAppPurchase2ProductId)
	}

//	func buyAutorenewableSubscription() {
//		unlockProduct(autorenewableSubscriptionProductId)
//	}
//
//
//	func buyNonrenewingSubscription() {
//		unlockProduct(nonrenewingSubscriptionProductId)
//	}
	


}

//class alert {
//    
//    func msg(message: String, title: String = "")
//    {
//        let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
//
//        alertView.addAction(UIAlertAction(title: "Done", style: .default, handler: nil))
//
//        UIApplication.shared.keyWindow?.rootViewController?.present(alertView, animated: true, completion: nil)
//    }
//    
//}
//
