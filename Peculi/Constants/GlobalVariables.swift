//
//  GlobalVariables.swift
//  Peculi
//
//  Created by iApp on 10/02/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import Foundation
struct GlobalVariables {
    
    static var currency = ""
    static var device_token = ""
    static var addPotName = ""
    static var addPotImageUrl = ""
    static var AddPotCatID = ""
    static var swaprificeName = ""
    static var swaprificeImage = ""
    static var swaprificeCatId = ""
    
    static var moveAfterTransactions = ""  //"0" for Reddem_VC, "1" for PeculliOffer_VC
    
    
}


var K_networkConnectivityCheck = "K_networkConnectivityCheck"
