//
//  LottiesFile.swift
//  Peculi
//
//  Created by PPI on 2/16/22.
//  Copyright © 2022 Stealth Tech. All rights reserved.
//

import Foundation
import UIKit
import Lottie


extension UIViewController {
    
    struct Peculi {
           static var animationView: AnimationView?
           static var viewBGanimationView =  UIView()
       }
    
    func addPeculiPopup(view : UIView){
        if Peculi.animationView != nil {
        if Peculi.animationView!.isAnimationPlaying {
            hidePeculiPopup(view: view)
            return
          }
        }
      
        Peculi.viewBGanimationView.frame =  self.view.bounds //
        Peculi.viewBGanimationView.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.5) ////UIColor(red: 95/255.0, green: 63/255.0, blue: 16/255.0, alpha: 0.5) //
        view.addSubview(Peculi.viewBGanimationView)
        Peculi.animationView = .init(name: "data")
        
        Peculi.animationView!.frame = CGRect(x: 0, y: 0, width: 90, height: 90)
        Peculi.animationView?.center = self.view.center
        
        // 3. Set animation content mode
        
        Peculi.animationView!.contentMode = .scaleAspectFit
        
        // 4. Set animation loop mode
        
        Peculi.animationView!.loopMode = .loop
        
        // 5. Adjust animation speed
        
        Peculi.animationView!.animationSpeed = 1 //1.5
        
        Peculi.viewBGanimationView.addSubview(Peculi.animationView!)
        
        // 6. Play animation
        Peculi.animationView!.play()
    }
    
    func hidePeculiPopup(view : UIView) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            if Peculi.animationView != nil {
                Peculi.animationView?.removeFromSuperview()
                Peculi.viewBGanimationView.removeFromSuperview()
          }
        }
    }
    
    
//    func LOADERSHOW(view:UIView)
//    {
//
//
//
//        let progressHUD =  LottieProgressHUD.shared
//        progressHUD.animationFileName = "bouncing_ball" //name of the file
//        progressHUD.hudHeight = 100 //height of ProgressHUD
//        progressHUD.hudWidth = 100  //weight of ProgressHUD
//        progressHUD.hudBackgroundColor = UIColor.white //set background color of ProgressHUD
//        self.view.addSubview(progressHUD)  // add to view
//        progressHUD.show() // show ProgressHUD, to hide progressHUD.hide()
//
//    }
//
    
    
    
    
}
    

