//
//  FingerprintConstants.swift
//  Peculi
//
//  Created by iApp on 18/05/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import Foundation

enum FingerprintConstants{
    
    case none
    case authenticationFailed
    case authenticationError
    case success
    case cancel
    
}
