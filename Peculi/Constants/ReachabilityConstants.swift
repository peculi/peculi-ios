//
//  ReachabilityConstants.swift
//  Peculi
//
//  Created by iApp on 01/06/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import Foundation

enum ReachabilityConstants: String{
    
    case disconnected = "disconnected"
    case connected = "connected"
    case none = "none"
    
}
