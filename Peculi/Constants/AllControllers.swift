//
//  AllControllers.swift
//  Peculi
//
//  Created by iApp on 28/05/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import Foundation

enum AllControllers{
    
    case AddPotVc
    case FingerprintVC
    case SwaprificeVC
    case MyPotDetailsVC
    
    case none
    
}
