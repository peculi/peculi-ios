import UIKit

extension String {
    
    subscript (i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
//    subscript (r: Range<Int>) -> String {
//        let start = index(startIndex, offsetBy: r.lowerBound)
//        let end = index(startIndex, offsetBy: r.upperBound)
//        return String(self[(start ..< end)])
//    }
    
    var languageSet : String{
        return self//LocalizationSystem.SharedInstance.localizedStingforKey(key: self, comment: "")
    }
    var containsAlphabets: Bool {
        //Checks if all the characters inside the string are alphabets
        let set = CharacterSet.letters
        return self.utf16.contains {
            guard let unicode = UnicodeScalar($0) else { return false }
            return set.contains(unicode)
        }
    }
}

// MARK: - NSAttributedString extensions
public extension String {
    
    /// Regular string.
    public var regular: NSAttributedString {
        return NSMutableAttributedString(string: self, attributes: [.font: UIFont.systemFont(ofSize: UIFont.systemFontSize)])
    }
    
    /// Bold string.
    public var bold: NSAttributedString {
        return NSMutableAttributedString(string: self, attributes: [.font: UIFont.boldSystemFont(ofSize: UIFont.systemFontSize)])
    }
    
    /// Underlined string
    public var underline: NSAttributedString {
        return NSAttributedString(string: self, attributes: [.underlineStyle: NSUnderlineStyle.single.rawValue])
    }
    
    /// Strikethrough string.
    public var strikethrough: NSAttributedString {
        return NSAttributedString(string: self, attributes: [.strikethroughStyle: NSNumber(value: NSUnderlineStyle.single.rawValue as Int)])
    }
    
    /// Italic string.
    public var italic: NSAttributedString {
        return NSMutableAttributedString(string: self, attributes: [.font: UIFont.italicSystemFont(ofSize: UIFont.systemFontSize)])
    }
    
    /// Add color to string.
    ///
    /// - Parameter color: text color.
    /// - Returns: a NSAttributedString versions of string colored with given color.
    public func colored(with color: UIColor) -> NSAttributedString {
        return NSMutableAttributedString(string: self, attributes: [.foregroundColor: color])
    }
}

extension Array where Element: NSAttributedString {
    func joined(separator: NSAttributedString) -> NSAttributedString {
        var isFirst = true
        return self.reduce(NSMutableAttributedString()) {
            (r, e) in
            if isFirst {
                isFirst = false
            } else {
                r.append(separator)
            }
            r.append(e)
            return r
        }
    }
    
    func joined(separator: String) -> NSAttributedString {
        return joined(separator: NSAttributedString(string: separator))
    }
}

extension String{
    func toTime()->String{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = formatter.date(from: self) ?? Date()
        formatter.dateFormat = "hh:mm a"
        return formatter.string(from: date)
    }
    func toDate()->String{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.000+0000"
        let date = formatter.date(from: self) ?? Date()
        formatter.dateFormat = "dd-MMM-yyyy hh:mm a"
        return formatter.string(from: date)
    }
    func toDateString()->String{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.0"
        let date = formatter.date(from: self) ?? Date()
     //   formatter.locale = NSLocale(localeIdentifier: "en_US") as Locale?
        formatter.dateFormat = "dd-MMM-yyyy hh:mm a"
        return formatter.string(from: date)
    }
    
//    func vitalDateString()->String{
//        let formatter = DateFormatter()
//        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.0"
//        formatter.locale = NSLocale(localeIdentifier: "en_US") as Locale?
//        let date = formatter.date(from: self) ?? Date()
//
//        formatter.dateFormat = "dd-MMM-yyyy hh:mm a"
//        return formatter.string(from: date)
//    }
    
    
    
    
    
    func buffertoDateString()->String{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.0"
        let date = formatter.date(from: self) ?? Date()
        formatter.locale = NSLocale(localeIdentifier: "en_US") as Locale?
        formatter.dateFormat = "dd-MMM-yyyy hh:mm a"
        return formatter.string(from: date)
    }
    
    
//    func DateString()->String{
//        let formatter = DateFormatter()
//        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.0"
//        let date = formatter.date(from: self) ?? Date()
//        formatter.dateFormat = "dd-MMM-yyyy hh.mm a"
//        return formatter.string(from: date)
//    }
    
    
    func convertDate(type:String)->String{
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMM-yy hh:mm a"
        let date = formatter.date(from: self) ?? Date()
        formatter.dateFormat = type
        return formatter.string(from: date)
    }
    
    func vitalDate(type:String)->String{
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMM-yy hh:mm a"
        formatter.locale = NSLocale(localeIdentifier: "en_US") as Locale?
        let date = formatter.date(from: self) ?? Date()
        formatter.dateFormat = type
        return formatter.string(from: date)
    }
    
    
    
    
    func bufferMin(type:String,mint:Int)->String{
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMM-yyyy hh:mm a"

        let date = formatter.date(from: self) ?? Date()
        formatter.dateFormat = type

        return formatter.string(from: date.adding(.minute, value: mint))
    }
    
    
    
    func convertDateFormat(type:String)->String{
    let formatter = DateFormatter()
    formatter.dateFormat = "dd-MMM-yy hh:mm a"
//        if LocalizationSystem.sharedInstance.getLanguage() == "en" {
            formatter.locale = NSLocale(localeIdentifier: "en_US") as Locale?
       // }
    let date = formatter.date(from: self) ?? Date()
    formatter.dateFormat = type
    return formatter.string(from: date)
    }
    
    
//    func getBufferTime(type:String) -> String? {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
//        dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale?
//        dateFormatter.timeZone = TimeZone(identifier: "Asia/Bahrain")
//        dateFormatter.dateFormat = "HH.mm a"
//        
//        return dateFormatter.string(from: Date())
//    }
    
    
    
    func toCalenderDate()->String{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.000+0000"
        let date = formatter.date(from: self) ?? Date()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: date)
    }
    
    
    func toCalenderTime()->String{
        let formatter = DateFormatter()
        
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
//        if LocalizationSystem.sharedInstance.getLanguage() == "en" {
            formatter.locale = NSLocale(localeIdentifier: "en_US") as Locale?
        //}
        let date = formatter.date(from: self) ?? Date()
       // formatter.timeZone = TimeZone(abbreviation: "UTC")!
        formatter.timeZone = TimeZone(identifier: "Asia/Bahrain")!

         formatter.dateFormat = "hh:mm a"
        
        
        
        return formatter.string(from: date)
    }
    
   
    
    
    func toCalenderDateTime()->Date{
        let formatter = DateFormatter()
        
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        let date = formatter.date(from: self) ?? Date()
        // formatter.timeZone = TimeZone(abbreviation: "UTC")!
        formatter.timeZone = TimeZone(identifier: "Asia/Bahrain")!
        
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        let result = formatter.string(from: date)
        
          print(result)

        let dataaa = formatter.date (from: result)

        print(dataaa)
        
        return formatter.date(from: result)!
    }
    
    
    
    func toNotificationTime()->String{
        let formatter = DateFormatter()
        // formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.000+0000"
        
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
//        if LocalizationSystem.sharedInstance.getLanguage() == "en" {
            formatter.locale = NSLocale(localeIdentifier: "en_US") as Locale?
        //}
        let date = formatter.date(from: self) ?? Date()
        // formatter.timeZone = TimeZone(abbreviation: "UTC")!
        formatter.timeZone = TimeZone(identifier: "Asia/Bahrain")!
        
        formatter.dateFormat = "dd-MMM-yy hh:mm a"
        
        
        
        return formatter.string(from: date)
    }
    
    
}
