import UIKit

extension UIViewController {
    
    var alertController: UIAlertController? {
        guard let alert = UIApplication.topViewController() as? UIAlertController else { return nil }
        return alert
    }
    
        class var storyboardID : String {
            return "\(self)"
        }
        enum Appstoryboard {
            case main
            var instance : UIStoryboard{
                return UIStoryboard(name:  "Main", bundle: Bundle.main)
            }
            func viewcontroller<T: UIViewController>(viewcontrollerClass:T.Type) -> T {
                let storyboardID = (viewcontrollerClass as UIViewController.Type).storyboardID
                return instance.instantiateViewController(withIdentifier: storyboardID) as! T
            }
            
            func initialViewController()-> UIViewController?{
                return instance.instantiateInitialViewController()
            }
        }
        
        static func instantiateFromAppStoryboard(appStoryboard:Appstoryboard) -> Self{
            
            return appStoryboard.viewcontroller(viewcontrollerClass: self)
        }
    
    func pushViewController(viewController:Any){
        self.navigationController?.pushViewController(viewController as! UIViewController, animated: true)
        
    }
    func popVc(){
         self.navigationController?.popViewController(animated: true)
    }
    
    func presentDetail(_ viewControllerToPresent: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.4
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromTop
        self.navigationController?.view.layer.add(transition, forKey: kCATransition)
        self.navigationController?.pushViewController(viewControllerToPresent, animated: false)
        
    }
    
    func dismissDetail() {
        let transition = CATransition()
        transition.duration = 0.4
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromBottom
        self.navigationController?.view.layer.add(transition, forKey:kCATransition)
        self.navigationController?.popViewController(animated: false)
    }
}

extension UIStoryboard {
    func instantiateVC<T: UIViewController>() -> T? {
        // get a class name and demangle for classes in Swift
        if let name = NSStringFromClass(T.self).components(separatedBy: ".").last {
            return instantiateViewController(withIdentifier: name) as? T
        }
        return nil
    }
    
}

enum StoryBoardID : String {
    case Main = "Main"
    case Dashboard = "Dashboard"
    case Specialities = "Specialities"
    case GuestDashboard = "GuestDashboard"
    case Retail = "Retail"
    case Payment = "Payment"
    case AboutApp = "AboutApp"
}

extension UIViewController
{
    class func instantiateFromStoryboard(_ name: StoryBoardID = .Main) -> Self
    {
        return instantiateFromStoryboardHelper(name.rawValue)
    }
    
    fileprivate class func instantiateFromStoryboardHelper<T>(_ name: String) -> T
    {
        let storyboard = UIStoryboard(name: name, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! T
        return controller
    }
    func presentAlert(withTitle title: String, message : String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.setTitle(font: UIFont(name: "TheSans-Plain", size: 20)!, color: (UIColor.init(displayP3Red: 0/255, green: 84/255, blue: 151/255, alpha: 1)))
        //let OK = NSAttributedString()
        
        
        //alertController.addAction(title: String)
        if LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            let OKAction = UIAlertAction(title: "حسنا", style: .default, handler: nil)
            OKAction.setValue(UIColor.init(displayP3Red: 0/255, green: 84/255, blue: 151/255, alpha: 1), forKey: "titleTextColor")
            alertController.addAction(OKAction)
        }else{
          let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            OKAction.setValue(UIColor.init(displayP3Red: 0/255, green: 84/255, blue: 151/255, alpha: 1), forKey: "titleTextColor")
            alertController.addAction(OKAction)
        }
        
        
        self.present(alertController, animated: true, completion: nil)
    }
}
