//
//  JsonDeserilizer.swift
//  iBlah-Blah
//
//  Created by Aditya Srivastava on 07/04/19.
//  Copyright © 2019 Aditya Srivastava. All rights reserved.
//

import Foundation

protocol JsonDeserilizer {
    init()
    mutating func deserilize(values : Dictionary<String,Any>?)
}
