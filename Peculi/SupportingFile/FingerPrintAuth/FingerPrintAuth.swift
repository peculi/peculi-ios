//
//  FingerPrintAuth.swift
//  Peculi
//
//  Created by iApp on 28/04/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import Foundation
import LocalAuthentication
import UIKit

class FingerprintAuth{
    
    static let share = FingerprintAuth()
    let vc = UIViewController()
    
    typealias FingerPrintCallBack = (( _ check: Bool, _ error: Error?, _ type: FingerprintConstants?)-> Void)
    
    func accessAuth(callback: @escaping FingerPrintCallBack){
        
        let manager = LAContext()
        
        let authMessage = "Authentication is needed to access App."
        manager.localizedFallbackTitle = ""
        
        var authError: NSError?
        
        if manager.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError){
            
            manager.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: authMessage) { (success, error) in
                
                
                if manager.biometryType == .touchID || manager.biometryType == .faceID{
                    
                    if success{
                        
                        DispatchQueue.main.async {
                            callback(true,nil,FingerprintConstants.success )
                        }
                        
                        
                    }
                    else{
                        
                        if error?.errorCode == -2{
                            
                            callback(false, error, FingerprintConstants.cancel)
                        }
                        else{
                        callback(false,error, FingerprintConstants.authenticationError)
                        }
                        
                        
                    }
                    
                    
                }
                else if manager.biometryType == .none{
                    
                    callback(false,error, FingerprintConstants.none)
                    
                    
                    
                }
                else{
                    
                    
                    callback(false,error,FingerprintConstants.authenticationError)
                    
                }
                
                
            }
            
        }
        
        else{
            
            print(authError?.errorMessage ?? "")
            callback(false, authError,FingerprintConstants.authenticationFailed)
            
        }
    }
    
}


