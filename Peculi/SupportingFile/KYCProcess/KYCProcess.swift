//
//  KYCProcess.swift
//  Peculi
//
//  Created by iApp on 07/07/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import Foundation
//import Netverify
import SVProgressHUD
import Alamofire


//class KYCProcess:NSObject, NetverifyViewControllerDelegate{
//    
//    static let share = KYCProcess()
//    var netVC: NetverifyViewController?
//    var refrenceID = 0
//    var uuid  = ""
//    var scanRefrenceID = ""
////    let rootViewController = UIApplication.shared.keyWindow!.rootViewController!
//    
//    
//    func topViewController() -> UIViewController{
//        if var topController = UIApplication.shared.keyWindow?.rootViewController {
//            while let presentedViewController = topController.presentedViewController {
//                topController = presentedViewController
//            }
//
//           return topController
//        }
//        else{
//            return UIViewController()
//        }
//        
//    }
//    
//    func getKYCUuidApiCall(){
//        
//        SVProgressHUD.show()
//        
//        let userID = UserDefaults.standard.string(forKey: UserDefaultConstants.UserID.rawValue) ?? ""
//        
//        let params:[String: Any] = ["user_id": userID]
//        
//        DataService.sharedInstance.postApiCall(endpoint: constantApis.getKYC_uuid_ID, params: params) { (response, error) in
//            SVProgressHUD.dismiss()
//            print(response ?? [:])
//            if response != nil{
//                let message = response?["message"] as? String ?? ""
//                let codeInt = response?["code"] as? Int ?? 0
//                let codeString = response?["code"] as? String ?? ""
//                if codeInt == 200 || codeString == "200"{
//                    if let result = response?["result"] as? [String: Any]{
//                        
//                        if let data = result["data"] as? [String: Any]{
//                            
//                            self.uuid = data["uuid"] as? String ?? ""
//                            
//                            
//                        }
//                        
//                        self.refrenceID = result["referenceId"] as? Int ?? 0
//                        
//                        self.setupNetVerifyProcess()
//                        
//                    }
//                }
//                else{
//                    self.topViewController().customAlertDismiss(title: "Message", message: message)
//                }
//            }
//            else{
//                self.topViewController().customAlertDismiss(title: "Message", message: "Something went wrong!")
//            }
//            
//            
//        }
//    }
//    
//    private func setupNetVerifyProcess(){
//        
//        
//        if JumioDeviceInfo.isJailbrokenDevice(){
//            print("isJailbrokenDevice")
//            return
//        }
//        
//
//        let config: NetverifyConfiguration = NetverifyConfiguration()
//        config.apiToken = "0e53fa71-b752-4dbc-b3ed-254dc6bf100b"
//        config.apiSecret = "T7gMCnZkIYsHzkwnvw43nm4H7O7M5rDT"
//        config.dataCenter = JumioDataCenterEU
//        config.enableVerification = true
//        config.enableIdentityVerification = true
//        config.cameraPosition = JumioCameraPositionBack
//        config.statusBarStyle = UIStatusBarStyle.lightContent
//        config.preselectedCountry = "UK"
////        config.enableIdentityVerification = true
//        var documentTypes: NetverifyDocumentType = []
//               documentTypes.insert(.visa)
//               documentTypes.insert(.driverLicense)
//               documentTypes.insert(.identityCard)
//               documentTypes.insert(.passport)
//               
//               config.preselectedDocumentTypes = documentTypes
//        config.preselectedDocumentVariant = .plastic
//        config.userReference = String(format: "%@%d", "IID_", refrenceID)
//        config.customerInternalReference = uuid
//        config.reportingCriteria = uuid
////        config.callbackUrl = "https://netverify.com/api/netverify/v2/scans/\(self.scanRefrenceID)"
//        config.delegate = self
//        
//        
//        do {
//            
//            try ObjcExceptionHelper.catchException({
//                self.netVC = NetverifyViewController(configuration: config)
//            })
//        } catch {
//            let err = error as NSError
//            print(err)
//            AlertView.simpleViewAlert(title: err.localizedDescription, err.userInfo[NSLocalizedFailureReasonErrorKey] as? String ?? "", topViewController())
//        }
//       
//        NetverifyBaseView.jumioAppearance().disableBlur = true
//        NetverifyBaseView.jumioAppearance().foregroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//        NetverifyDocumentSelectionButton.jumioAppearance().setBackgroundColor(.white, for: .normal)
//        NetverifyBaseView.jumioAppearance().backgroundColor = #colorLiteral(red: 0.07450980392, green: 0.1019607843, blue: 0.1529411765, alpha: 1)
//        UINavigationBar.jumioAppearance().backgroundColor = #colorLiteral(red: 0.07450980392, green: 0.1019607843, blue: 0.1529411765, alpha: 1)
//        UINavigationBar.jumioAppearance().tintColor = .white
//        UINavigationBar.jumioAppearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white] as [NSAttributedString.Key : Any]
//        NetverifyPositiveButton.jumioAppearance().setTitleColor(#colorLiteral(red: 0.07450980392, green: 0.1019607843, blue: 0.1529411765, alpha: 1), for: .normal)
//        print(NetverifyViewController.sdkVersion())
//    
////        NetverifyBaseView.jumioAppearance().customBoldFontName = "Montserrat-Bold"
////        NetverifyBaseView.jumioAppearance().customRegularFontName = "Montserrat-regular"
////        NetverifyBaseView.jumioAppearance().customMediumFontName = "Montserrat-medium"
////        NetverifyBaseView.jumioAppearance().customLightFontName = "Montserrat-semibold"
//        topViewController().present(netVC!, animated: true, completion: nil)
//        
//        
//    }
//    
//    func netverifyViewController(_ netverifyViewController: NetverifyViewController, didFinishInitializingWithError error: NetverifyError?) {
//           print(error?.message ?? "" )
//       }
//       
//       func netverifyViewController(_ netverifyViewController: NetverifyViewController, didFinishWith documentData: NetverifyDocumentData, scanReference: String, accountId: String?, authenticationResult: Bool) {
//        
//        print(accountId)
//        print(scanReference)
//        scanRefrenceID = scanReference
//        
//        let frontImageBase64 = documentData.frontImage?.jpegData(compressionQuality: 1)?.base64EncodedString() ?? ""
//        var documentTypeStr = String()
//        switch (documentData.selectedDocumentType) {
//        case .driverLicense:
//            documentTypeStr = "DRIVING_LICENSE"
//            break;
//        case .identityCard:
//            documentTypeStr = "ID_CARD"
//            break;
//        case .passport:
//            documentTypeStr = "PASSPORT"
//            break;
//        case .visa:
//            documentTypeStr = "VISA"
//            break;
//        default:
//            documentTypeStr = ""
//            break;
//        }
//        netVerifyCallBackAPICall(scanID: scanReference, frontImage: frontImageBase64, country: documentData.selectedCountry ?? "", idType: documentTypeStr)
//        let documentDetails = fetchDocumentData(documentData: documentData)
//        if authenticationResult == false{
//            topViewController().dismiss(animated: true) {
//                self.netVC?.destroy()
//                self.netVC = nil
//                self.topViewController().customAlertDismiss(title: "Message", message: documentDetails as String)
//            }
//        }
//        else{
//            
//            topViewController().dismiss(animated: true) {
//                self.netVC?.destroy()
//                self.netVC = nil
//                self.topViewController().customAlertDismiss(title: "message", message: documentDetails as String)
//            }
//        }
//           
//       }
//       
//       func netverifyViewController(_ netverifyViewController: NetverifyViewController, didCancelWithError error: NetverifyError?, scanReference: String?, accountId: String?) {
//        scanRefrenceID = scanReference ?? ""
//           print("sfwesfergF\(error?.message) ferfgergr --- \(scanReference)")
//           
//        topViewController().dismiss(animated: true){
//               self.netVC?.destroy()
//               self.netVC = nil
//           }
//       }
//    
//    func fetchDocumentData(documentData: NetverifyDocumentData) -> NSMutableString{
//        
//        let selectedCountry:String = documentData.selectedCountry ?? ""
//                let selectedDocumentType:NetverifyDocumentType = documentData.selectedDocumentType
//                var documentTypeStr:String
//                switch (selectedDocumentType) {
//                case .driverLicense:
//                    documentTypeStr = "DL"
//                    break;
//                case .identityCard:
//                    documentTypeStr = "ID"
//                    break;
//                case .passport:
//                    documentTypeStr = "PP"
//                    break;
//                case .visa:
//                    documentTypeStr = "Visa"
//                    break;
//                default:
//                    documentTypeStr = ""
//                    break;
//                }
//                
//                //id
//                let idNumber:String? = documentData.idNumber
//                let personalNumber:String? = documentData.personalNumber
//                let issuingDate:Date? = documentData.issuingDate
//                let expiryDate:Date? = documentData.expiryDate
//                let issuingCountry:String? = documentData.issuingCountry
//                let optionalData1:String? = documentData.optionalData1
//                let optionalData2:String? = documentData.optionalData2
//                
//                //person
//                let lastName:String? = documentData.lastName
//                let firstName:String? = documentData.firstName
//                let dateOfBirth:Date? = documentData.dob
//                let gender:NetverifyGender = documentData.gender
//                var genderStr:String;
//                switch (gender) {
//                    case .unknown:
//                        genderStr = "Unknown"
//                    
//                    case .F:
//                        genderStr = "female"
//                    
//                    case .M:
//                        genderStr = "male"
//                    
//                    case .X:
//                        genderStr = "Unspecified"
//                    
//                    default:
//                        genderStr = "Unknown"
//                }
//               
//                let originatingCountry:String? = documentData.originatingCountry
//                let placeOfBirth:String? = documentData.placeOfBirth
//                
//                //address
//                let street:String? = documentData.addressLine
//                let city:String? = documentData.city
//                let state:String? = documentData.subdivision
//                let postalCode:String? = documentData.postCode
//                
//                // Raw MRZ data
//                let mrzData:NetverifyMrzData? = documentData.mrzData
//                
//                let message:NSMutableString = NSMutableString.init()
//                message.appendFormat("Selected Country: %@", selectedCountry)
//                message.appendFormat("\nDocument Type: %@", documentTypeStr)
//                if (idNumber != nil) { message.appendFormat("\nID Number: %@", idNumber!) }
//                if (personalNumber != nil) { message.appendFormat("\nPersonal Number: %@", personalNumber!) }
//                if (issuingDate != nil) { message.appendFormat("\nIssuing Date: %@", issuingDate! as CVarArg) }
//                if (expiryDate != nil) { message.appendFormat("\nExpiry Date: %@", expiryDate! as CVarArg) }
//                if (issuingCountry != nil) { message.appendFormat("\nIssuing Country: %@", issuingCountry!) }
//                if (optionalData1 != nil) { message.appendFormat("\nOptional Data 1: %@", optionalData1!) }
//                if (optionalData2 != nil) { message.appendFormat("\nOptional Data 2: %@", optionalData2!) }
//                if (lastName != nil) { message.appendFormat("\nLast Name: %@", lastName!) }
//                if (firstName != nil) { message.appendFormat("\nFirst Name: %@", firstName!) }
//                if (dateOfBirth != nil) { message.appendFormat("\ndob: %@", dateOfBirth! as CVarArg) }
//                message.appendFormat("\nGender: %@", genderStr)
//                if (originatingCountry != nil) { message.appendFormat("\nOriginating Country: %@", originatingCountry!) }
//                if (placeOfBirth != nil) { message.appendFormat("\nPlace of birth: %@", placeOfBirth!) }
//                if (street != nil) { message.appendFormat("\nStreet: %@", street!) }
//                if (city != nil) { message.appendFormat("\nCity: %@", city!) }
//                if (state != nil) { message.appendFormat("\nState: %@", state!) }
//                if (postalCode != nil) { message.appendFormat("\nPostal Code: %@", postalCode!) }
//                if (mrzData != nil) {
//                    if (mrzData?.line1 != nil) {
//                    message.appendFormat("\nMRZ Data: %@\n", (mrzData?.line1)!)
//                    }
//                    if (mrzData?.line2 != nil) {
//                        message.appendFormat("%@\n", (mrzData?.line2)!)
//                    }
//                    if (mrzData?.line3 != nil) {
//                        message.appendFormat("%@\n", (mrzData?.line3)!)
//                    }
//                }
//        
//        
//        print(message)
//        return message
//        
//    }
//    
//    
//    private func netVerifyCallBackAPICall(scanID: String, frontImage: String, country: String,idType: String){
//        SVProgressHUD.show()
//        let url = "https://lon.netverify.com/api/netverify/v2/scans/" + scanID
//        
//        let headers:HTTPHeaders = [
//            "Accept": "application/json",
//            "Content-Type": "application/json",
//            "Authentication": "Basic MGU1M2ZhNzEtYjc1Mi00ZGJjLWIzZWQtMjU0ZGM2YmYxMDBiOlQ3Z01DblprSVlzSHprd252dzQzbm00SDdPN001ckRU",
//            "User-Agent": "Peculi Peculi/1.0"
////            "Content-Length": "1234"
//        ]
//        let params:[String: Any] = ["merchantIdScanReference" : scanID,
//                                    "frontsideImage": frontImage,
//                                    "country": country,
//                                    "idType": idType]
//        
//        AF.request(url, method: .post,parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
//            
//            SVProgressHUD.dismiss()
//            switch response.result {
//                
//            case .success(let value):
//                
//                let successItem = response.value as? [String:Any]
//                
//                print(successItem)
//                
//                break
//                
//            case .failure(let error):
//                
//                print(error.localizedDescription)
//                
//                break
//            }
//
//            
//        }
//        
//        
//    }
//    
//    
//    
//}
