//
//  NetworkMonitor.swift
//  Peculi
//
//  Created by apple on 24/02/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import Foundation
import Network
import NotificationBannerSwift

final class NetworkMonitor {
    static let shared = NetworkMonitor()
    
    var superView = UIView()
    var canShowTost = false
    private let queue = DispatchQueue.global()
    private let monitor : NWPathMonitor
    typealias callback = ((_ status: String) -> Void)
    public private(set) var isConnected = Bool()
    public private(set) var connectionType: ConnectionType = .unknown
    
    enum ConnectionType {
        case wifi
        case cellular
        case ethernet
        case unknown
    }
    
    private init(){
        monitor = NWPathMonitor()
    }
    
    public func startMonitoring(callback: @escaping callback){
           monitor.start(queue: queue)
           monitor.pathUpdateHandler = { path in
            
            //if self.canShowTost == true {
           // DispatchQueue.main.async {
            if path.status == .satisfied {
                if self.canShowTost == true {
                    DispatchQueue.main.async {
                        print("We're connected!")
                        callback(ReachabilityConstants.connected.rawValue)


                    }
                
                    
                }
                else{
                    callback(ReachabilityConstants.none.rawValue)
//                    self.canShowTost = true
                }
            }else if path.status == .unsatisfied {
                
                DispatchQueue.main.async {
                    print("No connection.")
                    self.canShowTost = true
                    callback(ReachabilityConstants.disconnected.rawValue)

                }
   
            }
            else{
                
                callback(ReachabilityConstants.disconnected.rawValue)
            }
            
          //  print(path.isExpensive)
           // print(path.status)
        }
        
        let queue = DispatchQueue(label: "Monitor")
        monitor.start(queue: queue)
       }
     
    public func stopMonitoring(){
        monitor.cancel()
    }
    
    private func getConnectionType(_ path: NWPath){
        if path.usesInterfaceType(.wifi){
            connectionType = .wifi
        }
        else if path.usesInterfaceType(.cellular){
            connectionType = .cellular
        }
        else if path.usesInterfaceType(.wiredEthernet){
            connectionType = .ethernet
        }
        else{
            connectionType = .unknown
        }
        //NotificationCenter.default.post(name: Notification.Name("MessageReceived"), object: nil)
    }
}
