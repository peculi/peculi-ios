//
//  CommonFunctions.swift
//  Peculi
//
//  Created by iApp on 05/08/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import Foundation
import UIKit

class CommonFunctions{
    
    
    static func customAlertDismiss(title: String, message: String){
        
        let storyboard = UIStoryboard(name: "Main", bundle:nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "CustomDefaultAlertViewController") as? CustomDefaultAlertViewController else {
            return
        }
        
        vc.setTransition()
        vc.titleVal = title
        vc.messageVal = message
        vc.completionHandler = {
            vc.dismiss(animated: true, completion: nil)
        }
        
        vc.present(vc, animated: true, completion: nil)
        
        
        
    }
    
}
