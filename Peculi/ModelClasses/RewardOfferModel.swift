//
//  RewardOfferModel.swift
//  Peculi
//
//  Created by iApp on 29/04/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import Foundation

struct RewardOfferModel: JsonDeserilizer {
    
    var _id:String = ""
    var created_at:String = ""
    var retailer_code:String = ""
    var discount = Int()
    var tier1 = Int()
    var tier2 = Int()
    var tier3 = Int()
    var premium = Int()
    var tier1_status = Int()
    var tier2_status = Int()
    var tier3_status = Int()
    var premium_status = Int()
    var status: String = ""
    var __v: String = ""
    var retailerDetails = RewardRetailerDetails()
    
    
    
    mutating func deserilize(values: Dictionary<String, Any>?) {
        
        _id = values?["_id"] as? String ?? ""
        created_at = values?["created_at"] as? String ?? ""
        retailer_code = values?["retailer_code"] as? String ?? ""
        discount = values?["discount"] as? Int ?? 0
        tier1 = values?["tier1"] as? Int ?? 0
        tier2 = values?["tier2"] as? Int ?? 0
        tier3 = values?["tier3"] as? Int ?? 0
        premium = values?["premium"] as? Int ?? 0
        tier1_status = values?["tier1_status"] as? Int ?? 0
        tier2_status = values?["tier2_status"] as? Int ?? 0
        tier3_status = values?["tier3_status"] as? Int ?? 0
        premium_status = values?["premium_status"] as? Int ?? 0
        status = values?["status"] as? String ?? ""
        __v = values?["__v"] as? String ?? ""
        
        if let retailData = values?["retailer_details"] as? Dictionary<String, Any>{
            
            retailerDetails.deserilize(values: retailData)
            
        }
        
        
    }
    
    
}


struct RewardRetailerDetails: JsonDeserilizer{
    
    var _id: String = ""
    var available_denominations = [String]()
    var categories = [String]()
    var countries = [String]()
    var created_at: String = ""
    var availability: String = ""
    var barcode_format: String = ""
    var card_image_url: String = ""
    var logo_image_url: String = ""
    var code: String = ""
    var currency_code: String = ""
    var denomination_type: String = ""
    var description: String = ""
    var e_code_usage_type: String = ""
    var expiry_date_policy: String = ""
    var expiry_in_months: String = ""
    var maximum_value: String = ""
    var minimum_value: String = ""
    var name: String = ""
    var percent_discount:String = ""
    var redeem_instructions_html: String = ""
    var redeemable_at: String = ""
    var state: String = ""
    var terms_and_conditions_pdf_url: String = ""
    var terms_and_conditions_html: String = ""
    var terms_and_conditions_url: String = ""
    var wrap_supported: String = ""
    var __v: String = ""
    
    
    
    
    
    
    
    
    mutating func deserilize(values: Dictionary<String, Any>?) {
        
        _id = values?["_id"] as? String ?? ""
        available_denominations = values?["available_denominations"] as? [String] ?? []
        categories = values?["categories"] as? [String] ?? []
        countries = values?["countries"] as? [String] ?? []
        created_at = values?["created_at"] as? String ?? ""
        availability = values?["availability"] as? String ?? ""
        barcode_format = values?["barcode_format"] as? String ?? ""
        card_image_url = values?["card_image_url"] as? String ?? ""
        logo_image_url = values?["logo_image_url"] as? String ?? ""
        code = values?["values"] as? String ?? ""
        currency_code = values?["currency_code"] as? String ?? ""
        denomination_type = values?["denomination_type"] as? String ?? ""
        description = values?["description"] as? String ?? ""
        e_code_usage_type = values?["e_code_usage_type"] as? String ?? ""
        expiry_date_policy = values?["expiry_date_policy"] as? String ?? ""
        expiry_in_months = values?["expiry_in_months"] as? String ?? ""
        maximum_value = values?["maximum_value"] as? String ?? ""
        minimum_value = values?["minimum_value"] as? String ?? ""
        name = values?["name"] as? String ?? ""
        percent_discount = values?["percent_discount"] as? String ?? ""
        redeem_instructions_html = values?["redeem_instructions_html"] as? String ?? ""
        redeemable_at = values?["redeemable_at"] as? String ?? ""
        state = values?["state"] as? String ?? ""
        terms_and_conditions_pdf_url = values?["terms_and_conditions_pdf_url"] as? String ?? ""
        terms_and_conditions_html = values?["terms_and_conditions_html"] as? String ?? ""
        
        terms_and_conditions_url = values?["terms_and_conditions_url"] as? String ?? ""
        
        wrap_supported = values?["wrap_supported"] as? String ?? ""
        __v = values?["__v"] as? String ?? ""
        
        
    }
    
    
    
    
    
}

struct RewardCategoryData:JsonDeserilizer{
    
    var _id: String = ""
    var created_at: String = ""
    var user_id: String = ""
    var category_id: String = ""
    var target: String = ""
    var weekly_target: String = ""
    var category_name: String = ""
    var __v: String = ""
    
    mutating func deserilize(values: Dictionary<String, Any>?) {
        
        _id = values?["_id"] as? String ?? ""
        created_at = values?["created_at"] as? String ?? ""
        user_id = values?["user_id"] as? String ?? ""
        category_id = values?["category_id"] as? String ?? ""
        target = values?["target"] as? String ?? ""
        weekly_target = values?["weekly_target"] as? String ?? ""
        category_name = values?["category_name"] as? String ?? ""
        __v = values?["__v"] as? String ?? ""
        
        
    }
    
    
    
    
}



