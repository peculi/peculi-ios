//
//  SignUpModelStructure.swift
//  Peculi
//
//  Created by PPI on 12/11/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//


import Foundation

struct Signup_ModelStructure {
    
    var signUpId: String
    var signupOtp: Int
    
    
    init(signUpId:String, signupOtp: Int) {
        self.signupOtp = signupOtp
        self.signUpId = signUpId
    }
    
}


struct allPostCodeData {
    
    var line_1 : String
    var town_or_city : String
    var country : String
    
    init(line_1:String, town_or_city:String, country:String) {
        
        self.line_1 = line_1
        self.town_or_city = town_or_city
        self.country = country

    }
    
}
