//
//  UserEditCategoryModel.swift
//  Peculi
//
//  Created by iApp on 08/06/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Alamofire
import SVProgressHUD

class UserEditCategoryModel: NSObject {

public func fetchInfo(_ user_id : String,_ category_id : String,_ target : String,_ weekly_target : String,completion : @escaping (( [EditCategoriesInfo] )->())){
    
            let params:[String:AnyObject] = [
                "user_id":user_id as AnyObject,
                "category_id":category_id as AnyObject,
                "target":target as AnyObject,
                "weekly_target":weekly_target as AnyObject,
               
               ]
    
    
        
               SVProgressHUD.show()
    
        let url = AppConfig.BaseUrl  + constantApis.userEditCategory
    
    
//            var authorizationToken = String()
//            authorizationToken = UserDefaults.standard.value(forKey: "accessToken") as! String
//
//            var token = String()
//            token = String(format: "Bearer %@",authorizationToken)
    
            let urL = URL(string: url)
            if let data = try? JSONSerialization.data(withJSONObject: params, options: .prettyPrinted),
                let jsonString = String(data: data, encoding: .utf8) {
                
                var request = URLRequest(url: urL!)
                request.httpMethod = HTTPMethod.post.rawValue
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                // request.setValue(token, forHTTPHeaderField: "Authorization")
                request.httpBody = jsonString.data(using: .utf8)
                
                
                AF.request(request)
                    .responseJSON { response in
                        switch response.result
                        {
                        case .failure(let error):
                            if let data = response.data {
                                print("Print Server Error: " + String(data: data, encoding: String.Encoding.utf8)!)
                            }
                            
                            SVProgressHUD.dismiss()
                            print(error)
                            
                        case .success(let value):
                            var dataUser:[EditCategoriesInfo] = []
                            SVProgressHUD.dismiss()
                            
                            do {
                                if let parseJSON = try JSONSerialization.jsonObject(with: response.data!)  as? Dictionary<String,AnyObject>,
                                    let resultValue = parseJSON["status"] as? String {
                                    let message = parseJSON["message"] as? String
                                    let status = parseJSON["status"] as? String
                                    if resultValue == "success" {
                                        
                                        print(parseJSON)
                                        
                                        var  addDictionary = Dictionary<String, Any> ()
                                        var sessionDateList = EditCategoriesInfo()
                                        
                                        addDictionary.updateValue(message as Any, forKey: "message")
                                        addDictionary.updateValue(status as Any, forKey: "Status")
                                        
                                        
                                        sessionDateList.deserilize(values:addDictionary)
                                        dataUser.append(sessionDateList)
                                        
                                        SVProgressHUD.dismiss()
                                        completion(dataUser)
                                    }
                                    else {
                                        if let code = parseJSON["code"] as? Int{
                                            
                                            if code == 403 {
                                                
                                                var sessionDateList = EditCategoriesInfo()
                                                var  addDictionary = Dictionary<String, Any> ()
                                                addDictionary.updateValue(message as Any, forKey: "message")
                                                addDictionary.updateValue(status as Any, forKey: "Status")
                                                sessionDateList.deserilize(values:addDictionary)
                                                dataUser.append(sessionDateList)
                                                SVProgressHUD.dismiss()
                                                completion(dataUser)
                                            }
                                                
                                            else  if code == 400{
                                                var sessionDateList = EditCategoriesInfo()
                                                var addDictionary = Dictionary<String, Any> ()
                                                addDictionary.updateValue(message as Any, forKey: "message")
                                                addDictionary.updateValue(status as Any, forKey: "Status")
                                                sessionDateList.deserilize(values:addDictionary)
                                                dataUser.append(sessionDateList)
                                                SVProgressHUD.dismiss()
                                                completion(dataUser)
                                            }
                                            else  if code == 401{
                                                var sessionDateList = EditCategoriesInfo()
                                                var  addDictionary = Dictionary<String, Any> ()
                                                addDictionary.updateValue(message as Any, forKey: "message")
                                                addDictionary.updateValue(status as Any, forKey: "Status")
                                                sessionDateList.deserilize(values:addDictionary)
                                                dataUser.append(sessionDateList)
                                                SVProgressHUD.dismiss()
                                                completion(dataUser)
                                            }
                                                
                                                
                                                
                                                
                                                
                                                
                                            else{
                                                
                                                SVProgressHUD.dismiss()
                                                
                                                completion(dataUser)
                                                
                                            }
                                        }
                                        else{
                                            
                                            SVProgressHUD.dismiss()
                                            
                                            completion(dataUser)
                                            
                                        }
                                    }
                                }else{
                                    SVProgressHUD.dismiss()
                                    completion(dataUser)
                                }
                                
                                
                                
                            } catch {
                                SVProgressHUD.dismiss()
                                print(error)
                                
                            }
                            
                        }
                }
                
            }else{
                SVProgressHUD.dismiss()
            }
    
    
        }
    
    
    
    }
    
    
    
    struct EditCategoriesInfo:JsonDeserilizer  {
    
       
        var message:String = ""
        var statuscheck:String = ""
       
        
  
    
        init() {
    
        }
    
        mutating func deserilize(values: Dictionary<String,Any>?) {
    
    
          
            message = values?["message"] as? String ?? ""
           
            statuscheck = values?["Status"] as? String ?? ""
            
            
         
            }
    
    }
