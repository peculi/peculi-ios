//
//  TransactionPopUpModel.swift
//  Peculi
//
//  Created by PPI on 4/27/22.
//  Copyright © 2022 Stealth Tech. All rights reserved.
//

import Foundation

struct TransactionPopUp_Model {
    
    var categoryImage: String
    var createdAt: String
    var time: String
    var amount: Double
    var transactionType: String
    
    init(categoryImage:String, createdAt: String, time:String, amount:Double, transactionType:String) {
        
        self.categoryImage = categoryImage
        self.createdAt = createdAt
        self.time = time
        self.amount = amount
        self.transactionType = transactionType

    }
}


struct potTransactionHistory_Model {
    
    var toCategoryName: String
    var toCategoryImage: String
    var fromCategoryName: String
    var fromCategoryImage: String
    var createdAt: String
    var amount: String
    
    init(createdAt: String,amount:String, toCategoryName:String,toCategoryImage:String,fromCategoryName:String,fromCategoryImage:String) {
        
        self.createdAt = createdAt
        self.amount = amount
        self.toCategoryName = toCategoryName
        self.toCategoryImage = toCategoryImage
        self.fromCategoryName = fromCategoryName
        self.fromCategoryImage = fromCategoryImage

    }

}




struct bankTransactionHistory_Model {
    
    var createdAt: String
    var toCategoryName: String
    var toCategoryImage: String
    var firstName: String
    var lastName: String
    var accountIdentifier: String
    var paymentAmount: String
    
    init(createdAt: String,toCategoryName:String,toCategoryImage:String,firstName:String,lastName:String,accountIdentifier:String,paymentAmount:String) {
        
        self.createdAt = createdAt
        self.toCategoryName = toCategoryName
        self.toCategoryImage = toCategoryImage
        self.firstName = firstName
        self.lastName = lastName
        self.accountIdentifier = accountIdentifier
        self.paymentAmount = paymentAmount

    }

}


struct DirectCreditHistory_Model {
    
    
    var createdAt: String
    var categoryImage: String
    var paymentAmount: String
    
    init(categoryImage:String, createdAt: String, paymentAmount:String) {
        
        self.categoryImage = categoryImage
        self.createdAt = createdAt
        self.paymentAmount = paymentAmount
       

    }
}






//"_id": "6268e00726a7fbcd4515556f",
//            "status": true,
//            "transactionStatus": true,
//            "createdAt": "2022-03-25",
//            "time": "7:43 AM",
//            "amount": 5,
//            "transactionType": "recurrence",
//            "transaction_id": "8abdc43d-7739-427b-b33a-55243d2e6941",
//            "merchent_token": "2b57ae0c-66d8-46df-bafd-d1ca8636c2f5",
//            "categoryName": "Bookseller",
//            "categoryImage": "ic_bookseller_pot1.png"
