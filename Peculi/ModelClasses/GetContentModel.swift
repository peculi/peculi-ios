//
//  GetContentModel.swift
//  Peculi
//
//  Created by Stealth on 20/11/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import SVProgressHUD

class GetContentModel: NSObject {
    public func fetchInfo(_ type : String,completion : @escaping (( [ContentInfo] )->())){
        
                let params:[String:AnyObject] = [
                    "type":type as AnyObject,
                   
                   ]
        
        
            
                   SVProgressHUD.show()
        
            let url = AppConfig.BaseUrl  + constantApis.GetContentApi
        
        
    //            var authorizationToken = String()
    //            authorizationToken = UserDefaults.standard.value(forKey: "accessToken") as! String
    //
    //            var token = String()
    //            token = String(format: "Bearer %@",authorizationToken)
        
                let urL = URL(string: url)
                if let data = try? JSONSerialization.data(withJSONObject: params, options: .prettyPrinted),
                    let jsonString = String(data: data, encoding: .utf8) {
        
                    var request = URLRequest(url: urL!)
                    request.httpMethod = HTTPMethod.post.rawValue
                    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                   // request.setValue(token, forHTTPHeaderField: "Authorization")
                   request.httpBody = jsonString.data(using: .utf8)
        
        
                    AF.request(request)
                        .responseJSON { response in
                            switch response.result
                            {
                            case .failure(let error):
                                if let data = response.data {
                                    print("Print Server Error: " + String(data: data, encoding: String.Encoding.utf8)!)
                                }
                                
                                SVProgressHUD.dismiss()
                                print(error)
        
                            case .success(let value):
                                var dataUser:[ContentInfo] = []
                                 SVProgressHUD.dismiss()
        
                                do {
                                    if let parseJSON = try JSONSerialization.jsonObject(with: response.data!)  as? Dictionary<String,AnyObject>,
                                        let resultValue = parseJSON["status"] as? String {
                                        let message = parseJSON["message"] as? String
                                        let status = parseJSON["status"] as? String
                                        if resultValue == "success"{
                                       
                                           // let message = parseJSON["message"] as? String
                                       if let dataList = parseJSON["result"] as? Dictionary<String, Any> {
        
                                        print(parseJSON)
        
                                            var  addDictionary = Dictionary<String, Any> ()
                                             
                                                var sessionDateList = ContentInfo()
                                        addDictionary = dataList as? Dictionary<String, Any> ?? [:]
                                                addDictionary.updateValue(message as Any, forKey: "message")
                                                addDictionary.updateValue(status as Any, forKey: "Status")
                                           
        
        
                                                sessionDateList.deserilize(values:addDictionary)
                                                dataUser.append(sessionDateList)
                                        
                                        
                                        SVProgressHUD.dismiss()
                                        completion(dataUser)
                                       }else{
                                        SVProgressHUD.dismiss()
                                        completion(dataUser)
                                            }
                                       
                                       
                                    }
                                    else{
                                            if let code = parseJSON["code"] as? Int{
        
                                                if code == 403{
                                                   var sessionDateList = ContentInfo()
                                                    var  addDictionary = Dictionary<String, Any> ()
                                                    addDictionary.updateValue(message as Any, forKey: "message")
                                                    addDictionary.updateValue(status as Any, forKey: "Status")
                                                    sessionDateList.deserilize(values:addDictionary)
                                                    dataUser.append(sessionDateList)
                                                     SVProgressHUD.dismiss()
                                                    completion(dataUser)
                                                }
                                                
                                                else  if code == 400{
                                                var sessionDateList = ContentInfo()
                                                var addDictionary = Dictionary<String, Any> ()
                                                                                               addDictionary.updateValue(message as Any, forKey: "message")
                                                                                               addDictionary.updateValue(status as Any, forKey: "Status")
                                                                                               sessionDateList.deserilize(values:addDictionary)
                                                                                               dataUser.append(sessionDateList)
                                                                                                SVProgressHUD.dismiss()
                                                                                               completion(dataUser)
                                                                                           }
                                                    else  if code == 401{
                                                                                                  var sessionDateList = ContentInfo()
                                                                                                   var  addDictionary = Dictionary<String, Any> ()
                                                                                                   addDictionary.updateValue(message as Any, forKey: "message")
                                                                                                   addDictionary.updateValue(status as Any, forKey: "Status")
                                                                                                   sessionDateList.deserilize(values:addDictionary)
                                                                                                   dataUser.append(sessionDateList)
                                                                                                    SVProgressHUD.dismiss()
                                                                                                   completion(dataUser)
                                                                                               }
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                else{
        
                                                   SVProgressHUD.dismiss()
        
                                                    completion(dataUser)
        
                                                }
                                            }
                                            else{
        
                                                 SVProgressHUD.dismiss()
        
                                                completion(dataUser)
        
                                            }
                                        }
                                    }else{
                                         SVProgressHUD.dismiss()
                                      completion(dataUser)
                                    }
        
        
        
                                } catch {
                                     SVProgressHUD.dismiss()
                                    print(error)
        
                                }
        
                            }
                    }
        
                }else{
                    SVProgressHUD.dismiss()
                }
        
        
            }
        
        
        
        }
        
        
        
        struct ContentInfo:JsonDeserilizer  {
        
           
            var message:String = ""
            var statuscheck:String = ""
            var _id:String = ""
            var created_at:String = ""
            var title:String = ""
            var content:String = ""
            var type:String = ""
            var __v:Int = 0
           
            
                
        
           
        
        
            init() {
        
            }
        
            mutating func deserilize(values: Dictionary<String,Any>?) {
                message = values?["message"] as? String ?? ""
               
                statuscheck = values?["Status"] as? String ?? ""
                _id = values?["_id"] as? String ?? ""
                
                 created_at = values?["created_at"] as? String ?? ""
                title = values?["title"] as? String ?? ""
                
                 content = values?["content"] as? String ?? ""
                type = values?["type"] as? String ?? ""
                
                 __v = values?["__v"] as? Int ?? 0
        
                }
        
        }

   
