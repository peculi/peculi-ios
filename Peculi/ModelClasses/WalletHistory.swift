//
//  WalletHistory.swift
//  Peculi
//
//  Created by iApp on 10/02/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import Foundation

//struct WalletHistory{
//    var amount: String = ""
//    var expiryDate: String = ""
//    var walletHistory: NSDictionary = [:]
//
//    init(_ dict: [String:Any]) {
//        if let value = dict["amount"] as? String{
//            self.amount = value
//        }
//        if let value = dict["expiry_date"] as? String{
//                   self.expiryDate = value
//               }
//        if let value = dict["amount"] as? String{
//                   self.amount = value
//               }
//        if let value = dict["product_details"] as? NSDictionary{
//                          self.walletHistory = value
//
//                      }
//
//
//
//    }
//}
//struct WalletHistory {
//
//    var name: String = ""
//    var logo_image_url: String = ""
//    var description: String = ""
//    var amount: String = ""
//    var cardImageURL: String = ""
////    var logoImageURL: String
//    var code: String = ""
//    var currencyCode: String = ""
//    var denomenationType: String = ""
//    var expiryPolicy: String = ""
//    var expiryMonths: String = ""
//    var maxValue: String = ""
//    var minValue: String = ""
//    var percentDiscount: String = ""
//    var redeemInstructionsHTML: String = ""
//    var termsAndConditionsHTML: String = ""
//
//
//    init(_ dict: [String: Any])
//    {
//        if let value = dict["name"] as? String{
//            self.name = value
//        }
//        if let value = dict["logo_image_url"] as? String{
//            self.logo_image_url = value
//        }
//        if let value = dict["description"] as? String{
//            self.description = value
//        }
//        if let value = dict["amount"] as? String{
//            self.amount = value
//        }
//        if let value = dict["card_image_url"] as? String{
//            self.cardImageURL = value
//        }
//        if let value = dict["code"] as? String{
//            self.code = value
//        }
//        if let value = dict["currency_code"] as? String{
//            self.currencyCode = value
//        }
//        if let value = dict["denomination_type"] as? String{
//            self.denomenationType = value
//        }
//        if let value = dict["expiry_date_policy"] as? String{
//            self.expiryPolicy = value
//        }
//        if let value = dict["expiry_in_months"] as? String{
//            self.expiryMonths = value
//        }
//        if let value = dict["maximum_value"] as? String{
//            self.maxValue = value
//        }
//        if let value = dict["minimum_value"] as? String{
//            self.minValue = value
//        }
//        if let value = dict["percent_discount"] as? String{
//            self.percentDiscount = value
//        }
//        if let value = dict["redeem_instructions_html"] as? String{
//            self.redeemInstructionsHTML = value
//        }
//        if let value = dict["terms_and_conditions_html"] as? String{
//            self.termsAndConditionsHTML = value
//        }
//
//
//
//
//
//
//    }
//}

struct WalletHistory {
    var voucherId:String
    var amount: String
    var expiryDate: String
    var voucherUrl:String
    var product_Data:[productData]
    var categoryDetails:WalletCategoryDetails
    
    init(voucherId :String,amount:String, expiryDate:String,voucherUrl:String, product_Data: [productData], categoryData: WalletCategoryDetails) {
        self.voucherId = voucherId
        self.amount = amount
        self.expiryDate = expiryDate
        self.voucherUrl = voucherUrl
        self.product_Data = product_Data
        self.categoryDetails = categoryData
     }
    
}

struct productData {

    var name: String = ""
    var logo_image_url: String = ""
    var description: String = ""
    
    var cardImageURL: String = ""
//    var logoImageURL: String
    var code: String = ""
    var currencyCode: String = ""
    var denomenationType: String = ""
    var expiryPolicy: String = ""
    var expiryMonths: String = ""
    var maxValue: String = ""
    var minValue: String = ""
    var percentDiscount: String = ""
    var redeemInstructionsHTML: String = ""
    var termsAndConditionsHTML: String = ""

    init(name:String, logo_image_url:String, description: String,cardImageURL:String, code:String, currencyCode: String,denomenationType:String, expiryPolicy:String, expiryMonths: String,maxValue:String, minValue:String, percentDiscount: String,redeemInstructionsHTML:String, termsAndConditionsHTML:String) {
       self.name = name
       self.logo_image_url = logo_image_url
       self.description = description
        self.cardImageURL = cardImageURL
        self.code = code
        self.currencyCode = currencyCode
        self.denomenationType = denomenationType
        self.expiryPolicy = expiryPolicy
        self.expiryMonths = expiryMonths
        self.maxValue = maxValue
        self.minValue = minValue
        self.percentDiscount = percentDiscount
        self.redeemInstructionsHTML = redeemInstructionsHTML
        self.termsAndConditionsHTML = termsAndConditionsHTML
        
      
    }
    
}

struct WalletCategoryDetails{
    
    var _id: String = ""
    var name: String = ""
    var new_name: String = ""
    
    init(_id: String, name: String, newName: String) {
        self._id = _id
        self.name = name
        self.new_name = newName
    }
    
}
