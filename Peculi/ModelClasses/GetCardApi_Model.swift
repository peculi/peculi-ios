//
//  GetCardApi_Model.swift
//  Peculi
//
//  Created by PPI on 4/14/22.
//  Copyright © 2022 Stealth Tech. All rights reserved.
//

import Foundation
struct GetCArd_ModelStructure {
    
    var isEnable: Bool
    var _id: String
    var cardName: String
    var primaryCard: Bool
    var merchentToken: String
    var userId: String
    var createdDate: String
    var updatedDate: String
    var __v: Int
    
    init(isEnable:Bool, _id: String, cardName:String, primaryCard:Bool, merchentToken:String, userId:String, createdDate:String, updatedDate:String, __v:Int ) {
        
        self.isEnable = isEnable
        self._id = _id
        self.cardName = cardName
        self.primaryCard = primaryCard
        self.merchentToken = merchentToken
        self.userId = userId
        self.createdDate = createdDate
        self.updatedDate = updatedDate
        self.__v = __v
      
    }
}

