//
//  SpecialOfferModel.swift
//  Peculi
//
//  Created by iApp on 30/04/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import Foundation

struct SpecialOfferModel: JsonDeserilizer{
    
    var __v: String = ""
    var _id: String = ""
    var category_id: String = ""
    var created_at: String = ""
    var end_date: String = ""
    var id: String = ""
    var percentage: String = ""
    var potValue: String = ""
    var start_date: String = ""
    var status: Int = 0
    var user_id: String = ""
    var voucher_code: String = ""
    var voucherDetails = SpecialVoucherDetails()
    
    
    mutating func deserilize(values: Dictionary<String, Any>?) {
        
        __v = values?["__v"] as? String ?? ""
        _id = values?["_id"] as? String ?? ""
        category_id = values?["category_id"] as? String ?? ""
        created_at = values?["created_at"] as? String ?? ""
        end_date = values?["end_date"] as? String ?? ""
        id = values?["id"] as? String ?? ""
        percentage = values?["percentage"] as? String ?? ""
        potValue = values?["pot_value"] as? String ?? ""
        start_date = values?["start_date"] as? String ?? ""
        status = values?["status"] as? Int ?? 0
        user_id = values?["user_id"] as? String ?? ""
        voucher_code = values?["voucher_code"] as? String ?? ""
        
        if let voucherData = values?["voucher_details"] as? Dictionary<String, Any>{
            
            voucherDetails.deserilize(values: voucherData)
            
        }
        
        
        
        
        
    }
    
    
    
}

struct SpecialVoucherDetails: JsonDeserilizer {
    
    var _id: String = ""
    var available_denominations = [String]()
    var code: String = ""
    var currency_code: String = ""
    var denomination_type: String = ""
    var description: String = ""
    var logo_image_url: String = ""
    var maximum_value: String = ""
    var minimum_value: String = ""
    var name: String = ""
    var percent_discount: String = ""
    var terms_and_conditions_html: String = ""
    
    
    
    mutating func deserilize(values: Dictionary<String, Any>?) {
        
        
        _id = values?["_id"] as? String ?? ""
        available_denominations = values?["available_denominations"] as? [String] ?? []
        code = values?["code"] as? String ?? ""
        currency_code = values?["currency_code"] as? String ?? ""
        denomination_type = values?["denomination_type"] as? String ?? ""
        description = values?["description"] as? String ?? ""
        logo_image_url = values?["logo_image_url"] as? String ?? ""
        maximum_value = values?["maximum_value"] as? String ?? ""
        minimum_value = values?["minimum_value"] as? String ?? ""
        name = values?["name"] as? String ?? ""
        percent_discount = values?["percent_discount"] as? String ?? ""
        terms_and_conditions_html = values?["terms_and_conditions_html"] as? String ?? ""
        
        
    }
    
    
}
