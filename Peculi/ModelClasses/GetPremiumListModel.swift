//
//  GetPremiumList.swift
//  Peculi
//
//  Created by iApp on 01/04/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import Foundation
import SVProgressHUD
import Alamofire

class GetPremiumListModel: NSObject{
    
    
    
    public func fetchInfo(completion : @escaping (( [GetPremiumInfo] )->())){
        
        
        
        
        
        SVProgressHUD.show()
        
        let url = AppConfig.BaseUrl  + constantApis.getPremiumList
        
        
        //            var authorizationToken = String()
        //            authorizationToken = UserDefaults.standard.value(forKey: "accessToken") as! String
        //
        //            var token = String()
        //            token = String(format: "Bearer %@",authorizationToken)
        
        let urL = URL(string: url)
        if let data = try? JSONSerialization.data(withJSONObject: [], options: .prettyPrinted),
            let jsonString = String(data: data, encoding: .utf8) {
            
            var request = URLRequest(url: urL!)
            request.httpMethod = HTTPMethod.post.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            //request.setValue(token, forHTTPHeaderField: "Authorization")
            request.httpBody = jsonString.data(using: .utf8)
            
            
            AF.request(request)
                .responseJSON { response in
                    switch response.result
                    {
                    case .failure(let error):
                        if let data = response.data {
                            print("Print Server Error: " + String(data: data, encoding: String.Encoding.utf8)!)
                        }
                        
                        SVProgressHUD.dismiss()
                        print(error)
                        
                    case .success(let value):
                        var dataUser:[GetPremiumInfo] = []
                        SVProgressHUD.dismiss()
                        
                        do {
                            if let parseJSON = try JSONSerialization.jsonObject(with: response.data!)  as? Dictionary<String,AnyObject>,
                                let resultValue = parseJSON["status"] as? String {
                                let message = parseJSON["message"] as? String
                                let status = parseJSON["status"] as? String
                                if resultValue == "success"{
                                    
                                    // let message = parseJSON["message"] as? String
                                    if let dataList = parseJSON["result"] as? NSArray{
                                        
                                        print(parseJSON)
                                        
                                        var  addDictionary = Dictionary<String, Any> ()
                                        for item in dataList{
                                            var sessionDateList = GetPremiumInfo()
                                            addDictionary = item as! Dictionary<String, Any>
                                            addDictionary.updateValue(message as Any, forKey: "message")
                                            addDictionary.updateValue(status as Any, forKey: "resultValue")
                                            
                                            
                                            
                                            sessionDateList.deserilize(values:addDictionary)
                                            dataUser.append(sessionDateList)
                                        }
                                        
                                        SVProgressHUD.dismiss()
                                        completion(dataUser)
                                    }else{
                                        SVProgressHUD.dismiss()
                                        completion(dataUser)
                                    }
                                    
                                    
                                }
                                else{
                                    if let code = parseJSON["code"] as? Int{
                                        
                                        if code == 403{
                                            var sessionDateList = GetPremiumInfo()
                                            var  addDictionary = Dictionary<String, Any> ()
                                            addDictionary.updateValue(message as Any, forKey: "message")
                                            addDictionary.updateValue(status as Any, forKey: "resultValue")
                                            sessionDateList.deserilize(values:addDictionary)
                                            dataUser.append(sessionDateList)
                                            SVProgressHUD.dismiss()
                                            completion(dataUser)
                                        }
                                            
                                        else  if code == 400{
                                            var sessionDateList = GetPremiumInfo()
                                            var addDictionary = Dictionary<String, Any> ()
                                            addDictionary.updateValue(message as Any, forKey: "message")
                                            addDictionary.updateValue(status as Any, forKey: "resultValue")
                                            sessionDateList.deserilize(values:addDictionary)
                                            dataUser.append(sessionDateList)
                                            SVProgressHUD.dismiss()
                                            completion(dataUser)
                                        }
                                        else  if code == 401{
                                            var sessionDateList = GetPremiumInfo()
                                            var  addDictionary = Dictionary<String, Any> ()
                                            addDictionary.updateValue(message as Any, forKey: "message")
                                            addDictionary.updateValue(status as Any, forKey: "resultValue")
                                            sessionDateList.deserilize(values:addDictionary)
                                            dataUser.append(sessionDateList)
                                            SVProgressHUD.dismiss()
                                            completion(dataUser)
                                        }
                                            
                                            
                                            
                                            
                                            
                                            
                                        else{
                                            
                                            SVProgressHUD.dismiss()
                                            
                                            completion(dataUser)
                                            
                                        }
                                    }
                                    else{
                                        
                                        SVProgressHUD.dismiss()
                                        
                                        completion(dataUser)
                                        
                                    }
                                }
                            }else{
                                SVProgressHUD.dismiss()
                                completion(dataUser)
                            }
                            
                            
                            
                        } catch {
                            SVProgressHUD.dismiss()
                            print(error)
                            
                        }
                        
                    }
            }
            
        }else{
            SVProgressHUD.dismiss()
        }
        
        
    }
    
    
    
    
    
}


struct GetPremiumInfo: JsonDeserilizer {
    
    var _id: String = ""
    var created_at: String = ""
    var name: String = ""
    var description: String = ""
    var price: String = ""
    var plan_type: String = ""
    var plan_days: String = ""
    var status: String = ""
    var __v: String = ""
    var message: String = ""
    var code = Int()
    var statusCheck = ""
    
    
    init() {
        
    }
    mutating func deserilize(values: Dictionary<String,Any>?) {
        
        _id = values?["_id"] as? String ?? ""
        created_at = values?["created_at"] as? String ?? ""
        name = values?["name"] as? String ?? ""
        description = values?["description"] as? String ?? ""
        price = values?["price"] as? String ?? ""
        plan_type = values?["plan_type"] as? String ?? ""
        plan_days = values?["plan_days"] as? String ?? ""
        status = values?["status"] as? String ?? ""
        __v = values?["__v"] as? String ?? ""
        message = values?["message"] as? String ?? ""
        statusCheck = values?["resultValue"] as? String ?? ""
        
        
        
    }
    
}








