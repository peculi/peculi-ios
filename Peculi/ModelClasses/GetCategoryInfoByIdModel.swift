//
//  GetCategoryInfoByIdModel.swift
//  Peculi
//
//  Created by iApp on 25/06/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import Foundation

struct GetCategoryInfoByIdModel: JsonDeserilizer{
    var totalAmount: Double = 0.00
    var userData = GetCatInfoUserdata()
    
    mutating func deserilize(values: Dictionary<String, Any>?) {
        totalAmount = values?["total_amount"] as? Double ?? 0.00
        if let userDetails = values?["user_data"] as? Dictionary<String, Any>{
            
            userData.deserilize(values: userDetails)
            
        }
    }
    
    struct GetCatInfoUserdata: JsonDeserilizer{
        
        var categoryDetails = GetCatInfoCategoryDetails()
        var category_id: String = ""
        var categoryName: String = ""
        var createdAt: String = ""
        var id: String = ""
        var target: String = ""
        var userID: String = ""
        var weeklyTarget: String = ""
        
        mutating func deserilize(values: Dictionary<String, Any>?) {
            if let catData = values?["category_details"] as? Dictionary<String, Any>{
                
                categoryDetails.deserilize(values: catData)
                
            }
            category_id = values?["category_id"] as? String ?? ""
            categoryName = values?["category_name"] as? String ?? ""
            createdAt = values?["created_at"] as? String ?? ""
            id = values?["id"] as? String ?? ""
            target = values?["target"] as? String ?? ""
            userID = values?["user_id"] as? String ?? ""
            weeklyTarget = values?["weekly_target"] as? String ?? ""
        }
        
        
        
        
    }
    
    
    struct GetCatInfoCategoryDetails: JsonDeserilizer {
        var __v: String = ""
        var _id: String = ""
        var createdAt: String = ""
        var image: String = ""
        var name: String = ""
        var newName: String = ""
        var slug: String = ""
        var status: String = ""
        var tier1: String = ""
        var tier2: String = ""
        var tier3: String = ""
        
        //values
        mutating func deserilize(values: Dictionary<String, Any>?) {
            __v = values?["__v"] as? String ?? ""
            _id = values?["_id"] as? String ?? ""
            createdAt = values?["created_at"] as? String ?? ""
            image = values?["image"] as? String ?? ""
            name = values?["name"] as? String ?? ""
            newName = values?["new_name"] as? String ?? ""
            slug = values?["slug"] as? String ?? ""
            status = values?["status"] as? String ?? ""
            tier1 = values?["tier1"] as? String ?? ""
            tier2 = values?["tier2"] as? String ?? ""
            tier3 = values?["tier3"] as? String ?? ""
        }
        
        
    }
    
    
    
    
    
}
