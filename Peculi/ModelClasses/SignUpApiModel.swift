//
//  SignUpApiModel.swift
//  Peculi
//
//  Created by Stealth on 17/11/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import SVProgressHUD

class SignupApiModel: NSObject{
    
    public func signUpApiCall(param:[String:Any], callback: @escaping (([String:Any]) -> Void)){
        
        SVProgressHUD.show()
        
        DataService.sharedInstance.postApiCall(endpoint: constantApis.signup, params: param) { (response, error) in
            
            SVProgressHUD.dismiss()
            
            print(response)
            
            callback(response ?? [:])
            
        }
    }
}
