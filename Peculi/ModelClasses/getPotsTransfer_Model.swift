

import Foundation
struct GetPotsTransferModel {
    var fromid: String
    var categoryBalance: String
    var category_details:category_details
//    var fromCatId:String
//    var name:String
//    var image:String
//

    init(fromid:String,categoryBalance:String,category_details:category_details) {
        
        self.fromid = fromid
        self.categoryBalance = categoryBalance
        self.category_details = category_details
//        self.fromCatId = fromCatId
//        self.name = name
//        self.image = image
       
    }
}



struct category_details{
    
    var fromCatId: String = ""
    var name: String = ""
    var image: String = ""
    
    init(fromCatId: String, name: String, image: String) {
        self.fromCatId = fromCatId
        self.name = name
        self.image = image
    }
}
