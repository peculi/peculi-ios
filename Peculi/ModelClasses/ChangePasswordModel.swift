//
//  ChangePasswordModel.swift
//  Peculi
//
//  Created by Stealth on 18/11/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import SVProgressHUD

class ChangePasswordModel: NSObject {
    public func fetchInfo(_ password : String,_ old_password : String, _ email : String,completion : @escaping (( [ChangePasswordInfo] )->())){
        
        let params:[String:AnyObject] = [
            "password":password as AnyObject,
            "old_password":old_password as AnyObject,
            "email":email as AnyObject,
        ]
        
        
        
        SVProgressHUD.show()
        
        let url = AppConfig.BaseUrl  + constantApis.ChangePasswordApi
        
        
        //            var authorizationToken = String()
        //            authorizationToken = UserDefaults.standard.value(forKey: "accessToken") as! String
        //
        //            var token = String()
        //            token = String(format: "Bearer %@",authorizationToken)
        
        let urL = URL(string: url)
        if let data = try? JSONSerialization.data(withJSONObject: params, options: .prettyPrinted),
           let jsonString = String(data: data, encoding: .utf8) {
            
            var request = URLRequest(url: urL!)
            request.httpMethod = HTTPMethod.post.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            // request.setValue(token, forHTTPHeaderField: "Authorization")
            request.httpBody = jsonString.data(using: .utf8)
            
            
            AF.request(request)
                .responseJSON { response in
                    switch response.result
                    {
                    case .failure(let error):
                        if let data = response.data {
                            print("Print Server Error: " + String(data: data, encoding: String.Encoding.utf8)!)
                        }
                        
                        SVProgressHUD.dismiss()
                        print(error)
                        
                    case .success(let value):
                        var dataUser:[ChangePasswordInfo] = []
                        SVProgressHUD.dismiss()
                        
                        do {
                            if let parseJSON = try JSONSerialization.jsonObject(with: response.data!)  as? Dictionary<String,AnyObject>,
                               let resultValue = parseJSON["status"] as? String {
                                let message = parseJSON["message"] as? String
                                let status = parseJSON["status"] as? String
                                let code = parseJSON["code"] as? Int
                                if code == 200{
                                    
                                    // let message = parseJSON["message"] as? String
                                    
                                    
                                    print(parseJSON)
                                    
                                    var  addDictionary = Dictionary<String, Any> ()
                                    
                                    var sessionDateList = ChangePasswordInfo()
                                    
                                    addDictionary.updateValue(message as Any, forKey: "message")
                                    addDictionary.updateValue(status as Any, forKey: "Status")
                                    addDictionary.updateValue(code as Any, forKey: "code")
                                    
                                    
                                    
                                    sessionDateList.deserilize(values:addDictionary)
                                    dataUser.append(sessionDateList)
                                    
                                    
                                    SVProgressHUD.dismiss()
                                    completion(dataUser)
                                }
                                else{
                                    if let code = parseJSON["code"] as? Int{
                                        
                                        if code == 403{
                                            var sessionDateList = ChangePasswordInfo()
                                            var  addDictionary = Dictionary<String, Any> ()
                                            addDictionary.updateValue(message as Any, forKey: "message")
                                            addDictionary.updateValue(status as Any, forKey: "Status")
                                            addDictionary.updateValue(code as Any, forKey: "code")
                                            sessionDateList.deserilize(values:addDictionary)
                                            dataUser.append(sessionDateList)
                                            SVProgressHUD.dismiss()
                                            completion(dataUser)
                                        }
                                        
                                        else  if code == 400{
                                            var sessionDateList = ChangePasswordInfo()
                                            var addDictionary = Dictionary<String, Any> ()
                                            addDictionary.updateValue(message as Any, forKey: "message")
                                            addDictionary.updateValue(status as Any, forKey: "Status")
                                            addDictionary.updateValue(code as Any, forKey: "code")
                                            sessionDateList.deserilize(values:addDictionary)
                                            dataUser.append(sessionDateList)
                                            SVProgressHUD.dismiss()
                                            completion(dataUser)
                                        }
                                        else  if code == 401{
                                            var sessionDateList = ChangePasswordInfo()
                                            var  addDictionary = Dictionary<String, Any> ()
                                            addDictionary.updateValue(message as Any, forKey: "message")
                                            addDictionary.updateValue(status as Any, forKey: "Status")
                                            addDictionary.updateValue(code as Any, forKey: "code")
                                            sessionDateList.deserilize(values:addDictionary)
                                            dataUser.append(sessionDateList)
                                            SVProgressHUD.dismiss()
                                            completion(dataUser)
                                        }
                                        
                                        
                                        
                                        
                                        
                                        
                                        else{
                                            
                                            SVProgressHUD.dismiss()
                                            
                                            completion(dataUser)
                                            
                                        }
                                    }
                                    else{
                                        
                                        SVProgressHUD.dismiss()
                                        
                                        completion(dataUser)
                                        
                                    }
                                }
                            }else{
                                SVProgressHUD.dismiss()
                                completion(dataUser)
                            }
                            
                            
                            
                        } catch {
                            SVProgressHUD.dismiss()
                            print(error)
                            
                        }
                        
                    }
                }
            
        }else{
            SVProgressHUD.dismiss()
        }
        
        
    }
    
    
    
}



struct ChangePasswordInfo:JsonDeserilizer  {
    
    
    var message:String = ""
    
    var statuscheck:String = ""
    var code = Int()
    
    
    
    
    init() {
        
    }
    
    mutating func deserilize(values: Dictionary<String,Any>?) {
        
        
        
        message = values?["message"] as? String ?? ""
        
        statuscheck = values?["Status"] as? String ?? ""
        
        code = values?["code"] as? Int ?? 0
        
        
        
    }
    
}





