//
//  OurRetailersModel.swift
//  Peculi
//
//  Created by iApp on 13/07/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import Foundation

struct OurRetailersModel: JsonDeserilizer {
    var _id: String = ""
    var logo_image_url: String = ""
    var code: String = ""
    var name: String = ""
    mutating func deserilize(values: Dictionary<String, Any>?) {
        _id = values?["_id"] as? String ?? ""
        logo_image_url = values?["logo_image_url"] as? String ?? ""
        code = values?["code"] as? String ?? ""
        name = values?["name"] as? String ?? ""
        
    }    
    
}

class RetailersData: NSObject{
    
    public func fetchRetailersDetails(callback: @escaping (([OurRetailersModel], String) -> Void)){
        
        DataService.sharedInstance.getAllVouchersImages { (response, error) in
            var retailerList = [OurRetailersModel]()
            if response != nil {
                
                let codeInt = response?["code"] as? Int
                let codeString = response?["code"] as? String
                let message = response?["message"] as? String
                if codeInt == 200 || codeString == "200"{
                    if let result = response?["result"] as? NSArray{
                        for data in result {
                            
                            if let details = data as? [String: Any]{
                                var session = OurRetailersModel()
                                session.deserilize(values: details)
                                retailerList.append(session)
                            }

                        }
                        
                        callback(retailerList,"")
                    }
                }
                else {
                    callback([], message ?? "")
                }
                
            }
            else {
                callback([], "Something went wrong")
            }
            
        }
        
    }
 
}

