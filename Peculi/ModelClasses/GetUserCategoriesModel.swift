//
//  GetUserCategoriesModel.swift
//  Peculi
//
//  Created by Stealth on 19/11/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import SVProgressHUD

class GetUserCategoriesModel: NSObject {
    
    public func fetchInfo(_ user_id : String,completion : @escaping (( [UserCategoriesInfo] )->())){
        
        let params:[String:AnyObject] = [
            "user_id":user_id as AnyObject,
        ]
                
        print(params)
        
       
        
        SVProgressHUD.show()
        
        let url = AppConfig.BaseUrl  + constantApis.GetUserCategoriesApi
        
        //            var authorizationToken = String()
        //            authorizationToken = UserDefaults.standard.value(forKey: "accessToken") as! String
        //
        //            var token = String()
        //            token = String(format: "Bearer %@",authorizationToken)
        
        let urL = URL(string: url)
        if let data = try? JSONSerialization.data(withJSONObject: params, options: .prettyPrinted),
           let jsonString = String(data: data, encoding: .utf8) {
            
            var request = URLRequest(url: urL!)
            request.httpMethod = HTTPMethod.post.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            //request.setValue(token, forHTTPHeaderField: "Authorization")
            request.httpBody = jsonString.data(using: .utf8)
            
            
            AF.request(request)
                .responseJSON { response in
                    switch response.result
                    {
                    case .failure(let error):
                        if let data = response.data {
                            print("Print Server Error: " + String(data: data, encoding: String.Encoding.utf8)!)
                        }
                        
                        SVProgressHUD.dismiss()
                        print(error)
                        
                    case .success(let value):
                        var dataUser:[UserCategoriesInfo] = []
                        SVProgressHUD.dismiss()
                        
                        do {
                            if let parseJSON = try JSONSerialization.jsonObject(with: response.data!)  as? Dictionary<String,AnyObject>,
                               
                                
                               let resultValue = parseJSON["status"] as? String {
                                let message = parseJSON["message"] as? String
                                let status = parseJSON["status"] as? String
                                if resultValue == "success"{
                                    
                                    // let message = parseJSON["message"] as? String
                                    
                                    if let dataList = parseJSON["result"] as? NSArray {
                                        
                                        
                                        print(parseJSON)
                                        
                                        var  addDictionary = Dictionary<String, Any> ()
                                        for item in dataList {
                                            var sessionDateList = UserCategoriesInfo()
                                            addDictionary = item as! Dictionary<String, Any>
                                            addDictionary.updateValue(message as Any, forKey: "message")
                                            addDictionary.updateValue(status as Any, forKey: "Status")
                                            
                                            
                                            
                                            sessionDateList.deserilize(values:addDictionary)
                                            dataUser.append(sessionDateList)
                                        }
                                        
                                        SVProgressHUD.dismiss()
                                        completion(dataUser)
                                    }else{
                                        SVProgressHUD.dismiss()
                                        completion(dataUser)
                                    }
                                    
                                }
                                else{
                                    if let code = parseJSON["code"] as? Int{
                                        
                                        if code == 403{
                                            var sessionDateList = UserCategoriesInfo()
                                            var  addDictionary = Dictionary<String, Any> ()
                                            addDictionary.updateValue(message as Any, forKey: "message")
                                            addDictionary.updateValue(status as Any, forKey: "Status")
                                            sessionDateList.deserilize(values:addDictionary)
                                            dataUser.append(sessionDateList)
                                            SVProgressHUD.dismiss()
                                            completion(dataUser)
                                        }
                                        
                                        else  if code == 400{
                                            var sessionDateList = UserCategoriesInfo()
                                            var addDictionary = Dictionary<String, Any> ()
                                            addDictionary.updateValue(message as Any, forKey: "message")
                                            addDictionary.updateValue(status as Any, forKey: "Status")
                                            sessionDateList.deserilize(values:addDictionary)
                                            dataUser.append(sessionDateList)
                                            SVProgressHUD.dismiss()
                                            completion(dataUser)
                                        }
                                        else  if code == 401{
                                            var sessionDateList = UserCategoriesInfo()
                                            var  addDictionary = Dictionary<String, Any> ()
                                            addDictionary.updateValue(message as Any, forKey: "message")
                                            addDictionary.updateValue(status as Any, forKey: "Status")
                                            sessionDateList.deserilize(values:addDictionary)
                                            dataUser.append(sessionDateList)
                                            SVProgressHUD.dismiss()
                                            completion(dataUser)
                                        }
    
                                        else{
                                            
                                            SVProgressHUD.dismiss()
                                            
                                            completion(dataUser)
                                            
                                        }
                                    }
                                    else{
                                        
                                        SVProgressHUD.dismiss()
                                        
                                        completion(dataUser)
                                        
                                    }
                                }
                            }else{
                                SVProgressHUD.dismiss()
                                completion(dataUser)
                            }
                            
                            
                            
                        } catch {
                            SVProgressHUD.dismiss()
                            print(error)
                            
                        }
                        
                    }
                }
            
        }else{
            SVProgressHUD.dismiss()
        }
        
        
    }
    
    
    
}



struct UserCategoriesInfo:JsonDeserilizer  {
    
    
    var message:String = ""
    var statuscheck:String = ""
    var total_amount = Double()
    
    var data1:userdata = userdata()
    
    
    init() {
        
    }
    
    mutating func deserilize(values: Dictionary<String,Any>?) {
        
        
        message = values?["message"] as? String ?? ""
        
        statuscheck = values?["Status"] as? String ?? ""
      //  total_amount = values?["total_amount"] as? Double ?? 0
        if let dictMessageBy = values?["user_data"] as? NSDictionary {
            data1.deserilize(values: dictMessageBy as? Dictionary<String, Any>)
        }
    }

}

struct userdata:JsonDeserilizer  {
    
    var categoryBalance = ""
    var _id:String = ""
    var created_at:String = ""
    var category_id:String = ""
    var id:String = ""
    
    var __v:Int = 0
    
    
    var category_name: String = ""
    var user_id:String = ""
    var target:String = ""
    var weekly_target:String = ""
    
    var categorydata:categorydetails = categorydetails()
    
    init() {
        
    }
    
    mutating func deserilize(values: Dictionary<String,Any>?) {
        
        categoryBalance = values?["categoryBalance"] as? String ?? ""
        _id = values?["_id"] as? String ?? ""
        created_at = values?["created_at"] as? String ?? ""
        category_id = values?["category_id"] as? String ?? ""
        
        id = values?["_id"] as? String ?? ""
        
        created_at = values?["created_at"] as? String ?? ""
        user_id = values?["user_id"] as? String ?? ""
        
        target = values?["target"] as? String ?? ""
        weekly_target = values?["weekly_target"] as? String ?? ""
        
        __v = values?["__v"] as? Int ?? 0
        category_name = values?["category_name"] as? String ?? ""
        
        if let dictMessageBy = values?["category_details"] as? NSDictionary{
            categorydata.deserilize(values: dictMessageBy as? Dictionary<String, Any>)
        }
        
    }
    
    
    
}
struct categorydetails:JsonDeserilizer  {
    
    var _id:String = ""
    var created_at:String = ""
    var image:String = ""
    var id:String = ""
    
    var __v:Int = 0
    var tier1: String = ""
    var tier2: String = ""
    var tier3: String = ""
    
    var new_name: String = ""
    var name:String = ""
    var slug:String = ""
    

    init() {
        
    }
    
    mutating func deserilize(values: Dictionary<String,Any>?) {
        
        
        _id = values?["_id"] as? String ?? ""
        created_at = values?["created_at"] as? String ?? ""
        image = values?["image"] as? String ?? ""
        
        id = values?["_id"] as? String ?? ""
        
        created_at = values?["created_at"] as? String ?? ""
        name = values?["name"] as? String ?? ""
        new_name = values?["new_name"] as? String ?? ""
        slug = values?["slug"] as? String ?? ""
        tier1 = values?["tier1"] as? String ?? ""
         tier2 = values?["tier2"] as? String ?? ""
         tier3 = values?["tier3"] as? String ?? ""
        
        __v = values?["__v"] as? Int ?? 0
        
        
        
    }
    
    
    
}
