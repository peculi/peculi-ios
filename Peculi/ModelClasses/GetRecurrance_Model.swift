//
//  GetRecurrance_Model.swift
//  Peculi
//
//  Created by PPI on 4/27/22.
//  Copyright © 2022 Stealth Tech. All rights reserved.
//

import Foundation
struct GetRecurrance_Model {
    
    var cancelStatus: Bool
    var _id: String
    var targetType: String
    var targetAmount: String
    var amountPaid: String
    var startDate: String
    var dueDate: String
    var categoryId: String
    var categoryName: String
    var cardName: String
    
    init(cancelStatus:Bool, _id: String, targetType:String, targetAmount:String, amountPaid:String, startDate:String, dueDate:String, categoryId:String, categoryName:String, cardName:String) {
        
        self.cancelStatus = cancelStatus
        self._id = _id
        self.targetType = targetType
        self.targetAmount = targetAmount
        self.amountPaid = amountPaid
        self.startDate = startDate
        self.dueDate = dueDate
        self.categoryId = categoryId
        self.categoryName = categoryName
        self.cardName = cardName
        
      
    }
}



struct GetSubcription_Model {
    
    
    var expiryDate: String
    var price: String
    var subscriptionId: String
    var deviceType : String
    
    init(expiryDate:String, price: String, subscriptionId:String, deviceType:String) {
        
        self.expiryDate = expiryDate
        self.price = price
        self.subscriptionId = subscriptionId
        self.deviceType = deviceType
       
}
    
}
