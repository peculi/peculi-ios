//
//  AllTiersModel.swift
//  Peculi
//
//  Created by Stealth on 1/6/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import Foundation
struct AllTiersModel {
    var max: String
    var min: String
    var name: String
    var percentDiscount: String
    var denominationType: String
    var logoImage: String
    var description: String
    var currencyCode: String
    var denominationData : [String]
    var code: String
    var id : String
    var termsAndConditions:String
    var voucher_Id:String
var voucherDiscountValue:Double
    
    
    init(max:String,min:String,name:String,percentDiscount: String,denominationType:String,logoImage:String,denominationData:[String],description: String,currencyCode: String,code:String,id:String, termsAndConditon: String,voucher_Id:String,voucherDiscountValue:Double){
        self.max = max
        self.min = min
        self.name = name
        self.percentDiscount = percentDiscount
        self.denominationType = denominationType
        self.logoImage = logoImage
        self.denominationData = denominationData
        self.description = description
        self.currencyCode = currencyCode
        self.code = code
        self.id = id
        self.termsAndConditions = termsAndConditon
        self.voucher_Id = voucher_Id
        self.voucherDiscountValue = voucherDiscountValue
    }
}
