//
//  tutorialModelClass.swift
//  Peculi
//
//  Created by Stealth on 29/10/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit

class tutorialModelClass: NSObject {
    
     var ImgArray = [UIImage(named: "icTutorial5Vector"),UIImage(named: "tutorial_Img2"),UIImage(named: "tutorial_Img1"),UIImage(named: "tutorial_Img3")]
    
    var Title = ["Welcome to Peculi", "Reward With Your Favourite Brands", "No Risk, No Fees, No Tax", "Say Goodbye To Low Returns"] //, "Low returns from banks give no incentive to save"
    
    var Description = ["Peculi is a revolutionary new savings app that allows you to earn fantastic returns on your savings to spend with your favourite retailers.", "Peculi has partnered with some of the world’s most popular brands, so you can save on the things you want most.", "Peculi is completely free to download, your money is always accessible, and our rewards are risk and tax-free. What’s not to like?", "Where the big boys offer interest of 0.25% on your savings, we can offer you up to 10% with our rewards! We have fantastic offers available 24/7 from a huge range of high street retailers." ]
//  //"Where the big boys offer returns of 0.25% on your savings, we can offer up to 10%! We have fantastic offers available 24/7 from a huge range of high street retailers."      "Don’t fancy 0.1% returns on your savings from the big boys, how about 3%, or 4%, or 5%, or maybe even more! We have fantastic offers specific to you available 24/7 from a huge range of High street retailers."
}


//var Description = ["Peculi is the revolutionary savings app we’ve all been missing! Peculi allows you to earn fantastic returns instantly on your savings to spend at your favourite retailers.", "Peculi has partnered with some of the world’s most popular brands to offer you the best rewards possible!", "Peculi believes in incorporating old fashioned values with a modern twist! Save up to buy with an added market busting reward! It’s easy!", "Peculi is completely free to download, your money is accessible at all times, all of our rewards are risk and tax free, what’s not to like?", "Don’t fancy 0.1% returns on your savings from the big boys, how about 3%, or 4%, or 5%, or maybe even more! We have fantastic offers specific to you available 24/7 from a huge range of High street retailers."
//]


//["Welcome to Peculi", "Let’s Get Your Favourite Brands to Reward With Your Favourite Brands for Saving", "Why do we save? To buy stuff!", " Zero risk, zero fees, zero tax, zero commitment", "Low returns from banks give no incentive to save"]
