//
//  GetUserProfileModel.swift
//  Peculi
//
//  Created by PPI on 4/28/22.
//  Copyright © 2022 Stealth Tech. All rights reserved.
//

import Foundation

struct GetUserProfile_Model {
    
    var username: String
    var email: String
    var gender: String
    var dob: String
    var phone: String
    var image: String
    var city: String
    var countrycode: String
    var zipcode: String
    var address: String
    var income: String
    var occupation: String
    var profession: String
    
    init(username:String, email: String, gender:String, dob:String, phone:String,image:String, city: String, countrycode:String, zipcode:String, address:String,income:String, occupation:String, profession:String) {
        
        self.username = username
        self.email = email
        self.gender = gender
        self.dob = dob
        self.phone = phone
        self.image = image
        self.zipcode = zipcode
        self.city = city
        self.countrycode = countrycode
        self.address = address
        self.income = income
        self.occupation = occupation
        self.profession = profession
       

    }
}
