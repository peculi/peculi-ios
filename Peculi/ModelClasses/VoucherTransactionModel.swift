//
//  VoucherTransactionModel.swift
//  Peculi
//
//  Created by iApp on 20/04/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import Foundation


struct VoucherTransactionModel: JsonDeserilizer {
    
    var __v: String = ""
    var _id: String = ""
    var admin_profit: String = ""
    var amount: String = ""
    var barcode_format: String = ""
    var barcode_string: String = ""
    var calculate_amount = Double()
    var category_id: String = ""
    var code: String = ""
    var created_at:String = ""
    var currency_code:String = ""
    var discount: String = ""
    var error_code: String = ""
    var error_string: String = ""
    var expiry_date: String = ""
    var id: String = ""
    var order_id: String = ""
    var pin: String = ""
    var product_code: String = ""
    var status: String = ""
    var user_id: String = ""
    var voucher_discount: String = ""
    var productDetails: ProductDetails = ProductDetails()
    
    
    
    init() {
        
    }
    mutating func deserilize(values: Dictionary<String,Any>?) {
        
      __v = values?["__v"] as? String ?? ""
        _id = values?["_id"] as? String ?? ""
        admin_profit = values?["admin_profit"] as? String ?? ""
        amount = values?["amount"] as? String ?? ""
        barcode_format = values?["barcode_format"] as? String ?? ""
        barcode_string = values?["barcode_format"] as? String ?? ""
        calculate_amount = values?["calculate_amount"] as? Double ?? 0.00
        category_id = values?["category_id"] as? String ?? ""
        code = values?["code"] as? String ?? ""
        created_at = values?["created_at"] as? String ?? ""
        currency_code = values?["currency_code"] as? String ?? ""
        discount = values?["discount"] as? String ?? ""
        error_code = values?["error_code"] as? String ?? ""
        error_string = values?["error_string"] as? String ?? ""
        expiry_date = values?["expiry_date"] as? String ?? ""
        id = values?["id"] as? String ?? ""
        order_id = values?["order_id"] as? String ?? ""
        pin = values?["pin"] as? String ?? ""
        product_code = values?["product_code"] as? String ?? ""
        status = values?["status"] as? String ?? ""
         user_id = values?["user_id"] as? String ?? ""
         voucher_discount = values?["voucher_discount"] as? String ?? ""
        if let dictMessageBy = values?["product_details"] as? NSDictionary{
            productDetails.deserilize(values: dictMessageBy as? Dictionary<String, Any>)
        }
        
        
        
    }
    
}


struct ProductDetails: JsonDeserilizer {
    
    var _id: String = ""
    var code: String = ""
    var description: String = ""
    var logo_image_url: String = ""
    var name: String = ""
    var terms_and_conditions_html: String = ""
    
    
    init() {
        
    }
    mutating func deserilize(values: Dictionary<String, Any>?) {
        
        _id = values?["_id"] as? String ?? ""
        code = values?["code"] as? String ?? ""
        description = values?["description"] as? String ?? ""
        logo_image_url = values?["logo_image_url"] as? String ?? ""
        name = values?["name"] as? String ?? ""
        terms_and_conditions_html = values?["terms_and_conditions_html"] as? String ?? ""
    }
    
    
}
