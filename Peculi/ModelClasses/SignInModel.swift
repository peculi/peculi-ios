//
//  SignInModel.swift
//  Peculi
//
//  Created by Stealth on 18/11/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import SVProgressHUD

class SignInModel: NSObject {
    
    public func fetchInfo(_ email : String,_ password : String, completion : @escaping (( [SignInInfo] )->())){
        
    
            let params:[String:Any] = [

                "email":email,
                "password": password,
//                "device_token": device_token,
//                "device_id": device_id ,
//                "device_type": "ios",
            ]
        
               SVProgressHUD.show()
    
        let url = AppConfig.BaseUrl  + constantApis.SigninApi
    
//            var authorizationToken = String()
//            authorizationToken = UserDefaults.standard.value(forKey: "accessToken") as! String
//
//            var token = String()
//            token = String(format: "Bearer %@",authorizationToken)
    
            let urL = URL(string: url)
            if let data = try? JSONSerialization.data(withJSONObject: params, options: .prettyPrinted),
                let jsonString = String(data: data, encoding: .utf8) {
    
                var request = URLRequest(url: urL!)
                request.httpMethod = HTTPMethod.post.rawValue
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
               // request.setValue(token, forHTTPHeaderField: "Authorization")
               request.httpBody = jsonString.data(using: .utf8)
    
                AF.request(request)
                    .responseJSON {
                        response in
                        switch response.result
                        {
                        case .failure(let error):
                            if let data = response.data {
                                
                                print("Print Server Error: " + String(data: data, encoding: String.Encoding.utf8)!)
                                
                            }
                            
                            SVProgressHUD.dismiss()
                            print(error)
    
                        case .success(let values):
                            var dataUser:[SignInInfo] = []
                             SVProgressHUD.dismiss()
    
                            do {
                                if let parseJSON = try JSONSerialization.jsonObject(with: response.data!)  as? Dictionary<String,AnyObject>,
                                    let resultValue = parseJSON["code"] as? Int {
                                    let message = parseJSON["message"] as? String
                                    let status = parseJSON["status"] as? String
                                    
                                    UserDefaults.standard.setValue(resultValue, forKey: "SAVECODE")
                                    
                                    if resultValue == 200 {  //"success"
                                        
                                        UserDefaults.standard.setValue(resultValue, forKey: "SAVECODE")
                                        
                                    if let dataList = parseJSON["result"] as?  Dictionary<String,AnyObject> {
                                       // let message = parseJSON["message"] as? String
    
    
                                    print(parseJSON)
    
                                        var  addDictionary = Dictionary<String, Any> ()
                                            var sessionDateList = SignInInfo()
                                            addDictionary = dataList
                                            addDictionary.updateValue(message as Any, forKey: "message")
                                            addDictionary.updateValue(status as Any, forKey: "Status")
    
                                            sessionDateList.deserilize(values:addDictionary)
                                            dataUser.append(sessionDateList)
                                        
                                    }
                                     SVProgressHUD.dismiss()
                                    completion(dataUser)
                                }
                                else {
                                        if let code = parseJSON["code"] as? Int{
    
                                            if code == 403 {
                                               var sessionDateList = SignInInfo()
                                                var  addDictionary = Dictionary<String, Any> ()
                                                addDictionary.updateValue(message as Any, forKey: "message")
                                                addDictionary.updateValue(status as Any, forKey: "Status")
                                                sessionDateList.deserilize(values:addDictionary)
                                                dataUser.append(sessionDateList)
                                                 SVProgressHUD.dismiss()
                                                completion(dataUser)
                                            }
                                            
                                            else  if code == 400{
                                            var sessionDateList = SignInInfo()
                                            var addDictionary = Dictionary<String, Any> ()
                                                                                           addDictionary.updateValue(message as Any, forKey: "message")
                                                                                           addDictionary.updateValue(status as Any, forKey: "Status")
                                                                                           sessionDateList.deserilize(values:addDictionary)
                                                                                           dataUser.append(sessionDateList)
                                                                                            SVProgressHUD.dismiss()
                                                                                           completion(dataUser)
                                                                                       }
                                                else  if code == 401{
                                                                                              var sessionDateList = SignInInfo()
                                                                                               var  addDictionary = Dictionary<String, Any> ()
                                                                                               addDictionary.updateValue(message as Any, forKey: "message")
                                                                                               addDictionary.updateValue(status as Any, forKey: "Status")
                                                                                               sessionDateList.deserilize(values:addDictionary)
                                                                                               dataUser.append(sessionDateList)
                                                                                                SVProgressHUD.dismiss()
                                                                                               completion(dataUser)
                                                                                           }
                    
                                                
                                            else {
    
                                               SVProgressHUD.dismiss()
    
                                                completion(dataUser)
    
                                            }
                                        }
                                        else {
    
                                             SVProgressHUD.dismiss()
    
                                            completion(dataUser)
            
                                        }
                                    }
                                } else {
                                     SVProgressHUD.dismiss()
                                  completion(dataUser)
                                }
    
    
    
                            } catch {
                                 SVProgressHUD.dismiss()
                                print(error)
    
                            }
    
                        }
                }
    
            } else {
                SVProgressHUD.dismiss()
                
            }
    
    
        }
    
    
    
    }
    
    
    
    struct SignInInfo:JsonDeserilizer  {
    
        var _id:String = ""
        var created_at:String = ""
        var firstName:String = ""
        var lastName:String = ""
        var email:String = ""
        var password:String = ""
        var gender:String = ""
        var dob:String = ""
        var message:String = ""
        var phone:String = ""
        var address:String = ""
        var country:String = ""
        var countryCode:String = ""
        var state:String = ""
        var city:String = ""
        var statuscheck:Int = 0
        var currency: String = ""
        var image:String = ""
        var fingerPrint:String = ""
        var device_token:String = ""
        var device_id:String = ""
        var device_type:String = ""
        var __v:Int = 0
        var pin:String = ""
        var status:String = ""
        var address1: String = ""
        var address2: String = ""
        var postalCode: String = ""
        var zipCode: String = ""
        var countryName: String = ""
        var personalDetailsStatus = ""
        var pfsStatus = ""
        var kycStatus = ""
        var pfsAccountStatus = ""
        var otpStatus: String = ""
        var otp : Int = 0
        var jwToken = ""
    
        init() {
    
        }
    
        mutating func deserilize(values: Dictionary<String,Any>?) {
    
    
            _id = values?["_id"] as? String ?? ""
            created_at = values?["created_at"] as? String ?? ""
            firstName = values?["firstname"] as? String ?? ""
            lastName = values?["lastname"] as? String ?? ""
            email = values?["email"] as? String ?? ""
            password = values?["password"] as? String ?? ""
    
            gender = values?["gender"] as? String ?? ""
            dob = values?["dob"] as? String ?? ""
            message = values?["message"] as? String ?? ""
          
            statuscheck = values?["code"] as? Int ?? 0 //String ?? ""
            image = values?["image"] as? String ?? ""
            fingerPrint = values?["fingerPrint"] as? String ?? ""
            device_token = values?["device_token"] as? String ?? ""
            jwToken =  values?["jwtToken"] as? String ?? ""
            device_id = values?["device_id"] as? String ?? ""
            device_type = values?["device_type"] as? String ?? ""
            
            __v = values?["__v"] as? Int ?? 0
            pin = values?["pin"] as? String ?? ""
            phone = values?["phone"] as? String ?? ""
            address = values?["address"] as? String ?? ""
            country = values?["country"] as? String ?? ""
            countryCode = values?["countrycode"] as? String ?? ""
            
            state = values?["state"] as? String ?? ""
            city = values?["city"] as? String ?? ""
            status = values?["status"] as? String ?? ""
            currency = values?["currency"] as? String ?? ""
            address1 = values?["address1"] as? String ?? ""
            address2 = values?["address2"] as? String ?? ""
            postalCode = values?["postcode"] as? String ?? ""
            zipCode = values?["zipcode"] as? String ?? ""
            
            countryName = values?["country_name"] as? String ?? ""
            kycStatus = values?["kyc_status"] as? String ?? ""
            otpStatus = values?["otp_status"] as? String ?? ""
            otp = values?["otp"] as? Int ?? 0
            personalDetailsStatus = values?["personal_details_status"] as? String ?? ""
            pfsAccountStatus = values?["pfs_account_status"] as? String ?? ""
            pfsStatus = values?["pfs_status"] as? String ?? ""
            
            }
    
    }
    
    
    
