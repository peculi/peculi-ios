//
//  NetworkManager.swift
//  Peculi
//
//  Created by Stealth on 1/4/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import Foundation
import Alamofire

typealias CompletionBlock           = (Bool, String?) -> Void
typealias CompletionBlockWithDict   = ([String:Any]?, String?) -> Void
typealias CompletionBlockWithArray  = ([Any]?, String?) -> Void
typealias CompletionBlockWithStatus  = (Bool?, String?) -> Void

class DataService: NSObject {
    
    static let sharedInstance = DataService()
    
    

    
    
    func signUp(firstname:String, lastName: String, email:String, password:String, gender: String, dob:String, phone:String, city:String, countryCode:String, zipcode:String, address:String, deviceId:String, deviceToken:String, deviceType:String, userId:String,profession:String,occupation:String,income:String,completionHandler: @escaping CompletionBlockWithDict){
        
        let url = AppConfig.BaseUrl + constantApis.SignupApi
        
        let params : Parameters = [
            
            "firstname":firstname,
              "lastname":lastName,
              "email":email,
              "password":password,
              "gender":gender,
              "dob":dob,
              "phone":phone,
              "city":city,
              "countrycode":countryCode,
              "zipcode":zipcode,
              "address":address,
              "device_id":deviceId,
              "device_token":deviceToken,
              "device_type":deviceType,
              "user_id":userId,
            "profession":profession,
            "occupation":occupation,
            "income":income,
            
        ]
        
        print (params)
        
        
        AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            
            switch response.result {
                
            case .success(let value):
                
                let successItem = response.value as? [String:Any]
                
                completionHandler(successItem!,nil)
                
                break
                
            case .failure(let error):
                
                completionHandler(nil,error.localizedDescription)
                
                break
            }
        }
    }
    
    
    
    func postalCodeSearch(postCode:String,completionHandler: @escaping CompletionBlockWithDict){
        
        let url = AppConfig.BaseUrl + constantApis.postalCodeSearch


        let params : Parameters = [
            "postcode":postCode,
        ]
        
        print (params)
        
        
        AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            
            switch response.result {
                
            case .success(let value):
                
                let successItem = response.value as? [String:Any]
                
                completionHandler(successItem!,nil)
                
                break
                
            case .failure(let error):
                
                completionHandler(nil,error.localizedDescription)
                
                break
            }
        }
    }
    
    
    
    func postalCodeSearchNew(postCode:String,completionHandler: @escaping CompletionBlockWithDict){
        
        let url = AppConfig.BaseUrl + constantApis.postalCodeSearchNew


        let params : Parameters = [
            "zipCode":postCode,
        ]
        
        print (params)
        
        
        AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            
            switch response.result {
                
            case .success(let value):
                
                let successItem = response.value as? [String:Any]
                
                completionHandler(successItem!,nil)
                
                break
                
            case .failure(let error):
                
                completionHandler(nil,error.localizedDescription)
                
                break
            }
        }
    }
   
    
    
    
    func updatePin(pin: String, completionHandler: @escaping CompletionBlockWithDict) {
        
        let url = AppConfig.BaseUrl + constantApis.updateUserPin
        
        let params : Parameters = [
            "user_id": UserDefaults.standard.value(forKey: UserDefaultConstants.UserID.rawValue) ?? "",
            "pin": pin
        ]
        
        AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            
            switch response.result {
                
            case .success(let value):
                
                let successItem = response.value as? [String:Any]
                
                completionHandler(successItem!,nil)
                
                break
                
            case .failure(let error):
                
                completionHandler(nil,error.localizedDescription)
                
                break
            }
        }
    }
    
    
   // MMMMMMM
    
    
    func getCard_Api( completionHandler: @escaping CompletionBlockWithDict) {
        
        let url = AppConfig.BaseUrl + constantApis.getCards_Api
        let token = UserDefaults.standard.value(forKey: UserDefaultConstants.jwToken.rawValue) as? String ?? ""
        
        let headerToken : HTTPHeaders = [
            "Authorization": "Bearer " + token
                    ]
        
        print(headerToken)
        //let headerToken = "" + "\(UserDefaults.standard.value(forKey: UserDefaultConstants.jwToken.rawValue))"//jwToken
        
        let params : Parameters = [
           "" : ""
        ]
        AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: headerToken).responseJSON { response in
            debugPrint(response)
            
            switch response.result {
                
            case .success(let value):
                
                let successItem = response.value as? [String:Any]
                
                completionHandler(successItem!,nil)
                
                break
                
            case .failure(let error):
                
                completionHandler(nil,error.localizedDescription)
                
                break
            }
        }
    }
    
    
    
    
    

    func getTiersRange(completionHandler: @escaping CompletionBlockWithDict) {
        
        let url = AppConfig.BaseUrl + constantApis.Get_Tiers_Range
        
        let params : Parameters = [:]
        AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            
            switch response.result {
                
            case .success(let value):
                
                let successItem = response.value as? [String:Any]
                
                completionHandler(successItem!,nil)
                
                break
                
            case .failure(let error):
                
                completionHandler(nil,error.localizedDescription)
                
                break
            }
        }
    }
    
    func getAllVouchers(categories:String, tierName: String, completionHandler: @escaping CompletionBlockWithDict) {
        
        let url = AppConfig.BaseUrl + constantApis.Get_All_Vouchers
//        var shortCountry = String()
//        if let country = UserDefaults.standard.value(forKey: "country") as? String{
//            shortCountry = country
//        }
        
        let params : Parameters = [
            "category": categories,
            "country": "GB",
            "type": tierName
        ]
        print("fsdfsd-------->",params)
        AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            
            switch response.result {
                
            case .success(let value):
                
                let successItem = response.value as? [String:Any]
                
                completionHandler(successItem!,nil)
                
                break
                
            case .failure(let error):
                
                completionHandler(nil,error.localizedDescription)
                
                break
            }
        }
    }
    
    func addAmountSwaprifice(categoryId:String,amount:String,completionHandler: @escaping CompletionBlockWithDict) {
        
        let url = AppConfig.BaseUrl + constantApis.Add_Swaprifice_Amount
        
        let params : Parameters = [
            "user_id":UserDefaults.standard.value(forKey: UserDefaultConstants.UserID.rawValue) ?? "",
            "category_id": categoryId,
            "amount":amount,
        ]
        AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            
            switch response.result {
                
            case .success(let value):
                
                let successItem = response.value as? [String:Any]
                
                completionHandler(successItem!,nil)
                
                break
                
            case .failure(let error):
                
                completionHandler(nil,error.localizedDescription)
                
                break
            }
        }
    }
    
    func getGlobalPrice(completionHandler: @escaping CompletionBlock) {
        
        let url = AppConfig.BaseUrl + constantApis.Get_Global_Price
        
        let params : Parameters = [
            "user_id":UserDefaults.standard.value(forKey: UserDefaultConstants.UserID.rawValue) ?? "",
        ]
        AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            
            switch response.result {
                
            case .success(let value):
                
                let successItem = response.value as? [String:Any]
                
                  
                              if let globalBalance = successItem?["global_balance"] as? String  {
                                  print(globalBalance)
                                  let globalBal = (globalBalance as NSString).doubleValue.format()
                                UserDefaults.standard.set("\(globalBal)", forKey: UserDefaultConstants.GlobalBalance.rawValue)
                                  UserDefaults.standard.synchronize()
                                  
                              }
                              if let globalBalance = successItem?["global_balance"] as? Double  {
                                  print(globalBalance)
                                  let globalBal = globalBalance.format()
                                UserDefaults.standard.set("\(globalBal)", forKey: UserDefaultConstants.GlobalBalance.rawValue)
                                  UserDefaults.standard.synchronize()
                                  
                              }
                              else {
                                  if let message = successItem?["message"] as? String {
                                      //self.presentAlert(withTitle: "", message: message)
                                  }
                              }
                
                completionHandler(true,nil)
                          
                
                break
                
            case .failure(let error):
                
                completionHandler(false,error.localizedDescription)
                
                break
            }
        }
    }
 
    func getCategoryPrice(categoryId:String,completionHandler: @escaping CompletionBlockWithDict) {
        
        let url = AppConfig.BaseUrl + constantApis.Get_Category_Balance
        
        let params : Parameters = [
            "user_id":UserDefaults.standard.value(forKey: UserDefaultConstants.UserID.rawValue) ?? "",
            "category_id":categoryId
        ]
        AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            
            switch response.result {
                
            case .success(let value):
                
                let successItem = response.value as? [String:Any]
                
                completionHandler(successItem!,nil)
                
                break
                
            case .failure(let error):
                
                completionHandler(nil,error.localizedDescription)
                
                break
            }
        }
    }

    func redeemVoucher(categoryId:String,productCode:String,amount:String,calculate_amount:String,voucher_discount: String ,discount:String,currencyCode:String,voucherId:String,selectedPotId:String, completionHandler: @escaping CompletionBlockWithDict) {
        
        let url = AppConfig.BaseUrl + constantApis.Save_Redeem_Voucher
        
        
//        "user_id
//        category_id
//        product_code
//        amount
//        calculate_amount
//        voucher_discount
//        discount
//        currency_code
//        "
        
        let params : Parameters = [
            "user_id":UserDefaults.standard.value(forKey: UserDefaultConstants.UserID.rawValue) ?? "",
            "category_id":categoryId,
            "product_code":productCode,
            "amount":amount,
            "calculate_amount": calculate_amount,
            "voucher_discount": voucher_discount,
            "discount":discount,
            "currency_code":currencyCode,
            "voucherId" : voucherId,
            "catId":selectedPotId
        ]
        AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            
            switch response.result {
                
            case .success(let value):
                
                let successItem = response.value as? [String:Any]
                
                completionHandler(successItem!,nil)
                
                break
                
            case .failure(let error):
                
                completionHandler(nil,error.localizedDescription)
                
                break
            }
        }
    }

    func redeemVoucherHistory(completionHandler: @escaping CompletionBlockWithDict) {
        let url = AppConfig.BaseUrl + constantApis.Redeem_Voucher_History
        
        let params : Parameters = [
            "user_id":UserDefaults.standard.value(forKey: UserDefaultConstants.UserID.rawValue) ?? "",
        ]
        
        print(params)
        AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            
            switch response.result {
                
            case .success(let value):
                
                let successItem = response.value as? [String:Any]
                
                completionHandler(successItem!,nil)
                
                break
                
            case .failure(let error):
                
                completionHandler(nil,error.localizedDescription)
                
                break
            }
        }
    }
    
    func getCountries(completionHandler: @escaping CompletionBlockWithDict) {
        
        let url = AppConfig.BaseUrl + constantApis.Get_Countries
        
        let params : Parameters = [:]
        AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            
            switch response.result {
                
            case .success(let value):
                
                let successItem = response.value as? [String:Any]
                
                completionHandler(successItem!,nil)
                
                break
                
            case .failure(let error):
                
                completionHandler(nil,error.localizedDescription)
                
                break
            }
        }
    }
    
    func getAllStates(shortName:String,completionHandler: @escaping CompletionBlockWithDict) {
        let url = AppConfig.BaseUrl + constantApis.Get_States
        
        let params : Parameters = [
            "country_shortName":shortName,
        ]
        AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            
            switch response.result {
                
            case .success(let value):
                
                let successItem = response.value as? [String:Any]
                
                completionHandler(successItem!,nil)
                
                break
                
            case .failure(let error):
                
                completionHandler(nil,error.localizedDescription)
                
                break
            }
        }
    }

    func getAllCities(shortName:String,stateName:String,completionHandler: @escaping CompletionBlockWithDict) {
        let url = AppConfig.BaseUrl + constantApis.Get_Cities
        
        let params : Parameters = [
            "country_shortName":shortName,
            "stateName":stateName
        ]
        AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            
            switch response.result {
                
            case .success(let value):
                
                let successItem = response.value as? [String:Any]
                
                completionHandler(successItem!,nil)
                
                break
                
            case .failure(let error):
                
                completionHandler(nil,error.localizedDescription)
                
                break
            }
        }
    }
    
    
    
    func checkPostCodeApiCall( postCode:String, completionHandler: @escaping CompletionBlockWithDict) {
        let url = AppConfig.BaseUrl + constantApis.checkPostcode
        
        let params : Parameters = [
            
            "postcode":postCode
        ]
        AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            
            switch response.result {
                
            case .success(let value):
                
                let successItem = response.value as? [String:Any]
                
                completionHandler(successItem!,nil)
                
                break
                
            case .failure(let error):
                
                completionHandler(nil,error.localizedDescription)
                
                break
            }
        }
    }
    
    
    
    func updateCategoryApiCall( userID:String, categoryName:String, categoryId: String, completionHandler: @escaping CompletionBlockWithDict) {
           let url = AppConfig.BaseUrl + constantApis.userCategoryUpdate
           
           let params : Parameters = [
               
               "user_id": userID,
               "category_id": categoryId,
               "category_name": categoryName
           ]
        
           AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { response in
               debugPrint(response)
               
               switch response.result {
                   
               case .success(let value):
                   
                   let successItem = response.value as? [String:Any]
                   
                   completionHandler(successItem!,nil)
                   
                   break
                   
               case .failure(let error):
                   
                   completionHandler(nil,error.localizedDescription)
                   
                   break
               }
           }
       }
    
    
    func deleteCategoryApiCall( userID:String, categoryId: String, completionHandler: @escaping CompletionBlockWithDict) {
        let url = AppConfig.BaseUrl + constantApis.userCategoryDelete
        
        let params : Parameters = [
            
            "user_id": userID,
            "category_id": categoryId
        ]
     
        AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            
            switch response.result {
                
            case .success(let value):
                
                let successItem = response.value as? [String:Any]
                
                completionHandler(successItem!,nil)
                
                break
                
            case .failure(let error):
                
                completionHandler(nil,error.localizedDescription)
                
                break
            }
        }
    }
    
    func voucherTransactionsByCatIdApiCall(userId: String, categoryId: String, completionHandler: @escaping CompletionBlockWithDict){
        
        let url = AppConfig.BaseUrl + constantApis.voucherTransactionsByCatId
             
             let params : Parameters = [
                 "user_id": userId,
                 "category_id": categoryId
             ]
          
        
             AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { response in
                 debugPrint(response)
                 
                 switch response.result {
                     
                 case .success(let value):
                     
                     let successItem = response.value as? [String:Any]
                     
                     completionHandler(successItem!,nil)
                     
                     break
                     
                 case .failure(let error):
                     
                     completionHandler(nil,error.localizedDescription)
                     
                     break
                 }
             }
    }
    
    
    func todayOfferApiCall(userId: String, completionHandler: @escaping CompletionBlockWithDict){
        
        
        let url = AppConfig.BaseUrl + constantApis.todayOffer
             
             let params : Parameters = [
                 "user_id": userId
             ]
          
             AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { response in
                 debugPrint(response)
                 
                 switch response.result {
                     
                 case .success(let value):
                     
                     let successItem = response.value as? [String:Any]
                     
                     completionHandler(successItem!,nil)
                     
                     break
                     
                 case .failure(let error):
                     
                     completionHandler(nil,error.localizedDescription)
                     
                     break
                 }
             }
    }
    
    
    func rewardOfferApiCall(userId: String, completionHandler: @escaping CompletionBlockWithDict){
           
           
           let url = AppConfig.BaseUrl + constantApis.rewardOffer
                
                let params : Parameters = [
                    
                    "user_id": userId
                    
                ]
             
                AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { response in
                    debugPrint(response)
                    
                    switch response.result {
                        
                    case .success(let value):
                        
                        let successItem = response.value as? [String:Any]
                        
                        completionHandler(successItem!,nil)
                        
                        break
                        
                    case .failure(let error):
                        
                        completionHandler(nil,error.localizedDescription)
                        
                        break
                    }
                }
       }
    
    
    func getSpecialOfferByUserIDApiCall(userId: String, completionHandler: @escaping CompletionBlockWithDict){
        
        
        let url = AppConfig.BaseUrl + constantApis.getSpecialOfferByUserID
             
             let params : Parameters = [
                 
                 "user_id": userId
                 
             ]
          
             AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { response in
                 debugPrint(response)
                 
                 switch response.result {
                     
                 case .success(let value):
                     
                     let successItem = response.value as? [String:Any]
                     
                     completionHandler(successItem!,nil)
                     
                     break
                     
                 case .failure(let error):
                     
                     completionHandler(nil,error.localizedDescription)
                     
                     break
                 }
             }
    }
    
   
  
    func updateToken(deviceToken: String, completionHandler: @escaping CompletionBlockWithDict) {
         
         let url = AppConfig.BaseUrl + constantApis.updateToken
        
    //    let deviceid = UIDevice.current.identifierForVendor?.uuidString ?? ""
//        "user_id
//        device_id
//        device_token
//        device_type"
         
         let params : Parameters = [
            
             "user_id": UserDefaults.standard.value(forKey: UserDefaultConstants.UserID.rawValue) ?? "",

//             "device_token": deviceToken,
//             "device_id": deviceid ,
//             "device_type": "ios",
             
         ]
        
        print(params)
        
         AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { response in
             debugPrint(response)
             
             switch response.result {
                 
             case .success(let value):
                 
                 let successItem = response.value as? [String:Any]
                 
                 completionHandler(successItem!,nil)
                 
                 break
                 
             case .failure(let error):
                 
                 completionHandler(nil,error.localizedDescription)
                 
                 break
             }
         }
     }
    
    
   
    func guestLogin(completionHandler: @escaping CompletionBlockWithDict) {
             
             let url = AppConfig.BaseUrl + constantApis.guestLogin
            
            let deviceid = UIDevice.current.identifierForVendor?.uuidString ?? ""
             
             let params : Parameters = [
                 "device_token": GlobalVariables.device_token,
                 "device_id": deviceid,
                 "device_type":"ios"
             ]
        
        print(params)
        
             AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { response in
                 debugPrint(response)
                 
                 switch response.result {
                     
                 case .success(let value):
                     
                     let successItem = response.value as? [String:Any]
                     
                     completionHandler(successItem!,nil)
                     
                     break
                     
                 case .failure(let error):
                    completionHandler(nil,error.localizedDescription)
                     
                     break
                 }
             }
         }
    
    
    func getCategoryInfoByID(catID: String, completionHandler: @escaping CompletionBlockWithDict) {
         
         let url = AppConfig.BaseUrl + constantApis.getCategoryInfoByID
    
         
         let params : Parameters = [
             "user_id": UserDefaults.standard.value(forKey: UserDefaultConstants.UserID.rawValue) ?? "",
            "category_id": catID
         ]
        
         AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { response in
             debugPrint(response)
             
             switch response.result {
                 
             case .success(let value):
                 
                 let successItem = response.value as? [String:Any]
                 
                 completionHandler(successItem!,nil)
                 
                 break
                 
             case .failure(let error):
                 
                 completionHandler(nil,error.localizedDescription)
                 
                 break
             }
         }
     }

    
    func getAllVouchersImages(completionHandler: @escaping CompletionBlockWithDict) {
         
         let url = AppConfig.BaseUrl + constantApis.getAllVouchersImages
   
        AF.request(url, method: .post, parameters: [:], encoding: URLEncoding.default, headers: nil).responseJSON { response in
             debugPrint(response)
             
             switch response.result {
                 
             case .success(let value):
                 
                 let successItem = response.value as? [String:Any]
                 
                 completionHandler(successItem!,nil)
                 
                 break
                 
             case .failure(let error):
                 
                 completionHandler(nil,error.localizedDescription)
                 
                 break
             }
         }
     }
    
    
    
    func postApiCall(endpoint: String, params:[String: Any], completionHandler: @escaping CompletionBlockWithDict) {
         
         let url = AppConfig.BaseUrl + endpoint
        
        let token = UserDefaults.standard.value(forKey: UserDefaultConstants.jwToken.rawValue) as? String ?? ""
        
        let headerToken : HTTPHeaders = [
            "Authorization": "Bearer " + token
                    ]
        
        AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: headerToken).responseJSON { response in
            
             debugPrint(response)
             
             switch response.result {
                 
             case .success(let value):
                 
                 let successItem = response.value as? [String:Any]
                 
                 completionHandler(successItem!,nil)
                 
                 break
                 
             case .failure(let error) :
                 completionHandler(nil,error.localizedDescription)
                 
                 break
             }
         }
     }
    
    
    func getApiCall(endpoint: String, params:[String: Any], completionHandler: @escaping CompletionBlockWithDict) {
         
         let url = AppConfig.BaseUrl + endpoint
        
        let token = UserDefaults.standard.value(forKey: UserDefaultConstants.jwToken.rawValue) as? String ?? ""
        
        let headerToken : HTTPHeaders = [
            "Authorization": "Bearer " + token
                    ]
        
        AF.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: headerToken).responseJSON { response in
            
             debugPrint(response)
             
             switch response.result {
                 
             case .success(let value):
                 
                 let successItem = response.value as? [String:Any]
                 
                 completionHandler(successItem!,nil)
                 
                 break
                 
             case .failure(let error):
                 completionHandler(nil,error.localizedDescription)
                 
                 break
             }
         }
     }
    
  

    
    func formDataAPICall(endpoint: String, params: [String: Any], callback: @escaping ((message:String, list:[String: Any],isSuccess:Bool)) -> Void) {
        
        var result:(message:String, list:[String: Any],isSuccess:Bool) = (message: "Fail", list:[:],isSuccess : false)
        
        let url = AppConfig.BaseUrl + endpoint
        
        AF.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in params {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: url, usingThreshold: UInt64.init(), method: .post, headers: nil).responseJSON{ response in

            print(response)
            if((response.error == nil))
            {
                do
                {
                    if let jsonData = response.data
                    {
                        
                        let parsedData = try JSONSerialization.jsonObject(with: jsonData) as! Dictionary<String, AnyObject>
                        
                        let status = parsedData["status"] as? String ?? ""
                        let msg = parsedData["message"] as? String ?? ""
                        
                        if(status == "success") {
                            if let jsonArray = parsedData["result"] as? [String: Any] {
                                
                                result.isSuccess = true
                                result.list = jsonArray
                                
                            }
                            
                        } else {
                            
                            result.isSuccess = false
                            result.message = msg
                        }
                    }
                    
                    callback(result)
                }
                    catch {
                        callback(result)
                }
            } else {
                
                print("Resonse.error",response.error?.localizedDescription as Any)
                callback(result)
            }
        }

        
        
    }
    
    
}



