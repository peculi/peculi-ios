//
//  SwaprificeCollectionViewCell.swift
//  Peculi
//
//  Created by apple on 03/11/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit

class SwaprificeCollectionViewCell: UICollectionViewCell {
    
    //MARK:- outlets
    @IBOutlet weak var potProductImageView: UIImageView!
    @IBOutlet weak var potProductNameLbl: UILabel!
    
    override func prepareForReuse() {
        potProductNameLbl.text = ""
        potProductImageView.image = nil
    }
}
