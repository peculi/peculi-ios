//
//  AddPotsCollectionViewCell.swift
//  Peculi
//
//  Created by apple on 31/10/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit

class AddPotsCollectionViewCell: UICollectionViewCell {
    
    //MARK:- outlets
    @IBOutlet weak var potNameLbl: UILabel!
    @IBOutlet weak var cellBlurView: UIView!
    @IBOutlet weak var addPotButtonAction: UIButton!
    @IBOutlet weak var peculiProductImageView: UIImageView!
    
    
}
