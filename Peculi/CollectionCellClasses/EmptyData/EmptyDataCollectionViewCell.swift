//
//  EmptyDataCollectionViewCell.swift
//  Peculi
//
//  Created by iApp on 16/04/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import UIKit

class EmptyDataCollectionViewCell: UICollectionViewCell {

    //MARK:- outlets
    @IBOutlet weak var defaultImageView: UIImageView!
    @IBOutlet weak var defaultFirstLbl: UILabel!
    @IBOutlet weak var defaultSecondLbl: UILabel!
    @IBOutlet weak var addNewPotBtn: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
