//
//  RetailersCollectionViewCell.swift
//  Peculi
//
//  Created by iApp on 29/06/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import UIKit

class RetailersCollectionViewCell: UICollectionViewCell {
    
    //MARK:- outlets
    @IBOutlet weak var childView: UIView!
    @IBOutlet weak var retailerImageView: UIImageView!
 
    
}
