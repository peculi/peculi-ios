//
//  AppDelegate.swift
//  Peculi
//
//  Created by stealth tech on 26/10/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Network
import Firebase
import Reachability
import FirebaseMessaging
import UserNotifications
import Messages
import NBBottomSheet
import Lottie

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {
    
    var window: UIWindow?
    var navigationController: UINavigationController?
    let gcmMessageIDKey = "messageKey"
    
    var superView = UIView()
    let monitor = NWPathMonitor()
    private var status: NWPath.Status = .requiresConnection
    var isReachable: Bool { status == .satisfied }
    var isReachableOnCellular: Bool = true
    
    
    var reachability: Reachability?
    let hostNames = [nil, "google.com", "invalidhost"]
    var hostIndex = 0
    
    var offerFetchedData = SpecialOfferModel()
    var totalAmount = Double()
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure()
        self.registerForPushNotifications()
        Messaging.messaging().delegate = self
   
        GlobalVariables.device_token = Messaging.messaging().fcmToken ?? "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyYmZkYWUzYjdkZGUzNTJlYTQ5NzExZSIsImlhdCI6MTY1Njc0MDU3OSwiZXhwIjoxNjU2ODI2OTc5fQ._YMK6ODGxlcLPqo3CefFkXtbk3f8Jnw-lkWeXmEXt_I"
        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        } else {
            
            // Fallback on earlier versions
        }
        
        
        //MARK: - firbase setup

        if #available(iOS 13.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
        } else {
            
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
                (granted, error) in
                //Parse errors and track state
            }
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        
        Installations.installations().delete { error in
            if let error = error {
                print("Error deleting installation: \(error)")
                return
            }
            print("Installation deleted");
        }
        // try! reachability?.startNotifier()
        //checkReachability()
        NetworkHanding()
        //  checkReachablity()
        
        IQKeyboardManager.shared.enable = true
        UINavigationBar.appearance().tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        window?.backgroundColor = #colorLiteral(red: 0.07450980392, green: 0.1019607843, blue: 0.1529411765, alpha: 1)
//        UINavigationBar.appearance().barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//        UINavigationBar.appearance().barStyle = .black
//        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        autoLoginHandling()

        return true
        
    }
    
    //MARK: - Notification Setup
    
    func registerForPushNotifications() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
                (granted, error) in
                print("Permission granted: \(granted)")
                
                guard granted else { return }
                self.getNotificationSettings()
            }
        } else {
            // Fallback on earlier versions
        }
        
    }
    
    func getNotificationSettings() {
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                print("Notification settings: \(settings)")
                guard settings.authorizationStatus == .authorized else { return }
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        } else {
            
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
                (granted, error) in
                print("Permission granted: \(granted)")
                
                guard granted else { return }
                self.getNotificationSettings()
            }
            // Fallback on earlier versions
        }
    }
    
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        print(token)
        //        UserDefaults.standard.setValue(token, forKey: UserDefaultConstants.deviceId.rawValue)
        //        GlobalVariableConstants.deviceId = token
        //        UserDefaults.standard.synchronize()
        // GlobalVariabel.device_token = token
        Messaging.messaging().apnsToken = deviceToken as Data
        
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(fcmToken)")
        
        GlobalVariables.device_token = fcmToken ?? "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyYmZkYWUzYjdkZGUzNTJlYTQ5NzExZSIsImlhdCI6MTY1Njc0MDU3OSwiZXhwIjoxNjU2ODI2OTc5fQ._YMK6ODGxlcLPqo3CefFkXtbk3f8Jnw-lkWeXmEXt_I"
        
        updateTokenApiCall(fcmToken ?? "")
        
        let dataDict:[String: String] = ["token": (String(describing: fcmToken)) ]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        
        // UserDefaults.standard.set("\(fcmToken)", forKey: "token")
        // UserDefaults.standard.synchronize()
        // HomeMyTaskViewController().updateFCMToken(fcmToken: fcmToken)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        print("FIRSSSSTTTTTT")
        
        // Print full message.
        print(userInfo)
        
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        print("SECONDDDDDDDDD")
        // Print full message.
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        print(userInfo)
        
        let aps = userInfo["aps"] as? [String: AnyObject]
        
        let type = userInfo["type"] as? String
        
        if type == "specialOffer"{
            
            if UserDefaults.standard.string(forKey:  UserDefaultConstants.LoginStatus.rawValue) == "3" {
                //                rewardOfferApiCall()
                getSpecialOfferByUserIDApiCall()
            }
            
        }
        
        
        print(aps ?? "")
        
        completionHandler([[.alert, .sound, .badge]])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        print(userInfo)
        
        let aps = userInfo["aps"] as? [String: AnyObject]
        
        let type = userInfo["type"] as? String
        
        if type == "specialOffer"{
            if UserDefaults.standard.string(forKey:  UserDefaultConstants.LoginStatus.rawValue) == "3" {
                //                rewardOfferApiCall()
                getSpecialOfferByUserIDApiCall()
            }
            
        }
        
        completionHandler()
    }
    
    func NetworkHanding(){
        
        
        NetworkMonitor.shared.startMonitoring { (status) in
            
            
            DispatchQueue.main.async {
                
                if ReachabilityConstants.disconnected.rawValue == status{
                    
                    let alertController = UIAlertController(title: "Message", message: "You are offline. Please check your internet connection.", preferredStyle: .alert)
                    
                    alertController.setMessage(font: UIFont(name: "Montserrat-Bold", size: 14), color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
                    
                    alertController.setTitlet(font: UIFont(name: "Montserrat-ExtraBold", size: 16), color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
                    
                    self.window?.rootViewController?.present(alertController, animated: true, completion: {
                        Timer.scheduledTimer(withTimeInterval: 0.8, repeats: false) { (_) in
                            self.window?.rootViewController?.dismiss(animated: true, completion: nil)
                        }
                    })
                    
                }
                
                else if ReachabilityConstants.connected.rawValue == status {
                    
                    let alertController = UIAlertController(title: "Message", message: "Your are back online", preferredStyle: .alert)
                    alertController.setMessage(font: UIFont(name: "Montserrat-Bold", size: 14), color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
                    alertController.setTitlet(font: UIFont(name: "Montserrat-ExtraBold", size: 16), color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
                    self.window?.rootViewController?.present(alertController, animated: true, completion: {
                        Timer.scheduledTimer(withTimeInterval: 0.8, repeats: false) { (_) in
                            self.window?.rootViewController?.dismiss(animated: true, completion: nil)
                        }
                    })
                    
                }
                
                else if ReachabilityConstants.none.rawValue == status{
                    
                }
            }
            
            }
        
    }
    
    
    func checkUserDefaultKey(key: String) -> Bool{
        
        if UserDefaults.exists(key: key){
            
            if UserDefaults.standard.bool(forKey: key) == true {
                
                return true
                
            }
            else {
                return false
            }
            
        }
        else {
            
            return false
        }
    }
    
    
    
//    func statusProcessHandling(){
//
//        let currency = UserDefaults.standard.value(forKey: "currency") as? String
//        GlobalVariables.currency = currency ?? "£"
//
//
//
//
//        if checkUserDefaultKey(key: UserDefaultConstants.otpStatus.rawValue) == false{
//
//
//
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let controller1VC = storyboard.instantiateViewController(withIdentifier: "TutorialVc") as! TutorialVc
//            self.navigationController = UINavigationController(rootViewController: controller1VC)
//            self.window?.rootViewController = self.navigationController
//            self.window?.makeKeyAndVisible()
//
//        }
//        else{
//
//
//
//                if UserDefaults.exists(key: "Auth"){
//
//                    let authValue = UserDefaults.standard.value(forKey: "Auth") as? String
//                    if authValue == "finger"{
//
//                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                        let currency = UserDefaults.standard.value(forKey: "currency") as? String
//                        GlobalVariables.currency = currency ?? "£"
//                        let controller1VC = storyboard.instantiateViewController(withIdentifier: "FingerPrintAuthenticationViewController") as! FingerPrintAuthenticationViewController
//                        self.navigationController = UINavigationController(rootViewController: controller1VC)
//                        self.window?.rootViewController = self.navigationController
//                        self.window?.makeKeyAndVisible()
//
//                    }
//                    else if authValue == "pin"{
//
//
//                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                        let currency = UserDefaults.standard.value(forKey: "currency") as? String
//                        GlobalVariables.currency = currency ?? "£"
//                        let controller1VC = storyboard.instantiateViewController(withIdentifier: "EnterPinViewController") as! EnterPinViewController
//                        controller1VC.commingFromScreen = "SignIn"
//                        self.navigationController = UINavigationController(rootViewController: controller1VC)
//                        self.window?.rootViewController = self.navigationController
//                        self.window?.makeKeyAndVisible()
//
//                    }
//                    else{
//

//                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                        let currency = UserDefaults.standard.value(forKey: "currency") as? String
//                        GlobalVariables.currency = currency ?? "£"
//                        let controller1VC = storyboard.instantiateViewController(withIdentifier: "FingerprintVc") as! FingerprintVc
//                        controller1VC.commingFromScreen = "Signup"
//                        self.navigationController = UINavigationController(rootViewController: controller1VC)
//                        self.window?.rootViewController = self.navigationController
//                        self.window?.makeKeyAndVisible()
//

//                    }
//
//                }
//                else {
//
//                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                    let currency = UserDefaults.standard.value(forKey: "currency") as? String
//                    GlobalVariables.currency = currency ?? "£"
//                    let controller1VC = storyboard.instantiateViewController(withIdentifier: "FingerprintVc") as! FingerprintVc
//                    controller1VC.commingFromScreen = "Signup"
//                    self.navigationController = UINavigationController(rootViewController: controller1VC)
//                    self.window?.rootViewController = self.navigationController
//                    self.window?.makeKeyAndVisible()
//
//                }
//
//
//
//        }
//
//
//    }
  
    
    func autoLoginHandling(){
        
        
        print(UserDefaults.standard.string(forKey: "LoginStatus") )
        
        UserDefaults.standard.set(1, forKey:  UserDefaultConstants.bottomSheetValue.rawValue)
        UserDefaults.standard.synchronize()
        
        if UserDefaults.standard.value(forKey:  UserDefaultConstants.LoginStatus.rawValue) == nil {
            
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller1VC = storyboard.instantiateViewController(withIdentifier: "TutorialVc") as! TutorialVc
            self.navigationController = UINavigationController(rootViewController: controller1VC)
            self.window?.rootViewController = self.navigationController
            self.window?.makeKeyAndVisible()
            
        } else if UserDefaults.standard.string(forKey: UserDefaultConstants.LoginStatus.rawValue) == "1" {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let currency = UserDefaults.standard.value(forKey: "currency") as? String
            GlobalVariables.currency = currency ?? "£"
            
            let controller1VC = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self.navigationController = UINavigationController(rootViewController: controller1VC)
            self.window?.rootViewController = self.navigationController
            self.window?.makeKeyAndVisible()
            
        } else if UserDefaults.standard.string(forKey: UserDefaultConstants.LoginStatus.rawValue) == "2" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let currency = UserDefaults.standard.value(forKey: "currency") as? String
            GlobalVariables.currency = currency ?? "£"
            let controller1VC = storyboard.instantiateViewController(withIdentifier: "FingerprintVc") as! FingerprintVc
            controller1VC.commingFromScreen = "SignIn"
            self.navigationController = UINavigationController(rootViewController: controller1VC)
            self.window?.rootViewController = self.navigationController
            self.window?.makeKeyAndVisible()
            
        } else if UserDefaults.standard.string(forKey:  UserDefaultConstants.LoginStatus.rawValue) == "3"{
            
            let authValue = UserDefaults.standard.value(forKey:  UserDefaultConstants.Auth.rawValue) as? String
            if authValue == "finger"{
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let currency = UserDefaults.standard.value(forKey: "currency") as? String
                GlobalVariables.currency = currency ?? "£"
                let controller1VC = storyboard.instantiateViewController(withIdentifier: "FingerPrintAuthenticationViewController") as! FingerPrintAuthenticationViewController
                self.navigationController = UINavigationController(rootViewController: controller1VC)
                self.window?.rootViewController = self.navigationController
                self.window?.makeKeyAndVisible()
                
            }
            else if authValue == "pin"{
                
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let currency = UserDefaults.standard.value(forKey: "currency") as? String
                GlobalVariables.currency = currency ?? "£"
                let controller1VC = storyboard.instantiateViewController(withIdentifier: "EnterPinViewController") as! EnterPinViewController
                controller1VC.commingFromScreen = "SignIn"
                self.navigationController = UINavigationController(rootViewController: controller1VC)
                self.window?.rootViewController = self.navigationController
                self.window?.makeKeyAndVisible()
                
            }
            else{
                
                
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let currency = UserDefaults.standard.value(forKey: "currency") as? String
                GlobalVariables.currency = currency ?? "£"
                let controller1VC = storyboard.instantiateViewController(withIdentifier: "FingerprintVc") as! FingerprintVc
                controller1VC.commingFromScreen = "Signup"
                self.navigationController = UINavigationController(rootViewController: controller1VC)
                self.window?.rootViewController = self.navigationController
                self.window?.makeKeyAndVisible()
                
                
                
                
            }
        
        }
        else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller1VC = storyboard.instantiateViewController(withIdentifier: "TutorialVc") as! TutorialVc
            self.navigationController = UINavigationController(rootViewController: controller1VC)
            self.window?.rootViewController = self.navigationController
            self.window?.makeKeyAndVisible()
        }
        
    }
    
    
    func updateTokenApiCall(_ token: String){
        
        DataService.sharedInstance.updateToken(deviceToken: token) { (response, Error) in

            print(response)
          
        }
        
    }
    
    func getSpecialOfferByUserIDApiCall(){
        
        var configuration = NBBottomSheetConfiguration()
        
        let userID = UserDefaults.standard.value(forKey: UserDefaultConstants.UserID.rawValue) ?? ""
        DataService.sharedInstance.getSpecialOfferByUserIDApiCall(userId: userID as! String) { (response, error) in
            
            print(response)
            if response != nil {
                
                if let status = response?["status"] as? String{
                    
                    if status == "success" {
                        
                        self.totalAmount = response?["category_balance"] as? Double ?? 0.00
                        
                        if let result = response?["result"] as? NSArray{
                            
                            if let resultData = result[0] as? Dictionary<String, Any>{
                                
                                self.offerFetchedData.deserilize(values: resultData)
                                
                            }
                        }
                        
                        
                        let imageURL = self.offerFetchedData.voucherDetails.logo_image_url
                        let name = self.offerFetchedData.voucherDetails.name
                        let percentAmount = (self.offerFetchedData.percentage as NSString).doubleValue
                        let minVal = (self.offerFetchedData.voucherDetails.minimum_value as NSString).doubleValue
                        let maxVal = (self.offerFetchedData.voucherDetails.maximum_value as NSString).doubleValue
                        
                        
                        let descriptionText = "\(name) are offering an \(percentAmount.format())% reward on any amount b/w \(GlobalVariables.currency)\(minVal.format()) to \(GlobalVariables.currency)\(maxVal.format())"
                        
                        
                        configuration = NBBottomSheetConfiguration(sheetSize: .fixed(330))
                        let vc  = BottomXIBViewController()
                        vc.from = "Notification"
                        vc.offerdescription = descriptionText
                        vc.offerType = "Reward of the Day"
                        vc.offerImage = imageURL
                        vc.offerData = self.offerFetchedData
                        vc.totalAmount = self.totalAmount
                        vc.clickDelegate = self
                        let bottomSheetController = NBBottomSheetController(configuration: configuration)
                        HomeViewController.setRootVC()
                        if var topController = UIApplication.shared.keyWindow?.rootViewController {
                            
                            while let presentedViewController = topController.presentedViewController {
                                topController = presentedViewController
                            }
                            
                            DispatchQueue.main.asyncAfter(deadline: .now()) {
                                bottomSheetController.present(vc, on: topController)
                            }
                            
                            
                        }

                        
                    }
                    
                }
                
            }
            
            
        }
        
        
    }
}

extension AppDelegate: ClickHere{
    
    func clickedAction(flag: Int) {
        if flag == 1{
            
            let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc = storyBoard.instantiateViewController(withIdentifier: "RedeemNowVC") as! RedeemNowVC
            vc.logoImageURL = self.offerFetchedData.voucherDetails.logo_image_url
            vc.allAvailbleDenomination = self.offerFetchedData.voucherDetails.available_denominations
            vc.discreption = self.offerFetchedData.voucherDetails.description
            vc.discount = self.offerFetchedData.percentage
            vc.navName = self.offerFetchedData.voucherDetails.name
            vc.catId = self.offerFetchedData.category_id
            vc.productCode = self.offerFetchedData.voucherDetails.code
            vc.currencyCode = self.offerFetchedData.voucherDetails.currency_code
            vc.type = self.offerFetchedData.voucherDetails.denomination_type
            vc.termsAndConditions = self.offerFetchedData.voucherDetails.terms_and_conditions_html
            vc.potBalance = totalAmount.format()
            vc.tierVal = self.offerFetchedData.percentage
            if self.offerFetchedData.voucherDetails.denomination_type == "fixed"{
                vc.price = self.offerFetchedData.voucherDetails.maximum_value
            }
            else {
                
                let percentageIntValue = (self.offerFetchedData.percentage as NSString).doubleValue
                //            if let potValue = potlist[indexPath.row].total_amount {
                let value: Double = totalAmount + (totalAmount * (percentageIntValue / 100))
//                let value: Double = totalAmount * 100.00/(100.00 - percentageIntValue)
                print(value)
                
                vc.price = String(format: "%.2f", value)
                //        }
            }
            
            vc.minimumVal = offerFetchedData.voucherDetails.minimum_value
            vc.maximumVal = offerFetchedData.voucherDetails.maximum_value
            
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
    
    
}
//
