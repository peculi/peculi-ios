//
//  AllTransactionTV_Cell.swift
//  Peculi
//
//  Created by PPI on 4/26/22.
//  Copyright © 2022 Stealth Tech. All rights reserved.


import UIKit

class AllTransactionTV_Cell: UITableViewCell {
    
    @IBOutlet weak var lblDepositValue: UILabel!
    @IBOutlet weak var lblCardPayment: UILabel!
    @IBOutlet weak var cardImg: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var oneTimeTransactionView: UIView!
    
    @IBOutlet weak var potTransactionView: UIView!
    
    @IBOutlet weak var lblDate1: UILabel!
    @IBOutlet weak var toImg: UIImageView!
    @IBOutlet weak var tolblName: UILabel!
    @IBOutlet weak var fromLbl: UILabel!
    @IBOutlet weak var fromImg: UIImageView!
    @IBOutlet weak var lblAmount: UILabel!
    
    @IBOutlet weak var bankTransactionView: UIView!
    
    @IBOutlet weak var bankToImg: UIImageView!
    @IBOutlet weak var bankTolbl: UILabel!
    @IBOutlet weak var bankDateCreate: UILabel!
    @IBOutlet weak var bankFirstName: UILabel!
    @IBOutlet weak var bankAcoountNo: UILabel!
    @IBOutlet weak var lblaymentAmount: UILabel!
    
   
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }

}
