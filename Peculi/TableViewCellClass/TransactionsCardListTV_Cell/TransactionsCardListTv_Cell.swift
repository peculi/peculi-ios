//
//  TransactionsCardListTv_Cell.swift
//  Peculi
//
//  Created by PPI on 4/18/22.
//  Copyright © 2022 Stealth Tech. All rights reserved.
//

import UIKit

class TransactionsCardListTv_Cell: UITableViewCell {
    
    
    @IBOutlet weak var circleImg: UIImageView!
    @IBOutlet weak var cardLbl: UILabel!
    @IBOutlet weak var lblPrimaryCard: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
      
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
