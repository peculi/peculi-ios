//
//  PotsDetailTableViewCell.swift
//  Peculi
//
//  Created by apple on 31/10/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit

class PotsDetailTableViewCell: UITableViewCell {

    //MARK: - Outlets
    @IBOutlet weak var retailerTitleLbl: UILabel!
    @IBOutlet weak var peculiOfferTitleLbl: UILabel!
    @IBOutlet weak var rewardTitleLbl: UILabel!
    
    
    @IBOutlet weak var cellNumberCountLbl: UILabel!
    @IBOutlet weak var retailerNameLbl: UILabel!
    @IBOutlet weak var postValueLbl: UILabel!
    @IBOutlet weak var peculiOfferLbl: UILabel!
    @IBOutlet weak var premiumImage: UIImageView!
    @IBOutlet weak var redeemArrowBtn: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        retailerNameLbl.numberOfLines = 2
        postValueLbl.numberOfLines = 0
        peculiOfferLbl.numberOfLines = 2
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
