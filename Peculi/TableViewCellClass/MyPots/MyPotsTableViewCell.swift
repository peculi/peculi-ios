//
//  MyPotsTableViewCell.swift
//  Peculi
//
//  Created by apple on 31/10/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit

class MyPotsTableViewCell: UITableViewCell {

    //MARK:- TableCell Outlets
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var productPrizeLbl: UILabel!
    @IBOutlet weak var sepratorLineView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
