//
//  PeculiWalletTableViewCell.swift
//  Peculi
//
//  Created by apple on 31/10/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit

class PeculiWalletTableViewCell: UITableViewCell {

    //MARK:- Outlets
    @IBOutlet weak var cellSideColorView: UIView!
    @IBOutlet weak var cellQrView: UIView!
    @IBOutlet weak var dashBorderLineView: UIView!
    @IBOutlet weak var blackShadowEffectView: UIView!
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var cellDescription: UILabel!
    
    @IBOutlet weak var nameWidth: NSLayoutConstraint!
    @IBOutlet weak var cellAmount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        cellSideColorView.layer.cornerRadius = 18
        cellSideColorView.layer.masksToBounds = true
        cellSideColorView.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMinXMinYCorner]
        cellQrView.layer.cornerRadius = 18
        cellQrView.layer.masksToBounds = true
        cellQrView.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMaxXMinYCorner]
        dashBorderLineView.addDashedBorder(conerRadius: 0)
        
  
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
