//
//  PeculiViewOfferTableViewCell.swift
//  Peculi
//
//  Created by apple on 03/11/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit

class PeculiViewOfferTableViewCell: UITableViewCell {

    //MARK: - Outlets
    @IBOutlet weak var tierNameLbl: UILabel!
    @IBOutlet weak var completeStatusLbl: UILabel!
    @IBOutlet weak var viewOfferButton: UIButton!
    @IBOutlet weak var superView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        superview?.backgroundColor = #colorLiteral(red: 0.07450980392, green: 0.1019607843, blue: 0.1529411765, alpha: 1)
        superView.backgroundColor = #colorLiteral(red: 0.07450980392, green: 0.1019607843, blue: 0.1529411765, alpha: 1)
        self.backgroundColor =  #colorLiteral(red: 0.07450980392, green: 0.1019607843, blue: 0.1529411765, alpha: 1)
        
    
    }

    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
}
