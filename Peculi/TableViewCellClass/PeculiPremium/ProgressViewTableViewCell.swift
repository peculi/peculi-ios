//
//  ProgressViewTableViewCell.swift
//  Peculi
//
//  Created by apple on 03/11/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit

class ProgressViewTableViewCell: UITableViewCell {
    
    //MARK: - Outltes
    
    @IBOutlet weak var lblPopUpText: UILabel!
    @IBOutlet weak var yellowCircleImg: UIImageView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var superView: UIView!
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var targetValueLbl: UILabel!
    @IBOutlet weak var potNameLbl: UILabel!
    @IBOutlet weak var tierNameLbl: UILabel!
    
    @IBOutlet weak var btnAddNewTarget: UIButton!
    @IBOutlet weak var btnAddMoney: UIButton!
    
    @IBOutlet weak var iconBtnAddTarget: UIImageView!
    
    @IBOutlet weak var lblBtnAddTarget: UILabel!
    @IBOutlet weak var lblBtnAddMony: UILabel!
    @IBOutlet weak var iconBtnAddMony: UIImageView!
    
    @IBOutlet weak var btnOk: UIButton!
    //MARK: - variables
    
    let shapeLayer = CAShapeLayer()
    var toValue = Double()
    
    var btnAddNewTargetPressed : (() -> ()) = {}
    var btnAddMoneyPressed : (() -> ()) = {}
    var btnOkPressed : (() -> ()) = {}


    override func awakeFromNib() {
        
        super.awakeFromNib()
        createCircularPath()
        handleProgressView()
        superview?.backgroundColor = #colorLiteral(red: 0.2117647059, green: 0.168627451, blue: 0.3098039216, alpha: 1)  //#colorLiteral(red: 0.2117647059, green: 0.168627451, blue: 0.3098039216, a
        superView.backgroundColor =  #colorLiteral(red: 0.2117647059, green: 0.168627451, blue: 0.3098039216, alpha: 1) //#colorLiteral(red: 0.07450980392, green: 0.1019607843, blue: 0.1529411765, alpha: 1)
        self.backgroundColor = #colorLiteral(red: 0.2117647059, green: 0.168627451, blue: 0.3098039216, alpha: 1)
        
    }
    
    
    fileprivate func createCircularPath(){
        
        let circulrPath = UIBezierPath(arcCenter: CGPoint(x: progressView.frame.width/2, y: progressView.frame.height/2), radius: 100, startAngle: CGFloat(-0.5 * .pi), endAngle: CGFloat(1.5 * .pi), clockwise: true)
        
        shapeLayer.path = circulrPath.cgPath
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineWidth = 10
        shapeLayer.strokeColor = #colorLiteral(red: 1, green: 0.6588235294, blue: 0.231372549, alpha: 1)
        shapeLayer.strokeEnd = 0
        shapeLayer.lineCap = .round
        progressView.layer.addSublayer(shapeLayer)
        
    }
    
    
    func handleProgressView(){
        
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.toValue = toValue
        animation.duration = 2
        animation.fillMode = .forwards
        animation.isRemovedOnCompletion = false
        shapeLayer.add(animation, forKey: "basic")
        
    }
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    @IBAction func btnAddNewTarget_Action(_ sender: Any) {
        btnAddNewTargetPressed()
        
    }
 
    
    
    @IBAction func btnAddMoney_Action(_ sender: Any) {
        
        btnAddMoneyPressed()
    }
    
    
    @IBAction func btnOk_Pressed(_ sender: Any) {
        
        btnOkPressed()
    }
    
    
}
