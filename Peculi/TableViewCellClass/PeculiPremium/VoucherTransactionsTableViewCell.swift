//
//  VoucherTransactionsTableViewCell.swift
//  Peculi
//
//  Created by iApp on 20/04/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import UIKit

class VoucherTransactionsTableViewCell: UITableViewCell {
    
    //MARK:- outlets
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var defaultLbl: UILabel!
    @IBOutlet weak var seperatorView: UIView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
