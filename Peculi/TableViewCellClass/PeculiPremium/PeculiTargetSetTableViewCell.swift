//
//  PeculiTargetSetTableViewCell.swift
//  Peculi
//
//  Created by iApp on 03/06/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import UIKit

class PeculiTargetSetTableViewCell: UITableViewCell {
    
   
    
    //MARK:- outlets
    @IBOutlet weak var currentLbl: UILabel!
    @IBOutlet weak var weeklyLbl: UILabel!
    @IBOutlet weak var addNewTargetBtn: UIButton!
    @IBOutlet weak var addMoneyBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
