//
//  PeculiTransactionsTableViewCell.swift
//  Peculi
//
//  Created by apple on 03/11/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit

protocol CustomTableView {
    func fetchTableViewHeight(height: CGFloat)
}


class PeculiTransactionsTableViewCell: UITableViewCell {
    
    //MARK: - Outltes
    
    @IBOutlet weak var superView: UIView!
    @IBOutlet weak var childView: UIView!
    @IBOutlet weak var DefaultLbl: UILabel!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var transactionLblBackView: UIView!
    @IBOutlet weak var transactionDetailView: UIView!
    @IBOutlet weak var transactionTableView: UITableView!
    
    @IBOutlet weak var getPremiumBtn: UIButton!
    @IBOutlet weak var transferBtn: UIButton!
    
    @IBOutlet weak var trasferBtnLbl: UILabel!
    
    //MARK: - variables
    
    var customTableViewDelegate: CustomTableView!
    var voucherData = [VoucherTransactionModel]()
    var viewController = UIViewController()
    var heightDelegate: CustomTableView!
    
    var btnTransferPressed : (() -> ()) = {}
    var btnTransactionHistryPressed : (() -> ()) = {}
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        /**** Register tableview cell */
        
        transactionTableView.register(UINib(nibName: "VoucherTransactionsTableViewCell", bundle: nil), forCellReuseIdentifier: "VoucherTransactionsTableViewCell")

        transactionLblBackView.layer.cornerRadius = 10
        transactionLblBackView.layer.masksToBounds = true
        transactionLblBackView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        transactionDetailView.layer.cornerRadius = 10
        transactionDetailView.layer.masksToBounds = true
        transactionDetailView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        transactionTableView.delegate = self
        transactionTableView.dataSource = self
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    
    @IBAction func btnTransfer_Action(_ sender: Any) {
        self.btnTransferPressed()
        
    }
    
    
    @IBAction func btnTransactionsHistry_Action(_ sender: Any) {
        
        self.btnTransactionHistryPressed()
        
    }
    
    
    
    func reload(){
        
        self.transactionTableView.reloadData()
    }
    
}

//MARK: - tableview delegates

extension PeculiTransactionsTableViewCell: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if voucherData.count == 0 {
            return 1
        }
        
        return voucherData.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = transactionTableView.dequeueReusableCell(withIdentifier: "VoucherTransactionsTableViewCell") as! VoucherTransactionsTableViewCell
        if !voucherData.isEmpty {
            cell.titleLbl.text = String(format: "%@ %@", "Paid for", voucherData[indexPath.row].productDetails.name)
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let newDate = dateFormatter.date(from: voucherData[indexPath.row].created_at) ?? Date()
            
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let finalDate = dateFormatter.string(from: newDate)
            cell.dateLbl.text = finalDate
            
            cell.amountLbl.text = "\(GlobalVariables.currency)\(voucherData[indexPath.row].calculate_amount.format())"
            cell.defaultLbl.text = ""
            
            if (voucherData.count - 1) == indexPath.row {
                cell.seperatorView.isHidden = true
            }
            else {
                
                cell.seperatorView.isHidden = false
            }
        }
        else {
            
            cell.titleLbl.text = ""
            cell.dateLbl.text = ""
            cell.amountLbl.text = ""
            cell.seperatorView.isHidden = true
         }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if voucherData.count>0 {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "VoucherTransactionDetailsViewController") as! VoucherTransactionDetailsViewController
            
            vc.fetchedData = voucherData[indexPath.row]
            
            self.viewController.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if let lastVisibleIndexPath = tableView.indexPathsForVisibleRows?.last {
            
            if indexPath == lastVisibleIndexPath {
                
                var tableViewHeight: CGFloat {
                    transactionTableView.layoutIfNeeded()
                    return transactionTableView.contentSize.height
                }
                
                if voucherData.count > 0 {
                    DispatchQueue.main.async {
                        self.transactionTableView.frame.size.height = self.transactionTableView.contentSize.height
                        self.transactionTableView.layoutIfNeeded()
                        self.heightDelegate.fetchTableViewHeight(height: self.transactionTableView.frame.size.height)
                    }
                    
                } else {
                    
                    DispatchQueue.main.async {
                        self.heightDelegate.fetchTableViewHeight(height: 70)
                    }
                    
                }
            }
        }
    }

    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
   
    
}
