//
//  SettingsTableViewCell.swift
//  Peculi
//
//  Created by apple on 02/11/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {

    //MARK:- Cell Outltes
    @IBOutlet weak var settingIconImageView: UIImageView!
    @IBOutlet weak var settingsNameLbl: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
