//
//  PreviewTodaysOfferTableViewCell.swift
//  Peculi
//
//  Created by iApp on 26/04/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import UIKit

class PreviewTodaysOfferTableViewCell: UITableViewCell {
    
    //MARK:- outlets
    @IBOutlet weak var potImageView: UIImageView!
    @IBOutlet weak var potNameLbl: UILabel!
    @IBOutlet weak var potValueLbl: UILabel!
    @IBOutlet weak var arrowImageView: UIImageView!
    @IBOutlet weak var seperatorView: UIView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
