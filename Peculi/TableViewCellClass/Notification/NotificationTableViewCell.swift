//
//  NotificationTableViewCell.swift
//  Peculi
//
//  Created by apple on 02/11/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    //MARK:- Outlets
    @IBOutlet weak var roundCircleColorView: UIView!
    @IBOutlet weak var notificaitonLbl: UILabel!
    @IBOutlet weak var notificationDateLbl: UILabel!
    @IBOutlet weak var notificaitonTimeLbl: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
