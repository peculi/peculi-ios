//
//  SelectedPotTV_Cell.swift
//  Peculi
//
//  Created by PPI on 5/5/22.
//  Copyright © 2022 Stealth Tech. All rights reserved.
//

import UIKit

class SelectedPotTV_Cell: UITableViewCell {
    
    
    //MARK: - TableCellOutlets
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var productPrizeLbl: UILabel!
    @IBOutlet weak var sepratorLine: UIView!
    @IBOutlet weak var circleImg: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
