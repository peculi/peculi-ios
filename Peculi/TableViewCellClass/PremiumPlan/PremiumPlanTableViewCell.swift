//
//  PremiumPlanTableViewCell.swift
//  Peculi
//
//  Created by iApp on 31/03/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import UIKit

class PremiumPlanTableViewCell: UITableViewCell {

    
    //MARK:- outlets
    @IBOutlet weak var cellChildView: UIView!
    @IBOutlet weak var planTitleLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var checkBtn: UIButton!
    
    @IBOutlet weak var offerLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
