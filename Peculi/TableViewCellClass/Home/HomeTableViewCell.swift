//
//  HomeTableViewCell.swift
//  Peculi
//
//  Created by apple on 31/10/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {

    //MARK: - TableCellOutlets
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var productPrizeLbl: UILabel!
    @IBOutlet weak var sepratorLine: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    
    }

}




