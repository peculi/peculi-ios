//
//  EmptyDataTableViewCell.swift
//  Peculi
//
//  Created by iApp on 15/04/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import UIKit

class EmptyDataTableViewCell: UITableViewCell {

    
    //MARK:- outlets
    
    @IBOutlet weak var addNewPot: UIButton!
    @IBOutlet weak var defaultImageView: UIImageView!
    @IBOutlet weak var defaultFirstLbl: UILabel!
    @IBOutlet weak var defaultSecondLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
