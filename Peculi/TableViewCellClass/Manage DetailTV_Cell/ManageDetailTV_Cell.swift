//
//  ManageDetailTV_Cell.swift
//  Peculi
//
//  Created by PPI on 4/26/22.
//  Copyright © 2022 Stealth Tech. All rights reserved.
//

import UIKit

class ManageDetailTV_Cell: UITableViewCell {
    
    @IBOutlet weak var lblCardName: UILabel!
    @IBOutlet weak var lblPotName: UILabel!
    @IBOutlet weak var lblTarget: UILabel!
    @IBOutlet weak var lblFrequency: UILabel!
    @IBOutlet weak var lblAmountSaved: UILabel!
    @IBOutlet weak var lblDueDate: UILabel!
    @IBOutlet weak var lblActive: UILabel!
    @IBOutlet weak var btnDropDown: UIButton!
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var subscriptionView: UIView!
    
    @IBOutlet weak var btnDropDownS: UIButton!
    
    @IBOutlet weak var subAmount: UILabel!
    
    @IBOutlet weak var subType: UILabel!
    
    @IBOutlet weak var subExpiryDate: UILabel!
    
    
    var btnDropDownPressed : (() -> ()) = {}
    var btnDropDownPressedS : (() -> ()) = {}
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.bgView.dropShadow()
        self.subscriptionView.dropShadow()
        bgView.layer.cornerRadius = 5
        subscriptionView.layer.cornerRadius = 5

        
    }

    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
 
    }

    
    
    @IBAction func btnDropDown_Action(_ sender: Any) {
        
        btnDropDownPressed()
        
    }
    
    @IBAction func btnSubDropDown_Action(_ sender: Any) {
        btnDropDownPressedS()
    }
    
    
}


extension UIView {

    func dropShadow() {
    
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 2, height: 3)
        layer.masksToBounds = false
    
        layer.shadowOpacity = 0.3
        layer.shadowRadius = 3
        //layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.rasterizationScale = UIScreen.main.scale
        layer.shouldRasterize = true
    }
}
