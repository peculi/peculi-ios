//
//  LeftSideMenuTableViewCell.swift
//  Peculi
//
//  Created by apple on 31/10/20.
//  Copyright © 2020 Stealth Tech. All rights reserved.
//

import UIKit

class LeftSideMenuTableViewCell: UITableViewCell {

    //MARK:- Outlets
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var iconNameLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
