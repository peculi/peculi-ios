//
//  AddMoneyTV_Cell.swift
//  Peculi
//
//  Created by PPI on 4/12/22.
//  Copyright © 2022 Stealth Tech. All rights reserved.
//

import UIKit

class AddMoneyTV_Cell: UITableViewCell {
    
    
    @IBOutlet weak var circleImg: UIImageView!
    @IBOutlet weak var cardLbl: UILabel!
    @IBOutlet weak var btnEditCard: UIButton!
    
    var btnEditCardPressed : (() -> ()) = {}
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
    }
    
    
    @IBAction func btnEditCard_Action(_ sender: Any) {
        
        btnEditCardPressed()
    }
    
}
