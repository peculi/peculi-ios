//
//  VoucherDetailsTableViewCell.swift
//  Peculi
//
//  Created by iApp on 17/02/21.
//  Copyright © 2021 Stealth Tech. All rights reserved.
//

import UIKit

class VoucherDetailsTableViewCell: UITableViewCell {
    
    //MARK:- outlets
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var lblDescription: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
