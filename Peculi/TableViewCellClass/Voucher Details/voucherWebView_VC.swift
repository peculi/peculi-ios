//
//  voucherWebView_VC.swift
//  Peculi
//
//  Created by PPI on 5/13/22.
//  Copyright © 2022 Stealth Tech. All rights reserved.
//

import UIKit
import WebKit

class voucherWebView_VC: UIViewController, WKNavigationDelegate {
    
    @IBOutlet weak var paymentWebView: WKWebView!
    @IBOutlet var superView: UIView!
    
    var urlData = ""

    override func viewDidLoad() {
        super.viewDidLoad()
     
        
        paymentWebView.navigationDelegate = self
        paymentWebView.scrollView.bounces = false
        paymentWebView.load(NSURLRequest(url: URL(string: urlData)!) as URLRequest)
        
        print(urlData)
  
    }

    
    func navigartionSetup() {
        
        navigationController?.navigationBar.isHidden = false
        title = ""
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2122195363, green: 0.16875422, blue: 0.3097219765, alpha: 1)
        self.navigationController?.navigationBar.isTranslucent = true
        
        //MARK: - Navigation LeftBar Button:-
        
        let leftBarButton = UIBarButtonItem(image: UIImage(named: "icBackArrowWhite"), style: .done, target: self, action: #selector(icBackButtonAction))
        
        self.navigationItem.leftBarButtonItem = leftBarButton
        
    }
    
    //MARK: - Object Function Button Action
    
    @objc func icBackButtonAction(){

        popVc()
        
    }

}
